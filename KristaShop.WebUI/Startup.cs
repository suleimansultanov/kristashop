using System;
using KristaShop.Business.Utils;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Utils;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using SixLabors.ImageSharp.Web.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;
using KristaShop.WebUI.Utils;
using Microsoft.Extensions.Logging;

namespace KristaShop.WebUI {
    public class Startup {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }
        public static ILoggerFactory DebugConsole { get; private set; } = null;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment) {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services) {
            if (Environment.IsDevelopment()) {
                DebugConsole = LoggerFactory.Create(builder => { builder.AddDebug(); });
            }

            services.AddShopDbContext(Configuration, DebugConsole);
            services.AddBusiness();
            services.AddReplicaDbContext(Configuration, DebugConsole);
            services.AddReadOnlyBusiness();
            services.AddCommon();
            services.Configure<UrlSetting>(Configuration.GetSection("UrlSetting"));
            services.Configure<GlobalSettings>(Configuration.GetSection("GlobalSettings"));
            services.Configure<TextColors>(Configuration.GetSection("TextColors"));
            services.AddScoped<DynamicPagesRouteValueTransformer>();
            services.AddMvcConfiguration(Configuration);

            if (!Environment.IsDevelopment()) {
                services.AddDataProtection(Configuration);
            }
        }

        public void Configure(IApplicationBuilder app, IServiceScopeFactory serviceScopeFactory) {
            if (Environment.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/error/common500");
                app.UseHsts();
            }

            app.UseMiddleware<LinkBasedAuthMiddleware>();
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseImageSharp();
            app.UseStaticFiles(new StaticFileOptions {
                OnPrepareResponse = ctx => {
                    const int durationInSeconds = 60 * 60 * 24 * 7;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                }
            });
            const string defaultCulture = "ru-RU";
            var ci = new CultureInfo(defaultCulture);

            // Configure the Localization middleware
            app.UseRequestLocalization(new RequestLocalizationOptions {
                DefaultRequestCulture = new RequestCulture(ci),
                SupportedCultures = new List<CultureInfo> {
                    ci,
                },
                SupportedUICultures = new List<CultureInfo> {
                    ci,
                }
            });

            app.UseHttpsRedirection();
            app.UseStatusCodePagesWithReExecute("/error/common404");
            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "myarea",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllerRoute(
                    name: "Admin",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );
                endpoints.MapDynamicControllerRoute<DynamicPagesRouteValueTransformer>(
                    "{controller=Home}/{action=Index}");
            });
            app.InitializeApp(serviceScopeFactory);
        }
    }
}