﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Helpers;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using Serilog;

namespace KristaShop.WebUI.ApiControllers {
    [ApiController]
    [Route("api/[controller]")]
    public class LinkController : ControllerBase {
        private readonly ILinkService _linkService;
        private readonly ILogger _logger;

        public LinkController(ILinkService linkService, ILogger logger) {
            _linkService = linkService;
            _logger = logger;
        }

        public async Task<string> Get(Guid userId, AuthorizationLinkType type, string hash = "") {
            try {
                var hashCode = HashHelper.CalculateSha256Hash(userId.ToString().ToLowerInvariant() + GlobalConstant.SECRET_CODE);
                if (hash.Equals(hashCode) && userId != Guid.Empty && type != AuthorizationLinkType.None) {
                    var result = await _linkService.InsertLinkAuthAsync(userId, type);
                    if (!result.IsSuccess) {
                        _logger.Warning(result.Exception, "{message}, {@result}", result.ToString(), result);
                    }

                    return result.Model;
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to generate authorization link for asup. {message}", ex.Message);
            }
            return string.Empty;
        }
    }
}