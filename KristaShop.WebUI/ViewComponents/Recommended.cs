﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents {
    public class Recommended : ViewComponent {
        private readonly IFrontUserService _userService;
        private readonly IRecommendedService _recommendedService;

        public Recommended(IFrontUserService userService, IRecommendedService recommendedService) {
            _userService = userService;
            _recommendedService = recommendedService;
        }

        public async Task<IViewComponentResult> InvokeAsync(List<Guid> categoryIds) {
            var user = _userService.Get();
            var nomenclature = await _recommendedService.GetNRecommendedNomenclatureItems(categoryIds, 6, user?.UserId ?? Guid.Empty);
            return View(nomenclature);
        }
    }
}
