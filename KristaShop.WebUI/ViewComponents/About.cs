﻿using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents
{
    public class About : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}