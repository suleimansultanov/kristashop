﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents {
    public class DynamicPageMenu : ViewComponent {
        private readonly IFrontUserService _userService;
        private readonly IDynamicPagesManager _manager;

        public DynamicPageMenu(IDynamicPagesManager manager, IFrontUserService userService) {
            _userService = userService;
            _manager = manager;
        }

        public IViewComponentResult Invoke(string controller) {
            _manager.TryGetValuesByControllerForMenu(controller, _userService.Get() == null, out var result);
            return View(result);
        }
    }
}