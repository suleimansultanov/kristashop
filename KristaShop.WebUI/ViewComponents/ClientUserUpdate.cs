﻿using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KristaShop.WebUI.ViewComponents
{
    public class ClientUserUpdate : ViewComponent
    {
        private readonly IDictionaryService _dictionaryService;

        public ClientUserUpdate(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.Cities = new SelectList(_dictionaryService.GetCities(), "Id", "Name");
            UserUpdateViewModel model = new UserUpdateViewModel();
            return View(model);
        }
    }
}