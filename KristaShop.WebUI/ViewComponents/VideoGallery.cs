﻿using System;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace KristaShop.WebUI.ViewComponents {
    public class VideoGallery : ViewComponent {
        private readonly IVideoGalleryService _galleryService;
        private readonly IFrontUserService _userService;
        private readonly ILogger _logger;

        public VideoGallery(IVideoGalleryService galleryService, IFrontUserService userService, ILogger logger) {
            _galleryService = galleryService;
            _userService = userService;
            _logger = logger;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            try {
                var openOnly = _userService.Get() == null;
                var result = await _galleryService.GetFirstGalleryWithVideosAsync(4, openOnly);
                if (result != null) {
                    return View(result);
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get galleries list for video gallery ViewComponent. {message}", ex.Message);
            }

            return Content(string.Empty);
        }
    }
}
