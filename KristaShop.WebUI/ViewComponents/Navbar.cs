﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class Navbar : ViewComponent
    {
        private readonly IFrontUserService _userService;
        private readonly ICartService _cartService;
        private readonly ICurrencyService _currencyService;

        public Navbar (IFrontUserService userService, ICartService cartService, ICurrencyService currencyService)
        {
            _userService = userService;
            _cartService = cartService;
            _currencyService = currencyService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = _userService.Get();

            var model = new NavbarViewModel {
                IsManager = user?.AccessLevel == (int)AccessControlLevel.Manager,
                ClientFullName = user?.CounterParty?.ClientFullName
            };

            if (user != null)
            {
                var rate = await _currencyService.GetLastExchangeRate();
                var cartItems = await _cartService.GetCartItemsNavbar(user.UserId);
                model.CartCount = cartItems.Count;
                model.Sum = cartItems.Sum(x => x.TotalPrice);
                model.SumRu = model.Sum * rate;
            }
            return View(model);
        }
    }
}