﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace KristaShop.WebUI.ViewComponents {
    public class Success : ViewComponent {
        private readonly ICatalogService _catalogService;
        private readonly ILogger _logger;
        private readonly UserSession _user;

        public Success(ICatalogService catalogService, IFrontUserService frontUserService, ILogger logger) {
            _catalogService = catalogService;
            _logger = logger;
            _user = frontUserService.Get();
        }

        public async Task<IViewComponentResult> InvokeAsync(string header, string message) {
            ViewData["Header"] = header;
            ViewData["Message"] = message;
            ViewData["CatalogUri"] = string.Empty;

            try {
                var catalog = await _catalogService.GetCatalogsByUser(_user?.UserId);
                ViewData["CatalogUri"] = catalog.FirstOrDefault()?.Uri ?? string.Empty; }
            catch (Exception ex) {
                _logger.Error(ex, "Failed to get catalog for redirect. {message}", ex.Message);
            }
            
            return View();
        }
    }
}
