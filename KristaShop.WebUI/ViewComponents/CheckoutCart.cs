﻿using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Business.Interfaces.Core;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KristaShop.WebUI.ViewComponents
{
    public class CheckoutCart : ViewComponent
    {
        private readonly IDictionaryService _dictionaryService;
        private readonly ISettingsManager _settingsManager;
        private readonly IStorehouseService _storehouseService;


        public CheckoutCart(IDictionaryService dictionaryService, ISettingsManager settingsManager, IStorehouseService storehouseService) {
            _dictionaryService = dictionaryService;
            _settingsManager = settingsManager;
            _storehouseService = storehouseService;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            var packagingList = await _storehouseService.GetPackagingListAsync();
            ViewBag.PackagingList = new SelectList(packagingList, "Key", "Value");
            CheckoutCartVM model = new CheckoutCartVM();
            ViewData["DeliveryDetailsUri"] = _settingsManager.Settings.DeliveryDetails;
            ViewData["PaymentDetailsUri"] = _settingsManager.Settings.PaymentDetails;
            ViewData["TermsOfUse"] = _settingsManager.Settings.TermsOfUse;
            return View(model);
        }
    }
}