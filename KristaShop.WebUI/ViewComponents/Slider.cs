﻿using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class Slider : ViewComponent
    {
        private readonly IBannerService _bannerService;

        public Slider(IBannerService bannerService)
        {
            _bannerService = bannerService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<BannerItemDTO> banners = await _bannerService.GetBanners();
            return View(banners);
        }
    }
}