﻿using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents {
    public class VideoGalleryMenu : ViewComponent {
        private readonly IVideoGalleryService _videoGalleryService;
        private readonly IFrontUserService _frontUserService;

        public VideoGalleryMenu(IVideoGalleryService videoGalleryService, IFrontUserService frontUserService) {
            _videoGalleryService = videoGalleryService;
            _frontUserService = frontUserService;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            var openOnly = _frontUserService.Get() == null;
            var result = await _videoGalleryService.GetGalleriesAsync(openOnly);
            return View(result);
        }
    }
}
