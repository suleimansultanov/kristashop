﻿using System;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class Catalog : ViewComponent
    {
        private readonly IFrontUserService _userService;
        private readonly INomenclatureService _nomenclatureService;

        public Catalog(ICatalogService catalogService, IFrontUserService userService, INomenclatureService nomenclatureService)
        {
            _userService = userService;
            _nomenclatureService = nomenclatureService;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            var user = _userService.Get();
            var nomCatalog = await _nomenclatureService.TakeNTopNomenclatureItemsFromVisibleCatalog(user?.UserId, 6);
            return View(nomCatalog);
        }
    }
}