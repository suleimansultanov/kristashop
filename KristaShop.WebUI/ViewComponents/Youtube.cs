﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents
{
    public class Youtube : ViewComponent
    {
        private readonly ISettingsManager _settingsManager;

        public Youtube(ISettingsManager settingsManager) {
            _settingsManager = settingsManager;
        }

        public IViewComponentResult Invoke() {
            ViewData["Youtube"] = _settingsManager.Settings.KristaYoutube;
            ViewData["YoutubeSubscribe"] = _settingsManager.Settings.KristaYoutubeSubscribe;
            return View();
        }
    }
}