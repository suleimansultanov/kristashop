﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class Category : ViewComponent
    {
        private readonly ICategoryService _categoryService;

        public Category(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync(string viewName = null)
        {
            var categories = await _categoryService.GetCategories();
            if (!string.IsNullOrEmpty(viewName))
            {
                return View(viewName, categories);
            }
            return View(categories);
        }
    }
}