﻿using System.Threading.Tasks;
using KristaShop.Common.Interfaces;
using KristaShop.DataReadOnly.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents
{
    public class Profile : ViewComponent
    {
        private readonly IFrontUserService _frontUserService;
        private readonly IUserService _userService;

        public Profile(IFrontUserService frontUserService, IUserService userService) {
            _frontUserService = frontUserService;
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = _frontUserService.Get();
            if (user == null) {
                return Content(string.Empty);
            }

            var counterparty = await _userService.GetCounterpartyDetailsAsync(user.CounterParty.Id);
            if (counterparty == null) {
                return Content(string.Empty);
            }

            return View(counterparty);
        }
    }
}