﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Business.Services;
using KristaShop.Common.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.ViewComponents {
    public class Footer : ViewComponent {
        private readonly IFrontUserService _userService;
        private readonly IDynamicPagesManager _dynamicPagesManager;
        private readonly ISettingsManager _settingsManager;
        private readonly ICatalogService _catalogService;

        public Footer(IFrontUserService userService, IDynamicPagesManager dynamicPagesManager,
            ISettingsManager settingsManager, ICatalogService catalogService) {
            _userService = userService;
            _dynamicPagesManager = dynamicPagesManager;
            _settingsManager = settingsManager;
            _catalogService = catalogService;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            var user = _userService.Get();
            _dynamicPagesManager.TryGetValue(_settingsManager.Settings.FooterContacts, out var contactsContent);
            var catalog = (await _catalogService.GetCatalogsByUser(user?.UserId)).FirstOrDefault();

            var result = new FooterViewModel {
                TermsOfUseUri = _settingsManager.Settings.TermsOfUse,
                FacebookLink = _settingsManager.Settings.KristaFacebook,
                VkLink = _settingsManager.Settings.KristaVk,
                InstagramLink = _settingsManager.Settings.KristaInstagram,
                YoutubeLink = _settingsManager.Settings.KristaYoutubeSubscribe,
                Contacts = contactsContent?.Body ?? string.Empty,
                CatalogUri = catalog?.Uri ?? string.Empty
            };

            ViewData["IsAuthenticated"] = user != null;
            return View(result);
        }
    }
}
