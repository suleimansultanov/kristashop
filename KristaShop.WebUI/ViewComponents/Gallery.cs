﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents {
    public class Gallery : ViewComponent {
        private readonly IGalleryService _galleryService;

        public Gallery(IGalleryService galleryService) {
            _galleryService = galleryService;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool isRandom = false) {
            var result = isRandom ? await _galleryService.GetTopNRandomItems(4) : await _galleryService.GetGallerysForFront(4);
            return View(result);
        }
    }
}