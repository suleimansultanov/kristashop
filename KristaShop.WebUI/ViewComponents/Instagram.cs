﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents {
    public class Instagram : ViewComponent {
        private readonly IBlogService _blogService;
        private readonly ISettingsManager _settingsManager;

        public Instagram(IBlogService blogService, ISettingsManager settingsManager) {
            _blogService = blogService;
            _settingsManager = settingsManager;
        }

        public async Task<IViewComponentResult> InvokeAsync() {
            var blogs = await _blogService.GetBlogsForFront();
            ViewData["InstagramLink"] = _settingsManager.Settings.KristaInstagram;
            return View(blogs);
        }
    }
}