﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class PartialContents : ViewComponent
    {
        private readonly IMenuContentService _menuContentService;
        private readonly IFrontUserService _userService;

        public PartialContents(IMenuContentService menuContentService, IFrontUserService userService)
        {
            _menuContentService = menuContentService;
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync(string controllerName)
        {
            bool isAuthorize = _userService.Get() != null;
            var contents = await _menuContentService.GetMenusContentByControllerAsync(controllerName, isAuthorize);
            return View(contents);
        }
    }
}
