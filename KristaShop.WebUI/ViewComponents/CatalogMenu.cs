﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    public class CatalogMenu : ViewComponent
    {
        private readonly ICatalogService _catalogService;
        private readonly IFrontUserService _userService;

        public CatalogMenu(ICatalogService catalogService, IFrontUserService userService)
        {
            _catalogService = catalogService;
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = _userService.Get();
            var catalogs = await _catalogService.GetCatalogsByUser(user?.UserId);
            return View(catalogs);
        }
    }
}
