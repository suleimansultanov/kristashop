﻿using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.WebUI.ViewComponents
{
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class SearchPanel : ViewComponent
    {
        private readonly INomenclatureService _nomenclatureService;
        private readonly IFrontUserService _userService;

        public SearchPanel(INomenclatureService nomenclatureService, IFrontUserService userService)
        {
            _nomenclatureService = nomenclatureService;
            _userService = userService;
        }

        public async Task<IViewComponentResult> InvokeAsync(Guid catalogId, SearchingType searchingType, NomFilterDTO filter)
        {
            List<Guid> nomIds = new List<Guid>();
            if (searchingType == SearchingType.Catalog)
            {
                nomIds = await _nomenclatureService.GetNomIdsByCatalog(catalogId);
            }
            else if (searchingType == SearchingType.User)
            {
                var user = _userService.Get();
                nomIds = await _nomenclatureService.GetNomIdsByUserFavorite(user.UserId);
            }
            return View(new SearchPanelViewModel { NomSearch = await _nomenclatureService.GetListsForSearchByNomIds(nomIds), NomFilter = filter });
        }
    }
}