﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Common.Enums;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.WebUI.Models;

namespace KristaShop.WebUI.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CatalogDTO, CatalogViewModel>();

            CreateMap<CartItemViewModel, CartItemDTO>();

            CreateMap<CartItemDTO, CartItemVM>();

            CreateMap<ClientDTO, FeedbackViewModel>()
                .ForMember(dest => dest.Person, opt => opt.MapFrom(source => source.ClientFullName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(source => source.PhoneNumber))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(source => source.Email));

            CreateMap<ClientDTO, FeedbackDTO>()
                .ForMember(dest => dest.Person, opt => opt.MapFrom(source => source.ClientFullName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(source => source.PhoneNumber))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(source => source.Email));

            CreateMap<FeedbackViewModel, FeedbackDTO>();
            CreateMap<AuthFeedbackViewModel, FeedbackDTO>();
            CreateMap<ManagementContactsFeedbackViewModel, FeedbackDTO>();

            CreateMap<UserClientDTO, UserUpdateViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(source => source.UserId))
                .ForMember(dest => dest.Person, opt => opt.MapFrom(source => source.ClientFullName))
                .ForMember(dest => dest.Login, opt => opt.MapFrom(source => source.Login))
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(source => source.CityId))
                .ForMember(dest => dest.NewCity, opt => opt.MapFrom(source => source.CityName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(source => source.PhoneNumber))
                .ForMember(dest => dest.MallAddress, opt => opt.MapFrom(source => source.ShopName))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(source => source.Email))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(source => source.Status));
        }
    }
}