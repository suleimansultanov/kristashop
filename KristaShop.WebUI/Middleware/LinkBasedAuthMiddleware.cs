﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Middleware {
    public class LinkBasedAuthMiddleware {
        private readonly RequestDelegate _next;

        public LinkBasedAuthMiddleware(RequestDelegate next) {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context) {
            var randh = context.Request.Query["randh"];
            if (!string.IsNullOrEmpty(randh.ToString())) {
                var redirect = string.Empty;
                if (context.Request.Query.TryGetValue("subtreeId", out var subtreeId) &&
                    context.Request.Query.TryGetValue("docId", out var documentId)) {
                    redirect = $"/Personal/DocumentDetails?subtreeId={subtreeId}&documentId={documentId}";
                } else if (context.Request.Query.TryGetValue("redirect", out var redirectUrl)) {
                    redirect = redirectUrl;
                }

                context.Response.Redirect($"/Account/LoginByLink?code={randh}&returnUrl={Uri.EscapeDataString(redirect)}");
            } else {
                await _next.Invoke(context);
            }
        }
    }
}