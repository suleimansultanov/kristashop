﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using KristaShop.Business.DTOs;
using KristaShop.Common.Enums;
using KristaShop.Common.ValidationAttributes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KristaShop.WebUI.Models {
    public class UserUpdateViewModel
    {
        [Required(ErrorMessage = "Заполните поле {0}")]
        public Guid Id { get; set; }

        [Display(Name="Логин")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Login { get; set; }

        [Display(Name="Пароль")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Password { get; set; }

        [Display(Name = "ФИО")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [RegularExpression("^[а-яА-Я ]+$", ErrorMessage = "{0} должно быть на кириллице.")]
        public string Person { get; set; }

        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Phone { get; set; }
        
        [Display(Name = "Электронная почта")]
        [EmailAddress(ErrorMessage = "{0} имеет некорректный формат")]
        public string Email { get; set; }
        
        [Display(Name = "Название торгового центра")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string MallAddress { get; set; }

        [Display(Name = "Адрес")]
        public string CompanyAddress { get; set; }
        
        [Display(Name = "Город")]
        [RequiredThisOrOther(nameof(NewCity), ErrorMessage = "Заполните поле {0} или {1}")]
        public Guid? CityId { get; set; }

        [Display(Name = "Название города")]
        public string NewCity { get; set; }

        [Display(Name = "Статус")]
        public UserStatus Status { get; set; }

        [Display(Name = "Причина блокировки")]
        public string BanReason { get; set; }

        [Display(Name = "Окончание блокировки")]
        public DateTime? BanExpireDate { get; set; }

        [IgnoreDataMember]
        public SelectList Cities { get; set; }

        [IgnoreDataMember]
        public SelectList Statuses { get; set; }

        [IgnoreDataMember]
        public bool Activate { get; set; }

        [IgnoreDataMember]
        public List<Guid> NotVisibleCatalogsIds { get; set; }

        [IgnoreDataMember]
        public List<UserCatalogsDTO> Catalogs { get; set; }

        public UserUpdateViewModel() {
            NotVisibleCatalogsIds =  new List<Guid>();
        }
    }
}