﻿using System;

namespace KristaShop.WebUI.Models
{
    public class CatalogViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public int Order { get; set; }
    }
}