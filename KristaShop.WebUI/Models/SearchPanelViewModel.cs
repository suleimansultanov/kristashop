﻿using KristaShop.Business.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Models
{
    public class SearchPanelViewModel
    {
        public NomSearchDTO NomSearch { get; set; }
        public NomFilterDTO NomFilter { get; set; }
    }
}
