﻿using KristaShop.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using KristaShop.Common.ValidationAttributes;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace KristaShop.WebUI.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Логин")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Заполните поле {0}")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }

    public class NavbarViewModel
    {
        public bool IsManager { get; set; }

        public string ClientFullName { get; set; }

        public double Sum { get; set; }

        public double SumRu { get; set; }

        public int CartCount { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "ФИО")]
        [RegularExpression("^[а-яА-Я ]+$", ErrorMessage = "{0} должно быть на кириллице.")]
        public string Person { get; set; }

        [Display(Name = "Город")]
        [RequiredThisOrOther(nameof(NewCity), ErrorMessage = "Заполните поле {0} или {1}")]
        public Guid? CityId { get; set; }

        [Display(Name = "Название города")]
        public string NewCity { get; set; }

        [Display(Name = "Название торгового центра")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string MallAddress { get; set; }

        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Phone { get; set; }

        [Display(Name = "Электронная почта")]
        [EmailAddress(ErrorMessage = "Введена некорректная электронная почта")]
        public string Email { get; set; }

        [Display(Name = "Адрес")]
        public string CompanyAddress { get; set; }

        [IgnoreDataMember]
        [IsTrue(ErrorMessage = "Для регистрации необходимо согласиться с условиями")]
        public bool IsAgree { get; set; }

        [IgnoreDataMember]
        public string TermsOfUse { get; set; }

        [IgnoreDataMember]
        public SelectList Cities { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class UserParamsViewModel {
        public Guid ParamUserId { get; set; }
        public bool AccessToSite { get; set; }
        public List<Guid> NonVisCtlgIds { get; set; } = new List<Guid>();
    }
}