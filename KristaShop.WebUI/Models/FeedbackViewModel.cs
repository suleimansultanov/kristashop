﻿using System.ComponentModel.DataAnnotations;
using KristaShop.Common.ValidationAttributes;
using Microsoft.AspNetCore.Http;

namespace KristaShop.WebUI.Models {
    public class FeedbackViewModel {
        [Display(Name = "ФИО")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Person { get; set; }

        [Display(Name = "Сообщение")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [MaxLength(2000, ErrorMessage = "Превышено максимальное количество символов")]
        public string Message { get; set; }

        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Phone { get; set; }

        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [EmailAddress(ErrorMessage = "Неверный формат поля {0}")]
        public string Email { get; set; }

        public string Captcha { get; set; }
    }

    public class AuthFeedbackViewModel {
        [Display(Name = "Сообщение")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [MaxLength(2000, ErrorMessage = "Превышено максимальное количество символов")]
        public string Message { get; set; }

        [Display(Name = "Файл")]
        [AllowedFileExtensions(".xlsx,.xls,.doc,.docx,.ppt,.pptx,.txt,.pdf,.zip,.rar,image/*", ErrorMessage = "Данное расширение файла не поддерживается")]
        [MaxFileSize(10 * 1024 * 1024, ErrorMessage = "Превышен максимальный размер файла")]
        public IFormFile File { get; set; }
    }

    public class ManagementContactsFeedbackViewModel {
        [Display(Name = "Сообщение")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [MaxLength(2000, ErrorMessage = "Превышено максимальное количество символов")]
        public string Message { get; set; }
    }
}