﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using KristaShop.Common.ValidationAttributes;

namespace KristaShop.WebUI.Models {
    public class ChangePasswordViewModel {
        public Guid UserId { get; set; }

        [Display(Name = "Текущий пароль")]
        [DataType(DataType.Password)]
        [IgnoreDataMember]
        public string CurrentPassword { get; set; }

        [Display(Name = "Новый пароль")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [DataType(DataType.Password)]
        [StringLength(256, ErrorMessage = "Длина пароля должна быть больше 3-х символов", MinimumLength = 4)]
        [PasswordComplexity(ErrorMessage = "Введенный пароль слишком простой, введите другой пароль")]
        public string Password { get; set; }

        [Display(Name = "Подтвержение пароля")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "Пароли должны совпадать")]
        [IgnoreDataMember]
        public string ConfirmPassword { get; set; }

        [IgnoreDataMember]
        public bool IsCurrentPasswordVisible { get; set; }
    }
}
