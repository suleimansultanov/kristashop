﻿using System;

namespace KristaShop.WebUI.Models
{
    public class CartItemViewModel
    {
        public Guid NomId { get; set; }
        public Guid CatalogId { get; set; }
        public string CatalogUri { get; set; }
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
        public int Amount { get; set; }
        public double PartsCount { get; set; }
    }
}