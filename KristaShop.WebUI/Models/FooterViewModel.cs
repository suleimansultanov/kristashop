﻿namespace KristaShop.WebUI.Models {
    public class FooterViewModel {
        public string TermsOfUseUri { get; set; }
        public string FacebookLink { get; set; }
        public string VkLink { get; set; }
        public string InstagramLink { get; set; }
        public string YoutubeLink { get; set; }
        public string Contacts { get; set; }
        public string CatalogUri { get; set; }
    }
}
