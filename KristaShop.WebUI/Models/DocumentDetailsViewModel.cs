﻿using System.Collections.Generic;
using KristaShop.Business.DTOs;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.DTOs;

namespace KristaShop.WebUI.Models {
    public class DocumentDetailsViewModel {
        public List<CartItemDTO> Items { get; set; }
        public List<DocRegistryFileDTO> Files { get; set; }
        public string DocNum { get; set; }
        public UniversalDocState Status { get; set; }
        public string StatusName { get; set; }
        public string Sum { get; set; }
        public string TotalPayIn { get; set; }
        public string TotalDebt { get; set; }
        public string TotalPrice { get; set; }
        public string PreorderPrice { get; set; }
        public string InStockPrice { get; set; }
        public bool HasPrepay { get; set; }
        public string Prepay { get; set; }
        public double TotalPartsCount { get; set; }
    }
}
