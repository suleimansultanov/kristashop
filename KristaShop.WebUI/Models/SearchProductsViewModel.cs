﻿using KristaShop.Business.DTOs;
using P.Pager;
using System.Collections.Generic;

namespace KristaShop.WebUI.Models
{
    public class SearchCatalogViewModel
    {
        public NomFilterDTO Filter { get; set; }
        public List<SearchProductsViewModel> SearchProducts { get; set; }
    }

    public class SearchProductsViewModel
    {
        public CatalogDTO Catalog { get; set; }
        public IPager<NomModelDTO> NomModels { get; set; }
        public DiscountDTO Discount { get; set; }
    }
}