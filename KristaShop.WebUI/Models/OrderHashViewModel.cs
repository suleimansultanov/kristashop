﻿using KristaShop.Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KristaShop.Common.ValidationAttributes;

namespace KristaShop.WebUI.Models
{
    public class OrderHashViewModel
    {
        public OrderItemVM OrderItem { get; set; }
        public BaseHashModel Hash { get; set; }
    }

    public class OrderItemVM
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
        public List<CartItemVM> CartItems { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class CartItemVM
    {
        public Guid ProductId { get; set; }
        public bool IsPreorder { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public string Articul { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
    }

    public class CheckoutCartVM
    {
        [Display(Name = "Упаковка")]
        public Guid? PackagingId { get; set; }

        [Display(Name = "Комментарий к заказу")]
        [MaxLength(5000, ErrorMessage = "Максимально количество символов {1}")]
        public string Description { get; set; }

        [Display(Name = "Я ознакомился (лась) *")]
        [IsTrue(ErrorMessage = "Необходимо ознакомиться с условиями")]
        public bool Accepted { get; set; }
    }
}