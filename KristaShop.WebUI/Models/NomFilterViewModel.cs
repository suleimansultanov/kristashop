﻿using KristaShop.Business.DTOs;
using P.Pager;
using System.Collections.Generic;

namespace KristaShop.WebUI.Models
{
    public class NomFilterViewModel
    {
        public NomFilterDTO Filter { get; set; }
        public CatalogDTO Catalog { get; set; }
        public List<CatalogDTO> Catalogs { get; set; }
        public IPager<NomModelDTO> PageList { get; set; }
        public DiscountDTO Discount { get; set; }
        public int Page { get; set; }
    }

    public class NomFavoriteViewModel
    {
        public NomFilterDTO Filter { get; set; }
        public IPager<NomFavoriteDTO> PageList { get; set; }
    }
}