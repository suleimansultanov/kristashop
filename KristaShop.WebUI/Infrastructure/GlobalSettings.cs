﻿namespace KristaShop.WebUI.Infrastructure {
    public class GlobalSettings {
        public string FilesDirectoryPath { get; set; }
        public string DocumentFilesDirectory { get; set; }
        public string EditorFileStorageDirectory { get; set; }
        public string GalleryDirectory { get; set; }
        public string GalleryCache { get; set; }
        public string FeedbackFilesDirectory { get; set; }
        public string MenuContentDirectory { get; set; }
        public string VideoPreviewsDirectory { get; set; }
        public string VideoGalleryPreviewsDirectory { get; set; }
        public string CatalogPreviewsDirectory { get; set; }
        public string FaqDocumentsDirectory { get; set; }

        public string GalleryPath => $"{FilesDirectoryPath}{GalleryDirectory}";
        public string DocumentFilesPath => $"{FilesDirectoryPath}{DocumentFilesDirectory}";
        public string EditorFileStoragePath => $"{FilesDirectoryPath}{EditorFileStorageDirectory}";
        public string FeedbackFilesPath => $"{FilesDirectoryPath}{FeedbackFilesDirectory}";
    }
}
