﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace KristaShop.WebUI.Infrastructure {
    public static class InitializeApplication {
        public static void InitializeApp(this IApplicationBuilder application, IServiceScopeFactory serviceScopeFactory) {
            _initializeSettingsService(serviceScopeFactory.CreateScope());
            _initializeDynamicPagesManager(serviceScopeFactory.CreateScope());
        }

        private static void _initializeSettingsService(IServiceScope serviceScope) {
            var settingsManager = serviceScope.ServiceProvider.GetRequiredService<ISettingsManager>();
            settingsManager.InitializeAsync(serviceScope);
        }

        private static void _initializeDynamicPagesManager(IServiceScope serviceScope) {
            var settingsManager = serviceScope.ServiceProvider.GetRequiredService<IDynamicPagesManager>();
            settingsManager.InitializeAsync(serviceScope);
        }
    }
}
