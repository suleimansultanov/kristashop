﻿using AutoMapper;
using KristaShop.Business.Clients;
using KristaShop.Business.Interfaces;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp.Web;
using SixLabors.ImageSharp.Web.Caching;
using SixLabors.ImageSharp.Web.Commands;
using SixLabors.ImageSharp.Web.DependencyInjection;
using SixLabors.ImageSharp.Web.Middleware;
using SixLabors.ImageSharp.Web.Processors;
using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography.X509Certificates;
using GoogleReCaptcha.V3;
using GoogleReCaptcha.V3.Interface;
using KristaShop.Common.ValidationAttributes;
using KristaShop.WebUI.Utils;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Configuration;

namespace KristaShop.WebUI.Infrastructure
{
    public static class MvcConfiguration
    {
        public static IServiceCollection AddMvcConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            var cacheFolder =
                configuration[$"{nameof(GlobalSettings)}:{nameof(GlobalSettings.FilesDirectoryPath)}"] +
                configuration[$"{nameof(GlobalSettings)}:{nameof(GlobalSettings.GalleryCache)}"];

            services.AddImageSharpCore()
                .SetRequestParser<QueryCollectionRequestParser>()
                .Configure<PhysicalFileSystemCacheOptions>(options =>
                {
                    options.CacheFolder = cacheFolder;
                })
                .SetCache(provider => new PhysicalFileSystemCache(
                    provider.GetRequiredService<IOptions<PhysicalFileSystemCacheOptions>>(),
                    provider.GetRequiredService<IWebHostEnvironment>(),
                    provider.GetRequiredService<IOptions<ImageSharpMiddlewareOptions>>(),
                    provider.GetRequiredService<FormatUtilities>()))
                .SetCacheHash<CacheHash>()
                .AddProvider<CustomPhysicalFileSystemProvider>()
                .AddProcessor<ResizeWebProcessor>()
                .AddProcessor<FormatWebProcessor>()
                .AddProcessor<BackgroundColorWebProcessor>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddMemoryCache();

            services.ConfigureNonBreakingSameSiteCookies();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie("BackendScheme", options => {
                    options.Cookie.Name = "Back.App";
                    options.LoginPath = "/Admin/Identity/Login/";
                    options.AccessDeniedPath = "/Admin/Identity/Login/";
                    options.ExpireTimeSpan = TimeSpan.FromHours(2);
                    options.SlidingExpiration = true;
                    options.Cookie.IsEssential = true;
                    options.Cookie.HttpOnly = true;
                    options.Cookie.SameSite = SameSiteMode.Strict;
                })
                .AddCookie("FrontendScheme", options => {
                    options.Cookie.Name = "Front.App";
                    options.LoginPath = "/Home/Index";
                    options.AccessDeniedPath = "/Home/Index";
                    options.ExpireTimeSpan = TimeSpan.FromHours(2);
                    options.SlidingExpiration = true;
                    options.Cookie.IsEssential = true;
                    options.Cookie.HttpOnly = true;
                    options.Cookie.SameSite = SameSiteMode.Strict;
                });

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });

            services.AddSingleton<IValidationAttributeAdapterProvider, CustomValidationAttributeAdapterProvider>();

            services.AddScoped<PermissionFilter>();
            services.AddHttpClient<IAsupApiClient<RegisterViewModel>, AsupApiClient<RegisterViewModel>>();
            services.AddHttpClient<IAsupApiClient<OrderHashViewModel>, AsupApiClient<OrderHashViewModel>>();
            services.AddHttpClient<IAsupApiClient<UserUpdateViewModel>, AsupApiClient<UserUpdateViewModel>>();
            services.AddHttpClient<IAsupApiClient<string>, AsupApiClient<string>>();
            services.AddHttpClient<ICaptchaValidator, GoogleReCaptchaValidator>();
            services.AddHttpContextAccessor();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);

            return services;
        }

        public static void AddDataProtection(this IServiceCollection services, IConfiguration configuration) {
            try {
                var dataProtectionKeysPath = configuration["DataProtection:KeysDirectory"];
                var certificatePath = configuration["DataProtection:CertificatePath"];
                var passwordEnvVariable = configuration["DataProtection:EnvironmentVariableWithCertificatePassword"];

                var password = Environment.GetEnvironmentVariable(passwordEnvVariable);

                if (File.Exists(certificatePath)) {
                    services.AddDataProtection()
                        .PersistKeysToFileSystem(new DirectoryInfo(dataProtectionKeysPath))
                        .ProtectKeysWithCertificate(new X509Certificate2(certificatePath, password))
                        .SetDefaultKeyLifetime(TimeSpan.FromDays(30))
                        .SetApplicationName("KristaShop.WebUI");
                } else {
                    Console.WriteLine("Data protection certificate not found. Failed to add DataProtection");
                }
            } catch (Exception ex) {
                Console.WriteLine($"Failed to add DataProtection. {ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}