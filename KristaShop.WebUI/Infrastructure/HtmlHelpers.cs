﻿using KristaShop.Common.Helpers;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace KristaShop.WebUI.Infrastructure
{
    public static class HtmlHelpers
    {
        public static string IsSelected(this IHtmlHelper html, string controller = null, string action = null, string cssClass = null)
        {
            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (string.IsNullOrEmpty(controller))
                controller = currentController;

            if (string.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ? cssClass : string.Empty;
        }

        public static string IsSelected(this IHtmlHelper html, Guid contentId, string cssClass = null)
        {
            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentId = html.ViewContext.RouteData.Values["id"]?.ToString();

            return contentId.ToString().Equals(currentId) ? cssClass : string.Empty;
        }

        public static bool IsSelected(this IHtmlHelper html, Guid contentId) {
            string currentId = html.ViewContext.RouteData.Values["id"]?.ToString();
            return contentId.ToString().Equals(currentId);
        }

        public static string AddValue(this IHtmlHelper html, bool allowAdd, string value) {
            return allowAdd ? value : string.Empty;
        }

        public static string PageClass(this IHtmlHelper htmlHelper)
        {
            string currentAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static string RemoveTags(this IHtmlHelper htmlHelper, string html) {
            return string.IsNullOrEmpty(html) ? string.Empty : Regex.Replace(html, "<.*?>", String.Empty);
        }


        private static string _version = string.Empty;
        public static string GetFileVersion(this IHtmlHelper htmlHelper, int length = 50) {
            if (string.IsNullOrEmpty(_version)) {
                Generator.NewString(length);
            }

            return _version;
        }
    }
}