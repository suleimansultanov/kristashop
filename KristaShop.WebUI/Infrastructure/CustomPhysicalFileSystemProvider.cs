﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp.Web;
using SixLabors.ImageSharp.Web.Providers;
using SixLabors.ImageSharp.Web.Resolvers;

namespace KristaShop.WebUI.Infrastructure {
    /// </summary>
    public class CustomPhysicalFileSystemProvider : IImageProvider {
        /// <summary>
        /// The file provider abstraction.
        /// </summary>
        private readonly IFileProvider fileProvider;

        /// <summary>
        /// Contains various format helper methods based on the current configuration.
        /// </summary>
        private readonly FormatUtilities formatUtilities;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomPhysicalFileSystemProvider"/> class.
        /// </summary>
        /// <param name="environment">The environment used by this middleware.</param>
        /// <param name="formatUtilities">Contains various format helper methods based on the current configuration.</param>
        public CustomPhysicalFileSystemProvider(
            IWebHostEnvironment environment,
            FormatUtilities formatUtilities,
            IOptions<GlobalSettings> options) {

            this.fileProvider = new PhysicalFileProvider(options.Value.FilesDirectoryPath);
            this.formatUtilities = formatUtilities;
        }

        /// <inheritdoc/>
        public ProcessingBehavior ProcessingBehavior { get; } = ProcessingBehavior.CommandOnly;

        /// <inheritdoc/>
        public Func<HttpContext, bool> Match { get; set; } = _ => true;

        /// <inheritdoc/>
        public bool IsValidRequest(HttpContext context) => this.formatUtilities.GetExtensionFromUri(context.Request.GetDisplayUrl()) != null;

        /// <inheritdoc/>
        public Task<IImageResolver> GetAsync(HttpContext context) {
            // Path has already been correctly parsed before here.
            var fileInfo = this.fileProvider.GetFileInfo(context.Request.Path.Value);

            try {
                // Check to see if the file exists.
                if (!fileInfo.Exists) {
                    return Task.FromResult<IImageResolver>(null);
                }

                var metadata = new ImageMetadata(fileInfo.LastModified.UtcDateTime);
                return Task.FromResult<IImageResolver>(new PhysicalFileSystemResolver(fileInfo, metadata));
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
