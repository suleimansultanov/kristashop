﻿using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PermissionAttribute : Attribute, IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            IFrontUserService userService = context.HttpContext.RequestServices.GetRequiredService<IFrontUserService>();

            var notFoundRoute = new RouteValueDictionary(new { controller = "Error", action = "Common404" });

            string controllerName = context.RouteData.Values["controller"].ToString().ToLower();
            var user = userService.Get();

            if (user == null || (user.AccessLevel == (int)AccessControlLevel.Client && !controllerName.Equals("manager") && !controllerName.Equals("faq")))
            {
                return;
            }
            else if (user.AccessLevel == (int)AccessControlLevel.Manager && (controllerName.Equals("manager") || controllerName.Equals("faq")))
            {
                return;
            }
            else
            {
                context.Result = new RedirectToRouteResult(notFoundRoute);
                return;
            }
        }
    }
}