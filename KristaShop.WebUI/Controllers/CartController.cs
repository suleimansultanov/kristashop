﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Serilog;

namespace KristaShop.WebUI.Controllers
{
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IFrontUserService _userService;
        private readonly INomenclatureService _nomenclatureService;
        private readonly ILogger _logger;
        private readonly IAsupApiClient<OrderHashViewModel> _apiClient;
        private readonly IMapper _mapper;

        public CartController (IAsupApiClient<OrderHashViewModel> apiClient, IMapper mapper,
            ICartService cartService, IFrontUserService userService,
            INomenclatureService nomenclatureService, ILogger logger)
        {
            _apiClient = apiClient;
            _mapper = mapper;
            _cartService = cartService;
            _userService = userService;
            _nomenclatureService = nomenclatureService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            UserSession user = _userService.Get();
            var cartItems = await _cartService.GetCartItems(user.UserId);
            if (cartItems.Count == 0)
                return RedirectToAction("CartEmpty", "Error");
            return View(cartItems);
        }
        
        [NonAction]
        public IActionResult Success(string header, string message) {
            ViewData["header"] = header;
            ViewData["Message"] = message;
            return View("Success");
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(CartItemViewModel model)
        {
            UserSession user = _userService.Get();
            model.UserId = user.UserId;
            var dto = _mapper.Map<CartItemDTO>(model);
            var result = await _cartService.InsertCartItem(dto);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction("Product", "Catalog", new { nomId = model.NomId, ctlgUri = model.CatalogUri });
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid cartId)
        {
            var result = await _cartService.RemoveCartItemById(cartId);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> OrderProducts(CheckoutCartVM checkoutCartVM, [FromServices] ISettingsManager settingsManager)
        {
            if (!checkoutCartVM.Accepted)
            {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Чтобы оформить заказ, Вы должны ознакомиться с условиями" }));
                return RedirectToAction(nameof(Index));
            }

            UserSession user = _userService.Get();
            var cartItems = await _cartService.GetCartItems(user.UserId);
            if (cartItems.Count == 0)
            {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Вы не можете оформить заказ.", "В данный момент корзина пуста." }));
                return RedirectToAction(nameof(Index));
            }

            var cartItemVMs = _mapper.Map<List<CartItemVM>>(cartItems);
            if (checkoutCartVM.PackagingId.HasValue)
            {
                var cartItemDto = await _nomenclatureService.GetPackagingCartItem(checkoutCartVM.PackagingId.Value);
                if (cartItemDto == null)
                {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Данной упаковки нет в наличии." }));
                    return RedirectToAction(nameof(Index));
                }
                cartItemVMs.Add(_mapper.Map<CartItemVM>(cartItemDto));
            }
            OrderItemVM orderItem = new OrderItemVM
            {
                UserId = user.UserId,
                Description = checkoutCartVM.Description,
                CartItems = cartItemVMs
            };
            var modelJson = orderItem.ToJson();
            OrderHashViewModel model = new OrderHashViewModel
            {
                OrderItem = orderItem,
                Hash = new BaseHashModel { HashCode = modelJson.ComputeSha256Hash() }
            };
            var result = await _apiClient.SendDataAsync(model, GlobalConstant.URLs.PostOrderData);
            if (result.IsSuccess)
            {
                await _cartService.RemoveCartItemsByUserId(user.UserId);
                return this.Success(result.Messages[0], settingsManager.Settings.CartSuccess);
            } else {
                _logger.Error("Failed co create order {@result}", result);
            }

            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction(nameof(Index));
        }
    }
}