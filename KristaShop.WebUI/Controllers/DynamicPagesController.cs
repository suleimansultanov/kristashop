﻿using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.Controllers {
    public class DynamicPagesController : Controller {
        private readonly IDynamicPagesManager _manager;
        private readonly IFrontUserService _frontUserService;

        public DynamicPagesController(IDynamicPagesManager manager, IFrontUserService frontUserService) {
            _frontUserService = frontUserService;
            _manager = manager;
        }

        [Route("[controller]/{uri}")]
        public IActionResult Index(string uri) {
            if (string.IsNullOrEmpty(uri)) {
                uri = nameof(Index);
            }

            var (controller, action) = UrlHelper.DeconstructUri(uri);
            ViewData["DynamicController"] = controller;
            ViewData["DynamicAction"] = action;
            ViewData["MetaInfoPath"] = uri;

            var user = _frontUserService.Get();

            if (!_manager.TryGetValuesByController(controller, user == null, out var pages)) {
                return RedirectToAction("Common404", "Error");
            }

            ViewData["Title"] = pages.Find(x => x.URL.Equals(uri))?.Title ?? string.Empty;
            return View(nameof(Index), pages);
        }

        [Route("[controller]/{uri}")]
        public IActionResult Single(string uri) {
            if (string.IsNullOrEmpty(uri)) {
                uri = nameof(Index);
            }


            var (controller, action) = UrlHelper.DeconstructUri(uri);
            ViewData["DynamicController"] = controller;
            ViewData["DynamicAction"] = action;
            ViewData["MetaInfoPath"] = uri;

            var user = _frontUserService.Get();
            if (!_manager.TryGetValue(uri, out var page) || (user == null && !page.IsOpen)) {
                return RedirectToAction("Common404", "Error");
            }

            ViewData["Title"] = page.Title;
            return View(nameof(Single), page);
        }
    }
}