﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces;

namespace KristaShop.WebUI.Controllers {
    public class VideoGalleryController : Controller {
        private readonly IVideoGalleryService _galleryService;
        private readonly ILogger _logger;
        private readonly IFrontUserService _userService;

        public VideoGalleryController(IVideoGalleryService galleryService, ILogger logger, IFrontUserService userService) {
            _galleryService = galleryService;
            _logger = logger;
            _userService = userService;
        }

        public async Task<IActionResult> Index() {
            try {
                var openOnly = _userService.Get() == null;
                var result = await _galleryService.GetGalleriesWithVideoAsync(4, openOnly);
                if (result.Any()) {
                    return View(result);
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get galleries list. {message}", ex.Message);
            }
            return BadRequest();
        }

        public async Task<IActionResult> Details(string gallery, int page = 0) {
            try {
                var openOnly = _userService.Get() == null;
                var galleryFound = await _galleryService.GetGalleryAsync(gallery, openOnly);
                if (galleryFound != null) {
                    var result = await _galleryService.GetVideosByGalleryOnPageAsync(galleryFound.Id, page);
                    ViewData["Gallery"] = galleryFound;
                    return View(result);
                }

            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get galleries list. {message}", ex.Message);
            }
            return BadRequest();
        }
    }
}
