﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Serilog;
using IUserService = KristaShop.Business.Interfaces.IUserService;

namespace KristaShop.WebUI.Controllers {
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class AccountController : Controller {
        private readonly IUserService _userService;
        private readonly ILinkService _linkService;
        private readonly ICatalogService _catalogService;
        private readonly IAsupApiClient<RegisterViewModel> _apiIdentityClient;
        private readonly IDictionaryService _dictionaryService;
        private readonly ILogger _logger;

        public AccountController(IUserService userService, IAsupApiClient<RegisterViewModel> apiIdentityClient,
            ILinkService linkService, ICatalogService catalogService, IDictionaryService dictionaryService, ILogger logger) {
            _userService = userService;
            _linkService = linkService;
            _apiIdentityClient = apiIdentityClient;
            _catalogService = catalogService;
            _dictionaryService = dictionaryService;
            _logger = logger;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login() {
            LoginViewModel model = new LoginViewModel();
            return PartialView("_LoginPartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var result = await _userService.SignIn(model.UserName, model.Password, model.RememberMe, false);
                    if (!result.IsSuccess) {
                        ModelState.TryAddModelErrors(result.Messages);
                    } else {
                        return StatusCode(StatusCodes.Status302Found, new RedirectViewModel("Index", "Home"));
                    }
                } catch (Exception ex) {
                    _logger.Error(ex, "Failed to login {user}. {message}", model.UserName, ex.Message);
                    ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                }
            }

            return PartialView("_LoginPartial", model);
        }

        [AllowAnonymous]
        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> LoginByLink(string code, string returnUrl) {
            try {
                var linkResult = await _linkService.GetUserIdByRandCodeAsync(code);
                if (linkResult.IsSuccess) {
                    await _userService.SignOut(false);
                    var result = await _userService.SignInByLink(linkResult.Model);

                    if (linkResult.Model.Type == AuthorizationLinkType.ChangePassword) {
                        returnUrl = Url.Action("ChangePassword", "Personal");
                    }

                    TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                    if (!result.IsSuccess) {
                        HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                    } else {
                        if (!string.IsNullOrEmpty(returnUrl)) {
                            return LocalRedirect(returnUrl);
                        }
                    }
                }

                return RedirectToAction("Index", "Home");
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to login by link {link}. {message}", code, ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(ex);
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<IActionResult> Logout() {
            await _userService.SignOut(false);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromServices] ISettingsManager settingsManager) {
            RegisterViewModel model = new RegisterViewModel();
            model.TermsOfUse = settingsManager.Settings.TermsOfUse;
            model.Cities = new SelectList(_dictionaryService.GetCities(), "Id", "Name");
            return PartialView("_RegisterPartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var result = await _apiIdentityClient.RegisterAsync(model);
                    if (result.StatusCode < 400) {
                        if (!result.UserId.IsEmpty()) {
                            await _catalogService.AddNonVisUserAllCatalogs(result.UserId);
                        }

                        return StatusCode(StatusCodes.Status201Created, new RedirectViewModel("Index", "Home", OperationResult.Success(result.ReadableMessage)));
                    }

                    ModelState.AddModelError(string.Empty, result.HasReadableMessage ? result.ReadableMessage : "Произошла ошибка во время регистрации, повторите попытку позже");
                    _logger.Error("User wasn't been registered {@request} {@result}", model, result);
                } catch (Exception ex) {
                    _logger.Error(ex, "Failed to register user {@model}. {message}", model, ex.Message);
                    ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                }
            }
            model.Cities = new SelectList(_dictionaryService.GetCities(), "Id", "Name");
            return PartialView("_RegisterPartial", model);
        }
    }
}