﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Controllers
{
    public class InstaBlogController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IMenuContentService _menuContentService;
        private readonly string _url;

        public InstaBlogController(IBlogService blogService, IMenuContentService menuContentService)
        {
            _blogService = blogService;
            _menuContentService = menuContentService;
            _url = "/Instagram/Index";
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            var content = await _menuContentService.GetMenuContentByUrlAsync(_url);
            ViewBag.Description = content?.Body;
            const int modelInPage = 12;
            var list = await _blogService.GetPaginationBlogs(page, modelInPage);
            return View(list);
        }
    }
}