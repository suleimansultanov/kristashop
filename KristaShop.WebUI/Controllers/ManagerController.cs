﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Extensions;
using KristaShop.Common.Helpers;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.AspNetCore.Http;
using Serilog;
using IUserService = KristaShop.Business.Interfaces.IUserService;

namespace KristaShop.WebUI.Controllers
{
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class ManagerController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICatalogService _catalogService;
        private readonly ILinkService _linkService;
        private readonly IAsupApiClient<UserUpdateViewModel> _apiClient;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public ManagerController
            (IUserService userService, ICatalogService catalogService,
            ILinkService linkService, IAsupApiClient<UserUpdateViewModel> apiClient,
            IMapper mapper, ILogger logger)
        {
            _userService = userService;
            _catalogService = catalogService;
            _linkService = linkService;
            _apiClient = apiClient;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IActionResult> Index(UserParamsViewModel model, [FromServices] IFrontUserService userService)
        {
            var user = userService.Get();
            var clients = _userService.GetAllUsers(user);
            ViewBag.Catalogs = await _catalogService.GetCatalogs();
            ViewBag.NewUsers = new SelectList(clients.Where(x => x.Status == (int)UserStatus.Await).ToList(), "UserId", "ClientFullName", model.ParamUserId);
            ViewBag.AllUsers = new SelectList(clients, "UserId", "TitlePerson", model.ParamUserId);
            return View(model);
        }

        public async Task<IActionResult> GetUserParams(Guid? userId)
        {
            if (!userId.HasValue) {
                return BadRequest("Пользователь не выбран.");
            }

            var result = await _linkService.InsertLinkAuthAsync(userId.Value, AuthorizationLinkType.MultipleAccess, false);
            if (!result.IsSuccess) {
                _logger.Warning(result.Exception, "{message}. {@result}", result.ToString(), result);
                return BadRequest($"Произошла ошибка при получении ссылки для входа. {result.ReadableMessage}");
            }

            var ctlgIds = await _catalogService.GetNonVisUserCatalogs(userId.Value);
            bool isActiveUser = await _userService.IsActiveUserAsync(userId.Value);
            return Ok(new { link = result.Model, ctlgIds, isActiveUser });
        }

        public async Task<IActionResult> GetUserDetails(Guid userId, [FromServices] IDictionaryService dictionaryService) {
            if (userId == Guid.Empty) {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Клиент не выбран." }));
                return RedirectToAction(nameof(Index), new UserParamsViewModel());
            }

            var user = await _userService.GetUserAsync(userId);
            var model = _mapper.Map<UserUpdateViewModel>(user);
            model.Cities = new SelectList(dictionaryService.GetCities(), "Id", "Name");
            model.Statuses = new SelectList(UserStatusExtension.GetActivateStatuses(), "Key", "Value" );
            return PartialView("_UserDetails", model);
        }

        [HttpPost]
        public async Task<IActionResult> SetUserParams(UserParamsViewModel model) {
            try {
                if (!model.ParamUserId.IsEmpty()) {
                     await _catalogService.AddNonVisUserCatalogs(model.ParamUserId, model.NonVisCtlgIds);
                } else {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure());
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to set not visible catalogs for user {@model}. {message}", ex.Message, model);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure());
            }

            return RedirectToAction(nameof(Index), new { model });
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserClient(UserUpdateViewModel model, [FromServices] IDictionaryService dictionaryService) {
            if (model.Id.IsEmpty()) {
                ModelState.TryAddModelError(string.Empty, "Пользователь не выбран");
            } else {
                model.Password = HashHelper.TransformPassword(model.Password);
                var result = await _apiClient.UpdateUserAsync(model);
                if (result.StatusCode < 400) {
                    return StatusCode(StatusCodes.Status206PartialContent, OperationResult.Success("Данные пользователя успешно обновлены"));
                }

                _logger.Error("Failed to update new user data {@model} {@result}", model, result);
                if (result.HasReadableMessage) {
                    ModelState.TryAddModelError(string.Empty, result.ReadableMessage);
                } else {
                    ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                }
            }

            model.Cities = new SelectList(dictionaryService.GetCities(), "Id", "Name");
            model.Statuses = new SelectList(UserStatusExtension.GetActivateStatuses(), "Key", "Value" );
            return PartialView("_UserDetails", model);
        } 
    }
}