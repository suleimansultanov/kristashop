﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Controllers
{
    public class GalleryController : Controller
    {
        private readonly IGalleryService _galleryService;
        private readonly IMenuContentService _menuContentService;
        private readonly string _url;

        public GalleryController(IGalleryService galleryService, IMenuContentService menuContentService)
        {
            _galleryService = galleryService;
            _menuContentService = menuContentService;
            _url = "/Gallery/Index";
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            var content = await _menuContentService.GetMenuContentByUrlAsync(_url);
            ViewBag.Description = content?.Body;
            const int modelInPage = 12;
            var list = await _galleryService.GetPaginationGallerys(page, modelInPage);
            return View(list);
        }
    }
}