﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using GoogleReCaptcha.V3.Interface;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Models;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace KristaShop.WebUI.Controllers {
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class FeedbackController : Controller {
        private readonly IFeedbackService _feedbackService;
        private readonly IMapper _mapper;
        private readonly IFrontUserService _userService;
        private readonly ICaptchaValidator _captchaValidator;
        private readonly GlobalSettings _globalSettings;

        public FeedbackController(IFeedbackService feedbackService, IMapper mapper, IFrontUserService userService, 
            IOptions<GlobalSettings> _globalSettingsOptions, ICaptchaValidator captchaValidator) {
            _feedbackService = feedbackService;
            _mapper = mapper;
            _userService = userService;
            _captchaValidator = captchaValidator;
            _globalSettings = _globalSettingsOptions.Value;
        }

        [AllowAnonymous]
        public IActionResult Get() {
            var user = _userService.Get();
            var model = user?.CounterParty != null ? _mapper.Map<FeedbackViewModel>(user.CounterParty) : new FeedbackViewModel();
            return PartialView("_FeedbackFormPartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FeedbackViewModel model) {
            if (ModelState.IsValid) {
                if (await _captchaValidator.IsCaptchaPassedAsync(model.Captcha)) {
                    try {
                        var feedbackModel = _mapper.Map<FeedbackDTO>(model);
                        feedbackModel.Type = FeedbackType.Basic;
                        var result = await _feedbackService.InsertFeedback(feedbackModel);

                        if (!result.IsSuccess) {
                            ModelState.TryAddModelErrors(result.Messages);
                        }
                    } catch (Exception ex) {
                        Console.WriteLine(ex);
                        ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                    }
                } else {
                    ModelState.AddModelError(string.Empty, "Капча не валидна");
                }
            }

            return PartialView("_FeedbackFormPartial", model);
        }

        public IActionResult GetWithFile() {
            var model = new AuthFeedbackViewModel();
            return PartialView("_FeedbackCoplaintsPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateWithFile(AuthFeedbackViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var user = _userService.Get();

                    var feedbackModel = _mapper.Map<FeedbackDTO>(user.CounterParty);
                    _mapper.Map(model, feedbackModel);
                    feedbackModel.Type = FeedbackType.ComplaintsAndSuggestions;

                    OperationResult result;
                    if (model.File != null) {
                        var file = await _getFeedbackFileDto(model);
                        result = await _feedbackService.InsertFeedbackWithFileAsync(feedbackModel, file);
                    } else {
                        result = await _feedbackService.InsertFeedback(feedbackModel);
                    }

                    if (!result.IsSuccess) {
                        ModelState.TryAddModelErrors(result.Messages);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex);
                    ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                }
            }

            return PartialView("_FeedbackCoplaintsPartial", model);
        }

        private async Task<FeedbackCreateFileDTO> _getFeedbackFileDto(AuthFeedbackViewModel model) {
            var file = new FeedbackCreateFileDTO {
                OriginalName = model.File.FileName,
                FileStream = new MemoryStream(),
                FilesDirectoryPath = _globalSettings.FilesDirectoryPath,
                Directory = _globalSettings.FeedbackFilesDirectory
            };
            await model.File.CopyToAsync(file.FileStream);
            return file;
        }

        public IActionResult GetManagementContacts() {
            return PartialView("_FeedbackManagementContactsPartial", new ManagementContactsFeedbackViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateManagementContacts(ManagementContactsFeedbackViewModel model) {
            if (ModelState.IsValid) {
                try {
                    var user = _userService.Get();
                    var feedbackModel = _mapper.Map<FeedbackDTO>(user.CounterParty);
                    _mapper.Map(model, feedbackModel);
                    feedbackModel.Type = FeedbackType.ManagementContacts;
                    var result = await _feedbackService.InsertFeedback(feedbackModel);

                    if (!result.IsSuccess) {
                        ModelState.TryAddModelErrors(result.Messages);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex);
                    ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                }
            }

            return PartialView("_FeedbackManagementContactsPartial", model);
        }
    }
}
