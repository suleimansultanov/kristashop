﻿using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Extensions;
using KristaShop.DataReadOnly.DTOs;
using Serilog;
using ControllerBase = KristaShop.WebUI.Controllers.Common.ControllerBase;

namespace KristaShop.WebUI.Controllers {
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class CatalogController : ControllerBase
    {
        private readonly INomenclatureService _nomenclatureService;
        private readonly ICatalogService _ctlgService;
        private readonly IDiscountService _discountService;
        private readonly ICategoryService _ctgrService;
        private readonly ILogger _logger;

        public CatalogController (INomenclatureService nomenclatureService, ICategoryService ctgrService,
            ICatalogService ctlgService, IDiscountService discountService, ILogger logger)
        {
            _nomenclatureService = nomenclatureService;
            _ctgrService = ctgrService;
            _ctlgService = ctlgService;
            _discountService = discountService;
            _logger = logger;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index(NomFilterDTO filter, int page = 1)
        {
            const int modelInPage = 15;
            CatalogDTO catalog;
            if (!string.IsNullOrEmpty(filter.CatalogUri))
            {
                catalog = await _ctlgService.GetCatalogDetailsByUri(filter.CatalogUri, true, UserSession?.UserId ?? Guid.Empty);
                if (catalog == null) return BadRequest();

                filter.CatalogId = catalog?.Id;
            }
            else if(filter.CatalogId != null) {
                catalog = await _ctlgService.GetCatalogDetails(filter.CatalogId.Value, true, UserSession?.UserId ?? Guid.Empty);
                if (catalog == null) return BadRequest();

                filter.CatalogId = catalog?.Id;
                filter.CatalogUri = catalog?.Uri;
            } else {
                return BadRequest();
            }

            if (string.IsNullOrEmpty(filter.CatalogUri))
            {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "У вас закрыт доступ к каталогам." }));
                return RedirectToAction(nameof(Index), "Home");
            }
            if (!catalog.IsOpen && UserSession == null)
            {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "У вас закрыт доступ к каталогам." }));
                return RedirectToAction(nameof(Index), "Home");
            }

            filter.IsSet = catalog.IsSet;
            if (filter.OrderNew)
                filter.OrderColumn = filter.OrderColumn.Equals("OrderNewAsc") ? "OrderNewDesc" : "OrderNewAsc";
            else if (filter.OrderPrice)
                filter.OrderColumn = filter.OrderColumn.Equals("OrderPriceAsc") ? "OrderPriceDesc" : "OrderPriceAsc";

            if (string.IsNullOrEmpty(filter.OrderColumn))
                filter.OrderColumn = "";
            
            var discount = UserSession != null && !catalog.IsDisableDiscount ? await _discountService.GetDiscountAsync((Guid) filter.CatalogId, UserSession.UserId, Guid.Empty) : new DiscountDTO {DiscountPrice = 0, Type = DiscountType.None};

            var model = new NomFilterViewModel {
                Filter = filter,
                Catalog = catalog,
                Catalogs = await _ctlgService.GetCatalogsByUser(UserSession?.UserId),
                PageList = await _nomenclatureService.GetNomModelsByFilter(filter, page, modelInPage, UserSession?.UserId),
                Discount = discount,
                Page = page
            };
            return View(model);
        }

        public async Task<IActionResult> Search(NomFilterDTO filter)
        {
            var nomIds = await _nomenclatureService.GetNomIdsForSearch();
            ViewBag.Catalogs = new SelectList(await _ctlgService.GetCatalogsByUser(UserSession?.UserId), "Uri", "Name", filter.CatalogUri);
            return View(new SearchPanelViewModel { NomSearch = await _nomenclatureService.GetListsForSearchByNomIds(nomIds), NomFilter = filter });
        }

        [AllowAnonymous]
        public async Task<IActionResult> Products(NomFilterDTO filter,
            [FromServices] ISettingsManager settingsManager, [FromServices] IMenuContentService contentService)
        {
            List<SearchProductsViewModel> models = new List<SearchProductsViewModel>();
            if (!string.IsNullOrEmpty(filter.CatalogUri)) {
                var catalog = await _ctlgService.GetCatalogDetailsByUri(filter.CatalogUri);
                if (catalog == null) return BadRequest();

                filter.CatalogId = catalog.Id;
                var model = new SearchProductsViewModel {
                    Catalog = catalog,
                    NomModels = await _nomenclatureService.GetNomModelsByFilter(filter, 1, 1000, UserSession?.UserId),
                    Discount = UserSession != null && !catalog.IsDisableDiscount ? await _discountService.GetDiscountAsync((Guid)filter.CatalogId, UserSession.UserId, Guid.Empty) : new DiscountDTO { DiscountPrice = 0, Type = DiscountType.None }
                };
                models.Add(model);
            } else {
                var catalogs = await _ctlgService.GetCatalogsByUser(UserSession?.UserId);
                foreach (var catalog in catalogs) {
                    filter.CatalogId = catalog.Id;
                    var model = new SearchProductsViewModel {
                        Catalog = catalog,
                        NomModels = await _nomenclatureService.GetNomModelsByFilter(filter, 1, 1000, UserSession?.UserId),
                        Discount = UserSession != null && !catalog.IsDisableDiscount ? await _discountService.GetDiscountAsync((Guid)filter.CatalogId, UserSession.UserId, Guid.Empty) : new DiscountDTO { DiscountPrice = 0, Type = DiscountType.None }
                    };

                    if (model.NomModels.TotalItemCount > 0) {
                        models.Add(model);
                    }
                }
                filter.CatalogId = null;
                filter.CatalogUri = null;
            }

            if (filter.CategoryIds != null && filter.CategoryIds.Count == 1) {
                var category = await _ctgrService.GetCategoryDetails(filter.CategoryIds.First());
                ViewData["SearchTitle"] = category.Name;
                var description = await contentService.GetMenuContentByUrlAsync(settingsManager.Settings.OpenCatalogSearchDescription);
                ViewData["SearchDescription"] = description?.Body;
            }

            var searchCatalog = new SearchCatalogViewModel {
                Filter = filter,
                SearchProducts = models
            };

            ViewData["HasAccess"] = UserSession != null;
            return View(searchCatalog);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Product(Guid? nomId, string ctlgUri, int page = 1)
        {
            if (nomId.HasValue && !string.IsNullOrEmpty(ctlgUri))
            {
                var catalog = await _ctlgService.GetCatalogDetailsByUri(ctlgUri, true, UserSession?.UserId ?? Guid.Empty);
                if (catalog == null) return RedirectToAction("Model404", "Error");

                if (!catalog.IsOpen && UserSession == null)
                {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "У вас закрыт доступ к каталогу." }));
                    return RedirectToAction(nameof(Index), "Home");
                }

                var model = await _nomenclatureService.GetNomModelById(nomId.Value, ctlgUri, UserSession?.UserId);
                if (model == null) {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Модель не найдена." }));
                    return RedirectToAction(nameof(Index), new {catalogUri = ctlgUri, page});
                }

                ViewBag.CatalogName = catalog.Name;
                ViewBag.IsOpenCatalog = catalog.IsOpen;
                model.CatalogId = catalog.Id;
                model.CatalogUri = ctlgUri;
               

                ViewData["CatalogPage"] = page;
                return View(model);
            }
            return RedirectToAction(nameof(Index));
        }

        [AllowAnonymous]
        public async Task<IActionResult> GetColors(Guid nomenclatureId, Guid sizeId, Guid colorId,
            Guid catalogId, bool isSet) {
            try {
                if (nomenclatureId.IsEmpty() || sizeId.IsEmpty() || catalogId.IsEmpty())
                    return BadRequest();

                var catalog = await _ctlgService.GetCatalogDetails(catalogId);
                var isInStock = catalog.OrderForm == (int) OrderFormType.InStock;

                var colors = await _nomenclatureService.GetColorsBySizeAsync(catalogId, nomenclatureId, sizeId, isInStock);
                if (!colors.Any()) {
                    return BadRequest();
                }

                ProductDetailsDTO prices = null;
                if (UserSession != null) {
                    colorId = colors.Any(x => x.Id == colorId) ? colorId : colors.First().Id;
                    prices = await _nomenclatureService.GetProductDetails(nomenclatureId, sizeId, colorId, UserSession.UserId, catalogId, isSet);
                }

                return Ok(new { Colors = colors, Prices = prices, SelectedColorId = colorId });
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get colors list. {message}", ex.Message);
                return BadRequest();
            }
        }

        public async Task<IActionResult> GetProduct(Guid nomenclatureId, Guid sizeId, Guid colorId,
            Guid catalogId, bool isSet) {
            try {
                if (nomenclatureId.IsEmpty() || sizeId.IsEmpty() || colorId.IsEmpty() || catalogId.IsEmpty()) {
                    return BadRequest();
                }

                var productDetails = await _nomenclatureService.GetProductDetails(nomenclatureId, sizeId, colorId, UserSession.UserId, catalogId, isSet);
                return Ok(productDetails);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get product prices list. {message}", ex.Message);
                return BadRequest();
            }
        }
    }
}