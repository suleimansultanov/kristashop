﻿using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.Controllers {
    public class CategoryController : Controller {
        private readonly ICategoryService _categoryService;
        private readonly IMenuContentService _contentService;
        private readonly ISettingsManager _settingsManager;

        public CategoryController(ICategoryService categoryService, IMenuContentService contentService,
            ISettingsManager settingsManager) {
            _categoryService = categoryService;
            _contentService = contentService;
            _settingsManager = settingsManager;
        }

        public async Task<IActionResult> Index() {
            var categoriesDescription = await _contentService.GetMenuContentByUrlAsync(_settingsManager.Settings.CategoriesDescription);
            if (categoriesDescription != null) {
                ViewData["Description"] = categoriesDescription.Body;
            }

            var categories = await _categoryService.GetCategories();
            return View(categories);
        }
    }
}
