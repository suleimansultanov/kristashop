﻿using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Controllers
{
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class FavoriteController : Controller
    {
        private readonly IFavoriteService _favoriteService;

        public FavoriteController(IFavoriteService favoriteService)
        {
            _favoriteService = favoriteService;
        }

        public async Task<IActionResult> Index(NomFilterDTO filter, [FromServices] IFrontUserService userService, int page = 1)
        {
            const int modelInPage = 15;
            var user = userService.Get();
            if (filter.OrderNew)
                filter.OrderColumn = filter.OrderColumn.Equals("OrderNewAsc") ? "OrderNewDesc" : "OrderNewAsc";
            else if (filter.OrderPrice)
                filter.OrderColumn = filter.OrderColumn.Equals("OrderPriceAsc") ? "OrderPriceDesc" : "OrderPriceAsc";

            if (string.IsNullOrEmpty(filter.OrderColumn))
                filter.OrderColumn = "";
            NomFavoriteViewModel model = new NomFavoriteViewModel
            {
                Filter = filter,
                PageList = await _favoriteService.GetFavoritesAsync(filter, page, modelInPage, user.UserId)
            };
            return View(model);
        }

        public async Task<IActionResult> AddOrDeleteFavorite(Guid nomId, Guid catalogId, [FromServices] IFrontUserService userService) {
            try {
                var user = userService.Get();
                var result = await _favoriteService.AddOrDeleteFavoriteAsync(user.UserId, nomId, catalogId);
                if (result.IsSuccess) {
                    return Ok(result);
                }

                return BadRequest(result);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                return BadRequest(OperationResult.Failure());
            }
        }

        public async Task<IActionResult> DeleteFavorite(Guid nomId, Guid catalogId, [FromServices] IFrontUserService userService) {
            try {
                var user = userService.Get();
                var result = await _favoriteService.DeleteFavoriteAsync(user.UserId, nomId, catalogId);
                if (result.IsSuccess) {
                    return Ok(result);
                }

                return BadRequest(result);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                return BadRequest(OperationResult.Failure());
            }
        }
    }
}