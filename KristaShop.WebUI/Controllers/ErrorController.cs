﻿using KristaShop.WebUI.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.Controllers {
    public class ErrorController : Controller {
        public IActionResult Common404() {
            return View();
        }

        public IActionResult Common500() {
            return View();
        }

        public IActionResult Model404() {
            return View();
        }

        [Permission]
        [Authorize(AuthenticationSchemes = "FrontendScheme")]
        public IActionResult CartEmpty() {
            return View();
        }
    }
}