﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Extensions;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.DataReadOnly.Documents;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.WebUI.Models;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.Extensions.Options;
using Serilog;
using ControllerBase = KristaShop.WebUI.Controllers.Common.ControllerBase;
using IUserService = KristaShop.Business.Interfaces.IUserService;

namespace KristaShop.WebUI.Controllers {
    [Permission]
    [Authorize(AuthenticationSchemes = "FrontendScheme")]
    public class PersonalController : ControllerBase {
        private readonly IDocRegistryService _docRegistryService;
        private readonly IDocOrderService _docOrderService;
        private readonly IAsupApiClient<string> _asupApiClient;
        private readonly ILogger _logger;
        private readonly UrlSetting _urlSetting;
        private readonly GlobalSettings _globalSettings;

        public PersonalController
            (IDocRegistryService docRegistryService, IDocOrderService docOrderService,
            IAsupApiClient<string> asupApiClient, IOptions<UrlSetting> options, 
            IOptions<GlobalSettings> globalSettingsOptions, ILogger logger) {
            _docRegistryService = docRegistryService;
            _docOrderService = docOrderService;
            _asupApiClient = asupApiClient;
            _logger = logger;
            _urlSetting = options.Value;
            _globalSettings = globalSettingsOptions.Value;
        }

        public async Task<IActionResult> ChangePassword([FromServices] ILinkService linkService) {
            var checkCurrentPassword = !UserSession.IsSignedInForChangePassword;
            if (UserSession.IsSignedInForChangePassword) {
                if (!await linkService.IsLinkExistAsync(UserSession.LinkCode)) {
                    checkCurrentPassword = true;
                }
            }

            var model = new ChangePasswordViewModel {IsCurrentPasswordVisible = checkCurrentPassword};
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel passwordModel,
            [FromServices] IUserService userService, [FromServices] ILinkService linkService) {
            try {
                if (ModelState.IsValid) {
                    passwordModel.UserId = UserSession.UserId;
                    passwordModel.Password = HashHelper.TransformPassword(passwordModel.Password);
                    passwordModel.CurrentPassword = HashHelper.TransformPassword(passwordModel.CurrentPassword);

                    if (passwordModel.Password.Equals(passwordModel.CurrentPassword)) {
                        ModelState.TryAddModelError(string.Empty, "Текущий пароль и новый пароль не должны совпадать");
                        return View(passwordModel);
                    }

                    var checkCurrentPassword = !UserSession.IsSignedInForChangePassword;

                    if (UserSession.IsSignedInForChangePassword) {
                        if (!await linkService.IsLinkExistAsync(UserSession.LinkCode)) {
                            checkCurrentPassword = true;
                        }
                    }
                    
                    if (checkCurrentPassword) {
                        var isPasswordValid = await userService.ValidatePasswordAsync(UserSession.UserId, passwordModel.CurrentPassword);
                        if (!isPasswordValid) {
                            ModelState.TryAddModelError(nameof(ChangePasswordViewModel.CurrentPassword), "Текущий пароль введен неверно");
                            return View(passwordModel);
                        }
                    }

                    var result = await _asupApiClient.ChangePasswordAsync(passwordModel);
                    if (result.StatusCode < 400) {
                        try {
                            await linkService.RemoveLinkByCodeAsync(UserSession.LinkCode);
                        } catch (Exception ex) {
                            _logger.Error(ex, "Failed to remove user's password change link {@model} {message}", passwordModel, ex.Message);
                        }

                        TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Success(result.ReadableMessage));
                        return RedirectToAction(nameof(Orders));
                    }

                    _logger.Error("Failed to change user's password {@model} {@result}", passwordModel, result);
                    if (result.HasReadableMessage) {
                        ModelState.TryAddModelError(string.Empty, result.ReadableMessage);
                    } else {
                        ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                    }
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to change user's password {@model} {message}", passwordModel, ex.Message);
                ModelState.AddModelError(string.Empty, "Произошла ошибка при измении пароля, повторите операцию позже.");
            }

            return View(passwordModel);
        }

        public async Task<IActionResult> Orders() {
            var docs = await _docRegistryService.GetCounterpartyCurrentOrdersAsync(UserSession.CounterParty.Id);
            return View(docs);
        }

        public async Task<IActionResult> OrdersHistory() {
            var docs = await _docRegistryService.GetCounterpartyCompletedOrdersAsync(UserSession.CounterParty.Id);
            return View(docs);
        }

        public async Task<IActionResult> GetPartialDocuments(Guid subtreeId) {
            try {
                var docs = await _docRegistryService.GetCounterpartyOrderSubtreeDocumentsAsync(UserSession.CounterParty.Id, subtreeId, true);
                return PartialView("_DocumentsPartial", docs);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get order documents list. {message}", ex.Message);
                return Content(string.Empty);
            }
        }

        public async Task<IActionResult> GetPartialFiles(Guid docId) {
            try {
                var files = await _docRegistryService.GetCounterpartyFilesByDocId(docId, UserSession.CounterParty.Id);
                return PartialView("_FilesPartial", files);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get document files list. {message}", ex.Message);
                return Content(string.Empty);
            }
        }

        public async Task<IActionResult> DownloadFile(Guid id) {
            try {
                var file = await _docRegistryService.GetCounterpartyFileById(id, UserSession.CounterParty.Id);
                if (file == null) {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> {"Данный файл не найден."}));
                    return RedirectToAction(nameof(Orders));
                }

                string fileFolder = $"{_globalSettings.DocumentFilesPath}/";
                string filePath = Path.Combine(fileFolder, file.FileName);
                string fileType = FileTypeExtensions.GetContentType(file.VirtualPath);
                if (System.IO.File.Exists(filePath)) {
                    return PhysicalFile(filePath, fileType, file.FileName);
                }

                var fileArray = await _asupApiClient.GetFileDataAsync(_urlSetting.KristaFileServiceDocumentsVirtualPath, file.VirtualPath);
                if (fileArray == null) {
                    TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> {"Не удалось загрузить данный файл."}));
                    return RedirectToAction(nameof(Orders));
                }

                if (!Directory.Exists(fileFolder)) {
                    Directory.CreateDirectory(fileFolder);
                }

                await System.IO.File.WriteAllBytesAsync(filePath, fileArray);
                return File(fileArray, fileType, file.FileName);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to download document file. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> {"Не удалось загрузить данный файл."}));
                return RedirectToAction(nameof(Orders));
            }
        }

        public async Task<IActionResult> DocumentDetails(Guid subtreeId, Guid documentId, [FromServices] INomenclatureService nomenclatureService) {
            try {
                var doc = await _docRegistryService.GetDocumentByIdAsync(documentId, UserSession.CounterParty.Id, true);
                if (doc == null || !doc.IsDetail) {
                    doc = await _docRegistryService.GetOrderDocumentBySubtreeIdAsync(subtreeId, UserSession.CounterParty.Id, true);

                    if (doc == null) {
                        return RedirectToAction("Common404", "Error");
                    }
                }

                ViewData["Referer"] = _getRefererLink(doc.Status);

                if (doc.DocumentType == DocumentType.Order) {
                    return View(nameof(OrderDetails), await _getOrderDetailsDataAsync(doc, nomenclatureService));
                }

                if (doc.DocumentType == DocumentType.CashboxIncomeOrder ||
                    doc.DocumentType == DocumentType.CashboxOutcomeOrder || 
                    doc.DocumentType == DocumentType.PrepayInvoice ||
                    doc.DocumentType == DocumentType.PaymentInvoice) {
                    return View(nameof(SuborderDetails), await _getSuborderDetailsDataAsync(doc, nomenclatureService));
                }

                if (doc.DocumentType == DocumentType.Shipment) {
                    return View(nameof(StoreIncomeDetails), await _getStoreIncomeDetailsDataAsync(doc, nomenclatureService));
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to display document details. {message}", ex.Message);
            }

            return NotFound();
        }

        public async Task<IActionResult> OrderDetails(Guid docId, [FromServices] INomenclatureService nomenclatureService) {
            var doc = await _docRegistryService.GetDocumentByIdAsync(docId, UserSession.CounterParty.Id, true);
            if (doc == null) {
                return RedirectToAction("Common404", "Error");
            }

            ViewData["Referer"] = _getRefererLink(doc.Status);

            var documentDetails = await _getOrderDetailsDataAsync(doc, nomenclatureService);
            return View(documentDetails);
        }

        public async Task<IActionResult> SuborderDetails(Guid docId, [FromServices] INomenclatureService nomenclatureService) {
            var doc = await _docRegistryService.GetDocumentByIdAsync(docId, UserSession.CounterParty.Id, true);
            if (doc == null) {
                return RedirectToAction("Common404", "Error");
            }

            ViewData["Referer"] = _getRefererLink(doc.Status);

            var documentDetails = await _getSuborderDetailsDataAsync(doc, nomenclatureService);
            return View(documentDetails);
        }

        public async Task<IActionResult> StoreIncomeDetails(Guid docId, [FromServices] INomenclatureService nomenclatureService) {
            var doc = await _docRegistryService.GetDocumentByIdAsync(docId, UserSession.CounterParty.Id, true);
            if (doc == null) {
                return RedirectToAction("Common404", "Error");
            }

            ViewData["Referer"] = _getRefererLink(doc.Status);

            var documentDetails = await _getStoreIncomeDetailsDataAsync(doc, nomenclatureService);
            return View(documentDetails);
        }

        private async Task<DocumentDetailsViewModel> _getOrderDetailsDataAsync(DocRegistryDTO doc, INomenclatureService nomenclatureService) {
            var documentDetails = new DocumentDetailsViewModel {
                StatusName = doc.StatusName,
                DocNum = $"{doc.RuDocTypeName} №{doc.DocNum}",
                Status = doc.Status,
                TotalPayIn = $"{doc.SumPayIn.ToTwoDecimalPlaces()} $ | {doc.SumRuPayIn.ToTwoDecimalPlaces()} р",
                TotalDebt = $"{doc.SumDebt.ToTwoDecimalPlaces()} $ | {doc.SumRuDebt.ToTwoDecimalPlaces()} р",
                TotalPrice = $"{doc.Sum.ToTwoDecimalPlaces()} $ | {doc.SumRu.ToTwoDecimalPlaces()} р",
                PreorderPrice = $"{doc.PreOrderSum.ToTwoDecimalPlaces()} $ | {doc.PreOrderSumRu.ToTwoDecimalPlaces()} р",
                HasPrepay = doc.PrepaySum > GlobalConstant.Epsilon,
                Prepay = $"{doc.PrepaySum.ToTwoDecimalPlaces()} $ | {doc.PrepaySumRu.ToTwoDecimalPlaces()} р",
                InStockPrice = $"{doc.InStockSum.ToTwoDecimalPlaces()} $ | {doc.InStockSumRu.ToTwoDecimalPlaces()} р",
                Files = doc.Files
            };
            var orderItems = await _docOrderService.GetOrderItemsWithStatuses(doc.DocId, false);
            documentDetails.Items =  await nomenclatureService.GetCartOrderItems(orderItems, doc.Rate);
            documentDetails.TotalPartsCount = documentDetails.Items.Sum(x => x.TotalAmount);

            return documentDetails;
        }

        private async Task<DocumentDetailsViewModel> _getSuborderDetailsDataAsync(DocRegistryDTO doc, INomenclatureService nomenclatureService) {
            var documentDetails = new DocumentDetailsViewModel {
                StatusName = doc.StatusName,
                DocNum = $"{doc.RuDocTypeName} №{doc.DocNum}",
                Sum = $"{doc.Sum.ToTwoDecimalPlaces()} $ | {doc.Sum.ToTwoDecimalPlaces()} р",
                Files = doc.Files
            };

            var orderItems = await _docOrderService.GetOrderItemsPricesAsync(doc.DocId);
            var cartOrderItems = await nomenclatureService.GetCartOrderItems(orderItems, doc.Rate, doc.Percent);

            // Округлить сумму документа до ближайшего (вверх) целого числа
            // Остаток от округления распределить по позициям документа
            if (doc.DocumentType == DocumentType.PrepayInvoice && doc.CurrencyType != CurrencyType.RUB && doc.Sum % 1 > 1E-6) {
                var ceilSum = Math.Ceiling(doc.Sum);
                var remainder = Math.Round(ceilSum - doc.Sum, 2);
                var modulo = Math.Round(remainder / cartOrderItems.Count, 2);
                foreach (var item in cartOrderItems) {
                    item.TotalPrice += modulo;
                }

                documentDetails.Sum = $"{ceilSum.ToTwoDecimalPlaces()} $ | {doc.SumRu.ToTwoDecimalPlaces()} р";
            }

            documentDetails.Items = cartOrderItems;
            documentDetails.TotalPartsCount = orderItems.Sum(x => x.PartsCount);
            return documentDetails;
        }

        private async Task<DocumentDetailsViewModel> _getStoreIncomeDetailsDataAsync(DocRegistryDTO doc, INomenclatureService nomenclatureService) {
            var orderItems = await _docOrderService.GetOrderItemsFromStoreIncomeAsync(doc.DocId);
            var cartOrderItems = await nomenclatureService.GetCartOrderItems(orderItems, 0);
            var documentDetails = new DocumentDetailsViewModel {
                StatusName = doc.StatusName,
                DocNum = $"{doc.RuDocTypeName} №{doc.DocNum}",
                Files = doc.Files,
                Items = cartOrderItems
            };
            return documentDetails;
        }

        private string _getRefererLink(UniversalDocState status) {
            return status == UniversalDocState.Completed || status <= 0 ? $"{nameof(OrdersHistory)}" : $"{nameof(Orders)}";
        }
    }
}