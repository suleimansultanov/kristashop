﻿using KristaShop.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMenuContentService _menuContentService;
        private readonly Dictionary<int, string> _urls;

        public HomeController(IMenuContentService menuContentService)
        {
            _menuContentService = menuContentService;
            _urls = new Dictionary<int, string>
            {
                [1] = "/Gallery/Index",
                [2] = "/Category/Index",
                [3] = "/Instagram/Index",
                [4] = "/Abus/Index",
                [5] = "/Youtube/Index"
            };
        }

        public async Task<IActionResult> Index() {
            var contents = await _menuContentService.GetMenusContentByUrlsAsync(_urls.Values.ToList());
            ViewBag.GalleryDesc = contents.Find(x => string.Equals(x.URL, _urls[1], System.StringComparison.OrdinalIgnoreCase))?.Body;
            ViewBag.CategoryDesc = contents.Find(x => string.Equals(x.URL, _urls[2], System.StringComparison.OrdinalIgnoreCase))?.Body;
            ViewBag.InstagramDesc = contents.Find(x => string.Equals(x.URL, _urls[3], System.StringComparison.OrdinalIgnoreCase))?.Body;
            ViewBag.AboutDesc = contents.Find(x => string.Equals(x.URL, _urls[4], System.StringComparison.OrdinalIgnoreCase))?.Body;
            ViewBag.YoutubeDesc = contents.Find(x => string.Equals(x.URL, _urls[5], System.StringComparison.OrdinalIgnoreCase))?.Body;
            return View();
        }
    }
}