﻿using System;
using System.Security.Claims;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace KristaShop.WebUI.Controllers.Common {
    public class ControllerBase : Controller {
        public RedirectResult RedirectToActionWithQueryString(string action, string queryString) {
            var area = ControllerContext.RouteData.Values.ContainsKey("area") ? $"/{ControllerContext.RouteData.Values["area"]}" : string.Empty;
            return new RedirectResult($"{area}/{ControllerContext.RouteData.Values["controller"]}/{action}?{queryString}");
        }

        #region authorization
        private UserSession _userSession;

        protected string CurrentScheme { get; private set; }
        protected UserSession UserSession => _userSession ??= _setUserSessionData(_authenticateByCurrentScheme());

        protected string[] _getAuthenticationSchemes() {
            var authorizeAttribute = HttpContext.GetEndpoint().Metadata.GetMetadata<AuthorizeAttribute>();
            if (authorizeAttribute == null) return default;
            var schemes = authorizeAttribute.AuthenticationSchemes.Split(",", StringSplitOptions.RemoveEmptyEntries);
            return schemes;
        }

        protected AuthenticateResult? _authenticateByCurrentScheme() {
            var schemes = _getAuthenticationSchemes();
            foreach (var scheme in schemes) {
                var authResult = HttpContext.AuthenticateAsync(scheme).Result;
                if (authResult != null && authResult.Succeeded) {
                    CurrentScheme = scheme;
                    return authResult;
                }
            }

            return null;
        }

        protected virtual UserSession _setUserSessionData(AuthenticateResult authenticateResult) {
            if (authenticateResult != null && authenticateResult.Succeeded && !string.IsNullOrEmpty(CurrentScheme)) {
                var userData = authenticateResult.Principal.FindFirstValue(GlobalConstant.SessionKeys.GetKeyByScheme(CurrentScheme));
                if (!string.IsNullOrEmpty(userData)) {
                    return JsonConvert.DeserializeObject<UserSession>(userData);
                }
            }

            return null;
        }
        #endregion
    }
}
