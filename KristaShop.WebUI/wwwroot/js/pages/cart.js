﻿$(function () {
    var orderForm = $("#orderForm");
    var termsAcceptedCheckBox = orderForm.find("#Accepted");
    var submitOrderButton = orderForm.find("button[type='submit']");
    var submitOrderButtonHover = orderForm.find(".button-hover");

    termsAcceptedCheckBox.on("click", onTermsAcceptedClick);
    submitOrderButtonHover.on("click", onSubmitOrderHoverClick);

    function onTermsAcceptedClick(event) {
        if (isFormTermsValid()) {
            submitOrderButton.attr("disabled", false);
            submitOrderButtonHover.hide();
        } else {
            submitOrderButton.attr("disabled", true);
            submitOrderButtonHover.show();
        }
    }

    function isFormTermsValid() {
        $.validator.unobtrusive.parse(orderForm);
        return termsAcceptedCheckBox.valid();
    }

    function onSubmitOrderHoverClick(event) {
        isFormTermsValid();
    }
});