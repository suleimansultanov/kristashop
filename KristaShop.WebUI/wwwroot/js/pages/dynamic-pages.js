﻿$(function() {
    handleImages();

    function handleImages() {
        var images = $(".dynamic-page").find("img");
        for (var i = 0; i < images.length; i++) {
            var img = $(images[i]);
            var float = img.css("float");

            if (float !== "none") {
                if (float === "left") {
                    img.css("margin-right", "45px");
                } else if (float === "right") {
                    img.css("margin-left", "45px");
                }
            }
        }
    }
})