﻿$(".search-panel-collapse").on("hide.bs.collapse", function () {
    var button = $(this).parent().find("button");
    button.text(button[0].dataset.collapseShow);
});

$(".search-panel-collapse").on("show.bs.collapse", function () {
    var button = $(this).parent().find("button");
    button.text(button[0].dataset.collapseHide);
});