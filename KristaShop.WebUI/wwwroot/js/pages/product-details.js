﻿$(function() {
    var slider = $('#imageGallery').lightSlider({
        gallery: true,
        adaptiveHeight: true,
        item: 1,
        thumbItem: 8,
        vThumbWidth: 200,
        isDragingThumbsNow: false,
        enableDrag: false,
        onBeforeStart: function(el) {
            el.children('li').each(function(index, element) {
                element.setAttribute("data-id", index);
            });
        }
    });

    $('input[type=radio][name=Size]').change(function(event) {
        event.preventDefault();
        var nomenclatureId = $("#NomId").val();
        var sizeId = $('input[name="Size"]:checked').val();
        var colorId = $('input[name="Color"]:checked').val();
        var catalogId = $("#CatalogId").val();
        var isSet = $("#IsSet").val();
        $.ajax({
            type: "GET",
            url: '/Catalog/GetColors',
            data: {
                nomenclatureId: nomenclatureId,
                sizeId: sizeId,
                colorId: colorId,
                catalogId: catalogId,
                isSet: isSet
            },
            dataType: "json",
            success: function(data) {
                $(".rounded-circle").attr('disabled', true);
                $("#Amount").attr('disabled', true);
                $("#Amount").val(1);
                $("add-to-cart").attr("disabled", true);
                $('div.div-colors').empty();
                $.each(data.colors,
                    function(key, obj) {
                        var bg = obj.image !== "" && obj.image != null ? `url(${obj.image})` : obj.code;
                        var color = `<label>
                                        <input type="radio" name="Color" value="${obj.id}" autocomplete="off" data-colorname="${obj.name}" ${data.selectedColorId === obj.id ? "checked" : ""}/>
                                        <span class="swatch" style="background: ${bg}" data-toggle="tooltip" data-placement="top" title="${obj.name}"></span>
                                    </label>`;
                        $("div.div-colors").append(color);

                        if (data.selectedColorId === obj.id) {
                            goToSlideWithColor(obj.id, obj.name);
                        }
                    });
                updatePrices(data.prices);
            }
        });
    });

    $('div.div-colors').on("change",
        'input[type=radio][name="Color"]',
        function() {
            var canAccess = Boolean.parse($("#requestCanAccess").val());
            var colorId = $('input[name="Color"]:checked').val();
            var colorName = this.getAttribute("data-colorname");
            if (canAccess === true) {
                var nomenclatureId = $("#NomId").val();
                var catalogId = $("#CatalogId").val();
                var isSet = $("#IsSet").val();
                var sizeId = $('input[name="Size"]:checked').val();
                $.ajax({
                    type: "GET",
                    url: '/Catalog/GetProduct',
                    data: {
                        nomenclatureId: nomenclatureId,
                        colorId: colorId,
                        sizeId: sizeId,
                        catalogId: catalogId,
                        isSet: isSet
                    },
                    dataType: "json",
                    success: function(data) {
                        updatePrices(data);
                    }
                });
            }
            goToSlideWithColor(colorId, colorName);
        });

    function goToSlideWithColor(colorId, colorName) {
        $("#colorName").text(colorName);
        var $slide = $("#imageGallery [data-colorid=" + colorId + "]");
        if ($slide.length > 0) {
            var indexSlide = +$slide[0].getAttribute("data-id");
            slider.goToSlide(indexSlide);
        }
    }

    function updatePrices(data) {
        $("#Amount").attr('disabled', false);
        $("#Amount").val(1);
        $(".rounded-circle").attr('disabled', false);
        document.getElementById("add-to-cart").disabled = false;
        var singlePrice = data.price / data.partsCount;
        var singlePriceRu = data.priceRu / data.partsCount;
        $('#SinglePrice').text(currencyFormatDE(singlePrice));
        $('#SinglePriceRu').text(currencyFormatDE(singlePriceRu));
        $('#ProductPrice').text(currencyFormatDE(data.price));
        $('#ProductPriceRu').text(currencyFormatDE(data.priceRu));
        $('#ProductId').val(data.productId);
        $('#PartsCount').val(data.partsCount);
        $('#defaultPrice').val(data.price);
        $('#defaultPriceRu').val(data.priceRu);
        var total_amount = $("#Amount").val();
        var price = $("#defaultPrice").val();
        var total_price = total_amount * price;
        var total_price_ru = total_amount * data.priceRu;
        $("#totalAmount").text(total_amount);
        $("#totalPrice").text(currencyFormatDE(total_price));
        $("#totalPriceRu").text(currencyFormatDE(total_price_ru));
        $("#totalLineAmount").text(data.partsCount);
        checkAndDisable();
    }


    function currencyFormatDE(num) {
        return (
            num
                .toFixed(2) // always two decimal digits
                .replace('.', ',') // replace decimal point character with ,
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                .replace('.', ' ') // replace decimal point character with ,
                .replace(',', '.') // replace decimal point character with ,
        );
    }


    $(".plus").click(function(e) {
        e.preventDefault();
        var $input = $("#Amount").val();
        var value = parseInt($input);
        if (value < 1000) {
            value = value + 1;
        } else {
            value = 1000;
        }
        $("#Amount").val(value);
        var total_amount = parseInt($("#Amount").val());
        var parts_count = parseInt($("#PartsCount").val());
        var total_line_amount = total_amount * parts_count;
        var price = parseFloat($("#defaultPrice").val());
        var priceRu = parseFloat($("#defaultPriceRu").val());
        var total_price = total_amount * price;
        var total_price_ru = total_amount * priceRu;
        $("#totalAmount").text(total_amount);
        $("#totalLineAmount").text(total_line_amount);
        $("#totalPrice").text(currencyFormatDE(total_price));
        $("#totalPriceRu").text(currencyFormatDE(total_price_ru));
        checkAndDisable();
    });

    $(".minus").click(function(e) {
        e.preventDefault();
        var $input = $("#Amount").val();
        var value = parseInt($input);
        if (value > 1) {
            value = value - 1;
            $(this).removeAttr('disabled');
        } else {
            value = 0;
            $(this).attr('disabled', true);
        }
        $("#Amount").val(value);
        var total_amount = parseInt($("#Amount").val());
        var parts_count = parseInt($("#PartsCount").val());
        var total_line_amount = total_amount * parts_count;
        var price = parseFloat($("#defaultPrice").val());
        var priceRu = parseFloat($("#defaultPriceRu").val());
        var total_price = total_amount * price;
        var total_price_ru = total_amount * priceRu;
        $("#totalAmount").text(total_amount);
        $("#totalLineAmount").text(total_line_amount);
        $("#totalPrice").text(currencyFormatDE(total_price));
        $("#totalPriceRu").text(currencyFormatDE(total_price_ru));
        checkAndDisable();
    });

    $("#Amount").on('change',
        function() {
            if ($(this).val() <= 0)
                $(this).val(0);
            else if ($(this).val() >= 1000)
                $(this).val(1000);

            checkAndDisable();
        });

    function checkAndDisable() {
        $(".minus").removeAttr('disabled');
        if ($("#Amount").val() <= 1) {
            $(".minus").attr('disabled', true);
        }
    }
});

function updateFavoriteButton(form) {
    var icon = $(form).find(".icon");
    var favoriteClass = icon[0].dataset.favorite;
    if (icon.hasClass(favoriteClass)) {
        icon.removeClass(favoriteClass);
    } else {
        icon.addClass(favoriteClass);
    }
}