﻿$(function() {
    $(".select2-dropdown").select2();
});

var isNewUsersViewOpened = false;

$(':radio[name="usersRadio"]').change(function () {
    isNewUsersViewOpened = Boolean.parse($(this).attr("value"));
    var targetUsers = $(`.users-${isNewUsersViewOpened}`);
    $(".users").not(targetUsers).attr("hidden", true);
    $(targetUsers).removeAttr("hidden");
    $(".user-selectbox option:not([value])").prop('selected', true);

    onUserDeselected();
});

$(".user-selectbox").change(function () {
    var userId = $(this).val();
    if (userId == "") {
        onUserDeselected();
    } else {
        onUserSelected(userId);
    }
});

function onUserSelected(userId) {
    $.ajax({
        type: "GET",
        url: "/Manager/GetUserParams?userid=" + userId,
        success: function (data) {
            handleLoginLink(data.link);
            
            $("#ParamUserId").val(userId);
            $("input[name=NonVisCtlgIds]:checkbox").prop("checked", false);
            $.each(data.ctlgIds, function (index, value) {
                $("#customCheck-" + value).prop("checked", true);
            });
            $("#change-user-btn").show();
            $("#catalogs-access-select").show();
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}

function onUserDeselected() {
    $("#login-link").closest(".form-group").hide();
    $("#change-user-btn").hide();
    $("#catalogs-access-select").hide();
}

function handleLoginLink(linkData) {
    var loginLink = $("#login-link");
    var loginLinkInput = $("#link");
    if (!isNewUsersViewOpened) {
        var link = `${location.origin}?${linkData}`;
        loginLink.attr("href", link);
        loginLinkInput.val(link);
        loginLink.closest(".form-group").show();
    } else {
        loginLink.closest(".form-group").hide();
    }
}

$("#new-users-select").on("change", function() {
    var user = $("#new-users-select option:selected");
    var clientTab = $("#client-data-tab");
    
    var baseUrl = clientTab.attr("data-base-url");
    clientTab.attr("data-url", `${baseUrl}?userId=${user.val()}`);
    clientTab.closest(".modal-dialog").find(".modal-title").html(user.text());
});