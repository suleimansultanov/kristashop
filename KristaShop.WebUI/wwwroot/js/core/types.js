﻿// Parse boolean
var falsy = /^(?:f(?:alse)?|no?|0+)$/i;
Boolean.parse = function (val) {
    return !falsy.test(val) && !!val;
};

// Invoke function by name without arguments
function invokeFunction(functionName) {
    if (functionName === "" || functionName == undefined) return;

    var invoke = eval(functionName);
    if (typeof invoke == "function") {
        invoke();
    }
}

$(document).on("click", "[data-generate-password]", function() {
    var pass = Math.random().toString().substring(2, 8);
    var target = this.dataset.generatePassword;
    $(target).val(pass);
});