﻿lightbox.option({
    wrapAround: true,
    albumLabel: "Изображение %1 из %2"
});

function showAlert(alert) {
    var htmlText = "";
    alert.messages.forEach(element => htmlText += element + "<br>");
    const Toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 10000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
        }
    });

    Toast.fire({
        icon: alert.alertType,
        title: htmlText
    });
}

// Fix bootstrap custom file input
$(document).ready(function() {
    $(".custom-file-input").on("change", function (e) {
        console.log("custom input changed");
        var files = [];
        for (var i = 0; i < $(this)[0].files.length; i++) {
            files.push($(this)[0].files[i].name);
        }
        $(this).next(".custom-file-label").html(files.join(", "));
    });
});


// Top right notification window

const Toast = swal.mixin({
    toast: true,
    position: 'top-end',
    padding: '1.5rem',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
});

function showNotification(alertType, alertHtml) {
    Toast.fire({
        icon: alertType,
        title: alertHtml
    });
}

function showOperationResultNotification(operationResult) {
    var alertHtml = "";
    operationResult.messages.forEach(element => alertHtml += element + "<br>");
    showNotification(operationResult.alertType, alertHtml);
}

function showGenericNotificationError() {
    Toast.fire({
        icon: "error",
        title: "Произошла ошибка во время выполнения операции"
    });
}

function showNotificationError(message) {
    Toast.fire({
        icon: "error",
        title: message
    });
}

function showNotificationSuccess(message) {
    Toast.fire({
        icon: "success",
        title: message
    });
}

var RECAPTCHA_SITE_KEY = "6LckvL4ZAAAAAFrjlGA2YXw_fdGLVpiYf--2qx3-";
function addRecapcha(placeholder, selector) {
    window.grecaptcha.execute(RECAPTCHA_SITE_KEY, { action: "home" }).then(function (token) {
        $(placeholder).find(selector).val(token);
    });
}