﻿function topDashboardUpdated(item, value) {
    if (+value > 0) {
        $(item).addClass("badge-danger");
    } else {
        $(item).removeClass("badge-danger");
    }
}