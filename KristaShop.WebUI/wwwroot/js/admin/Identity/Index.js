﻿var table;
var dateFormat = "D.MM.YYYY HH:mm";
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Identity/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "columns": [
            {
                "name": "number",
                "render": function (data, type, full, meta) {
                    var count = meta.row;
                    count = count + 1;
                    return count;
                }
            },
            {
                "name": "login",
                "data": "login",
                "width": "7%"
            },
            { "name": "person", "data": "clientFullName" },
            { "name": "cityName", "data": "cityName" },
            {
                "name": "phone",
                "data": "phoneNumber",
                "width": "10%"
            },
            {
                "name": "mallAddress",
                "data": "shopName",
                "width": "15%",
                "defaultContent": "Отсутствует"
            },
            {
                "name": "email",
                "data": "email",
                "width": "7%",
                "defaultContent": "Отсутствует"
            },
            {
                "name": "statusName",
                "data": "statusName",
                "width": "7%"
            },
            {
                "name": "status",
                "data": "status",
                "visible": false
            },
            {
                "name": "catalogNames",
                "data": "catalogNames",
                "width": "10%"
            },
            {
                "name": "lastSignIn",
                "data": "lastSignIn",
                "width": "8%",
                "render": function(data, type, full, meta) {
                    var date = moment(data).format(dateFormat);
                    if (moment(moment(date, dateFormat)).isSameOrBefore(moment("1.01.0001 05:07", dateFormat))) {
                        date = "";
                    }
                    return date;
                }
            },
            {
                "name": "cartStatusIcon",
                "data": "cartStatus",
                "width": "1%",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data) {
                        return '<i class="fas fa-shopping-cart text-success"></i>';
                    } else {
                        return '<i class="fas fa-shopping-cart"></i>';
                    }
                }
            },
            {
                "name": "cartStatus",
                "data": "cartStatus",
                "visible": false
            },
            {
                "name": "userActions",
                "data": "userId",
                "width": "10%",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    var update = "";
                    
                    if (full.status === 0) {
                        update = `<a class="dropdown-item" href="#" onclick="EditUser('${full.userId}')"><i class="fas fa-user-edit"></i> Активировать</a><div class="dropdown-divider"></div>`;
                    }

                    return ` <div class="input-group show">
                        <button class="btn btn-sm btn-info mr-1" data-toggle="tooltip" data-placement="bottom" title="Ссылка" onclick="LinkGenerate('${full.userId}')"><i class="fas fa-link"></i></button>
                        <button class="btn btn-sm btn-primary mr-1" data-toggle="tooltip" data-placement="bottom" title="Каталоги" onclick="NonVisCatalogs('${full.userId}')"><i class="fas fa-book-open"></i></button>
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true" title="Другие действия с пользователем"><i class="fas fa-edit"></i></button>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            ${update}
                            <a class="dropdown-item" href="#" onclick="RemoveLink('${full.userId}')"><i class="fas fa-trash"></i> Отвязать ссылки</a>
                        </div></div>`;
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    filterOnLoad();

    // Table filter applied to any input attribute with data-target-col="table_column_name"
    $("input[data-target-col]").on("keyup", onFilterInputChanged);
    $("select[data-target-col]").on("change", onFilterInputChanged);
    $("[data-datetime-picker]").on("change.datetimepicker", function(e) { applyFilter(); });

    function filterOnLoad() {
        $("[data-target-col]").each(function(index) {
            setColumnFilter(this.dataset.targetCol, this.value);
        });
        applyFilter();
    }

    function onFilterInputChanged(event) {
        setColumnFilter(this.dataset.targetCol, this.value);
        applyFilter();
    }

    function setColumnFilter(colName, value) {
        table
            .columns(`${colName}:name`)
            .search(value);
    }

    function applyFilter() {
        table.draw();
    }

    var targetRanges = getFilterRanges();
    function getFilterRanges() {
        var result = {};
        $("[data-target-range-col]").each(function(index) {
            var key = this.dataset.targetRangeCol;
            if (result[key] == undefined) {
                result[key] = {};
            }
            result[key][this.dataset.targetRangeType] = { item: this, colIndex: table.column(`${key}:name`).index()}
        });

        return result;
    }

    var keyFrom = "from";
    var keyTo = "to";
    $.fn.dataTable.ext.search.push(filterNew);
    function filterNew(settings, data, dataIndex, jsonData, index) {
        for (var key in targetRanges) {
            if (targetRanges.hasOwnProperty(key)) {
                var min = targetRanges[key][keyFrom].item.value;
                var max = targetRanges[key][keyTo].item.value;
                var colIndex = targetRanges[key][keyTo].colIndex;
                if (!filterDateColumnByRange(min, max, colIndex, data)) {
                    return false;
                }
            }
        }
        return true;
    }

    function filterDateColumnByRange(min, max, colIndex, data) {
        var colValue = moment(data[colIndex] || "1.01.0001 05:07", dateFormat);

        if ((min == "" && max == "")) {
            return true;
        } 
        
        if (min == "" && max != "") {
            return moment(colValue).isSameOrBefore(moment(max, dateFormat));
        }

        if (min != "" && max == "") {
            return moment(colValue).isSameOrAfter(moment(min, dateFormat));
        }

        if (moment(colValue).isSameOrBefore(moment(max, dateFormat)) &&
            moment(colValue).isSameOrAfter(moment(min, dateFormat))) {
            return true;
        }

        return false;
    }
});

function convertFormToQueryString(form) {
    var formData = new FormData(form);
    var data = [...formData.entries()];
    var queryString = data
        .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
        .join('&');
    return queryString;
}

function EditUser(userId) {
    var filterForm = $("#users-filter-form")[0];
    var query = convertFormToQueryString(filterForm);
    var encoded = encodeURIComponent(query);
    var newLocation = `/admin/identity/updateuser?userid=${userId}&filter=${encoded}`;
    location.href = newLocation;
}

function LinkGenerate(userId) {
    var url = "/Admin/Identity/CreateLink";
    $.ajax({
        type: "POST",
        url: url,
        data: { "userId": userId },
        success: function (data) {
            var linkModal = $("#LinkModal");

            var generatePasswordButton = linkModal.find(".generate-password-link-btn");
            generatePasswordButton.closest(".form-group").find("input[type='text']").val("");
            generatePasswordButton.attr("data-user-id", userId);
            generatePasswordButton.off();
            generatePasswordButton.on("click", generatePassword);
            
            var signInLinks = linkModal.find(".modal-body").find(".sign-in-links");
            signInLinks.html("");
            var linkContainer = linkModal.find(".doc-link").clone();

            var mainLink = linkContainer.clone();
            mainLink.find("label").html("Ссылка");
            mainLink.find("input[type='text']").val(data.link);
            mainLink.find("a[name='gotolink']").attr("href", data.link);
            mainLink.show();
            signInLinks.append(mainLink);

            for (var i = 0; i < data.docLinks.length; i++) {
                var linkData = data.docLinks[i];

                var container = linkContainer.clone();
                container.find("label").html(linkData.docName);
                container.find("input[type='text']").val(linkData.link);
                container.find("a[name='gotolink']").attr("href", linkData.link);
                container.show();

                signInLinks.append(container);
            }

            linkModal.modal("show");
        }
    });
};

$("#LinkModal").on("click", ".copy-link-btn", function() {
    var link = $(this).closest(".form-group").find("input[type='text']")[0];
    link.select();
    link.setSelectionRange(0, 99999);
    document.execCommand("copy");
});

function generatePassword(event) {
    var userId = this.dataset.userId;
    var url = "/Admin/Identity/CreateChangePasswordLink";
    var button = $(this);
    $.ajax({
        type: "POST",
        url: url,
        data: { "userId": userId },
        success: function (data, response, type) {
            button.closest(".form-group").find("input[type='text']").val(data.link);
        },
        error: function(jqXHR, exception) {
            showNotificationError("Не удалось сгенерировать ссылку для смены пароля");
        }
    });
}

function RemoveLink(userId) {
    Swal.fire({
        title: "Вы уверены?",
        text: "Отвязать все ссылки для входа от пользователя!",
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Отмена",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Да, отвязать!",
    }).then((result) => {
        if (result.value) {
            var url = "/Admin/Identity/RemoveLink";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    userId: userId
                },
                success: function (alert) {
                    showAlert(alert);
                }
            });
        }
    });
};

function NonVisCatalogs(userId) {
    var url = "/Admin/Identity/GetNonVisCatalogs";
    $.ajax({
        type: "GET",
        url: url,
        data: { "userId": userId },
        success: function (data) {
            $('#UserId').val(userId);
            $('#Catalogs').selectpicker('val', data);
            $("#VisCatalogsModal").modal("show");
        }
    });
};

$('#VisCatalogsModal').on('hidden.bs.modal', function (e) {
    $(this).find('form')[0].reset();
});

createSuccess = function (alert) {
    //showAlert(alert);
    table.ajax.reload(null, false);
    $("#VisCatalogsModal").modal("hide");
};
createError = function (alert) {
    //showAlert(alert);
    $("#VisCatalogsModal").modal("hide");
};

function ResetUserAllValues() {
    $('.card-body').find('.user-input:text').val('').trigger('keyup');
    $('.card-body').find('.selectpicker').val('').trigger('change');
    $("#lastSignInFromPicker").datetimepicker("clear");
    $("#lastSignInToPicker").datetimepicker("clear");
    table.draw();
}