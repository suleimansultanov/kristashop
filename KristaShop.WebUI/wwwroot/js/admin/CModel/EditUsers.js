﻿var table;
$(document).ready(function () {
    var nomId = $("#NomId").val();
    table = $('#users-table').DataTable({
        "ajax": {
            "url": "/Admin/CModel/LoadUsers",
            "type": "GET",
            "datatype": "json",
            "data": {
                "id": nomId
            },
            "dataSrc": ""
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    return count;
                }
            },
            { "data": "clientFullName" },
            { "data": "clientLogin" },
            { "data": "cityName" },
            { "data": "mallAddress" },
            { "data": "statusName" },
            { "data": "status", "visible": false },
            {
                "data": "userId",
                "targets": 0,
                "searchable": false,
                "orderable": false,
                "className": 'dt-body-center',
                "render": function (data, type, full, meta) {
                    if (full.notVisible)
                        return '<input type="checkbox" checked="checked" id="Clients" name="Clients" value="' + $('<div/>').text(data).html() + '">';
                    else
                        return '<input type="checkbox" id="Clients" name="Clients" value="' + $('<div/>').text(data).html() + '">';
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        "pageLength": 10,
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Все"]],
        "dom": 'rt<"row"<"bottom col-sm-12 col-md-4 mt-3 text-left"l><"bottom col-sm-12 col-md-4 text-center"i><"bottom col-sm-12 col-md-4 col-auto"p>>',
        'order': [[0, 'asc']],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    $("#Login").keyup(function () {
        table
            .columns(2)
            .search($(this).val());
        table.draw();
    });

    $("#ClientName").keyup(function () {
        table
            .columns(1)
            .search($(this).val());
        table.draw();
    });

    $("#CityName").keyup(function () {
        table
            .columns(3)
            .search($(this).val());
        table.draw();
    });

    $("#ShopName").keyup(function () {
        table
            .columns(4)
            .search($(this).val());
        table.draw();
    });

    $('#Status').change(function () {
        table
            .columns(6)
            .search($(this).val());
        table.draw();
    });

    //$('#example-select-all').on('click', function () {
    //    // Get all rows with search applied
    //    var rows = table.rows({ 'search': 'applied' }).nodes();
    //    // Check/uncheck checkboxes for all rows in the table
    //    $('input[type="checkbox"]', rows).prop('checked', this.checked);
    //});

    // Handle click on checkbox to set state of "Select all" control
    $('#users-table tbody').on('change', 'input[type="checkbox"]', function () {
        // If checkbox is not checked
        if (!this.checked) {
            var el = $('#example-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });
});

function ResetUserAllValues() {
    $('.card-body').find('.user-input:text').val('').trigger('keyup');
    $('.card-body').find('.selectpicker').val('').trigger('change');
}