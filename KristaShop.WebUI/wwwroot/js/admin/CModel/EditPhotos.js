﻿var tablePhoto;
$(document).ready(function () {
    var nomId = $("#NomId").val();
    tablePhoto = $('#photos-table').DataTable({
        "ajax": {
            "url": "/Admin/CModel/LoadPhotos",
            "type": "GET",
            "datatype": "json",
            "data": {
                "id": nomId
            },
            "dataSrc": ""
        },
        "processing": true,
        "rowReorder": {
            "dataSrc": 'order'
        },
        "columns": [
            { "data": "order", "className": 'reorder' },
            {
                "data": "photoPath",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<img src="' + data + '?width=100" width="100" alt="Alternate Text" />';
                    else
                        return '<img src="/Gallery/nophoto.png?width=100" width="100" alt="Alternate Text" />';
                }
            },
            { "data": "colorName" },
            {
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<button type="button" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Установить на главную" onclick="PhotoSetMain(\'' + full.nomId + '\', \'' + full.photoPath + '\')"><i class="fas fa-check"></i></button>' +
                        '<button type="button" class="btn btn-sm btn-info mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить" onclick="PhotoEdit(\'' + data + '\')"><i class="fas fa-edit"></i></button>' +
                        '<button type="button" onclick="ReorderModelModal(\'' + full.nomId + '\',\'' + data + '\')" data-toggle="tooltip" data-placement="bottom" title="Упорядочить" class="btn btn-sm btn-primary mr-2"><i class="fas fa-sort"></i></button>' +
                        '<button type="button" class="btn btn-sm btn-warning mr-2" data-toggle="tooltip" data-placement="bottom" title="Обрезать" onclick="PhotoResize(\'' + data + '\', \'' + full.oldPhotoPath + '\')"><i class="fas fa-cut"></i></button>' +
                        '<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Удалить" onclick="PhotoDelete(\'' + data + '\')"><i class="fas fa-trash"></i></button>';
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    tablePhoto.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = tablePhoto.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/CModel/UpdatePhotoRow',
                data: {
                    Id: rowData.id,
                    fromPosition: diff[i].oldData,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});

function PhotoResize(id, path) {
    $('#photo-resize-id').val(id);
    $("#resizingPhoto").attr("src", path);
    document.getElementById("result").innerHTML = "";
    $("#resize-photo-modal").modal("show");
}

var image = document.getElementById('resizingPhoto');
var cropBoxData;
var canvasData;
var cropper;
var result = document.getElementById('result');

$('#resize-photo-modal').on('shown.bs.modal', function () {
    cropper = new Cropper(image, {
        movable: false,
        zoomable: false,
        rotatable: false,
        scalable: false,
        viewMode: 2,
        aspectRatio: 2 / 3,
        autoCropArea: 1,
        ready: function () {
            //Should set crop box data first here
            cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
        }
    });
}).on('hidden.bs.modal', function () {
    $('#btnSaveCrop').attr("disabled", true);
    document.getElementById("result").innerHTML = "";
    cropBoxData = cropper.getCropBoxData();
    canvasData = cropper.getCanvasData();
    cropper.destroy();
});

$('#btnCrop').click(function () {
    result.innerHTML = '';
    result.appendChild(cropper.getCroppedCanvas({ maxWidth: 1200, maxHeight: 800, imageSmoothingEnabled: true, imageSmoothingQuality: 'medium' }));
    $('#btnSaveCrop').attr("disabled", false);
});
$(document).ready(function () {
    $('#btnSaveCrop').attr("disabled", true);
    $('#btnSaveCrop').click(function () {
        cropper.getCroppedCanvas({ maxWidth: 4096, maxHeight: 4096 }).toBlob(function (blob) {
            var id = $('#photo-resize-id').val();
            var formData = new FormData();
            formData.append('file', blob, 'cropped.png');
            formData.append('id', id);
            $.ajax('/Admin/CModel/ResizePhotoModel', {
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function (alert) {
                    showAlert(alert);
                    tablePhoto.ajax.reload(null, false);
                    $("#resize-photo-modal").modal("hide");
                },
                error: function () {
                    alert('Ошибка, произошел сбой системы.');
                }
            });
        }, 'image/jpeg', 1);
    });
});

function PhotoSetMain(nomId, path) {
    var url = "/Admin/CModel/AddMainPhoto";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            nomId: nomId,
            path: path
        },
        success: function (alert) {
            showAlert(alert);
            $("#Image-img").attr("src", path);
        }
    });
}

function PhotoEdit(id) {
    $('#PhotoId').val(id);
    $("#edit-photo-modal").modal("show");
}

function EditPhotoEvent() {
    var nomId = $("#NomId").val();
    var photoId = $('#PhotoId').val();
    var colorId = $('#ColorId').val();
    var url = "/Admin/CModel/EditPhoto";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            nomId: nomId,
            photoId: photoId,
            colorId: colorId
        },
        success: function (alert) {
            showAlert(alert);
            tablePhoto.ajax.reload(null, false);
            $("#edit-photo-modal").modal("hide");
        }
    });
}

function PhotoDelete(id) {
    Swal.fire({
        title: "Вы уверены?",
        text: "Выбранная запись будет удалена из системы!",
        icon: "warning",
        showCancelButton: true,
        cancelButtonText: "Отмена",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Да, удалить запись!",
    }).then((result) => {
        if (result.value) {
            var url = "/Admin/CModel/DeletePhoto";
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    photoId: id
                },
                success: function (alert) {
                    showAlert(alert);
                    tablePhoto.ajax.reload(null, false);
                }
            });
        }
    });
}

function ReorderModelModal(NomId, PhotoId) {
    $("#OrderNum").val("");
    $("#NomId").val(NomId);
    $("#PhotoId").val(PhotoId);
    $("#reorder-model-modal").modal("show");
};

function ReorderModel() {
    var id = $("#PhotoId").val();
    var nomId = $("#NomId").val();
    var orderNum = $("#OrderNum").val();
    $.ajax({
        type: 'POST',
        url: '/Admin/CModel/ReorderPhotoModel',
        data: {
            id: id,
            nomId: nomId,
            toPosition: orderNum
        },
        dataType: 'json',
        success: function (alert) {
            showAlert(alert);
            tablePhoto.ajax.reload(null, false);
            $("#reorder-model-modal").modal("hide");
        }
    });
};