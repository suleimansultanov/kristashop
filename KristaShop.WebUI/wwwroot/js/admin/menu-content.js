﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/MBody/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "rowReorder": {
            "dataSrc": 'order'
        },
        "columns": [
            {
                "data": "order",
                "className": 'reorder'

            },
            { "data": "title" },
            { "data": "url" },
            { "data": "layoutName" },
            {
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/MBody/Edit/' + full.id + '" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></a>' +
                        '<a href="/Admin/MBody/Delete/' + full.id + '" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></a>';
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    table.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = table.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/MBody/UpdateRow',
                data: {
                    id: rowData.id,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});
