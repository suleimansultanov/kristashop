﻿$(".nav-tabs #models-tab").click(function () {
    $.fn.dataTable.ext.errMode = "none";
    var hasLastError = true;
    var table = $('#nom-table')
        .on("error.dt", function ( e, settings, techNote, message) {
            if (hasLastError) {
                handleTableError({ e, settings, techNote, message });
            }
            hasLastError = true;
        })
        .on("xhr.dt", function ( e, settings, json, xhr ) {
            handleAjaxResponse({ e, settings, json, xhr });
            hasLastError = false;
        })
        .DataTable({
        "ajax": {
            "url": "/Admin/Discount/LoadNomenclatureModels",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "processing": true,
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    totalCount = count;
                    return count;
                }
            },
            {
                "data": "photoPath",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<img src="' + data + '?width=100" width="100" alt="Alternate Text" />';
                    else
                        return '<img src="/galleryphotos/nophoto.png?width=100" width="100" alt="Alternate Text" />';
                }
            },
            {
                "data": "articul",
                "width": "5%"
            },
            {
                "data": "itemName",
                "width": "10%"
            },
            {
                "data": "colors",
                "width": "15%",
                "render": function (data, type, full, meta) {
                    if (data) {
                        var htmlText = "";
                        data.forEach((element, index, array) => {
                            if (index === (array.length - 1)) {
                                htmlText += element;
                            } else {
                                htmlText += element + ", ";
                            }
                        });
                        return htmlText;
                    } else {
                        return "Отсутствует";
                    }
                }
            },
            {
                "data": "sizes",
                "width": "10%",
                "render": function (data, type, full, meta) {
                    if (data) {
                        var htmlText = "";
                        data.forEach((element, index, array) => {
                            if (index === (array.length - 1)) {
                                htmlText += element;
                            } else {
                                htmlText += element + ", ";
                            }
                        });
                        return htmlText;
                    } else {
                        return "Отсутствует";
                    }
                }
            },
            {
                "data": "catalogs",
                "defaultContent": "Отсутствует",
                "width": "15%",
                "render": function (data, type, full, meta) {
                    if (data) {
                        var htmlText = "";
                        data.forEach((element, index, array) => {
                            if (index === (array.length - 1)) {
                                htmlText += element;
                            } else {
                                htmlText += element + ", ";
                            }
                        });
                        return htmlText;
                    } else {
                        return "Отсутствует";
                    }
                }
            },
            {
                "data": "categories",
                "defaultContent": "Отсутствует",
                "width": "15%",
                "render": function (data, type, full, meta) {
                    if (data) {
                        var htmlText = "";
                        data.forEach((element, index, array) => {
                            if (index === (array.length - 1)) {
                                htmlText += element;
                            } else {
                                htmlText += element + ", ";
                            }
                        });
                        return htmlText;
                    } else {
                        return "Отсутствует";
                    }
                }
            },
            {
                "data": "isVisible",
                "visible": false
            },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            { "data": "discountPrice" },
            {
                "data": "id",
                "searchable": false,
                "sortable": false,
                "width": "10%",
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/Discount/IndexType?parentId=' + data + '&discountType=1" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Скидка"><i class="fas fa-percent"></i></a>';
                }
            }
        ],
        "destroy": true,
        "pageLength": -1,
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Все"]],
        "dom": 'rt<"row"<"bottom col-sm-12 col-md-4 mt-3 text-left"l><"bottom col-sm-12 col-md-4 text-center"i><"bottom col-sm-12 col-md-4 col-auto"p>>',
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    function handleTableError(error) {
        console.error(error.message);
        showTableError();
    }

    function handleAjaxResponse(error) {
        if (error.xhr.status >= 400) {
            showAlert(error.xhr.responseJSON);
            setEmptyTableErrorTitle();
        } else if (error.xhr.status >= 500) {
            showTableError();
        }
    }

    function showTableError() {
        showGenericNotificationError();
        setEmptyTableErrorTitle();
    }

    function setEmptyTableErrorTitle() {
        $(".dataTables_empty").html("Не удалось загрузить записи.");
    }

    $("#Articul").keyup(function () {
        table
            .columns(2)
            .search($(this).val());
        table.draw();
    });

    $('#Color').change(function () {
        table
            .columns(4)
            .search($(this).val());
        table.draw();
    });

    $('#Size').change(function () {
        table
            .columns(5)
            .search($(this).val());
        table.draw();
    });

    $('#SizeLine').change(function () {
        table
            .columns(5)
            .search($(this).val());
        table.draw();
    });

    $('#Catalog').change(function () {
        table
            .columns(6)
            .search($(this).val());
        table.draw();
    });

    $('#Category').change(function () {
        table
            .columns(7)
            .search($(this).val());
        table.draw();
    });

    $('#Visibility').change(function () {
        table
            .columns(8)
            .search($(this).val());
        table.draw();
    });
});
function ResetAllValues() {
    $('.card-body').find('input:text').val('').trigger('keyup');
    $('.card-body').find('.selectpicker').val('').trigger('change');
}