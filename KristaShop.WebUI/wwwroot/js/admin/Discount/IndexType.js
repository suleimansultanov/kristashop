﻿var table;
var dateFormat = "D.MM.YYYY HH:mm";
$(document).ready(function () {
    var id = $('#ParentId').val();
    var type = $('#Type').val();
    $.fn.dataTable.ext.errMode = "none";
    var hasLastError = true;
    table = $('.table')
        .on("error.dt", function ( e, settings, techNote, message) {
            if (hasLastError) {
                handleTableError({ e, settings, techNote, message });
            }
            hasLastError = true;
        })
        .on("xhr.dt", function ( e, settings, json, xhr ) {
            handleAjaxResponse({ e, settings, json, xhr });
            hasLastError = false;
        })
        .DataTable({
        "ajax": {
            "url": "/Admin/Discount/LoadDataType",
            "data": {
                parentId: id,
                discountType: type
            },
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    totalCount = count;
                    return count;
                }
            },
            { "data": "discountPrice" },
            {
                "data": "isActive",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-toggle-on text-success"></i>';
                    else
                        return '<i class="fa fa-toggle-off text-danger"></i>';
                }
            },
            {
                "data": "startDate",
                "render": function(data, type, full, meta) {
                    var date = moment(data).format(dateFormat);
                    if (moment(moment(date, dateFormat)).isSameOrBefore(moment("1.01.0001 05:07", dateFormat))) {
                        date = "";
                    }
                    return date;
                }
            },
            {
                "data": "endDate",
                "render": function(data, type, full, meta) {
                    var date = moment(data).format(dateFormat);
                    if (moment(moment(date, dateFormat)).isSameOrBefore(moment("1.01.0001 05:07", dateFormat))) {
                        date = "";
                    }
                    return date;
                }
            },
            {
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return `<button class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить" onclick="Edit('${data}','${full.type}', '${full.parentId}')"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Удалить" onclick="Delete('${data}','${full.type}')"><i class="fas fa-trash"></i></button>`;
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    function handleTableError(error) {
        console.error(error.message);
        showTableError();
    }

    function handleAjaxResponse(error) {
        if (error.xhr.status >= 400) {
            showAlert(error.xhr.responseJSON);
            setEmptyTableErrorTitle();
        } else if (error.xhr.status >= 500) {
            showTableError();
        }
    }

    function showTableError() {
        showGenericNotificationError();
        setEmptyTableErrorTitle();
    }

    function setEmptyTableErrorTitle() {
        $(".dataTables_empty").html("Не удалось загрузить записи.");
    }
});


$('#create-edit-modal').on('hidden.bs.modal', function (e) {
    $('#Id').val("");
    $(this).find('form')[0].reset();
});
createOrEditSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#create-edit-modal").modal("hide");
};
createOrEditError = function (alert) {
    showAlert(alert.responseJSON);
    $("#create-edit-modal").modal("hide");
};


function Create(type, parentId) {
    var url = "/Admin/Discount/Create";
    $.ajax({
        type: "GET",
        url: url,
        data: {
            entityType: type,
            parentId: parentId
        },
        success: function (data) {
            $("#Type").val(type);
            var options;
            $.each(data.catalogs, function(index, object) {
                var isSelected = false;
                if (data.catalogIds != null && data.catalogIds.length > 0) {
                    var filtered = data.catalogIds.filter(x => x === object.key);
                    isSelected = filtered.length > 0;
                }

                options += `<option value="${object.key}" ${isSelected ? "selected" : ""}>${object.value}</option>`;
            });
            $("#CatalogIds").html(options).selectpicker("refresh");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            showAlert(jqXHR.responseJSON);
        }
    });
}

function Edit(id, type, parentId) {
    var url = "/Admin/Discount/Details";
    $.ajax({
        type: "GET",
        url: url,
        data: {
            entityId: id,
            entityType: type,
            parentId: parentId
        },
        success: function (data) {
            $("#Id").val(data.id);
            $("#Type").val(type);
            $("#IsActive").prop("checked", data.isActive);
            $("#DiscountPrice").val(data.discountPrice);
            $("#StartDate").val(moment(data.startDate).format(dateFormat));
            $("#EndDate").val(moment(data.endDate).format(dateFormat));
            $("#create-edit-modal").modal("show");

            var options;
            $.each(data.catalogs, function(index, object) {
                var isSelected = false;
                if (data.catalogIds != null && data.catalogIds.length > 0) {
                    var filtered = data.catalogIds.filter(x => x === object.key);
                    isSelected = filtered.length > 0;
                }

                options += `<option value="${object.key}" ${isSelected ? "selected" : ""}>${object.value}</option>`;
            });
            $("#CatalogIds").html(options).selectpicker("refresh");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            showAlert(jqXHR.responseJSON);
        }
    });
}

function Delete(id, type) {
    $('#EntityId').val(id);
    $('#EntityType').val(type);
    $("#delete-modal").modal("show");
}
deleteSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#delete-modal").modal("hide");
};

deleteError = function (response) {
    $("#delete-modal").modal("hide");
    showAlert(response.responseJSON);
};