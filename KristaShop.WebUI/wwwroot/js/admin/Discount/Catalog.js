﻿$(document).ready(function () {
    $.fn.dataTable.ext.errMode = "none";
    var hasLastError = true;
    var table = $("#catalog-table")
        .on("error.dt", function ( e, settings, techNote, message) {
            if (hasLastError) {
                handleTableError({ e, settings, techNote, message });
            }
            hasLastError = true;
        })
        .on("xhr.dt", function ( e, settings, json, xhr ) {
            handleAjaxResponse({ e, settings, json, xhr });
            hasLastError = false;
        })
        .DataTable({
        "ajax": {
            "url": "/Admin/Discount/LoadCatalogs",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "processing": true,
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    totalCount = count;
                    return count;
                }
            },
            { "data": "name" },
            { "data": "uri" },
            { "data": "orderFormName" },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            { "data": "discountPrice" },
            {
                "data": "id",
                "searchable": false,
                "width": "10%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/Discount/IndexType?parentId=' + data + '&discountType=0" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Скидка"><i class="fas fa-percent"></i></a>';
                }
            }
        ],
        "pageLength": -1,
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Все"]],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    function handleTableError(error) {
        console.error(error.message);
        showTableError();
    }

    function handleAjaxResponse(error) {
        if (error.xhr.status >= 400) {
            showAlert(error.xhr.responseJSON);
            setEmptyTableErrorTitle();
        } else if (error.xhr.status >= 500) {
            showTableError();
        }
    }

    function showTableError() {
        showGenericNotificationError();
        setEmptyTableErrorTitle();
    }

    function setEmptyTableErrorTitle() {
        $(".dataTables_empty").html("Не удалось загрузить записи.");
    }
});