﻿var table;
$(document).ready(function() {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Cart/LoadCartsReport",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "processing": true,
        "columns": [
            {
                "render": function(data, type, full, meta) {
                    var count = meta.row;
                    count = count + 1;
                    return count;
                }
            },
            { "data": "articul" },
            { "data": "colorName" },
            { "data": "size" },
            { "data": "amount" },
            { "data": "totalAmount" },
            { "data": "price" },
            { "data": "priceRu" },
            { "data": "discount" },
            { "data": "discountRu" },
            { "data": "totalPrice" },
            { "data": "totalPriceRu" }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });
});