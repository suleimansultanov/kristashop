﻿var table;
$(document).ready(function () {
    var galleryId = $("#GalleryId").val();
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Video/LoadData",
            "type": "GET",
            "datatype": "json",
            "data": {
                galleryId: galleryId
            },
            "dataSrc": ""
        },
        "rowReorder": {
            "dataSrc": "order"
        },
        "columns": [
            { "data": "order", "className": "reorder" },
            {
                "data": "previewPath",
                "width": "5%",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<img src="' + data + '?width=100" width="100"/>';
                    else
                        return '<img src="/galleryphotos/nophoto.png?width=100" width="100"/>';
                }
            },
            { "data": "title" },
            { "data": "description" },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            {
                "data": "id",
                "searchable": false,
                "width": "20%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return `<a href="/Admin/Video/Edit/${data}?galleryId=${galleryId}" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></a>
                        <a href="/Admin/Video/Delete/${data}?galleryId=${galleryId}" class="btn btn-sm btn-danger mr-2" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></a>`;
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    table.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = table.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/Video/UpdateRow',
                data: {
                    videoId: rowData.id,
                    galleryId: galleryId,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});