﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Banner/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "rowReorder": {
            "dataSrc": 'order'
        },
        "columns": [
            { "data": "order", "className": 'reorder' },
            {
                "data": "imagePath",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<img src="' + data + '?width=100" width="100" alt="Alternate Text" />';
                    else
                        return '<img src="/galleryphotos/nophoto.png?width=100" width="100" alt="Alternate Text" />';
                }
            },
            { "data": "title" },
            { "data": "description" },
            { "data": "link" },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            {
                "data": "id",
                "searchable": false,
                "width": "20%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/Banner/Edit/' + data + '" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></a>' +
                        '<a href="/Admin/Banner/Delete/' + data + '" class="btn btn-sm btn-danger mr-2" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></a>';
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    table.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = table.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/Banner/UpdateRow',
                data: {
                    id: rowData.id,
                    fromPosition: diff[i].oldData,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});