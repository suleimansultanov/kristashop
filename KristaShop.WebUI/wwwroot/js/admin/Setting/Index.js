﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Settings/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    totalCount = count;
                    return count;
                }
            },
            { "data": "key" },
            { "data": "value" },
            { "data": "description" },
            {
                "data": "id",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    var result = '<button class="btn btn-sm btn-success mr-2" onclick="Edit(\'' + data + '\')" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></button>';
                    if ($("#delete-modal")[0]) {
                        result += '<button class="btn btn-sm btn-danger" onclick="Delete(\'' + data + '\')" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></button>';
                    }
                    return result;
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });
});


$('#create-modal').on('hidden.bs.modal', function (e) {
    $(this).find('form')[0].reset();
});
createSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#create-modal").modal("hide");
};
createError = function (alert) {
    showAlert(alert.responseJSON);
    $("#create-modal").modal("hide");
};


function Edit(id) {
    var url = "/Admin/Settings/Details?id=" + id;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            $('#EditId').val(data.id);
            $('#EditKey').val(data.key);
            $('#EditValue').val(data.value);
            $('#EditDescription').val(data.description);
            $("#edit-modal").modal("show");
        }
    });
}
editSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#edit-modal").modal("hide");
};
editError = function (alert) {
    showAlert(alert.responseJSON);
    $("#edit-modal").modal("hide");
};


function Delete(id) {
    $('#Id').val(id);
    $("#delete-modal").modal("show");
}
deleteSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#delete-modal").modal("hide");
};
