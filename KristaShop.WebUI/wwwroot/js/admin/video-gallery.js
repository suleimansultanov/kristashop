﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/VideoGallery/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "rowReorder": {
            "dataSrc": "order"
        },
        "columns": [
            { "data": "order", "className": "reorder", "width": "2%"},
            { "data": "title" },
            { "data": "slug" },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            {
                "data": "id",
                "searchable": false,
                "width": "20%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/VideoGallery/Edit/' + data + '" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></a>' +
                        '<a href="/Admin/VideoGallery/Delete/' + data + '" class="btn btn-sm btn-danger mr-2" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></a>' +
                        '<a href="/Admin/Video/Index?galleryId=' + data + '" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Видео"><i class="fas fa-video"></i> <span class="badge badge-light">' + full.videosCount + '</span></a>';
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    table.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = table.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/VideoGallery/UpdateRow',
                data: {
                    id: rowData.id,
                    fromPosition: diff[i].oldData,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});