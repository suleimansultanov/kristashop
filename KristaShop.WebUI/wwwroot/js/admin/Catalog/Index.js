﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Admin/Catalog/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "rowReorder": {
            "dataSrc": 'order'
        },
        "columns": [
            { "data": "order", "className": 'reorder' },
            { "data": "name" },
            { "data": "uri" },
            { "data": "orderFormName" },
            {
                "data": "isVisible",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    if (data)
                        return '<i class="fa fa-eye text-success"></i>';
                    else
                        return '<i class="fa fa-eye text-danger"></i>';
                }
            },
            {
                "data": "id",
                "searchable": false,
                "width": "20%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<button type="button" onclick="AddCatalogUsers(\'' + data + '\')" class="btn btn-sm btn-secondary mr-2">Скрыть для всех</button>' +
                        '<button type="button" onclick="RemoveCatalogUsers(\'' + data + '\')" class="btn btn-sm btn-success">Показать всем</button>';
                }
            },
            {
                "data": "id",
                "searchable": false,
                "width": "20%",
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<a href="/Admin/Catalog/Edit/' + data + '" class="btn btn-sm btn-success mr-2" data-toggle="tooltip" data-placement="bottom" title="Изменить"><i class="fas fa-edit"></i></a>' +
                        '<a href="/Admin/Catalog/Delete/' + data + '" class="btn btn-sm btn-danger mr-2" data-toggle="tooltip" data-placement="bottom" title="Удалить"><i class="fas fa-trash"></i></a>' +
                        '<a href="/Admin/CModel/IndexByCatalog/' + data + '" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Модели"><i class="fas fa-eye"></i> <span class="badge badge-light">' + full.nomCount + '</span></a>';
                }
            }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });

    table.on('row-reorder.dt', function (e, diff, edit) {
        for (var i = 0; i < diff.length; i++) {
            var rowData = table.row(diff[i].node).data();
            $.ajax({
                type: "POST",
                url: '/Admin/Catalog/UpdateRow',
                data: {
                    id: rowData.id,
                    fromPosition: diff[i].oldData,
                    toPosition: diff[i].newData
                },
                dataType: "json"
            });
        }
    });
});

function AddCatalogUsers(ctlgId) {
    var url = "/Admin/Catalog/AddNonVisUsers";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            ctlgId: ctlgId
        },
        success: function (alert) {
            showAlert(alert);
            table.ajax.reload(null, false);
        }
    });
}

function RemoveCatalogUsers(ctlgId) {
    var url = "/Admin/Catalog/RemoveNonVisUsers";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            ctlgId: ctlgId
        },
        success: function (alert) {
            showAlert(alert);
            table.ajax.reload(null, false);
        }
    });
}