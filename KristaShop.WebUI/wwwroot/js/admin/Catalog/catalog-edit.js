﻿$(function() {
    $("[name='OrderForm']").on("change", function(event) { updateTimerField(); });

    updateTimerField();

    function updateTimerField() {
        var closeTime = $(".close-time");
        var orderForm = +$("[name='OrderForm']").val();
        if (orderForm === 2) {
             closeTime.show();
        } else {
            closeTime.hide();
        }
    }
});