﻿/*
 *
 *  For this script to work input and image placeholder should be in container with class "image-upload"
 *  File input should have data attributes:
 *  data-img-uploader - makes script to work, it's value should be image container selector (for example data-img-uploader="#img")
 *
 */

$(function() {
    var input = $(document).find("input[data-img-uploader]");
    input.on("change", function(event) { getImageByUrl(this); });

    function getImageByUrl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $(input).closest(".image-upload").find(input.dataset.imgUploader);
                img.attr("src", e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

    // old version

    $("#Image").change(function () { readURL(this); });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#Image-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    };
});
