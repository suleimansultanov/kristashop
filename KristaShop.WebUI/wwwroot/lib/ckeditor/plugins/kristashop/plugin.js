﻿CKEDITOR.plugins.add( 'kristashop', {
    lang:["ru", "en"],
    init: function( editor ) {

        editor.addContentsCss("/css/min/ckeditor-custom.min.css");
        editor.addContentsCss("/lib/twitter-bootstrap/css/bootstrap.min.css");
        editor.addContentsCss("/lib/parallax.js-1.5.0/parallax.min.js");

        editor.addCommand("contentWithFullWidthBgImgDialog",new CKEDITOR.dialogCommand("contentWithFullWidthBackgroundImageDialog"));
        CKEDITOR.dialog.add("contentWithFullWidthBackgroundImageDialog", `${this.path}dialog/content-fullwidth-bg-image-dialog.js`);

        editor.addCommand("contentWithImgDialog",new CKEDITOR.dialogCommand("contentWithImageDialog"));
        CKEDITOR.dialog.add("contentWithImageDialog", `${this.path}dialog/content-with-image-dialog.js`);

        var socialsContainer = initSocialsContainer(editor);
        var personContainer = initPersonContainer(editor);
        var gradientBackground = initGradientBackground(editor);
        var fullWidthBgImage = initFullWidthBgImage(editor);
        var containerWithImage = initContainerWidthImage(editor);
        var gradientText = initGradientText(editor);

        editor.addCommand(socialsContainer.name, { exec: function( editor ) { socialsContainer.execute(editor); } });
        editor.addCommand(personContainer.name, { exec: function( editor ) { personContainer.execute(editor); } });
        editor.addCommand(gradientBackground.name, { exec: function( editor ) { gradientBackground.execute(editor); } });
        editor.addCommand(fullWidthBgImage.name, { exec: function(editor) { fullWidthBgImage.execute(editor); } });
        editor.addCommand(containerWithImage.name, { exec: function(editor) { containerWithImage.execute(editor); } });
        editor.addCommand(gradientText.name, { exec: function(editor) { gradientText.execute(editor); } });

        editor.ui.addRichCombo("KristaStyles",
            {
                label: editor.lang.kristashop.kristaStyles,
                title: editor.lang.kristashop.kristaStylesTitle,
                toolbar: "insert",
                panel: {
                    css: [CKEDITOR.skin.getPath("editor")].concat(editor.config.contentsCss),
                    multiSelect: false,
                    attributes: { 'aria-label': editor.lang.kristashop.kristaStyles }
                },
                init: function(e) {
                    this.startGroup(editor.lang.kristashop.containers);
                    this.add(socialsContainer.name, socialsContainer.labelHtml, socialsContainer.title);
                    this.add(personContainer.name, personContainer.labelHtml, personContainer.title);
                    this.add(gradientBackground.name, gradientBackground.labelHtml, gradientBackground.title);
                    //this.add(fullWidthBgImage.name, fullWidthBgImage.labelHtml, fullWidthBgImage.title);
                    this.add(containerWithImage.name, containerWithImage.labelHtml, containerWithImage.title);

                    this.startGroup(editor.lang.kristashop.textStyles);
                    this.add(gradientText.name, gradientText.labelHtml, gradientText.title);
                },
                onClick: function(value) {
                    editor.focus();
                    editor.fire("saveSnapshot"); // for undo

                    editor.execCommand(value);

                    editor.fire("saveSnapshot");
                }
            });
    }
});

function initSocialsContainer(editor) {
    return  {
        name : "socialsContainer",
        labelHtml: editor.lang.kristashop.socials,
        title: editor.lang.kristashop.socialsCardTitle,
        execute: function(editor) {
            var html = $(".social-networks-wrapper").html();
            editor.insertHtml( html );
        }
    }
}

function initPersonContainer(editor) {
    return {
        name: "personContainer",
        labelHtml: editor.lang.kristashop.managerCard,
        title: editor.lang.kristashop.managerCard,
        execute: function(editor) {
            var html = $(".person-card-wrapper").html();
            editor.insertHtml(html);
        }
    }
}

function initGradientBackground(editor) {
    return {
        name: "gradientBackground",
        labelHtml: editor.lang.kristashop.gradientBg,
        title: editor.lang.kristashop.gradientBg.Title,
        execute: function(editor) {
            editor.insertHtml("<div class='bg-gradient-revert'></div>");
        }
    }
}

function initFullWidthBgImage(editor) {
    return {
        name: "fullWidthImage",
        labelHtml: editor.lang.kristashop.containerWithBgImage,
        title: editor.lang.kristashop.containerWithBgImageTitle,
        execute: function(editor) {
            editor.execCommand('contentWithFullWidthBgImgDialog');
        }
    }
}

function initContainerWidthImage(editor) {
    return {
        name: "fullWidthImage",
        labelHtml: editor.lang.kristashop.containerWithImage,
        title: editor.lang.kristashop.containerWithImageTitle,
        execute: function(editor) {
            editor.execCommand('contentWithImgDialog');
        }
    }
}

function initGradientText(editor) {
    return {
        name: "gradientText",
        labelHtml: editor.lang.kristashop.gradientText,
        title: editor.lang.kristashop.gradientTextTitle,
        execute: function(editor) {
            var element = editor.getSelection().getCommonAncestor();
            var parent = element.getAscendant(function(parentElement) {
                if (parentElement.type === 1) {
                    return parentElement.hasClass("text-gradient-main");
                }
                return false;
            });

            if (parent != null) {
                parent.removeClass("text-gradient-main");
            } else {
                var selectedText = editor.getSelection().getSelectedText();
                var newElement = new CKEDITOR.dom.element("span");
                newElement.addClass("text-gradient-main");
                newElement.setText(selectedText);
                editor.insertElement(newElement);
            }
        }
    }
}