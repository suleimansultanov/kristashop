﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;
using KristaShop.WebUI.Infrastructure;

namespace KristaShop.WebUI.Areas.Admin.Data
{
    public static class FileUpload
    {
        public static async Task<string> FilePathAsync(IFormFile file, GlobalSettings settings)
        {
            const string path = "/galleryphotos/";
            string imagePath = path + Guid.NewGuid() + Path.GetExtension(file.FileName);
            if (!Directory.Exists(settings.FilesDirectoryPath + path))
                Directory.CreateDirectory(settings.FilesDirectoryPath + path);

            await using (var fileStream = new FileStream(settings.FilesDirectoryPath + imagePath, FileMode.OpenOrCreate))
            {
                await file.CopyToAsync(fileStream);
            }
            return imagePath;
        }
    }
}