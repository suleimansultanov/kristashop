﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.WebUI.Areas.Admin.Models;

namespace KristaShop.WebUI.Areas.Admin.Data
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SettingEditViewModel, SettingsDTO>();
            CreateMap<SettingsDTO, SettingEditViewModel>();

            CreateMap<MenuContentDTO, MenuContentViewModel>()
                .ForMember(x => x.LayoutName, opt => opt.MapFrom(src => MenuContentLayout.TryGetLayoutTitle(src.Layout)));

            CreateMap<MenuContentViewModel, MenuContentDTO>()
                .ForMember(x => x.TitleIcon, opt => opt.Ignore());

            CreateMap<MenuItemViewModel, MenuItemDTO>();
            CreateMap<MenuItemDTO, MenuItemViewModel>();

            CreateMap<UrlAccessViewModel, UrlAccessDTO>();
            CreateMap<UrlAccessDTO, UrlAccessViewModel>();

            CreateMap<CategoryViewModel, CategoryDTO>();
            CreateMap<CategoryDTO, CategoryViewModel>();

            CreateMap<CatalogViewModel, CatalogDTO>()
                .ForMember(x => x.Preview, opt => opt.Ignore());
            CreateMap<CatalogDTO, CatalogViewModel>();

            CreateMap<CModelDTO, CModelViewModel>();

            CreateMap<NomViewModel, NomModelDTO>();
            CreateMap<NomModelDTO, NomViewModel>();

            CreateMap<DiscountViewModel, DiscountDTO>();
            CreateMap<DiscountDTO, DiscountViewModel>();

            CreateMap<BlogItemViewModel, BlogItemDTO>();
            CreateMap<BlogItemDTO, BlogItemViewModel>();

            CreateMap<GalleryItemViewModel, GalleryItemDTO>();
            CreateMap<GalleryItemDTO, GalleryItemViewModel>();

            CreateMap<BannerItemViewModel, BannerItemDTO>();
            CreateMap<BannerItemDTO, BannerItemViewModel>();

            CreateMap<FeedbackFileDTO, FileViewModel>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Filename, dest => dest.MapFrom(item => item.Filename));

            CreateMap<VideoDTO, VideoViewModel>();
            CreateMap<VideoViewModel, VideoDTO>()
                .ForMember(x => x.Preview, opt => opt.Ignore());

            CreateMap<VideoGalleryDTO, VideoGalleryViewModel>();
            CreateMap<VideoGalleryViewModel, VideoGalleryDTO>()
                .ForMember(x => x.Preview, opt => opt.Ignore());
        }
    }
}