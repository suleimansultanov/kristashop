﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Areas.Admin.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PermissionFilter : Attribute, IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            IUrlAclService urlAclService = context.HttpContext.RequestServices.GetRequiredService<IUrlAclService>();
            ICurrentUserService userService = context.HttpContext.RequestServices.GetRequiredService<ICurrentUserService>();

            var loginRoute = new RouteValueDictionary(new { controller = "Identity", action = "Login" });
            var forbiddenRoute = new RouteValueDictionary(new { controller = "Error", action = "Error403" });
            var notFoundRoute = new RouteValueDictionary(new { controller = "Error", action = "Error404" });

            string areaName = context.RouteData.Values["area"].ToString();
            string controllerName = context.RouteData.Values["controller"].ToString();
            string actionName = context.RouteData.Values["action"].ToString();

            var user = userService.Get();
            if (user == null)
            {
                context.Result = new RedirectToRouteResult(loginRoute);
                return;
            }
            else if (user.IsRoot)
            {
                return;
            }

            var urlAcls = (await urlAclService.GetUrlAcls())
                .Where(x => UrlHelper.CompareUrls(x.URL, UrlHelper.GetURL($"{areaName}/{controllerName}", actionName)))
                .OrderBy(x => x.Acl)
                .ToList();
            var urlAcl = urlAcls.FirstOrDefault();
            if (urlAcl == null)
            {
                context.Result = new RedirectToRouteResult(notFoundRoute);
                return;
            }
            var deniedGroups = urlAcl.DeniedGroupsJson;
            bool isDenied = deniedGroups.Any(dg => user.UserGroups.Any(ug => ug.GroupId == dg));
            if (isDenied)
            {
                context.Result = new RedirectToRouteResult(forbiddenRoute);
                return;
            }

            var allowedGroups = urlAcl.AccessGroupsJson;
            bool isAllowed = urlAcl.Acl <= user.AccessLevel
                || allowedGroups.Any(dg => user.UserGroups.Any(ug => ug.GroupId == dg));
            if (isAllowed)
            {
                return;
            }
            else
            {
                context.Result = new RedirectToRouteResult(forbiddenRoute);
                return;
            }
        }
    }
}