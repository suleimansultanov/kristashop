﻿using System;
using System.ComponentModel.DataAnnotations;
using KristaShop.Common.Enums;

namespace KristaShop.WebUI.Areas.Admin.Models.Filters {
    public class UsersFilter {
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Display(Name = "ФИО")]
        public string Person { get; set; }

        [Display(Name = "Город")]
        public string CityName { get; set; }

        [Display(Name = "Номер телефона")]
        public string Phone { get; set; }

        [Display(Name = "Адрес ТЦ")]
        public string MallAddress { get; set; }

        [Display(Name = "Статус")]
        public UserStatus Status { get; set; }

        [Display(Name = "Статус корзины")]
        public string CartStatus { get; set; }

        [Display(Name = "Последний вход от")]
        public DateTime? LastSignInFrom { get; set; }

        [Display(Name = "Последний вход до")]
        public DateTime? LastSignInTo { get; set; }

        public UsersFilter() {
            Status = UserStatus.None;
        }
    }
}