﻿namespace KristaShop.WebUI.Areas.Admin.Models {
    public class FileViewModel {
        public string Id { get; set; }
        public string Filename { get; set; }
    }
}
