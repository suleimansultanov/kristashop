﻿using System;

namespace KristaShop.WebUI.Areas.Admin.Models
{
    public class CModelViewModel
    {
        public Guid Id { get; set; }
        public string Articul { get; set; }
        public string ItemName { get; set; }
        public bool IsSet { get; set; }
    }
}