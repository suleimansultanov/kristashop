﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KristaShop.Common.Enums;
using KristaShop.Common.Models;

namespace KristaShop.WebUI.Areas.Admin.Models {
    public class DiscountViewModel {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public DiscountType Type { get; set; }

        [Display(Name = "Скидка в $")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public double DiscountPrice { get; set; }

        [Display(Name = "Активность")]
        public bool IsActive { get; set; }

        [Display(Name = "Действует от")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Действует до")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Каталоги")]
        public List<Guid> CatalogIds { get; set; }

        public List<LookUpItem<Guid, string>> Catalogs { get; set; }

        public DiscountViewModel() { }

        public DiscountViewModel(Guid parentId, DiscountType type) {
            ParentId = parentId;
            Type = type;
        }
    }
}