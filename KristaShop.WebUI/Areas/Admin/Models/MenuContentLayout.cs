﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KristaShop.WebUI.Areas.Admin.Models {
    public class MenuContentLayout {
        public string Name { get; set; }
        public string Title { get; set; }
        public bool IsDefault { get; set; }

        public MenuContentLayout(string name, string title, bool isDefault) {
            Name = name;
            Title = title;
            IsDefault = isDefault;
        }

        private static  Dictionary<string, MenuContentLayout> _layouts;
        public static Dictionary<string, MenuContentLayout> Layouts {
            get => _layouts ??= GetLayouts().ToDictionary(k => k.Name, v => v);
            protected set => _layouts = value;
        }

        public static List<MenuContentLayout> GetLayouts() {
            return new List<MenuContentLayout> {
                new MenuContentLayout("_ContentContainerLayout", "С ограничением по ширине страницы", true),
                new MenuContentLayout("_NoContainerLayout", "Без ограничений по ширине страницы", false)
            };
        }

        public static string TryGetLayoutTitle(string name) {
            try {
                if (Layouts.ContainsKey(name)) {
                    return Layouts[name].Title;
                }
            } catch (Exception) {
                return string.Empty;
            }

            return string.Empty;
        }
    }
}