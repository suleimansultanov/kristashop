﻿namespace KristaShop.WebUI.Areas.Admin.Models {
    public class TopMenuDashboardViewModel {
        public int NewUsers { get; set; }
        public int NewOrders { get; set; }

        public TopMenuDashboardViewModel(int newUsers, int newOrders) {
            NewUsers = newUsers;
            NewOrders = newOrders;
        }
    }
}
