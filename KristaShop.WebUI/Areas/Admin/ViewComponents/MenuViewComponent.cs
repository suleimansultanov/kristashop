﻿using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace KristaShop.WebUI.Areas.Admin.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IMenuService _menuService;
        private readonly IUrlAclService _urlAclService;
        private readonly ICurrentUserService _userService;

        public MenuViewComponent
            (IMenuService menuService, IUrlAclService urlAclService, ICurrentUserService userService)
        {
            _menuService = menuService;
            _urlAclService = urlAclService;
            _userService = userService;
        }

        public IViewComponentResult Invoke(MenuType menuType)
        {
            UserSession user = _userService.Get();
            List<MenuItemDTO> menuItems = _menuService.GetMenusByType(menuType);
            if (!user.IsRoot)
            {
                var urls = _urlAclService.GetUrlAclsByAcl(user.AccessLevel).Select(x => x.URL).ToList();
                menuItems = menuItems
                    .Where(x => urls.Any(u => UrlHelper.CompareUrls(u, UrlHelper.GetURL(x.ControllerName, x.ActionName)))).ToList();
            }
            return View(menuItems.OrderBy(x => x.Order).ToList());
        }
    }
}