﻿using KristaShop.Business.Interfaces;
using KristaShop.DataReadOnly.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace KristaShop.WebUI.Areas.Admin.ViewComponents
{
    public class SearchModelViewComponent : ViewComponent
    {
        private readonly IDictionaryService _dictService;
        private readonly ICatalogService _catalogService;
        private readonly ICategoryService _categoryService;

        public SearchModelViewComponent
            (IDictionaryService dictService, ICatalogService catalogService, ICategoryService categoryService)
        {
            _dictService = dictService;
            _catalogService = catalogService;
            _categoryService = categoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.Colors = new SelectList(_dictService.GetColors(), "Name", "Name");
            ViewBag.Sizes = new SelectList(_dictService.GetSizes(), "Name", "Name");
            ViewBag.SizeLines = new SelectList(_dictService.GetSizeLines(), "Name", "Name");
            ViewBag.Catalogs = new SelectList(await _catalogService.GetCatalogs(), "Name", "Name");
            ViewBag.Categories = new SelectList(await _categoryService.GetCategories(), "Name", "Name");
            return View();
        }
    }
}
