﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.Extensions.Options;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("ckeditor")]
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    public class CkeditorController : Controller {
        private readonly IFileService _fileService;

        private readonly GlobalSettings _globalSettings;
        public CkeditorController(IOptions<GlobalSettings> globalSettingsOptions, IFileService fileService) {
            _fileService = fileService;
            _globalSettings = globalSettingsOptions.Value;
        }

        [Route("upload_file")]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile upload) {
            var file = await upload.ConvertToFileDataProviderAsync(_globalSettings.FilesDirectoryPath,
                _globalSettings.EditorFileStorageDirectory);

            var result = await _fileService.SaveFileAsync(file, true);
            var uploaded = string.IsNullOrEmpty(result) ? 0 : 1;
            return Ok(new { uploaded });
        }

        [Route("browse_file")]
        public IActionResult BrowseFile() {
            ViewBag.Directory = _globalSettings.EditorFileStorageDirectory;
            ViewBag.FileInfos = _fileService.GetDirectoryFiles(_globalSettings.FilesDirectoryPath, _globalSettings.EditorFileStorageDirectory);
            return View("BrowseFile");
        }
    }
}