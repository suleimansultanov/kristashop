﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Data;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using KristaShop.WebUI.Infrastructure;
using Microsoft.Extensions.Options;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class BlogController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IMapper _mapper;
        private readonly GlobalSettings _globalSettings;

        public BlogController(IBlogService blogService, IMapper mapper, IOptions<GlobalSettings> options)
        {
            _blogService = blogService;
            _mapper = mapper;
            _globalSettings = options.Value;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData() => Ok(await _blogService.GetBlogs());

        [HttpPost]
        public async Task UpdateRow(Guid id, int fromPosition, int toPosition)
        {
            var dto = await _blogService.GetBlogDetailsNoTrack(id);
            dto.Order = toPosition;
            await _blogService.UpdateBlog(dto);
        }

        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(BlogItemViewModel model)
        {
            if (ModelState.IsValid && model.Image != null)
            {
                Image image = Image.FromStream(model.Image.OpenReadStream(), true, true);
                if (image.Width == image.Height)
                {
                    var dto = _mapper.Map<BlogItemDTO>(model);
                    dto.ImagePath = await FileUpload.FilePathAsync(dto.Image, _globalSettings);
                    var result = await _blogService.InsertBlog(dto);
                    TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                    return RedirectToAction(nameof(Index));
                }
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Ширина и высота фото должны быть одинаковыми." }));
            }
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _blogService.GetBlogDetails(id.Value);
            var model = _mapper.Map<BlogItemViewModel>(dto);
            if (model == null)
                return NotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BlogItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dto = _mapper.Map<BlogItemDTO>(model);
                if (model.Image != null)
                {
                    Image image = Image.FromStream(model.Image.OpenReadStream(), true, true);
                    if (image.Width == image.Height)
                    {
                        dto.ImagePath = await FileUpload.FilePathAsync(dto.Image, _globalSettings);
                    }
                    else
                    {
                        TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Ширина и высота фото должны быть одинаковыми." }));
                        return View(model);
                    }
                }
                var result = await _blogService.UpdateBlog(dto);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _blogService.GetBlogDetails(id.Value);
            var model = _mapper.Map<BlogItemViewModel>(dto);
            if (model == null)
                return NotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _blogService.DeleteBlog(id);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction(nameof(Index));
        }
    }
}