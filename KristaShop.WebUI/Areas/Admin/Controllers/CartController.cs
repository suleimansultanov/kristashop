﻿using System;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.WebUI.Areas.Admin.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers {
    [Area("Admin")]
    [PermissionFilter]
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    public class CartController : Controller {
        private readonly ICartService _cartService;
        private readonly ILogger _logger;

        public CartController(ICartService cartService, ILogger logger) {
            _cartService = cartService;
            _logger = logger;
        }

        public async Task<IActionResult> CartsReport() {
            try {
                var result = await _cartService.GetCartTotalsAsync();
                return View(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get cart totals for the report. {message}", ex.Message);
                return BadRequest();
            }
        }

        public async Task<IActionResult> LoadCartsReport() {
            try {
                var result = await _cartService.GetAllCartsItemsAsync();
                return Ok(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get carts report. {message}", ex.Message);
                return BadRequest();
            }
        }
    }
}
