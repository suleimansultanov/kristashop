﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Helpers;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Areas.Admin.Models.Filters;
using KristaShop.WebUI.Models;
using KristaShop.WebUI.Utils.Extensions;
using Serilog;
using ControllerBase = KristaShop.WebUI.Controllers.Common.ControllerBase;
using IUserService = KristaShop.Business.Interfaces.IUserService;
using LoginViewModel = KristaShop.WebUI.Areas.Admin.Models.LoginViewModel;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    public class IdentityController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILinkService _linkService;
        private readonly ICatalogService _catalogService;
        private readonly IDictionaryService _dictionaryService;
        private readonly IAsupApiClient<UserUpdateViewModel> _apiClient;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public IdentityController(IUserService userService, ILinkService linkService, ICatalogService catalogService,
            IDictionaryService dictionaryService, IAsupApiClient<UserUpdateViewModel> apiClient, IMapper mapper, ILogger logger) {
            _userService = userService;
            _linkService = linkService;
            _catalogService = catalogService;
            _dictionaryService = dictionaryService;
            _apiClient = apiClient;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IActionResult> Index(UsersFilter filter, [FromServices]ICatalogService catalogService) {
            ViewBag.Catalogs = new SelectList(await catalogService.GetCloseCatalogs(), "Id", "Name");
            return View();
        }

        public async Task<IActionResult> LoadData([FromServices]ICurrentUserService userService, [FromServices]ICartService cartService) {
            try {
                UserSession user = userService.Get();
                var clients = await _userService.GetUsersWithDataAsync(user);
                var userIds = await cartService.GetCartUserStatus();
                var dictUserCatalog = await _catalogService.GetDictionaryUserCatalogs();
                foreach (var item in clients)
                {
                    dictUserCatalog.TryGetValue(item.UserId, out string catalogNames);
                    item.CatalogNames = catalogNames;
                    item.CartStatus = userIds.Contains(item.UserId);
                }
                return Ok(clients);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load users list. {message}", ex.Message);
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateLink(Guid userId, [FromServices] IDocRegistryService docRegistryService) {
            try {
                var result = await _linkService.InsertLinkAuthAsync(userId);
                if (result.IsSuccess) {
                    var documents = (await docRegistryService.GetUserActiveDocumentsListAsync(userId)).OrderByDescending(x => x.DocNum).ToList();
                    var docLinks = documents.Select(x => new {DocName = x.FullName, Link = $"{result.Model}&SubtreeId={x.SubtreeId}&DocId={x.DocId}"});
                    return Ok(new {link = result.Model, docLinks});
                }

                _logger.Warning(result.Exception, "{message}. {@result}", result.ToString(), result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to create login link. {message}", ex.Message);
            }
            return BadRequest("Не удалось сгенерировать ссылку для смены пароля");
        }

        public async Task<IActionResult> CreateChangePasswordLink(Guid userId) {
            try {
                var result = await _linkService.InsertLinkAuthAsync(userId, AuthorizationLinkType.ChangePassword);
                if (!result.IsSuccess) {
                    return BadRequest($"Не удалось сгенерировать ссылку для смены пароля. {result.ReadableMessage}");
                }

                return Ok(new {link = result.Model});
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to generate change password link. {message}", ex.Message);
            }
            return BadRequest("Не удалось сгенерировать ссылку для смены пароля");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveLink(Guid userId) {
            try {
                var result = await _linkService.RemoveLinksByUserIdAsync(userId);
                if (!result.IsSuccess) {
                    _logger.Warning(result.Exception, "{message}. {@result}", result.ToString(), result);
                }

                return Ok(result.ToOperationResult());
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to remove user {userId} links. {message}", userId, ex.Message);
                return BadRequest("Не удалось удалить ссылки пользователя");
            }
        }

        public async Task<IActionResult> GetNonVisCatalogs(Guid userId)
        {
            Guid[] ctlgIds = await _catalogService.GetNonVisUserCatalogs(userId);
            return Ok(ctlgIds);
        }

        [HttpPost]
        public async Task<IActionResult> SetNonVisCatalogs(Guid userId, List<Guid> catalogs)
        {
            await _catalogService.AddNonVisUserCatalogs(userId, catalogs);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> UpdateUser(Guid userId, string filter) {
            if (userId == Guid.Empty) {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Клиент не выбран." }));
                return RedirectToAction(nameof(Index), new UserParamsViewModel());
            }

            var user = await _userService.GetUserAsync(userId);
            var model = _mapper.Map<UserUpdateViewModel>(user);
            model.Cities = new SelectList(_dictionaryService.GetCities(), "Id", "Name");
            model.Statuses = new SelectList(UserStatusExtension.GetActivateStatuses(), "Key", "Value" );
            model.Catalogs = await _catalogService.GetAllCatalogsWithNonVisibleCheckAsync(userId);

            ViewData["filter"] = filter;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser(UserUpdateViewModel model, string filter) {
            try {
                if (model.Id.IsEmpty()) {
                    ModelState.TryAddModelError(string.Empty, "Пользователь не выбран");
                } else {
                    model.Password = HashHelper.TransformPassword(model.Password);
                    var result = await _apiClient.UpdateUserAsync(model);
                    if (result.StatusCode < 400) {
                        await _catalogService.AddNonVisUserCatalogs(model.Id, model.NotVisibleCatalogsIds);
                        TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Success(result.ReadableMessage));
                        return RedirectToActionWithQueryString(nameof(Index), filter);
                    }

                    _logger.Error("Failed to update new user data in admin area {@model} {@result}", model, result);
                    if (result.HasReadableMessage) {
                        ModelState.TryAddModelError(string.Empty, result.ReadableMessage);
                    } else {
                        ModelState.TryAddModelErrors(OperationResult.Failure().Messages);
                    }
                }
                model.Cities = new SelectList(_dictionaryService.GetCities(), "Id", "Name");
                model.Statuses = new SelectList(UserStatusExtension.GetActivateStatuses(), "Key", "Value" );
                model.Catalogs = await _catalogService.GetAllCatalogsWithNonVisibleCheckAsync(model.Id);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to update user. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure());
                return RedirectToActionWithQueryString(nameof(Index), filter);
            }

            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var result = await _userService.SignIn(model.UserName, model.Password);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            TempData["test"] = "test";
            if (!result.IsSuccess)
            {
                HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                return View();
            }
            HttpContext.Items["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Клиент не выбран." }));
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await _userService.SignOut();
            return RedirectToAction(nameof(Login));
        }
    }
}