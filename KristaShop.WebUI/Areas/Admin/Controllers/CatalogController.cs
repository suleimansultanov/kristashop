﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Helpers;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.Extensions.Options;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class CatalogController : Controller
    {
        private readonly ICatalogService _catService;
        private readonly IMapper _mapper;
        private readonly GlobalSettings _globalSettings;

        public CatalogController(ICatalogService catService, IMapper mapper, IOptions<GlobalSettings> settings)
        {
            _catService = catService;
            _mapper = mapper;
            _globalSettings = settings.Value;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData() => Ok(await _catService.GetCatalogs());

        [HttpPost]
        public async Task UpdateRow(Guid id, int fromPosition, int toPosition)
        {
            var dto = await _catService.GetCatalogDetailsNoTrack(id);
            dto.Order = toPosition;
            await _catService.UpdateCatalog(dto);
        }

        [HttpPost]
        public async Task<IActionResult> AddNonVisUsers(Guid ctlgId, [FromServices]ICurrentUserService currentUser, [FromServices]IUserService userService)
        {
            UserSession user = currentUser.Get();
            var clients = userService.GetAllUsers(user);
            var result = await _catService.AddAllNonVisCatalogUsers(ctlgId, clients.Select(x => x.UserId).ToList());
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveNonVisUsers(Guid ctlgId)
        {
            var result = await _catService.RemoveAllNonVisCatalogUsers(ctlgId);
            return Ok(result);
        }

        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(CatalogViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dto = _mapper.Map<CatalogDTO>(model);
                dto.Preview = await model.Preview.ConvertToFileDataProviderAsync(_globalSettings.FilesDirectoryPath, _globalSettings.CatalogPreviewsDirectory);
                dto.VideoPath = UrlHelper.YoutubeUrlToEmbed(dto.VideoPath);
                var result = await _catService.InsertCatalog(dto);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _catService.GetCatalogDetails(id.Value);
            if (dto == null)
                return NotFound();

            var model = _mapper.Map<CatalogViewModel>(dto);
            if (model.CloseTime != null) {
                model.CloseTime = model.CloseTime.Value.ToOffset(DateTimeOffset.Now.Offset);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CatalogViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dto = _mapper.Map<CatalogDTO>(model);
                dto.Preview = await model.Preview.ConvertToFileDataProviderAsync(_globalSettings.FilesDirectoryPath, _globalSettings.CatalogPreviewsDirectory);
                dto.VideoPath = UrlHelper.YoutubeUrlToEmbed(dto.VideoPath);
                var result = await _catService.UpdateCatalog(dto);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _catService.GetCatalogDetails(id.Value);
            var model = _mapper.Map<CatalogViewModel>(dto);
            if (model == null)
                return NotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _catService.DeleteCatalog(id, _globalSettings.FilesDirectoryPath);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction(nameof(Index));
        }
    }
}