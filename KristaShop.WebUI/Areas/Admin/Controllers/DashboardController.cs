﻿using System;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers {
    [Area("Admin")]
    [PermissionFilter]
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    public class DashboardController : Controller {
        private readonly IOrdersService _orderService;
        private readonly IUserService _userService;
        private readonly ICurrentUserService _currentUserService;
        private readonly ILogger _logger;

        public DashboardController(IOrdersService orderService, IUserService userService, ICurrentUserService currentUserService, ILogger logger) {
            _orderService = orderService;
            _userService = userService;
            _currentUserService = currentUserService;
            _logger = logger;
        }

        public async Task<IActionResult> GetTopMenuValues() {
            try {
                var user = _currentUserService.Get();
                var newOrdersCount = await _orderService.GetNewOrdersCountAsync(user.UserId, user.AccessLevel);
                var newUsersCount = await _userService.GetManagerNewUsersCountAsync(user.UserId, user.AccessLevel);
                return Ok(new TopMenuDashboardViewModel(newUsersCount, newOrdersCount));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get top menu dashboard. {message}", ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }
    }
}
