﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Data;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.WebUI.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class BannerController : Controller
    {
        private readonly IBannerService _bannerService;
        private readonly IMapper _mapper;
        private readonly GlobalSettings _globalSettings;
        private readonly TextColors _textColors;

        public BannerController(IBannerService BannerService, IMapper mapper, IOptions<GlobalSettings> options, IOptions<TextColors> textColorsOptions) {
            _bannerService = BannerService;
            _mapper = mapper;
            _globalSettings = options.Value;
            _textColors = textColorsOptions.Value;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData() => Ok(await _bannerService.GetBanners());

        [HttpPost]
        public async Task UpdateRow(Guid id, int fromPosition, int toPosition)
        {
            var dto = await _bannerService.GetBannerDetailsNoTrack(id);
            dto.Order = toPosition;
            await _bannerService.UpdateBanner(dto);
        }

        public IActionResult Create() {
            var model = new BannerItemViewModel();
            model.Colors = _textColors.TitleColors.Select(x => new SelectListItem(x.Key, x.Value, x.Value.Equals("#000000"))).ToList();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(BannerItemViewModel model)
        {
            if (ModelState.IsValid && model.Image != null)
            {
                Image image = Image.FromStream(model.Image.OpenReadStream(), true, true);
                if (image.Width >= 1920 || (image.Width <= 1922 && image.Height == 1000))
                {
                    var dto = _mapper.Map<BannerItemDTO>(model);
                    dto.ImagePath = await FileUpload.FilePathAsync(model.Image, _globalSettings);

                    var color = ColorTranslator.FromHtml(model.Color);
                    dto.TitleColor = ColorTranslator.ToHtml(color != Color.Empty ? color : Color.Black);

                    var result = await _bannerService.InsertBanner(dto);
                    TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                    return RedirectToAction(nameof(Index));
                }
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Размеры фото должны быть 1920х1000" }));
            }

            return View(model);
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _bannerService.GetBannerDetails(id.Value);
            var model = _mapper.Map<BannerItemViewModel>(dto);
            if (model == null)
                return NotFound();

            model.Colors = _textColors.TitleColors.Select(x => new SelectListItem(x.Key, x.Value, x.Value.Equals("#000000"))).ToList();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BannerItemViewModel model)
        {
            if (ModelState.IsValid) {
                var dto = _mapper.Map<BannerItemDTO>(model);
                if (model.Image != null)
                {
                    Image image = Image.FromStream(model.Image.OpenReadStream(), true, true);
                    if (image.Width >= 1920 || (image.Width <= 1922 && image.Height == 1000))
                    {
                        dto.ImagePath = await FileUpload.FilePathAsync(model.Image, _globalSettings);
                    }
                    else
                    {
                        TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Размеры фото должны быть 1920х1000" }));
                        return View(model);
                    }
                }

                var color = ColorTranslator.FromHtml(model.Color);
                dto.TitleColor = ColorTranslator.ToHtml(color != Color.Empty ? color : Color.Black);

                var result = await _bannerService.UpdateBanner(dto);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _bannerService.GetBannerDetails(id.Value);
            var model = _mapper.Map<BannerItemViewModel>(dto);
            if (model == null)
                return NotFound();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _bannerService.DeleteBanner(id);
            TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            return RedirectToAction(nameof(Index));
        }
    }
}