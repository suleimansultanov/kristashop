﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces;
using KristaShop.WebUI.Utils.Extensions;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers {
    [Area("Admin")]
    [PermissionFilter]
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    public class SettingsController : Controller {
        private readonly ISettingService _settingService;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _userService;
        private readonly ILogger _logger;

        public SettingsController(ISettingService settingService, IMapper mapper, ICurrentUserService userService, ILogger logger) {
            _settingService = settingService;
            _mapper = mapper;
            _userService = userService;
            _logger = logger;
        }

        public IActionResult Index() {
            ViewData["IsRoot"] = _userService.Get().IsRoot;
            return View();
        }

        public async Task<IActionResult> LoadData() {
            var user = _userService.Get();
            var result = user.IsRoot ? await _settingService.GetSettingsAsync() : await _settingService.GetNotRootSettingsAsync();
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create(SettingEditViewModel model) {
            try {
                var user = _userService.Get();
                if (!user.IsRoot)
                    return Forbid();

                if (!ModelState.IsValid) {
                    return BadRequest(ModelState.Values.ErrorsAsOperationResult());
                }

                var dto = _mapper.Map<SettingsDTO>(model);
                var result = await _settingService.InsertAsync(dto);
                if (result.IsSuccess)
                    return Ok(result);
                else
                    return BadRequest(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to create settings {@model}. {message}", model, ex.Message);
                ModelState.AddModelError("", "Не удалось создать запись");
                return BadRequest(ModelState.Values.ErrorsAsOperationResult());
            }
           
        }

        public async Task<IActionResult> Details(Guid id) {
            return Ok(_mapper.Map<SettingEditViewModel>(await _settingService.GetByIdAsync(id)));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SettingEditViewModel model) {
            try {
                var user = _userService.Get();
                if (!ModelState.IsValid) {
                    return BadRequest(ModelState.Values.ErrorsAsOperationResult());
                }

                var dto = _mapper.Map<SettingsDTO>(model);
                var result = await _settingService.UpdateAsync(dto, user.IsRoot);
                if (result.IsSuccess)
                    return Ok(result);
                else
                    return BadRequest(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to edit settings {@model}. {message}", model, ex.Message);

                ModelState.AddModelError("", "Не удалось изменить запись");
                return BadRequest(ModelState.Values.ErrorsAsOperationResult());
            }
           
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid id) {
            try {
                var user = _userService.Get();
                if (!user.IsRoot)
                    return Forbid();
                return Ok(await _settingService.DeleteAsync(id));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to delete settings with id = {id}. {message}", id, ex.Message);

                ModelState.AddModelError("", "Не удалось удалить запись");
                return BadRequest(ModelState.Values.ErrorsAsOperationResult());
            }
        }
    }
}