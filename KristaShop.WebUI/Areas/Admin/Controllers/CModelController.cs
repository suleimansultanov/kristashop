﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.WebUI.Areas.Admin.Data;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.WebUI.Infrastructure;
using Microsoft.Extensions.Options;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class CModelController : Controller
    {
        private readonly INomenclatureService _nomenclatureService;
        private readonly IDictionaryService _dictService;
        private readonly ICatalogService _catalogService;
        private readonly ICategoryService _categoryService;

        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly GlobalSettings _globalSettings;

        public CModelController
            (INomenclatureService nomenclatureService, IDictionaryService dictService,
            ICatalogService catalogService, ICategoryService categoryService,
            IMapper mapper, IOptions<GlobalSettings> options, ILogger logger)
        {
            _nomenclatureService = nomenclatureService;
            _dictService = dictService;
            _catalogService = catalogService;
            _categoryService = categoryService;

            _mapper = mapper;
            _logger = logger;
            _globalSettings = options.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData() => Ok(_nomenclatureService.GetNomModels());

        public async Task<IActionResult> IndexByCatalog(Guid? id)
        {
            if (id == null)
                return NotFound();

            var dto = await _catalogService.GetCatalogDetails(id.Value);
            var model = _mapper.Map<CatalogViewModel>(dto);
            if (model == null)
                return NotFound();

            return View(model);
        }

        public IActionResult LoadDataByCatalog(Guid? id) => Ok(_nomenclatureService.GetNomModelsByCatalog(id.Value));

        [HttpPost]
        public async Task UpdateRow(Guid id, Guid catId, int toPosition)
        {
            await _nomenclatureService.UpdateNomCatalog(id, catId, toPosition);
        }

        [HttpPost]
        public async Task<IActionResult> ReorderModel(Guid id, Guid catId, int toPosition)
        {
            var result = await _nomenclatureService.UpdateNomCatalogOrders(id, catId, toPosition);
            return Ok(result);
        }

        [HttpPost]
        public async Task UpdatePhotoRow(Guid id, int toPosition)
        {
            await _nomenclatureService.UpdateNomPhotoPosition(id, toPosition);
        }

        [HttpPost]
        public async Task<IActionResult> ReorderPhotoModel(Guid id, Guid nomId, int toPosition)
        {
            var result = await _nomenclatureService.UpdateNomPhotoOrders(id, nomId, toPosition);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> ResizePhotoModel(Guid id, IFormFile file)
        {
            string path = await FileUpload.FilePathAsync(file, _globalSettings);
            var result = await _nomenclatureService.UpdateNomPhotoPath(id, path);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddMainPhoto(Guid nomId, string path)
        {
            var result = await _nomenclatureService.AddMainPhotoToNomenclature(nomId, path);
            return Ok(result);
        }

        public async Task<IActionResult> AddModelsCtlg(Guid id, List<Guid> modelIds)
        {
            var result = await _nomenclatureService.AddModelsCtlg(id, modelIds);
            return Ok(result);
        }

        public async Task<IActionResult> Edit(Guid? id, Guid? catalogId = null)
        {
            if (id == null)
                return NotFound();

            var dtoRO = await _nomenclatureService.GetCatalogModel(id.Value);
            if (dtoRO == null)
                return NotFound();

            var dto = await _nomenclatureService.GetNomModel(id.Value);
            var model = _mapper.Map<NomViewModel>(dto);
            model ??= new NomViewModel { IsVisible = true };
            model.NomId = dtoRO.Id;
            model.Articul = dtoRO.Articul;
            model.ItemName = dtoRO.ItemName;
            model.IsSet = dtoRO.IsSet;
            model.CurrentCatalogId = catalogId;

            ViewBag.Colors = new SelectList(_nomenclatureService.GetColorsByNomId(id.Value), "Id", "Name");
            ViewBag.Catalogs = new SelectList(await _catalogService.GetCatalogs(), "Id", "Name");
            ViewBag.Categories = new SelectList(await _categoryService.GetCategories(), "Id", "Name");
            ViewBag.NomCatalogs2 = new SelectList(await _catalogService.GetCatalogsByNomId(id.Value), "Id", "Name");
            ViewBag.NomCategories = new SelectList(await _categoryService.GetCategoriesByNomId(id.Value), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(NomViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Image != null)
                {
                    model.ImagePath = await FileUpload.FilePathAsync(model.Image, _globalSettings);
                }

                var dto = _mapper.Map<NomModelDTO>(model);
                if (model.Photos != null)
                {
                    foreach (var photo in model.Photos)
                    {
                        string photoPath = await FileUpload.FilePathAsync(photo, _globalSettings);
                        dto.PhotoPaths.Add(photoPath);
                    }
                }
                var result = await _nomenclatureService.AddNomModel(dto);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                return RedirectToAction(nameof(Edit), new { id = dto.NomId, catalogId = model.CurrentCatalogId });
            }
            ViewBag.Colors = new SelectList(_nomenclatureService.GetColorsByNomId(model.NomId), "Id", "Name");
            ViewBag.Catalogs = new SelectList(await _catalogService.GetCatalogs(), "Id", "Name");
            ViewBag.Categories = new SelectList(await _categoryService.GetCategories(), "Id", "Name");
            ViewBag.NomCatalogs = new SelectList(await _catalogService.GetCatalogsByNomId(model.NomId), "Id", "Name");
            ViewBag.NomCategories = new SelectList(await _categoryService.GetCategoriesByNomId(model.NomId), "Id", "Name");
            return View(model);
        }

        public IActionResult LoadProdCtlg(Guid nomId, Guid ctlgId) => Ok(_nomenclatureService.GetProdCtlgs(nomId, ctlgId));

        [HttpPost]
        public async Task<IActionResult> ChangeVisProdCtlg(Guid nomId, Guid ctlgId, Guid prodId)
        {
            await _nomenclatureService.AddVisProdCtlg(nomId, ctlgId, prodId);
            return Ok(true);
        }

        public IActionResult LoadProdPrice(Guid nomId) => Ok(_nomenclatureService.GetPrices(nomId));

        [HttpPost]
        public async Task<IActionResult> ChangeProdPrice(Guid nomId, Guid prodId, double price) {
            try {
                if (nomId != Guid.Empty && prodId != Guid.Empty) {
                    await _nomenclatureService.AddProdPrice(nomId, prodId, price);
                    return Ok(OperationResult.Success());
                }
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to change the product price. Nomenclature: {nomenclatureId} Product: {productId}. {message}", nomId, prodId, ex.Message);
            }

            return BadRequest(OperationResult.Failure());
        }

        public async Task<IActionResult> GetProdPrice(Guid prodId, Guid nomId)
        {
            double price = await _nomenclatureService.GetProductPriceFromCatalogItemOrDefault(prodId, nomId);
            return Ok(price);
        }

        public IActionResult LoadSHAmount(Guid nomId) => Ok(_nomenclatureService.GetSHAmounts(nomId));

        public async Task<IActionResult> LoadPhotos(Guid? id)
        {
            var list = await _nomenclatureService.GetNomPhotos(id.Value);
            return Ok(list);
        }

        [HttpPost]
        public async Task<IActionResult> EditPhoto(Guid photoId, Guid colorId)
        {
            var result = await _nomenclatureService.AddColorPhoto(photoId, colorId);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> DeletePhoto(Guid photoId)
        {
            var result = await _nomenclatureService.RemovePhoto(photoId);
            return Ok(result);
        }

        public IActionResult LoadUsers(Guid? id, [FromServices] ICurrentUserService userService)
        {
            UserSession user = userService.Get();
            var list = _nomenclatureService.GetNomUsers(user, id.Value);
            return Ok(list);
        }
    }
}