﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers {
    [Area("Admin")]
    [PermissionFilter]
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    public class DiscountController : Controller {
        private readonly IDiscountService _discountService;
        private readonly INomenclatureService _nomenclatureService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public DiscountController (IDiscountService discountService, 
            INomenclatureService nomenclatureService, IMapper mapper, ILogger logger) {
            _discountService = discountService;
            _nomenclatureService = nomenclatureService;
            _mapper = mapper;
            _logger = logger;
        }

        public IActionResult Index() {
            return View();
        }

        public async Task<IActionResult> LoadCatalogs() {
            try {
                return Ok(await _discountService.GetCatalogsWithDiscountAsync());
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load catalogs page for discounts {message}", ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        public async Task<IActionResult> LoadNomenclatureModels() {
            try {
                return Ok(await _nomenclatureService.GetNomModelDiscounts());
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load nomenclature page for discounts {message}", ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        public async Task<IActionResult> LoadUsers([FromServices]ICurrentUserService userService) {
            try {
                var user = userService.Get();
                return Ok(await _discountService.GetUsersWithDiscountAsync(user));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load users page for discounts {message}", ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        public IActionResult IndexType(Guid parentId, DiscountType discountType) {
            try {
                var model = new DiscountViewModel(parentId, discountType);
                ViewBag.Title = EnumExtensions<DiscountType>.GetDisplayValue(discountType);
                return View(model);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load index page for discount type, parentId: {id}, type: {type}. {message}", parentId, discountType, ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Произошла ошибка при получении данных о скидках"));
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> LoadDataType(Guid parentId, DiscountType discountType) {
            try {
                var discounts = await _discountService.GetDiscountsAsync(parentId, discountType);
                return Ok(discounts);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to load discounts parentId: {id}, type: {type}. {message}", parentId, discountType, ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        public async Task<IActionResult> Details(Guid entityId, Guid parentId, DiscountType entityType) {
            try {
                var discount = await _discountService.GetDiscountDetailsAsync(entityId, entityType);
                var result = _mapper.Map<DiscountViewModel>(discount);
                if (entityType == DiscountType.Nom) {
                    result.Catalogs = await _nomenclatureService.GetNomenclatureCatalogsLookupListAsync(parentId);
                }

                return Ok(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get discount id: {id}, type: {type}. {message}", entityId, entityType, ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create(Guid parentId, DiscountType entityType) {
            try {
                var result = new DiscountViewModel(parentId, entityType);
                if (entityType == DiscountType.Nom) {
                    result.Catalogs = await _nomenclatureService.GetNomenclatureCatalogsLookupListAsync(parentId);
                }

                return Ok(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get discount model for create type: {type}. {message}", entityType, ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrEdit(DiscountViewModel model) {
            try {
                var dto = _mapper.Map<DiscountDTO>(model);
                var result = model.Id == Guid.Empty
                    ? await _discountService.AddDiscountAsync(dto)
                    : await _discountService.UpdateDiscountAsync(dto);

                if (result.IsSuccess)
                    return Ok(result);
                return BadRequest(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to edit or create discount {@model}. {message}", model, ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Guid entityId, DiscountType entityType) {
            try {
                var result = await _discountService.RemoveDiscountAsync(entityId, entityType);
                if (result.IsSuccess) {
                    return Ok(result);
                }

                return BadRequest(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to delete discount id: {id}, type: {type}. {message}", entityId, entityType, ex.Message);
                return BadRequest(OperationResult.Failure());
            }
        }
    }
}