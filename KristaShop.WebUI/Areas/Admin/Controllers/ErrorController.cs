﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    public class ErrorController : Controller
    {
        public IActionResult Error403()
        {
            return View();
        }

        public IActionResult Error404()
        {
            return View();
        }
    }
}