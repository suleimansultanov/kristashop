﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.WebUI.Areas.Admin.Filters;
using KristaShop.WebUI.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Models;
using KristaShop.WebUI.Infrastructure;
using KristaShop.WebUI.Utils.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using Serilog;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class MBodyController : Controller
    {
        private readonly IMenuContentService _menuContentService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly GlobalSettings _globalSettings;

        public MBodyController(IMenuContentService menuContentService, IMapper mapper, IOptions<GlobalSettings> options, ILogger logger)
        {
            _menuContentService = menuContentService;
            _mapper = mapper;
            _logger = logger;
            _globalSettings = options.Value;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData() {
            var result = await _menuContentService.GetMenusContentAsync();
            return Ok(_mapper.Map<List<MenuContentViewModel>>(result));
        }

        public IActionResult Create() {
            try {
                var model = new MenuContentViewModel {Layouts = _getLayoutsAsSelectList()};
                return View(model);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get menu content for create. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Возникла ошибка при открытии формы создания контент страницы"));
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MenuContentViewModel model) {
            try {
                if (ModelState.IsValid)
                {
                    var menuContent = _mapper.Map<MenuContentDTO>(model);
                    menuContent.TitleIcon = await model.TitleIcon.ConvertToFileDataProviderAsync(_globalSettings.FilesDirectoryPath, _globalSettings.MenuContentDirectory);
                    var result = await _menuContentService.InsertMenuContentAsync(menuContent);

                    TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                    return RedirectToAction(nameof(Index));
                }

                model.Layouts = _getLayoutsAsSelectList();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to create menu content. {message}", ex.Message);
                ModelState.AddModelError(string.Empty, "Возникла ошибка при создании контент страницы");
            }

            return View(model);
        }

        public async Task<IActionResult> Edit(Guid id = default)
        {
            try {
                if (id == Guid.Empty)
                    return NotFound();

                var menuContent = await _menuContentService.GetMenuContentByIdAsync(id);
                if (menuContent == null)
                    return NotFound();

                var model = _mapper.Map<MenuContentViewModel>(menuContent);
                model.Layouts = _getLayoutsAsSelectList();
                return View(model);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get menu content for edit. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Возникла ошибка при получении контент страницы для редактирования"));
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(MenuContentViewModel model) {
            try {
                if (ModelState.IsValid) {
                    var menuContent = _mapper.Map<MenuContentDTO>(model);
                    menuContent.TitleIcon = await model.TitleIcon.ConvertToFileDataProviderAsync(_globalSettings.FilesDirectoryPath, _globalSettings.MenuContentDirectory);
                    var result = await _menuContentService.UpdateMenuContentAsync(menuContent);

                    TempData["AlertResult"] = JsonConvert.SerializeObject(result);
                    return RedirectToAction(nameof(Index));
                }
                model.Layouts = _getLayoutsAsSelectList();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to edit menu content. {message}", ex.Message);
                ModelState.AddModelError(string.Empty, "Возникла ошибка при редактировании контент страницы");
            }

            return View(model);
        }

        public async Task<IActionResult> Delete(Guid id) {
            try {
                if (id == Guid.Empty)
                    return NotFound();

                var dto = await _menuContentService.GetMenuContentByIdAsync(id);
                var model = _mapper.Map<MenuContentViewModel>(dto);
                if (model == null)
                    return NotFound();

                return View(model);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get menu content for delete. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Возникла ошибка при получении контент страницы для удаления"));
                return RedirectToAction(nameof(Index));
            }
        
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id) {
            try {
                var result = await _menuContentService.DeleteMenuContentAsync(id, _globalSettings.FilesDirectoryPath);
                TempData["AlertResult"] = JsonConvert.SerializeObject(result);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to delete menu content. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Возникла ошибка при удалении контент страницы"));
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task UpdateRow(Guid id, int toPosition) {
            try {
                await _menuContentService.UpdateMenuContentOrderAsync(id, toPosition);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to update menu content order. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Возникла ошибка при обновлении позиции контент страницы"));
            }
        }

        private SelectList _getLayoutsAsSelectList() {
            var layouts = MenuContentLayout.GetLayouts();
            return new SelectList(layouts, nameof(MenuContentLayout.Name), nameof(MenuContentLayout.Title), layouts.FirstOrDefault(x => x.IsDefault)?.Name ?? string.Empty);
        }

        public async Task<IActionResult> ReloadCache([FromServices] IDynamicPagesManager dynamicPagesManager) {
            try {
                await dynamicPagesManager.ReloadAsync();
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Success("Кэш успешно обновлен"));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to update dynamic pages cache. {message}", ex.Message);
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure("Не удалось обновить кэш. Возникла ошибка при обновлении кэша"));
            }

            return RedirectToAction(nameof(Index));
        }
    }
}