﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebUI.Areas.Admin.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Extensions;
using KristaShop.WebUI.Areas.Admin.Models;
using KristaShop.WebUI.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace KristaShop.WebUI.Areas.Admin.Controllers
{
    [Authorize(AuthenticationSchemes = "BackendScheme")]
    [Area("Admin")]
    [PermissionFilter]
    public class FeedbackController : Controller
    {
        private readonly IFeedbackService _fbService;
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;
        private readonly GlobalSettings _globalSettings;

        public FeedbackController(IFeedbackService fbService, IFileService fileService, IOptions<GlobalSettings> settings, IMapper mapper) {
            _fbService = fbService;
            _fileService = fileService;
            _mapper = mapper;
            _globalSettings = settings.Value;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData() => Ok(await _fbService.GetFeedbackListAsync());

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, [FromServices]ICurrentUserService userService)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage);
                return BadRequest(OperationResult.Failure(errors.ToList()));
            }
            UserSession user = userService.Get();
            var result = await _fbService.UpdateFeedback(id, user.UserId);
            if (result.IsSuccess)
                return Ok(result);
            else
                return BadRequest(result);
        }

        public async Task<IActionResult> GetFiles(Guid id) {
            var result = _mapper.Map<List<FileViewModel>>(await _fbService.GetFilesByFeedbackIdAsync(id));

            ViewData["LinkAction"] = nameof(DownloadFile);
            return PartialView("_FilesPartial", result);
        }

        public async Task<IActionResult> DownloadFile(Guid id) {
            var file = await _fbService.GetFileAsync(id);
            if (file == null) {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Файл не найден." }));
                return RedirectToAction(nameof(Index));
            }

            var fileArray = await _fileService.GetFileAsync(_globalSettings.FilesDirectoryPath, file.VirtualPath);
            if (fileArray == null) {
                TempData["AlertResult"] = JsonConvert.SerializeObject(OperationResult.Failure(new List<string> { "Не удалось загрузить данный файл." }));
                return RedirectToAction(nameof(Index));
            }


            return File(fileArray, FileTypeExtensions.GetOctetStreamType(), file.Filename);
        }
    }
}