﻿using AutoMapper;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.ServicesAsup.DTOs.Mappings {
    public class CounterpartyMappingProfile : Profile {
        public CounterpartyMappingProfile() {
            CreateMap<CounterpartyDTO, Counterparty>()
                .ForMember(dest => dest.CityId, dest => dest.MapFrom(item => item.CityId))
                .ForMember(dest => dest.NewCity, dest => dest.MapFrom(item => item.NewCity))
                .ForMember(dest => dest.CompanyAddress, dest => dest.MapFrom(item => item.CompanyAddress))
                .ForMember(dest => dest.Person, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Email))
                .ForMember(dest => dest.MallAddress, dest => dest.MapFrom(item => item.MallAddress))
                .ForMember(dest => dest.Phone, dest => dest.MapFrom(item => item.Phone));
        }
    }
}
