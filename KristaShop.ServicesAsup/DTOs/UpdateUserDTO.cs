﻿using System;
using KristaShop.Common.Enums;

namespace KristaShop.ServicesAsup.DTOs {
    public class UpdateUserDTO {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Person { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MallAddress { get; set; }
        public string CompanyAddress { get; set; }
        public Guid? CityId { get; set; }
        public string NewCity { get; set; }
        public UserStatus Status { get; set; }
        public string BanReason { get; set; }
        public DateTime? BanExpireDate { get; set; }
    }
}
