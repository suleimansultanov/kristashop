﻿using System;

namespace KristaShop.ServicesAsup.DTOs {
    public class BanUserDTO {
        public Guid UserId { get; set; }
        public string Reason { get; set; }
        public DateTime ExpireDate { get; set; }

        public BanUserDTO() { }

        public BanUserDTO(Guid userId, string reason, DateTime expireDate) {
            UserId = userId;
            Reason = reason;
            ExpireDate = expireDate;
        }
    }
}
