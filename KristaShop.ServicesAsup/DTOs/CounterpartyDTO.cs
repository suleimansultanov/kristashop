﻿using System;

namespace KristaShop.ServicesAsup.DTOs {
    /// <summary>
    /// This DTO uses <see cref="Mappings.CounterpartyMappingProfile"/>
    /// </summary>
    public class CounterpartyDTO {
        public Guid UserId { get; set; }
        public string Person { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MallAddress { get; set; }
        public string CompanyAddress { get; set; }
        public Guid? CityId { get; set; }
        public string NewCity { get; set; }

        public override string ToString() {
            return $"{nameof(Person)}: {Person}, {nameof(Phone)}: {Phone}, {nameof(Email)}: {Email}, {nameof(MallAddress)}: {MallAddress}, {nameof(CompanyAddress)}: {CompanyAddress}, {nameof(CityId)}: {CityId}, {nameof(NewCity)}: {NewCity}";
        }
    }
}
