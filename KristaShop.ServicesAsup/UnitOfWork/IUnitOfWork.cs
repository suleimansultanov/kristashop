﻿using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.ServicesAsup.UnitOfWork {
    public interface IUnitOfWork : IUnitOfWorkBase, IAsupRepositories { }
}