﻿using System;
using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.ServicesAsup.UnitOfWork {
    public interface IAsupRepositories {
        IRepository<City, Guid> Cities { get; }
        IRepository<Counterparty, Guid> Counterparties { get; }
        IRepository<ClientCounter, Guid> ClientsCounter { get; }
        IRepository<User, Guid> Users { get; }
        IRepository<UserGroup, Guid> UsersGroups { get; }
        IRepository<UserGroupMembership, Guid> UserGroupMembership { get; }
        IRepository<ManagerRate, Guid> ManagerRates { get; }
        IRepository<WebApiRequest, Guid> WebApiRequests { get; }
    }
}
