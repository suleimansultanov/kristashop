﻿using System;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.Models;
using Serilog;

namespace KristaShop.ServicesAsup.UnitOfWork {
    public class UnitOfWork : UnitOfWorkBase, IUnitOfWork {
        public UnitOfWork(KristaAsupDbContext context, ILogger logger) : base(context, logger) { }

        #region Asup repositories implementation

        private IRepository<City, Guid> _citiesRepository;
        private IRepository<Counterparty, Guid> _counterpartyRepository;
        private IRepository<ClientCounter, Guid> _clientsCounterRepository;
        private IRepository<User, Guid> _usersRepository;
        private IRepository<UserGroup, Guid> _usersGroupsRepository;
        private IRepository<UserGroupMembership, Guid> _userGroupMembershipRepository;
        private IRepository<ManagerRate, Guid> _managerRatesRepository;
        private IRepository<WebApiRequest, Guid> _webApiRequestsRepository;

        public IRepository<City, Guid> Cities {
            get { return _citiesRepository ??= new Repository<City, Guid>(Context); }
        }

        public IRepository<Counterparty, Guid> Counterparties {
            get { return _counterpartyRepository ??= new Repository<Counterparty, Guid>(Context); }
        }

        public IRepository<ClientCounter, Guid> ClientsCounter {
            get { return _clientsCounterRepository ??= new Repository<ClientCounter, Guid>(Context); }
        }

        public IRepository<User, Guid> Users {
            get { return _usersRepository ??= new Repository<User, Guid>(Context); }
        }

        public IRepository<UserGroup, Guid> UsersGroups {
            get { return _usersGroupsRepository ??= new Repository<UserGroup, Guid>(Context); }
        }

        public IRepository<UserGroupMembership, Guid> UserGroupMembership {
            get { return _userGroupMembershipRepository ??= new Repository<UserGroupMembership, Guid>(Context); }
        }

        public IRepository<ManagerRate, Guid> ManagerRates {
            get { return _managerRatesRepository ??= new Repository<ManagerRate, Guid>(Context); }
        }

        public IRepository<WebApiRequest, Guid> WebApiRequests {
            get { return _webApiRequestsRepository ??= new Repository<WebApiRequest, Guid>(Context); }
        }

        #endregion
    }
}
