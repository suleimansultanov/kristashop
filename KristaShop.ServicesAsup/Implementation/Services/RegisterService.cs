﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Exceptions;
using KristaShop.Common.Extensions;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Models;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace KristaShop.ServicesAsup.Implementation.Services {
    public class RegisterService : IRegisterService {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly ILogger _logger;

        public RegisterService(IUnitOfWork uow, IMapper mapper, IEmailService emailService, ILogger logger) {
            _uow = uow;
            _mapper = mapper;
            _emailService = emailService;
            _logger = logger;
        }

        public async Task<bool> RegisterAsync(CounterpartyDTO newCounterparty) {
            var newUserLogin = $"{newCounterparty.Person.Split(" ")[0]}{newCounterparty.Phone}".ToValidLogin();
            await _validateCounterpartyDataAsync(newCounterparty, newUserLogin);

            var manager = await _getManagerForNewUserUsingRoundRobbing();
            var counterparty = _createCounterparty(newCounterparty, manager);
            var user = _createNewUserForCounterparty(counterparty, newUserLogin, newCounterparty.UserId);
            var customerGroup = await _getCustomerGroupAsync();
            var userGroup = new UserGroupMembership(user.Id, customerGroup.Id);
            user.Acl = customerGroup.Acl.Acl;

            await _uow.Counterparties.AddAsync(counterparty);
            await _uow.Users.AddAsync(user);
            await _uow.UserGroupMembership.AddAsync(userGroup);

            if (!await _uow.SaveInTransactionAsync()) {
                return false;
            }

            await _sendEmailAboutNewCounterpartyToManager(manager, counterparty);

            return true;
        }

        public Task<bool> IsUserRegisteredAsync(string hash) {
            return _uow.WebApiRequests.All.AnyAsync(x => x.RequestHash.Equals(hash));
        }

        public async Task<bool> AddRegistrationHashAsync(string hash) {
            await _uow.WebApiRequests.AddAsync(new WebApiRequest(hash), true);
            return await _uow.SaveAsync();
        }

        private async Task _validateCounterpartyDataAsync(CounterpartyDTO newCounterparty, string newUserLogin) {
            var isExists = await _uow.Counterparties.All.AnyAsync(x => x.Person.Equals(newCounterparty.Person) && x.Phone.Equals(newCounterparty.Phone));

            if (isExists) {
                throw new EntityAlreadyExistsException($"Entity {newCounterparty} already exists", $"Пользователь с именем {newCounterparty.Person} и номером телефона {newCounterparty.Phone} уже зарегистрирован", nameof(Counterparty));
            }

            if (string.IsNullOrEmpty(newCounterparty.NewCity)) {
                City city = null;
                if (newCounterparty.CityId.HasValue) {
                    city = await _uow.Cities.GetByIdAsync(newCounterparty.CityId.Value);
                }

                if (city == null) {
                    throw new EntityNotFoundException($"City {newCounterparty.CityId.GetValueOrDefault()} not found", "Указанный город не найден", nameof(City));
                }
            }

            if (await _uow.Users.All.AnyAsync(x => x.Login.Equals(newUserLogin))) {
                throw new EntityAlreadyExistsException($"Entity with login {newUserLogin} already exist", $"Пользователь с именем {newCounterparty.Person} и номером телефона {newCounterparty.Phone} уже зарегистрирован", nameof(User));
            }
        }

        private Counterparty _createCounterparty(CounterpartyDTO newCounterparty, User manager) {
            var counterparty = _mapper.Map<Counterparty>(newCounterparty);
            counterparty.Type = 0;
            counterparty.GenerateKey();
            counterparty.ManagerId = manager.Id;
            return counterparty;
        }

        private User _createNewUserForCounterparty(Counterparty counterparty, string login, Guid userId) {
            var password = HashHelper.TransformPassword(Generator.NewString(10));

            var user = new User(userId, login, password, counterparty.Id);
            return user;
        }

        private async Task<UserGroup> _getCustomerGroupAsync() {
            var customerGroup = await _uow.UsersGroups.All
                .Include(x => x.Acl)
                .Where(x => x.Type == UserType.Customer)
                .FirstOrDefaultAsync();

            if(customerGroup == null)
                throw new UserGroupNotFoundException(UserType.Customer);

            return customerGroup;
        }

        private async Task<long> _getNewUserCounterAsync() {
            var counter = await _uow.ClientsCounter.All.FirstOrDefaultAsync();
            if (counter == null) {
                counter = new ClientCounter(1);
                await _uow.ClientsCounter.AddAsync(counter, true);
            }
            else {
                counter.IncreaseCounter();
                _uow.ClientsCounter.Update(counter);
            }

            return counter.Counter - 1;
        }

        private async Task<User> _getManagerForNewUserUsingRoundRobbing() {
            var managerRates = await _uow.ManagerRates.All
                .Include(x => x.User)
                .Where(x => x.Rate > GlobalConstant.Epsilon)
                .ToListAsync();

            if (!managerRates.Any()) {
                throw new ManagerRateNotFoundException();
            }
            
            var managerRatesSum = managerRates.Sum(x => x.Rate);
            var remainder = 1 / managerRatesSum;

            var rates = managerRates.Select(x => new {
                Rate = Math.Round(x.Rate * remainder, 4),
                User = x.User
            }).OrderBy(x => x.User.RegistrationDate).ToList();


            var clientCounter = await _getNewUserCounterAsync();
            var minRate = rates.Min(x => x.Rate);

            var totalClientAmount = Convert.ToInt32(1 / minRate);
            var someValue = (clientCounter % (double)totalClientAmount) / totalClientAmount;
            someValue += minRate;

            double tempRate = 0;
            foreach (var rate in rates) {
                tempRate += rate.Rate;
                if (someValue - tempRate <= GlobalConstant.Epsilon) {
                    return rate.User;
                }
            }

            throw new ManagerRateNotFoundException();
        }

        private async Task _sendEmailAboutNewCounterpartyToManager(User manager, Counterparty counterparty) {
            if (string.IsNullOrEmpty(manager.Email)) return;
            try {
                const string subject = "Зарегестрировался новый пользователь.";
                var content = $"Зарегестрировался новый пользователь c именем {counterparty.Person}. Адрес магазина {counterparty.MallAddress}.";
                var emailMessage = new EmailMessage(manager.Email, subject, content);
                await _emailService.SendEmailAsync(emailMessage);
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to send email to the manager {@manager} about new counterparty {@counterparty}. {message}", manager, counterparty, ex.Message);
            }
        }
    }
}
