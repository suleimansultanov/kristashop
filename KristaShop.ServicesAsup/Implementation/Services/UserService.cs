﻿using System;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Exceptions;
using KristaShop.DataReadOnly.Models;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.ServicesAsup.Implementation.Services {
    public class UserService : IUserService {
        private readonly IUnitOfWork _uow;
        private readonly IRegisterService _registerService;

        public UserService(IUnitOfWork uow, IRegisterService registerService) {
            _uow = uow;
            _registerService = registerService;
        }

        public async Task<bool> UpdateUserAsync(UpdateUserDTO updateUser) {
            if (await _uow.Users.All.AnyAsync(x => x.Login.Equals(updateUser.Login) && x.Id != updateUser.Id)) {
                throw new EntityAlreadyExistsException($"User with same login {updateUser.Login} already exist", "Пользователь с таким логином уже существует", nameof(User));
            }
            
            var user = await _uow.Users.All
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == updateUser.Id);

            if (user == null) {
                throw new EntityNotFoundException($"User not found {updateUser.Id}", "Не удалось изменить статус пользователя. Пользователь не найден", nameof(User));
            }

            if (user.Status == UserStatus.Deleted) {
                throw new EntityUpdateException("User status can't be changed to active from deleted", "Пользователь не может быть активирован. Текущий статус пользователя удален.", nameof(User));
            }

            if (string.IsNullOrEmpty(user.Password) && string.IsNullOrEmpty(updateUser.Password)) {
                throw new EmptyValueException("User have no set password, password can't be empty", "У пользователя еще не установлен пароль, необходимо задать пароль пользователю", nameof(User), nameof(User.Password));
            }

            user.Login = updateUser.Login;
            if (!string.IsNullOrEmpty(updateUser.Password)) {
                user.Password = updateUser.Password;
            }

            user.Status = updateUser.Status;
            if (user.Status != UserStatus.Banned) {
                user.BanReason = string.Empty;
                user.BanExpireDate = null;
            } else {
                user.BanReason = updateUser.BanReason;
                user.BanExpireDate = updateUser.BanExpireDate;
            }
            user.Counterparty.Person = updateUser.Person;
            user.Counterparty.Email = updateUser.Email;
            user.Counterparty.CityId = updateUser.CityId;
            user.Counterparty.NewCity = updateUser.NewCity;
            user.Counterparty.MallAddress = updateUser.MallAddress;
            user.Counterparty.CompanyAddress = updateUser.CompanyAddress;
            user.Counterparty.Phone = updateUser.Phone;

            _uow.Users.Update(user);
            _uow.Counterparties.Update(user.Counterparty);

            return await _uow.SaveInTransactionAsync();
        }

        public async Task<bool> ActivateUserAsync(Guid userId) {
            var user = await _uow.Users.GetByIdAsync(userId);
            if (user == null) {
                throw new EntityNotFoundException($"User not found {userId}", "Не удалось изменить статус пользователя. Пользователь не найден");
            }

            if (user.Status == UserStatus.Deleted) {
                throw new EntityUpdateException("User status can't be changed to active from deleted", "Пользователь не может быть активирован. Текущий статус пользователя удален.", nameof(User));
            }

            user.Status = UserStatus.Active;
            user.BanReason = string.Empty;
            user.BanExpireDate = null;
            _uow.Users.Update(user);
            return await _uow.SaveAsync();
        }

         public async Task<bool> BanUserAsync(BanUserDTO ban) {
            var user = await _uow.Users.GetByIdAsync(ban.UserId);
            if (user == null) {
                throw new EntityNotFoundException($"User not found {ban.UserId}", "Не удалось изменить статус пользователя. Пользователь не найден");
            }

            if (user.Status == UserStatus.Deleted) {
                throw new EntityUpdateException("User status can't be changed to banned from deleted", "Пользователь не может быть заблокирован. Текущий статус пользователя удален.", nameof(User));
            }

            user.Status = UserStatus.Banned;
            user.BanReason = ban.Reason;
            user.BanExpireDate = ban.ExpireDate;

            _uow.Users.Update(user);
            return await _uow.SaveAsync();
         }

         public async Task<bool> ChangeUserPasswordAsync(Guid userId, string password) {
             if (string.IsNullOrEmpty(password)) {
                 throw new EntityUpdateException("Password can't be empty", "Невозможно изменить пароль. Пароль не может быть пустым", nameof(User));
             }

             var user = await _uow.Users.GetByIdAsync(userId);
             if (user == null) {
                 throw new EntityNotFoundException($"User not found {userId}", "Не удалось изменить пароль пользователя. Пользователь не найден");
             }

             if (user.Status != UserStatus.Active) {
                 throw new EntityUpdateException("Password for user with status != Active can't be changed", "Невозможно изменить пароль у неактивного пользователя", nameof(User));
             }

             user.Password = password;
             _uow.Users.Update(user);
             return await _uow.SaveAsync();
         }

         #region Register service implementation

        public Task<bool> RegisterAsync(CounterpartyDTO newCounterparty) {
            return _registerService.RegisterAsync(newCounterparty);
        }

        public Task<bool> IsUserRegisteredAsync(string hash) {
            return _registerService.IsUserRegisteredAsync(hash);
        }

        public Task<bool> AddRegistrationHashAsync(string hash) {
            return _registerService.AddRegistrationHashAsync(hash);
        }

        #endregion
    }
}
