﻿using System.Threading.Tasks;
using KristaShop.ServicesAsup.DTOs;

namespace KristaShop.ServicesAsup.Interfaces.Services {
    public interface IRegisterService {
        Task<bool> RegisterAsync(CounterpartyDTO newCounterparty);
        Task<bool> IsUserRegisteredAsync(string hash);
        Task<bool> AddRegistrationHashAsync(string hash);
    }
}
