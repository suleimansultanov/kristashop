﻿using System;
using System.Threading.Tasks;
using KristaShop.ServicesAsup.DTOs;

namespace KristaShop.ServicesAsup.Interfaces.Services {
    public interface IUserService : IRegisterService {
        Task<bool> UpdateUserAsync(UpdateUserDTO updateUser);
        Task<bool> ActivateUserAsync(Guid userId);
        Task<bool> BanUserAsync(BanUserDTO ban);
        Task<bool> ChangeUserPasswordAsync(Guid userId, string password);
    }
}
