﻿using System;
using KristaShop.DataReadOnly.Domain;
using KristaShop.ServicesAsup.Implementation.Services;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;

namespace KristaShop.ServicesAsup.Infrastructure {
    public static class DependencyInjection {
        public static IServiceCollection AddAsupServices(this IServiceCollection services) {
            services.AddScoped<IRegisterService, RegisterService>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();
            return services;
        }

        public static IServiceCollection AddAsupDbContext(this IServiceCollection services, IConfiguration configuration) {

            services.AddDbContextPool<KristaAsupDbContext>(options => {
                options.UseMySql(configuration.GetConnectionString("KristaConnectionMysql"), 
                    mySqlOptions => mySqlOptions.ServerVersion(new ServerVersion(new Version(8, 0, 18), ServerType.MySql)));
            });

            return services;
        }
    }
}