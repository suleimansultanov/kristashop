﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Common.Implementation.DataAccess {
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class {
        protected readonly DbContext Context;

        public RepositoryBase(DbContext context) {
            Context = context;
        }

        public virtual IQueryable<TEntity> All => Context.Set<TEntity>();
        public virtual IOrderedQueryable<TEntity> AllOrdered => null;

        public virtual async Task<TEntity> AddAsync(TEntity entity, bool generateKey = false) {
            if (generateKey && entity is IEntityKeyGeneratable entityGeneratable) {
                entityGeneratable.GenerateKey();
            }

            await Context.Set<TEntity>().AddAsync(entity);
            return entity;
        }

        public virtual TEntity Update(TEntity entity) {
            Context.Set<TEntity>().Update(entity);
            return entity;
        }

        public virtual TEntity Delete(TEntity entity) {
            Context.Set<TEntity>().Remove(entity);
            return entity;
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities) {
            await Context.Set<TEntity>().AddRangeAsync(entities);
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities) {
            Context.Set<TEntity>().UpdateRange(entities);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities) {
            Context.Set<TEntity>().RemoveRange(entities);
        }
    }

    public class Repository<TEntity, TKey> : RepositoryBase<TEntity>, IRepository<TEntity, TKey> where TEntity : class {
        public Repository(DbContext context) : base(context) { }
        
        public virtual async Task<TEntity> GetByIdAsync(TKey id) {
            return await Context.FindAsync<TEntity>(id);
        }

        public virtual async Task<TEntity> DeleteAsync(TKey id) {
            var entity = await Context.FindAsync<TEntity>(id);
            if (entity != null) {
                Context.Set<TEntity>().Remove(entity);
            }

            return entity;
        }
    }

    public class Repository<TEntity, TKey, TKey2> : RepositoryBase<TEntity>, IRepository<TEntity, TKey, TKey2> where TEntity : class {
        public Repository(DbContext context) : base(context) { }
        
        public async Task<TEntity> GetByIdAsync(TKey key, TKey2 key2) {
            return await Context.FindAsync<TEntity>(key, key2);
        }

        public async Task<TEntity> DeleteAsync(TKey key, TKey key2) {
            var entity = await Context.FindAsync<TEntity>(key, key2);
            if (entity != null) {
                Context.Set<TEntity>().Remove(entity);
            }

            return entity;
        }
    }
}
