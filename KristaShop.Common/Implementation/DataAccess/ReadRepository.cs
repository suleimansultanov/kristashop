﻿using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Common.Implementation.DataAccess {
    public class ReadRepositoryBase<TEntity> : IReadRepositoryBase<TEntity> where TEntity : class {
        protected readonly DbContext Context;

        public ReadRepositoryBase(DbContext context) {
            Context = context;
        }

        public virtual IQueryable<TEntity> All => Context.Set<TEntity>();
        public virtual IOrderedQueryable<TEntity> AllOrdered => null;
    }

    public class ReadRepository<TEntity, TKey> : ReadRepositoryBase<TEntity>, IReadRepository<TEntity, TKey> where TEntity : class {
        public ReadRepository(DbContext context) : base(context) { }
     
        public virtual async Task<TEntity> GetByIdAsync(TKey id) {
            return await Context.FindAsync<TEntity>(id);
        }
    }

    public class ReadRepository<TEntity, TKey, TKey2> : ReadRepositoryBase<TEntity>, IReadRepository<TEntity, TKey, TKey2> where TEntity : class {
        public ReadRepository(DbContext context) : base(context) { }
        public async Task<TEntity> GetByIdAsync(TKey key, TKey2 key2) {
            return await Context.FindAsync<TEntity>(key, key2);
        }
    }
}
