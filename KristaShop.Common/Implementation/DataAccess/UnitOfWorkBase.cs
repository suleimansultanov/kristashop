﻿using System;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces.DataAccess;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace KristaShop.Common.Implementation.DataAccess {
    public abstract class UnitOfWorkBase : IUnitOfWorkBase {
        protected readonly DbContext Context;
        protected readonly ILogger Logger;

        protected UnitOfWorkBase(DbContext context, ILogger logger) {
            Context = context;
            Logger = logger;
        }

        public virtual bool Save() {
            try {
                return Context.SaveChanges() > 0;
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to commit changes {message}", ex.Message);
                return false;
            }
        }

        public virtual async Task<bool> SaveAsync() {
            try {
                return await Context.SaveChangesAsync() > 0;
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to commit changes {message}", ex.Message);
                return false;
            }
        }

        public virtual async Task<bool> SaveInTransactionAsync() {
            await using (var transaction = await Context.Database.BeginTransactionAsync()) {
                try {
                    await Context.SaveChangesAsync();
                    await transaction.CommitAsync();
                } catch (Exception ex) {
                    Logger.Error(ex, "Failed to commit changes {message}", ex.Message);
                    await transaction.RollbackAsync();
                    return false;
                }
            }
            
            return true;
        }
        
        #region IDisposable implementation

        private bool _disposed;

        protected void Dispose(bool disposing) {
            if (!_disposed) {
                if (disposing) {
                    _dispose();
                }
            }

            _disposed = true;
        }

        protected virtual void _dispose() {
            Context.Dispose();
        }

        public void Dispose() {
            Dispose(true);
        }

        #endregion
    }
}
