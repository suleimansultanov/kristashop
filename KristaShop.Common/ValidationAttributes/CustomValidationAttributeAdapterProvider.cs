﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.DataAnnotations;
using Microsoft.Extensions.Localization;

namespace KristaShop.Common.ValidationAttributes {
    public class CustomValidationAttributeAdapterProvider : IValidationAttributeAdapterProvider {
        readonly IValidationAttributeAdapterProvider _baseProvider = new ValidationAttributeAdapterProvider();

        public IAttributeAdapter GetAttributeAdapter(ValidationAttribute attribute, IStringLocalizer stringLocalizer) {
            switch (attribute) {
                case AllowedFileExtensionsAttribute allowedFileExtensionsAttribute:
                    return new AllowedFileExtensionsAttributeAdapter(allowedFileExtensionsAttribute, stringLocalizer);
                case RequiredThisOrOtherAttribute requiredThisOrOtherAttribute:
                    return new RequiredThisOrOtherAttributeAdapter(requiredThisOrOtherAttribute, stringLocalizer);
                case IsTrueAttribute isTrueAttribute: 
                    return new IsTrueAttributeAdapter(isTrueAttribute, stringLocalizer);
                default:
                    return _baseProvider.GetAttributeAdapter(attribute, stringLocalizer);
            }
        }
    }
}