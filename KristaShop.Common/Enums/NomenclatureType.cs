﻿namespace KristaShop.Common.Enums
{
    public enum NomenclatureType
    {
        None = -1,
        Material = 0,
        Product,
        WorkKind,
        Service,
        Packaging,
        Other
    }
}