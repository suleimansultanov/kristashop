﻿namespace KristaShop.Common.Enums
{
    public enum CurrencyType
    {
        None = -1,
        Other = 0,
        USD = 1,
        EUR = 2,
        RUB = 3,
        KGS = 4
    }
}