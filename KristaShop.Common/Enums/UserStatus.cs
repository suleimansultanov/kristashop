﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KristaShop.Common.Enums {
    public enum UserStatus {
        [Display(Name = "Гость")]
        None = -1,

        [Display(Name = "Не активирован")]
        Await = 0,

        [Display(Name = "Активирован")]
        Active = 1,

        [Display(Name = "Заблокирован")]
        Banned = 2,

        [Display(Name = "Удален")]
        Deleted = 3
    }

    public static class UserStatusExtension {
        public static List<KeyValuePair<int, string>> GetActivateStatuses() {
            return new List<KeyValuePair<int, string>> {
                new KeyValuePair<int, string>((int) UserStatus.Await, UserStatus.Await.ToReadableString()),
                new KeyValuePair<int, string>((int) UserStatus.Active, UserStatus.Active.ToReadableString()),
            };
        }

        public static string ToReadableString(this UserStatus status) {
            switch (status) {
                case UserStatus.None:
                    return "Гость";
                case UserStatus.Await:
                    return "Не активирован";
                case UserStatus.Active:
                    return "Активирован";
                case UserStatus.Banned:
                    return "Заблокирован";
                case UserStatus.Deleted:
                    return "Удален";
                default:
                    return "---";
            }
        }
    }
}