﻿namespace KristaShop.Common.Enums
{
    public enum MovementType
    {
        None = -1,
        StoreIn = 0,
        Move,
        Combine,
        UnCombine,
        Reservation,
        UnReservation,
        WriteOff,
        Selling,
        Return
    }

    public enum MovementDirection
    {
        None = -1,
        In = 0,
        Out
    }
}