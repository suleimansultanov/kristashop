﻿namespace KristaShop.Common.Enums {
    public enum UserType {
        None,
        Other,
        Customer,
        Manager,
        Administrator,
        Modeler
    }
}