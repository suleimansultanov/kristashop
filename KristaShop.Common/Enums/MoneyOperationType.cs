﻿namespace KristaShop.Common.Enums {
    public enum MoneyOpType { // Money Operation Type
        None,
        Other,
        Payroll,
        Selling,
        Return,
        Accountability,
        ShipmentReturn,
        BalanceChange,
        CashIncome
    }
}
