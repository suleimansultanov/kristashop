﻿namespace KristaShop.Common.Enums
{
    public enum UniversalDocState
    {
        Canceled = -2,
        None = -1,
        Draft = 0,
        Aplied,
        Completed,
        ShipmentAwaiting,
        Shipped,
        AwaitOrderProcessing,
        OrderProcessing,
        DocumentProcessing
    }

    public static class UniversalDocStateExtension {
        public static string ToReadableString(this UniversalDocState state) {
            switch (state) {
                case UniversalDocState.Canceled:
                    return "Отменен";
                case UniversalDocState.None:
                    return "Не существует";
                case UniversalDocState.Draft:
                    return "Отменен";
                case UniversalDocState.Aplied:
                    return "Принят";
                case UniversalDocState.Completed:
                    return "Завершен";
                case UniversalDocState.ShipmentAwaiting:
                    return "Ожидает отправки";
                case UniversalDocState.Shipped:
                    return "Отправлен";
                case UniversalDocState.AwaitOrderProcessing:
                    return "Ожидание обработки";
                case UniversalDocState.OrderProcessing:
                    return "Обрабатывается";
                case UniversalDocState.DocumentProcessing:
                    return "В работе";
                default:
                    return "---";
            }
        }
    }
}