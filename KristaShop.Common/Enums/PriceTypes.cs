﻿namespace KristaShop.Common.Enums {
    public enum PriceTypes {
        All = -1,
        None = 0,
        CostPrice,
        TradePrice,
        RetailPrice,
        Purchase
    }
}
