﻿using System.ComponentModel.DataAnnotations;

namespace KristaShop.Common.Enums
{
    public enum DiscountType
    {
        None = -1,
        [Display(Name = "Скидка на каталог")]
        Catalog,
        [Display(Name = "Скидка на модель")]
        Nom,
        [Display(Name = "Скидка на пользователя")]
        User
    }
}