﻿using System;

namespace KristaShop.Common.Exceptions {
    public class EntityNotFoundException : ExceptionBase {
        public string EntityName { get; }

        public EntityNotFoundException(Exception ex, string entityName) : base(ex) {
            EntityName = entityName;
        }

        public EntityNotFoundException(Exception innerException, string entityName, string readableMessage) 
            : base(innerException, readableMessage) {
            EntityName = entityName;
        }

        public EntityNotFoundException(string systemMessage, string entityName, Exception innerException = null) 
            : base(systemMessage, innerException) {
            EntityName = entityName;
        }

        public EntityNotFoundException(string systemMessage, string readableMessage, string entityName, Exception innerException = null) 
            : base(systemMessage, readableMessage, innerException) {
            EntityName = entityName;
        }

        public override string ToString() {
            return $"{nameof(EntityName)}: {EntityName},\r\n {base.ToString()}";
        }
    }
}
