﻿using KristaShop.Common.Enums;

namespace KristaShop.Common.Exceptions {
    public class UserGroupNotFoundException : ExceptionBase {
        public UserType UserType { get; }

        public UserGroupNotFoundException(UserType userType) 
            : base($"User group {userType} not found", "Группа пользователей не найдена") {
            UserType = userType;
        }

        public override string ToString() {
            return $"{base.ToString()}, , {nameof(UserType)}: {UserType} ";
        }
    }
}
