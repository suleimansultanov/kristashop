﻿using System;

namespace KristaShop.Common.Exceptions {
    public class EmptyValueException : ExceptionBase {
        public string Entity { get; set; }
        public string Field { get; set; }

        public EmptyValueException(Exception ex) : base(ex) { }
        public EmptyValueException(Exception innerException, string readableMessage, string entity, string field) : base(innerException, readableMessage) {
            Entity = entity;
            Field = field;
        }

        public EmptyValueException(string systemMessage, string entity, string field,  Exception innerException = null) : base(systemMessage, innerException) {
            Entity = entity;
            Field = field;
        }

        public EmptyValueException(string systemMessage, string readableMessage, string entity, string field, Exception innerException = null) : base(systemMessage, readableMessage, innerException) {
            Entity = entity;
            Field = field;
        }
    }
}
