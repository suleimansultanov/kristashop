﻿namespace KristaShop.Common.Exceptions {
    public class ManagerRateNotFoundException : ExceptionBase {
        public ManagerRateNotFoundException() : base("No manager were found with rate > 0", "Менеджер не найден") { }
    }
}
