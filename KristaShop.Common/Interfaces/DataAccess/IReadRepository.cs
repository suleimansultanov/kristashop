﻿using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Common.Interfaces.DataAccess {
    public interface IReadRepositoryBase<TEntity> where TEntity : class {
        IQueryable<TEntity> All { get; }
        IOrderedQueryable<TEntity> AllOrdered { get; }
    }

    public interface IReadRepository<TEntity, in TKey> : IReadRepositoryBase<TEntity> where TEntity : class {
        Task<TEntity> GetByIdAsync(TKey id);
    }

    public interface IReadRepository<TEntity, in TKey, in TKey2> : IReadRepositoryBase<TEntity> where TEntity : class {
        Task<TEntity> GetByIdAsync(TKey key, TKey2 key2);
    }
}