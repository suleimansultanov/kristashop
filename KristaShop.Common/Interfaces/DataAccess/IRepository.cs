﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Common.Interfaces.DataAccess {
    public interface IRepositoryBase<TEntity> : IReadRepositoryBase<TEntity> where TEntity : class {
        Task<TEntity> AddAsync(TEntity entity, bool generateKey = false);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);

        Task AddRangeAsync(IEnumerable<TEntity> entities);
        void UpdateRange(IEnumerable<TEntity> entities);
        void DeleteRange(IEnumerable<TEntity> entities);
    }

    public interface IRepository<TEntity, in TKey> : IRepositoryBase<TEntity>, IReadRepository<TEntity, TKey> where TEntity : class {
        Task<TEntity> DeleteAsync(TKey id);
    }

    public interface IRepository<TEntity, in TKey, in TKey2> : IRepositoryBase<TEntity>, IReadRepository<TEntity, TKey, TKey2> where TEntity : class {
        Task<TEntity> DeleteAsync(TKey key, TKey key2);
    }
}
