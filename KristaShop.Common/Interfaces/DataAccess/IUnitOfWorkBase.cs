﻿using System;
using System.Threading.Tasks;

namespace KristaShop.Common.Interfaces.DataAccess {
    public interface IUnitOfWorkBase : IDisposable {
        bool Save();
        Task<bool> SaveAsync();
        Task<bool> SaveInTransactionAsync();
    }
}
