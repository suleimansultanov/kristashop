﻿using KristaShop.Common.Models;

namespace KristaShop.Common.Interfaces
{
    public interface ICurrentUserService
    {
        UserSession Get();
    }
}