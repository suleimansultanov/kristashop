﻿using KristaShop.Common.Models;

namespace KristaShop.Common.Interfaces
{
    public interface IFrontUserService
    {
        UserSession Get();
    }
}