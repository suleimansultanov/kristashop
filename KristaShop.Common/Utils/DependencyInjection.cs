﻿using KristaShop.Common.Interfaces;
using KristaShop.Common.Services;
using Microsoft.Extensions.DependencyInjection;

namespace KristaShop.Business.Utils
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommon(this IServiceCollection services)
        {
            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddTransient<IFrontUserService, FrontUserService>();
            return services;
        }
    }
}