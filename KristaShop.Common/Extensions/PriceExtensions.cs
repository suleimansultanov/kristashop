﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace KristaShop.Common.Extensions
{
    public static class PriceExtensions
    {
        public static string ToTwoDecimalPlaces(this double price)
        {
            var f = new NumberFormatInfo { NumberGroupSeparator = " " };
            return price.ToString("n", f);
        }
    }
}
