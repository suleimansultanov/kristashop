﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace KristaShop.Common.Extensions {
    public static class HttpClientExtensions {
        public static async Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient httpClient, T data) {
            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await httpClient.PostAsync(httpClient.BaseAddress, content);
        }

        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content) {
            var dataAsString = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(dataAsString);
        }

        public static async Task<HttpResponseMessage> PostAsync(this HttpClient httpClient) {
            var content = new StringContent(string.Empty);
            return await httpClient.PostAsync(httpClient.BaseAddress, content);
        }
    }
}