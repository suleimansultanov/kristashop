﻿using System.Text.RegularExpressions;
using Cyrillic.Convert;

namespace KristaShop.Common.Extensions {
    public static class StringExtensions {
        public static string ToValidFileName(this string name) {
            var result = name.ToRussianLatin();
            var regex = new Regex(@"[^a-zA-Z0-9\\.\\_-]");
            result = regex.Replace(result, "");
            return result;
        }

        public static string ToValidLogin(this string name) {
            var result = name.ToRussianLatin();
            var regex = new Regex(@"[^a-zA-Z0-9]");
            result = regex.Replace(result, "");
            return result;
        }
    }
}
