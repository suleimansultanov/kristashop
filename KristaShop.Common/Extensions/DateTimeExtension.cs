﻿using System;
using System.Globalization;

namespace KristaShop.Common.Extensions {
    public static class DateTimeExtension {
        public static string ToSystemString(this DateTime dateTime) {
            return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToSystemString(this DateTime? dateTime) {
            return dateTime != null ? ((DateTime)dateTime).ToString("yyyy-MM-dd HH:mm:ss") : "";
        }

        public static string ToBasicString(this DateTime dateTime) {
            return dateTime.ToString("dd.MM.yyyy");
        }

        public static string ToBasicString(this DateTime? dateTime) {
            return dateTime != null ? ((DateTime)dateTime).ToBasicString() : "";
        }

        public static string ToFullMonthString(this DateTime dateTime) {
            return dateTime.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("ru"));
        }

        public static string ToFullMonthString(this DateTime? dateTime) {
            return dateTime != null ? ((DateTime)dateTime).ToFullMonthString() : "";
        }

        public static string ToComputedString(this DateTimeOffset dateTime) {
            return dateTime.ToString("MM/dd/yyyy HH:mm:ss", CultureInfo.CreateSpecificCulture("eu"));
        }

        public static string ToComputedString(this DateTimeOffset? dateTime) {
            return dateTime != null ? ((DateTimeOffset)dateTime).ToString("MM/dd/yyyy HH:mm:ss", CultureInfo.CreateSpecificCulture("eu")) : "";
        }

    }
}
