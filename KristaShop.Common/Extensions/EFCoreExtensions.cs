﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace KristaShop.Common.Extensions
{
    public static class EFCoreExtensions
    {
        public static IQueryable<T> OrderByDynamic<T>(this IQueryable<T> q, string SortField, bool Ascending)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var prop = Expression.Property(param, SortField);
            var exp = Expression.Lambda(prop, param);
            string method = Ascending ? "OrderBy" : "OrderByDescending";
            Type[] types = new Type[] { q.ElementType, exp.Body.Type };
            var mce = Expression.Call(typeof(Queryable), method, types, q.Expression, exp);
            return q.Provider.CreateQuery<T>(mce);
        }

        public static IOrderedQueryable<T> OrderByDynamic<T, TKey>(this IQueryable<T> q, Expression<Func<T, TKey>> keySelector, bool Ascending)
        {
            if (Ascending)
                return q.OrderBy(keySelector);

            return q.OrderByDescending(keySelector);
        }

        public static IOrderedQueryable<T> ThenByDynamic<T, TKey>(this IOrderedQueryable<T> q, Expression<Func<T, TKey>> keySelector, bool Ascending)
        {
            if (Ascending)
                return q.ThenBy(keySelector);

            return q.ThenByDescending(keySelector);
        }
    }
}