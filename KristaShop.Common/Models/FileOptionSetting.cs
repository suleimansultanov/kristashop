﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KristaShop.Common.Models
{
    public class FileOptionSetting
    {
        public string ServiceUri { get; set; }
        public string FilePath { get; set; }
    }
}
