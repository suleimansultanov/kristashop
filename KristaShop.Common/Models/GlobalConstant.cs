﻿namespace KristaShop.Common.Models
{
    public static class GlobalConstant
    {
        public static class URLs
        {
            public const string PostUserData = "api/identity";
            public const string Register = "api/identity/register";
            public const string PutUserData = "api/identity/update";
            public const string PostOrderData = "api/order";
        }

        public static class SessionKeys
        {
            public const string BackUser = "user-key";
            public const string FrontUser = "user-key";

            public static string GetKeyByScheme(string scheme) {
                if (scheme.Equals("FrontendScheme")) {
                    return FrontUser;
                } else if(scheme.Equals("BackendScheme")) {
                    return BackUser;
                }

                return string.Empty;
            }
        }

        public const string SHA256_SALT = "s123Ewe265fgjghj!@#%$^&%$dwqewDcvx86357lkrtgjklhd#$";
        public const string SECRET_CODE = "u3og=ckxhb?YSCoMA;tFfP!Q$l4}Of?HfdHG4?<GM75]A2o;^,SHF)";
        public const string CRYPTO_SALT = "7q3t45msdrWSE$%Ywtwy4%&#E%^*UE%^uied56i8ehasedfghbZXDT";
        public const double Epsilon = 0.01;
    }
}