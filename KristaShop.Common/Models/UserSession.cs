﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;

namespace KristaShop.Common.Models
{
    public class UserSession
    {
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public bool IsRoot { get; set; }
        public int AccessLevel { get; set; }
        public bool IsSignByLink { get; set; }
        public AuthorizationLinkType LinkType { get; set; }
        public string LinkCode { get; set; }
        public ClientDTO CounterParty { get; set; }
        public List<GroupDTO> UserGroups { get; set; }

        public bool IsSignedInForChangePassword => IsSignByLink && LinkType == AuthorizationLinkType.ChangePassword;
    }

    public class ClientDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public string ClientFullName { get; set; }
        public string CityName { get; set; }
        public string PhoneNumber { get; set; }
        public string ShopName { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public double DiscountPrice { get; set; }
    }

    public class GroupDTO
    {
        public Guid GroupId { get; set; }
        public int UserType { get; set; }
        public string Name { get; set; }
        public int AccessLevel { get; set; }
    }
}