﻿namespace KristaShop.Common.Models {
    public class LookUpItem<TKey, TValue> {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public LookUpItem(TKey key, TValue value) {
            Key = key;
            Value = value;
        }
    }
}
