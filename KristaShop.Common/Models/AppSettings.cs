﻿using KristaShop.Common.Interfaces;

namespace KristaShop.Common.Models {
    public class AppSettings : IAppSettings {
        // Dynamic Pages Url settings
        public string TermsOfUse { get; set; } = string.Empty;
        public string DeliveryDetails { get; set; } = string.Empty;
        public string PaymentDetails { get; set; } = string.Empty;
        public string FooterContacts { get; set; } = string.Empty;
        public string CategoriesDescription { get; set; } = string.Empty;
        public string OpenCatalogSearchDescription { get; set; } = string.Empty;

        // Social Url settings
        public string KristaInstagram { get; set; } = string.Empty;
        public string KristaVk { get; set; } = string.Empty;
        public string KristaFacebook { get; set; } = string.Empty;
        public string KristaYoutube { get; set; } = string.Empty;
        public string KristaYoutubeSubscribe { get; set; } = string.Empty;

        // Messages
        public string CartSuccess { get; set; }
    }
}
