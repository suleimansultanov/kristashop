﻿using System.Net;

namespace KristaShop.Common.Models.ApiResponses {
    public class BaseResponse {
        public int StatusCode { get; set; }
        public string SystemMessage { get; set; }
        public string ReadableMessage { get; set; }
        public bool HasReadableMessage { get; set; }

        public BaseResponse() { }

        public BaseResponse(int statusCode, string systemMessage, string readableMessage = "") {
            StatusCode = statusCode;
            SystemMessage = systemMessage;
            ReadableMessage = readableMessage;
            HasReadableMessage = !string.IsNullOrEmpty(ReadableMessage);
        }

        public BaseResponse(HttpStatusCode statusCode, string systemMessage, string readableMessage = "") {
            StatusCode = (int) statusCode;
            SystemMessage = systemMessage;
            ReadableMessage = readableMessage;
            HasReadableMessage = !string.IsNullOrEmpty(ReadableMessage);
        }
    }
}
