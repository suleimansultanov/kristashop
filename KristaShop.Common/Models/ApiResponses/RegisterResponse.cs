﻿using System;
using System.Net;

namespace KristaShop.Common.Models.ApiResponses {
    public class RegisterResponse : BaseResponse {
        public Guid UserId { get; set; }

        public RegisterResponse() { }

        public RegisterResponse(int statusCode, Guid userId, string systemMessage, string readableMessage = "")
            : base(statusCode, systemMessage, readableMessage) {
            UserId = userId;
        }
        public RegisterResponse(HttpStatusCode statusCode, Guid userId, string systemMessage, string readableMessage = "")
            : base(statusCode, systemMessage, readableMessage) {
            UserId = userId;
        }
    }
}
