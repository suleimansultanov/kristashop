﻿using KristaShop.Common.Helpers;
using System.Security.Cryptography;
using System.Text;

namespace KristaShop.Common.Models
{
    public class BaseHashModel
    {
        public string HashCode { get; set; }
    }

    public static class HashRequestData
    {
        public static string ComputeSha256Hash(this string data)
        {
            return HashHelper.CalculateSha256Hash(data + GlobalConstant.SHA256_SALT);
        }
    }
}