﻿namespace KristaShop.Common.Models {
    public class KristaApiUrls {
        public string KristaWebApiRegister { get; set; }
        public string KristaWebApiActivateUser { get; set; }
        public string KristaWebApiBanUser { get; set; }
        public string KristaWebApiUpdateUser { get; set; }
    }
}
