﻿using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Utils;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace KristaShop.Common.Services
{
    public class EmailService : IEmailService
    {
        private readonly ILogger<EmailMessage> _logger;
        private readonly EmailsSetting _emailsSetting;

        public EmailService(IOptions<EmailsSetting> options, ILogger<EmailMessage> logger) {
            _logger = logger;
            _emailsSetting = options.Value;
        }

        public async Task SendEmailAsync(EmailMessage emailMessage) {
            try {
                MimeMessage mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress("Администрация Krista", _emailsSetting.FromEmailAddress));
                mimeMessage.To.Add(new MailboxAddress(emailMessage.ToEmailAddress, emailMessage.ToEmailAddress));
                mimeMessage.Subject = emailMessage.Subject + $" {emailMessage.ToEmailAddress}";
                mimeMessage.Importance = MessageImportance.High;
                mimeMessage.MessageId = MimeUtils.GenerateMessageId();
                mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = $"<h4>Уважаемый {emailMessage.ToEmailAddress},</h4><br>" + emailMessage.Content };

                using SmtpClient client = new SmtpClient();
                await client.ConnectAsync(_emailsSetting.Host, _emailsSetting.Port, _emailsSetting.EnableSSL);
                await client.AuthenticateAsync(_emailsSetting.Login, _emailsSetting.Password);
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }
            catch (Exception ex) {
                _logger.LogError("Failed to send email. {ex}.", ex);
            }
        }
    }
}