﻿using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Security.Claims;

namespace KristaShop.Common.Services
{
    public class FrontUserService : IFrontUserService
    {
        private UserSession User { get; }

        public FrontUserService(IHttpContextAccessor httpContextAccessor)
        {
            var authenticateResult = httpContextAccessor.HttpContext?.AuthenticateAsync("FrontendScheme").Result;
            if (authenticateResult.Succeeded)
            {
                string userData = authenticateResult.Principal.FindFirstValue(GlobalConstant.SessionKeys.FrontUser);
                if (!string.IsNullOrEmpty(userData))
                    User = JsonConvert.DeserializeObject<UserSession>(userData);
            }
        }

        public UserSession Get() => User;
    }
}