﻿using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Security.Claims;

namespace KristaShop.Common.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private UserSession User { get; }

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            var authenticateResult = httpContextAccessor.HttpContext?.AuthenticateAsync("BackendScheme").Result;
            if (authenticateResult.Succeeded)
            {
                string userData = authenticateResult.Principal.FindFirstValue(GlobalConstant.SessionKeys.BackUser);
                if (!string.IsNullOrEmpty(userData))
                    User = JsonConvert.DeserializeObject<UserSession>(userData);
            }
            //    string userData = httpContextAccessor.HttpContext?.User?.FindFirstValue(GlobalConstant.SessionKeys.BackUser);
            //    if (!string.IsNullOrEmpty(userData))
            //        User = JsonConvert.DeserializeObject<UserSession>(userData);
        }

        public UserSession Get() => User;
    }
}