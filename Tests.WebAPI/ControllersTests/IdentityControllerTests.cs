using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.Common.Models;
using KristaShop.Common.Models.ApiResponses;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.Models;
using KristaShop.ServicesAsup.DTOs.Mappings;
using KristaShop.ServicesAsup.Implementation.Services;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using KristaShop.WebAPI.Controllers;
using KristaShop.WebAPI.Utils;
using KristaShop.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NUnit.Framework;
using Tests.Common;
using Tests.Common.Stubs;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;

namespace Tests.WebAPI.ControllersTests {
    public class Tests {
        private IUnitOfWork _uow;
        private KristaAsupDbContext _context;
        private IUserService _userService;
        private IdentityController _identityController;

        [SetUp]
        public async Task Setup() {
            _context = InMemory.InitializeAsupDbDev();
            await _initializeDbContextDataAsync();

            var mapper = _initializeMapper();
            _uow = new UnitOfWork(_context, null);

            var registerService = new RegisterService(_uow, mapper, new EmailServiceStub(), new SerilogLoggerStub());
            _userService = new UserService(_uow, registerService);
            _identityController = new IdentityController(null, _userService, mapper, new SerilogLoggerStub());

        }

        [TearDown]
        public void Dispose() {
            _uow.Dispose();
            _context = null;
        }

        #region Initialization

        private static IMapper _initializeMapper() {
            var configuration = new MapperConfiguration(cfg => {
                cfg.AddProfile<MappingProfile>();
                cfg.AddProfile<CounterpartyMappingProfile>();
            });
            return configuration.CreateMapper();
        }

        private async Task _initializeDbContextDataAsync() {
            _context.Cities.AddRange(new List<City> {
                new City(new Guid("eb5c2b4b-5010-46b4-b1b7-b9fc9385b759"), "������"),
                new City(new Guid("da2b016e-07d8-4a53-84dd-f4c5c7c55bb6"), "�����-���������")
            });

            await _context.SaveChangesAsync();
        }
           
        private async Task _initializeOneManagerWithRate1Async() {
            var managerRate = new ManagerRate {UserId = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Rate = 1};
            var manager = new User {
                Id = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Acl = 20, Email = "manager@email.test", IsRoot = false,
                Login = "manager"
            };

            await _context.Users.AddAsync(manager);
            await _context.ManagerRates.AddAsync(managerRate);
        }

        private async Task _initializeAclsAndUserGroupsForClientAndManagerAsync() {
            var acls = new List<AccessControl> {
                new AccessControl {Id = new Guid("69e0ae8c-65bc-463a-8de4-84c2aabf00a7"), Acl = 10, AclName = "������"},
                new AccessControl {Id = new Guid("d3d3a490-d72a-483a-8163-2005b791d919"), Acl = 20, AclName = "��������"},
            };
            _context.AccessControls.AddRange(acls);

            var groups = new List<UserGroup> {
                new UserGroup {
                    Id = new Guid("d29bfacb-6b85-41f5-a9ca-33b5d38e1b05"),
                    AclId = new Guid("69e0ae8c-65bc-463a-8de4-84c2aabf00a7"), Type = UserType.Customer, GroupName = "�������"
                },
                new UserGroup {
                    Id = new Guid("a9768b03-3cfa-4e65-b9f5-53961e409ad6"),
                    AclId = new Guid("eadfa5e1-d3fc-47bb-abe9-44e39bead900"), Type = UserType.Manager, GroupName = "���������"
                }
            };
            await _context.UserGroups.AddRangeAsync(groups);
        }

        private async Task _initializeUsersAsync() {
            var users = new List<User> {
                new User { Id = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08"), Acl = 10, IsRoot = false, Login = "user1", Status = UserStatus.Await},
                new User { Id = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84"), Acl = 10, IsRoot = false, Login = "user2", Status = UserStatus.Await, CounterpartyId = new Guid("4d41925b-8738-407f-b61e-e2f56b009778")},
            };


            var counterparties = new List<Counterparty> {
                new Counterparty { Id = new Guid("4d41925b-8738-407f-b61e-e2f56b009778"), Title = "��������� ������", Type = 0, Person = "��������� ������", Phone="+79912341233", Email = "pushkin@email.test", MallAddress = "������, ��. ����������, �� ������", CompanyAddress = "������, ��. ����������", NewCity = "������"},
            };

            await _context.Counterparties.AddRangeAsync(counterparties);
            await _context.Users.AddRangeAsync(users);
            await _context.SaveChangesAsync();
        }

        #endregion

        #region Registration tests

        [Test]
        public void When_ModelStateIsInvalid_Expect_RegisterReturnsBadRequestResponse() {

            var registrationModel = new RegistrationViewModel();

            _identityController.ModelState.AddModelError("Test", "Test error");
            var result = (BadRequestObjectResult) _identityController.Register(registrationModel, "6cc68279dfcc21cef9fea66e609f30ff5ac1d84d702bbfb1e75b12fde61ce055").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreEqual("Test error", actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public void When_ValidateInvalidModelState_Expect_RegisterReturnsBadRequestResponse() {

            var registrationModel = new RegistrationViewModel();

            var context = new ValidationContext(registrationModel, null, null);
            var results = new List<ValidationResult>();
            var isModelStateValid = Validator.TryValidateObject(registrationModel, context, results, true);

            Assert.AreEqual(false, isModelStateValid);
            Assert.AreEqual(3, results.Count);
        }

        [Test]
        public void When_ViewModelIsEmpty_Expect_RegisterReturnsBadRequestResponse() {
            var registrationModel = new RegistrationViewModel();

            var result = (BadRequestObjectResult) _identityController.Register(registrationModel, "6cc68279dfcc21cef9fea66e609f30ff5ac1d84d702bbfb1e75b12fde61ce055").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public void When_ViewModelValidAndManagersListEmpty_Expect_RegisterReturnsBadRequestResponse() {
            var registrationModel = new RegistrationViewModel {
                NewCity = "������",
                Person = "��������� ������",
                Phone = "+773000192393"
            };

            var result = (BadRequestObjectResult) _identityController.Register(registrationModel, "6cc68279dfcc21cef9fea66e609f30ff5ac1d84d702bbfb1e75b12fde61ce055").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_ModelIsValid_Expect_RegisterReturnsOkResponse() {
            await _initializeOneManagerWithRate1Async();
            await _initializeAclsAndUserGroupsForClientAndManagerAsync();
            await _context.SaveChangesAsync();

            var registrationModel = new RegistrationViewModel {
                NewCity = "������",
                Person = "��������� ������",
                Phone = "+77300019239"
            };

            var result = (OkObjectResult) _identityController.Register(registrationModel, "6cc68279dfcc21cef9fea66e609f30ff5ac1d84d702bbfb1e75b12fde61ce055").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(200, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_ModelIsValidAndUserHashAlreadyExist_Expect_RegisterReturnsOkResponse() {
            _context.WebApiRequests.Add(new WebApiRequest("768747447d8557fe296797d89be3e54b6098ce1b47541cb72d04df62f47d048a"));
            await _context.SaveChangesAsync();

            var registrationModel = new RegistrationViewModel {
                NewCity = "������",
                Person = "��������� ������",
                Phone = "+77300019239"
            };

            var result = (OkObjectResult) _identityController.Register(registrationModel, "768747447d8557fe296797d89be3e54b6098ce1b47541cb72d04df62f47d048a").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(200, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public void When_HashIsInvalid_Expect_RegisterReturnsBadRequestResponse() {
            var registrationModel = new RegistrationViewModel {
                NewCity = "������",
                Person = "��������� ������",
                Phone = "+77300019239"
            };

            var result = (BadRequestObjectResult) _identityController.Register(registrationModel, "d84d702bbfb1e75b12fde61ce055").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(false, actual.HasReadableMessage);
            Assert.AreEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        #endregion

        #region Activate user tests

        [Test]
        public void When_UserNotExist_Expect_ActivateReturnsBadRequestResponse() {
            var userId = new Guid("9547903a-437f-44af-b932-9f6856e3f14c");
            var hash = JsonConvert.SerializeObject(userId.ToString().ComputeSha256Hash());
            var result = (BadRequestObjectResult) _identityController.ActivateUser(userId, hash).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_UserExist_Expect_ActivateReturnsOkResponse() {
            await _initializeUsersAsync();
            await _context.SaveChangesAsync();
            var userId = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08");
            var hash = JsonConvert.SerializeObject(userId.ToString().ComputeSha256Hash());
            var result = (OkObjectResult) _identityController.ActivateUser(userId, hash).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(200, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreEqual(string.Empty, actual.SystemMessage);
        }

        #endregion

        #region Ban user tests

        [Test]
        public void When_UserNotExist_Expect_BanUserReturnsBadRequestResponse() {
            var banModel = new BanViewModel {UserId = new Guid("9547903a-437f-44af-b932-9f6856e3f14c")};
            var result = (BadRequestObjectResult) _identityController.BanUser(banModel).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_UserExist_Expect_BanUserReturnsOkResponse() {
            await _initializeUsersAsync();
            await _context.SaveChangesAsync();

            var banModel = new BanViewModel {UserId = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08"), Reason = "Test reason",ExpireDate = DateTime.Now.AddDays(7)};
            var result = (OkObjectResult) _identityController.BanUser(banModel).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(200, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreEqual(string.Empty, actual.SystemMessage);
        }

        #endregion

        #region Ban user tests

        [Test]
        public void When_UserNotExist_Expect_UpdateReturnsBadRequestResponse() {
            var userModel = new UpdateUserViewModel {Id = new Guid("9547903a-437f-44af-b932-9f6856e3f14c")};
            var hash = JsonConvert.SerializeObject(userModel).ComputeSha256Hash();
            var result = (BadRequestObjectResult) _identityController.UpdateUser(userModel, hash).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_UserExist_Expect_UpdateUserReturnsOkResponse() {
            await _initializeUsersAsync();
            await _context.SaveChangesAsync();

            var userId = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var now = DateTime.Now.AddDays(7);
            var userModel = new UpdateUserViewModel {Id = userId, Login = "user5NewLogin", Password = password, Person = "��������� ��������� ������", Phone = "+79900000015", Email = "pushkin.a@mail.test", MallAddress = "�� ������", CompanyAddress = "������, ��. �����������", Status = UserStatus.Banned, BanReason = "Test reason", BanExpireDate = now};
            var hash = JsonConvert.SerializeObject(userModel).ComputeSha256Hash();
            var result = (OkObjectResult) _identityController.UpdateUser(userModel, hash).Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(200, actual.StatusCode);
            Assert.AreEqual(true, actual.HasReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.ReadableMessage);
            Assert.AreEqual(string.Empty, actual.SystemMessage);
        }

        [Test]
        public async Task When_InvalidHash_Expect_UpdateUserReturnsBadRequestResponse() {
            await _initializeUsersAsync();
            await _context.SaveChangesAsync();

            var userId = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var now = DateTime.Now.AddDays(7);
            var userModel = new UpdateUserViewModel {Id = userId, Login = "user5NewLogin", Password = password, Person = "��������� ��������� ������", Phone = "+79900000015", Email = "pushkin.a@mail.test", MallAddress = "�� ������", CompanyAddress = "������, ��. �����������", Status = UserStatus.Banned, BanReason = "Test reason", BanExpireDate = now};

            var result = (BadRequestObjectResult) _identityController.UpdateUser(userModel, "352e5fed4d6828d3cd6eeb615").Result;
            var actual = (BaseResponse) result.Value;

            Assert.AreEqual(400, actual.StatusCode);
            Assert.AreEqual(false, actual.HasReadableMessage);
            Assert.AreEqual(string.Empty, actual.ReadableMessage);
            Assert.AreNotEqual(string.Empty, actual.SystemMessage);
        }

        #endregion
    }
}