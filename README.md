## KRISTA SHOP SITE
На сервере **new.krista.fashion** проект лежит в папке ```/var/www/krista```
Для его запуска/остановки на сервере нужно выполнить команды:

* systemctl start krista.service
* systemctl stop krista.service

При деплое приложения содержимое папки ```/var/www/krista``` можно полностью удалять
Все файлы которые загружаются через приложения лежат в папке ```/var/www/krista-files```, папка **krista-files** содержит следующие папки:

* **colors** - изображения цветов моделей, она синхронизируется с серверером на котором стоит файловый сервис
* **files** - файлы прикрепленные к документам, здесь кешируются скачанные с файлового сервиса файлы документов
* **cache** - в данной папке кэшируются все изображения которые проходят через resize middleware приложения
* **galleryphotos** - здесь храняться все изображения которые загружаются через приложение
* **fileserver** - здесь храняться все изображения которые загружаются в хранилище wysiswyg редактора админки

Поскольку данные папки с файлами лежат не в проекте, для корректной работыы приложения, к ним настроены **location пути в nginx**:

```
location /galleryphotos {
 if ($is_args = "") {
  root /var/www/krista-files;
  break;
 }
 if ($is_args != "") {
  proxy_pass http://localhost:5000;
 }
}

location /colors {
 root /var/www/krista-files;
}

location /fileserver {
root /var/www/krista-files;
}
```

---

## KRISTA WEB API

**Web API лежит** на том же сервере где и файл сервис **в папке** ```/opt/krista-shop-web-api```
Для его запуска/остановки на сервере нужно выполнить команды:

* systemctl start kristashopdotnet.service
* systemctl stop kristashopdotnet.service

При деплое приложения содержимое папки ```/opt/krista-shop-web-api``` можно полностью удалять

---

## Переменные окружения

Конфиги приложений лежат в файлах ```appsettings.Development.json``` и ```appsettings.Production.json```
какой конфиг использовать **выбирается в зависимости от указанных переменных окружения**
для того чтобы задать environment переменную для всей машины нужно в **файле ```/etc/environment``` прописать**:

* ASPNETCORE_ENVIRONMENT=Production - для продакшн сервера
* ASPNETCORE_ENVIRONMENT=Development - для девелоп сервера

*если ASPNETCORE_ENVIRONMENT не сработает, то нужно прописать DOTNET_ENVIRONMENT, с такими же значениями*

Также, переменные **можно прописать в самом сервисе krista.service**, это нужно для того, чтобы переменная 
окружения была доступна только на уровне данного сервиса а не всей машины это делается следующим образом:

* ENVIRONMENT=ASPNETCORE_ENVIRONMENT=Production

---

## МИГРАЦИИ

Для выполнения миграций нужно стянуть проект с репозитория на сервер (сделать git clone если проекта еще нет на сервере)
На сервере **исходники** проекта лежат в папке ```/var/www/repository/krista_web_api```, нужно перейти в нее и выполнить команду **git pull**
Чтобы выполнить миграцию нужно перейти в папку ```var/www/repository/KristaShop.DataAccess``` и выполнить команду:

```
dotnet ef database update -s ../KristaShop.WebUI -c KristaShopDbContext 
```

Для того чтобы миграция выполнилась корретно нужно чтобы **на уровне машины** стояла **переменная окружения**
```ASPNETCORE_ENVIRONMENT=Production``` (Или DOTNET_ENVIRONMENT=Production), это нужно потому, что при выполнении миграций нужно подключаться к БД и для этого нужно подтянуть правильный конфиг.

---

## DEPLOY

Для деплоя нужно выполнить **publish** проекта, это можно сделать из Visual Studio
Правой кнопкой мыши по KristaShop.WebUI => выбрать Publish
Выбрать публиковать в папку (Folder profile) **с настройками**:

* Configuration: release
* Target framework: netcoreapp3.1
* Target runtime: linux-x64

после нажать Publish. Проект опубликуется в папку ```KristaShop.WebUI\bin\Release\netcoreapp3.1\publish```
содержимое данной папки необходимо **заархивировать в .zip** архив (например, с названием publish.zip)
перенести архив через SFTP (например, через Filezilla) на сервер в папку ```/var/www/krista```
на сервере перейти в эту папку и выполнить команду ```unzip -o publish.zip```

Перед выполнением этой команды нужно остановить сервис приложения ```systemctl stop krista.service```
После выполнения запустить сервис приложения ```systemctl start krista.service```

Для выполнения publish из командной строки, находять в папке с исходным кодом проекта можно выполнить данную команду:
```
dotnet publish KristaShop.WebUI --configuration Release --runtime linux-x64 --framework netcoreapp3.1
```

---

## Доступ к репозиторию на сервере

Для того **чтобы стянуть исходный код** проекта на сервер new.krista.fashion через git **НЕ НУЖНО логиниться** со своего аккаунта. Там реализован доступ по ключу [Bitbucket Access Key](https://confluence.atlassian.com/bitbucketserver/ssh-access-keys-for-system-use-776639781.html)
который дает только **readonly доступ** к репозиторию, т.е. с сервера нельзя делать push в репозиторий. Private ключ лежит на сервере в папке ```/home/kpalych/.ssh```, а public ключ лежит в настройках репозитория в разделе Access Keys.

---

## Структура solution (решения)

Solution состоит из следующих проектов:

* **KristaShop.WebUI** - UI и админка приложения
* **KristaShop.WebAPI** - API приложения (содержит отдельный контекст доступа к БД krista_asup_db для чтения и записи)
* **KristaShop.Business** - Бизнес логика приложения
* **KristaShop.Common** - Общие классы проекты/хелперы
* **KristaShop.DataAccess** - Контекст доступа к БД krista_shop
* **KristaShop.DataReadOnly** - Readonly контекст доступа к реплике БД krista_asup_db


## CSRF защита
В новых версиях .net core CSRF защиту, которую использует AntiForgeryToken нужно подключать вручную в методе ConfigureServices в Startup.cs, из коробки она работает не всегда. Для работы защиты необходимо сохранять ключи. В KristaShop.WebUI ключи сохраняются в директорию на диске и шифруются с помощью pfx сертификата. В appsettings.json есть раздел DataProtection, где указываются такие настройки как:

* **KeysDirectory** - путь к папке, куда будут сохраняться ключи
* **CertificatePath** - путь к сертификату, по которому будут шифроваться ключи
* **EnvironmentVariableWithCertificatePassword** - название переменной окружения в которой будет храниться пароль от сертификата
```
 "DataProtection": {
    "KeysDirectory": "/folder",
    "CertificatePath": "/folder/certificate.pfx",
    "EnvironmentVariableWithCertificatePassword": "VARIABLE_NAME"
  },
```