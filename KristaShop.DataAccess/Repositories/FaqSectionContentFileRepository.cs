﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.DataAccess.Repositories
{
    public class FaqSectionContentFileRepository : Repository<FaqSectionContentFile, Guid>, IFaqSectionContentFileRepository<FaqSectionContentFile, Guid>
    {
        public FaqSectionContentFileRepository(DbContext context) : base(context) { }
    }
}
