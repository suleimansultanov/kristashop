﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.DataAccess.Repositories {
    public class MenuContentRepository : Repository<MenuContent, Guid>, IMenuContentRepository<MenuContent, Guid> {
        public MenuContentRepository(DbContext context) : base(context) { }

        public override IOrderedQueryable<MenuContent> AllOrdered => All.OrderBy(x => x.Order);

        public IQueryable<MenuContent> GetAllOrderedOpenAndVisibleInMenuOnly(bool openOnly, bool visibleInMenuOnly) {
            var request = visibleInMenuOnly ? AllOrdered.Where(x => x.IsVisibleInMenu) : AllOrdered;
            return openOnly ? request.Where(x => x.IsOpen) : request;
        }

        public async Task<int> GetNewOrderValueAsync() {
            return await All.MaxAsync(x => (int?) x.Order) + 1 ?? 1;
        }
    }
}
