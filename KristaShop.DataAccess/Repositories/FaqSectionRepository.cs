﻿using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KristaShop.DataAccess.Repositories
{
    public class FaqSectionRepository : Repository<FaqSection, Guid>, IFaqSectionRepository<FaqSection, Guid>
    {
        public FaqSectionRepository(DbContext context) : base(context) { }
        public override IOrderedQueryable<FaqSection> AllOrdered => All.Include(z => z.FaqSectionContents).OrderBy(x => x.Id);

        public FaqSection GetFaqSectionByIdIncluding(Guid id)
        {
            return AllOrdered.FirstOrDefault(x => x.Id.Equals(id));
        }

        public async Task<List<FaqSection>> GetFaqSections(Guid id)
        {
            return await AllOrdered.Where(z => z.FaqId.Equals(id)).Include(z=>z.FaqSectionContents).ThenInclude(z=>z.FaqSectionContentFiles).ToListAsync();
        }
    }
}
