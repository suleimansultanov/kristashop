﻿using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KristaShop.DataAccess.Repositories
{
    public class FaqSectionContentRepository : Repository<FaqSectionContent, Guid>, IFaqSectionContentRepository<FaqSectionContent, Guid>
    {
        public FaqSectionContentRepository(DbContext context) : base(context) { }

        public async Task<List<FaqSectionContent>> GetFaqSectionContent(Guid id)
        {
            return await All.Where(x => x.FaqSectionId.Equals(id)).Include(z=>z.FaqSectionContentFiles).ToListAsync();
        }
    }
}
