﻿using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace KristaShop.DataAccess.Domain.Seeds
{
    internal static class CategorySeeds
    {
        internal static ModelBuilder CategoryBuilder(this ModelBuilder builder)
        {
            builder.Entity<Category>().HasData(
                new Category[]
                {
                    new Category { id = Guid.NewGuid(), name = "Комбиенезоны", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Универсальные", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Для пышных", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Аксессуары", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Выпускные", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Вечерние", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Для мам", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Свадебные", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Подругам невесты", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Дуэт", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Накидки", is_visible = true },
                    new Category { id = Guid.NewGuid(), name = "Коктейльные", is_visible = true }
                });

            return builder;
        }
    }
}