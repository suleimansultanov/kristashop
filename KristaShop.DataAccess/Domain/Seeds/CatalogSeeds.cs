﻿using KristaShop.Common.Enums;
using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace KristaShop.DataAccess.Domain.Seeds
{
    internal static class CatalogSeeds
    {
        internal static ModelBuilder CatalogBuilder(this ModelBuilder builder)
        {
            builder.Entity<Catalog>().HasData(
                new Catalog[]
                {
                    new Catalog { id = Guid.NewGuid(), name = "Распродажа", uri = "instock", order_form = (int)OrderFormType.Preorder, is_visible = true, order = 1 },
                    new Catalog { id = Guid.NewGuid(), name = "Распродажа не сериями", uri = "instock", order_form = (int)OrderFormType.InStock, is_visible = true, order = 2 },
                    new Catalog { id = Guid.NewGuid(), name = "Аксессуары", uri = "instock", order_form = (int)OrderFormType.InStock, is_visible = true, order = 3 },
                    new Catalog { id = Guid.NewGuid(), name = "В наличие", uri = "instock", order_form = (int)OrderFormType.InStock, is_visible = true, order = 4 }
                });

            return builder;
        }
    }
}