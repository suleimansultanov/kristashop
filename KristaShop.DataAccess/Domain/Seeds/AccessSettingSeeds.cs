﻿using KristaShop.Common.Enums;
using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using KristaShop.DataAccess.Domain.Seeds.Common;

namespace KristaShop.DataAccess.Domain.Seeds
{
    internal class AccessSettingSeeds : ISeeds {
        public ModelBuilder Seed(ModelBuilder builder) {
            builder.Entity<MenuItem>()
                .HasData(
                    new MenuItem {
                        id = new Guid("9b6bedbe-6fce-4a6a-919e-511a6c299e6c"),
                        title = "Главная",
                        action_name = "Index",
                        controller_name = "Admin/Home",
                        icon = "fa-home",
                        menu_type = (int) MenuType.Admin,
                        order = 1
                    },
                    new MenuItem {
                        id = new Guid("dcdf01a1-78ac-4065-af38-61e2d0d226f8"),
                        title = "Пункты меню",
                        action_name = "Index",
                        controller_name = "Admin/Menu",
                        icon = "fa-bars",
                        menu_type = (int) MenuType.Admin,
                        order = 2
                    },
                    new MenuItem {
                        id = new Guid("cc40cee9-2a35-4c9c-b629-7ef180166119"),
                        title = "Контент страниц",
                        action_name = "Index",
                        controller_name = "Admin/MBody",
                        icon = "fa-file-code",
                        menu_type = (int) MenuType.Admin,
                        order = 3
                    },
                    new MenuItem {
                        id = new Guid("0b029a4c-a06d-4109-8da8-bc1375c0357f"),
                        title = "Каталоги",
                        action_name = "Index",
                        controller_name = "Admin/Catalog",
                        icon = "fa-th",
                        menu_type = (int) MenuType.Admin,
                        order = 4
                    },
                    new MenuItem {
                        id = new Guid("5c3c902b-6461-4f82-a4dd-85e062180a97"),
                        title = "Категории",
                        action_name = "Index",
                        controller_name = "Admin/Category",
                        icon = "fa-tags",
                        menu_type = (int) MenuType.Admin,
                        order = 5
                    },
                    new MenuItem {
                        id = new Guid("b48f0234-cfed-4c22-b820-f7790382b19c"),
                        title = "Модели",
                        action_name = "Index",
                        controller_name = "Admin/CModel",
                        icon = "fa-eye",
                        menu_type = (int) MenuType.Admin,
                        order = 6
                    },
                    new MenuItem {
                        id = new Guid("eaf89685-c81c-45a2-aaa3-0edd9d3b9769"),
                        title = "Скидки",
                        action_name = "Index",
                        controller_name = "Admin/Discount",
                        icon = "fa-percent",
                        menu_type = (int) MenuType.Admin,
                        order = 7
                    },
                    new MenuItem {
                        id = new Guid("67ff1ca0-fbdd-44c8-8c88-4ba1e09b739c"),
                        title = "Блог",
                        action_name = "Index",
                        controller_name = "Admin/Blog",
                        icon = "fa-blog",
                        menu_type = (int) MenuType.Admin,
                        order = 8
                    },
                    new MenuItem {
                        id = new Guid("c5c26b3a-9bf8-42d4-9f3b-95f848cf428e"),
                        title = "Галерея",
                        action_name = "Index",
                        controller_name = "Admin/Gallery",
                        icon = "fa-images",
                        menu_type = (int) MenuType.Admin,
                        order = 9
                    },
                    new MenuItem {
                        id = new Guid("5e1ca256-4fda-4347-809d-03ee27f1cca9"),
                        title = "Сообщения",
                        action_name = "Index",
                        controller_name = "Admin/Feedback",
                        icon = "fa-mail-bulk",
                        menu_type = (int) MenuType.Admin,
                        order = 10
                    },
                    new MenuItem {
                        id = new Guid("2fa767b8-cdf3-47e1-a536-ea5b59ff6cd8"),
                        title = "Баннер",
                        action_name = "Index",
                        controller_name = "Admin/Banner",
                        icon = "fa-bullhorn",
                        menu_type = (int) MenuType.Admin,
                        order = 12
                    },
                    new MenuItem {
                        id = new Guid("1784ba0c-c201-4a9c-8afe-526b78101242"),
                        title = "Видеогалерея",
                        action_name = "Index",
                        controller_name = "Admin/VideoGallery",
                        icon = "fa-film",
                        menu_type = (int)MenuType.Admin,
                        order = 13
                    },
                    new MenuItem {
                        id = new Guid("fada87c8-3065-4420-b318-2fb5cc942ef6"),
                        title = "Доступ по URL",
                        action_name = "Index",
                        controller_name = "Admin/UrlAcl",
                        icon = "fa-link",
                        menu_type = (int) MenuType.Admin,
                        order = 100
                    },
                    new MenuItem {
                        id = new Guid("e094bf47-e8b5-4732-84ec-5970437f48c5"),
                        title = "Настройки",
                        action_name = "Index",
                        controller_name = "Admin/Settings",
                        icon = "fa-cog",
                        menu_type = (int)MenuType.Admin,
                        order = 120
                    },
                    new MenuItem {
                        id = new Guid("1d56e74b-9c90-4000-b673-66a6fb90fb9a"),
                        title = "Воронка",
                        action_name = "Index",
                        controller_name = "Admin/Faq",
                        icon = "fa-info",
                        menu_type = (int)MenuType.Admin,
                        order = 140
                    }
              );

            return builder;
        }
    }
}