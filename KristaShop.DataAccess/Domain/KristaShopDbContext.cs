﻿using System;
using System.Diagnostics;
using KristaShop.Common.Extensions;
using KristaShop.DataAccess.Configurations;
using KristaShop.DataAccess.Domain.Seeds;
using KristaShop.DataAccess.Domain.Seeds.Common;
using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.DataAccess.Domain
{
    public class KristaShopDbContext : DbContext
    {
        public KristaShopDbContext (DbContextOptions<KristaShopDbContext> options)
               : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.InitializeMysqlCustomFunctions();

            builder.ApplyConfiguration(new AuthorizationLinkConfiguration());
            builder.ApplyConfiguration(new MenuContentConfiguration());
            builder.ApplyConfiguration(new BlogItemConfiguration());
            builder.ApplyConfiguration(new CatalogDiscountConfiguration());
            builder.ApplyConfiguration(new GalleryItemConfiguration());
            builder.ApplyConfiguration(new FeedbackConfiguration());
            builder.ApplyConfiguration(new FeedbackFilesConfiguration());
            builder.ApplyConfiguration(new NomenclatureDiscountConfiguration());
            builder.ApplyConfiguration(new NomDiscountCatalogConfiguration());
            builder.ApplyConfiguration(new SettingsConfiguration());
            builder.ApplyConfiguration(new VideoGalleryConfiguration());
            builder.ApplyConfiguration(new VideoConfiguration());
            builder.ApplyConfiguration(new VideoGalleryVideosConfiguration());
            builder.ApplyConfiguration(new UserDataConfiguration());
            builder.ApplyConfiguration(new FaqConfiguration());
            builder.ApplyConfiguration(new FaqSectionConfiguration());
            builder.ApplyConfiguration(new FaqSectionContentConfiguration());
            builder.ApplyConfiguration(new UserDiscountConfiguration());

            //builder.Entity<NomCatalog>()
            //    .HasKey(nc => new { nc.nom_id, nc.catalog_id });
            builder.Entity<NomCatalog>()
                .HasOne(nc => nc.Nomenclature)
                .WithMany(c => c.NomCatalogs)
                .HasForeignKey(nc => nc.nom_id);
            builder.Entity<NomCatalog>()
                .HasOne(nc => nc.Catalog)
                .WithMany(c => c.NomCatalogs)
                .HasForeignKey(nc => nc.catalog_id);

            //builder.Entity<NomCategory>()
            //    .HasKey(nc => new { nc.nom_id, nc.category_id });
            builder.Entity<NomCategory>()
                .HasOne(nc => nc.Nomenclature)
                .WithMany(c => c.NomCategories)
                .HasForeignKey(nc => nc.nom_id);
            builder.Entity<NomCategory>()
                .HasOne(nc => nc.Category)
                .WithMany(c => c.NomCategories)
                .HasForeignKey(nc => nc.category_id);

            builder.Entity<VisibleNomUser>()
                .HasOne(nc => nc.Nomenclature)
                .WithMany(c => c.VisibleNomUsers)
                .HasForeignKey(nc => nc.nom_id);

            builder.Entity<NomPhoto>()
                .HasOne(nc => nc.Nomenclature)
                .WithMany(c => c.NomPhotos)
                .HasForeignKey(nc => nc.nom_id);

            builder.Entity<NomDiscount>()
                .HasOne(nc => nc.Nomenclature)
                .WithMany(c => c.NomDiscounts)
                .HasForeignKey(nc => nc.NomenclatureId);

            //builder.CatalogBuilder();
            //builder.CategoryBuilder();
            builder.Seed(new AccessSettingSeeds());
            builder.Seed(new SettingsSeeds());
        }

        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<MenuContent> MenuContents { get; set; }

        public DbSet<AuthorizationLink> AuthorizationLinks { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<UrlAccess> UrlAccess { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<NomenclatureModel> Nomenclatures { get; set; }
        public DbSet<NomCatalog> NomCatalogs { get; set; }
        public DbSet<NomCategory> NomCategories { get; set; }
        public DbSet<VisibleNomUser> VisibleNomUsers { get; set; }
        public DbSet<VisibleUserCatalog> VisibleUserCatalogs { get; set; }
        public DbSet<NomUserFavorite> NomUserFavorites { get; set; }

        public DbSet<NotVisibleProdCtlg> NotVisibleProdCtlgs { get; set; }
        public DbSet<NotVisibleProdCtgr> NotVisibleProdCtgrs { get; set; }
        public DbSet<NomProdPrice> NomProdPrices { get; set; }
        public DbSet<NomPhoto> NomPhotos { get; set; }
        public DbSet<NomPreorder> NomPreorders { get; set; }

        public DbSet<NomDiscount> NomDiscounts { get; set; }
        public DbSet<UserDiscount> UserDiscounts { get; set; }
        public DbSet<CatalogDiscount> CatalogDiscounts { get; set; }

        public DbSet<CartItem> CartItems { get; set; }

        public DbSet<BlogItem> BlogItems { get; set; }
        public DbSet<GalleryItem> GalleryItems { get; set; }
        public DbSet<BannerItem> BannerItems { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<FaqSection> FaqSections { get; set; }
        public DbSet<FaqSectionContent> FaqSectionContents { get; set; }
    }
}