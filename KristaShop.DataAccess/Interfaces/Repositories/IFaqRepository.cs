﻿using System;
using System.Collections.Generic;
using System.Text;
using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataAccess.Entities;

namespace KristaShop.DataAccess.Interfaces.Repositories
{
    public interface IFaqRepository<T, in TU> : IRepository<T, TU> where T : class
    {
        Faq GetFaqByIdIncluding(Guid id);
    }
}
