﻿using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KristaShop.DataAccess.Interfaces.Repositories
{
    public interface IFaqSectionRepository<T, in TU> : IRepository<T, TU> where T : class
    {
        FaqSection GetFaqSectionByIdIncluding(Guid id);
        Task<List<FaqSection>> GetFaqSections(Guid id);
    }
}
