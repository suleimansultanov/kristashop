﻿using System;
using System.Collections.Generic;
using System.Text;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataAccess.Interfaces.Repositories
{
    public interface IFaqSectionContentFileRepository<T, in TU> : IRepository<T, TU> where T : class
    {
    }
}
