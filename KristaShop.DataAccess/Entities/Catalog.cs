﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("dict_catalogs")]
    public class Catalog
    {
        [Key]
        public Guid id { get; set; }

        [Required, MaxLength(64)]
        public string name { get; set; }

        [Required, MaxLength(64)]
        public string uri { get; set; }

        public int order_form { get; set; }
        public string description { get; set; }
        public string meta_title { get; set; }
        public string meta_keywords { get; set; }
        public string meta_description { get; set; }
        public string additional_description { get; set; }
        public int order { get; set; }
        public bool is_disable_discount { get; set; }
        public bool is_visible { get; set; }
        public bool is_open { get; set; }
        public bool is_set { get; set; }

        [MaxLength(256)]
        public string preview_path { get; set; } = string.Empty;

        [MaxLength(256)]
        public string video_path { get; set; } = string.Empty;

        public DateTimeOffset? close_time { get; set; }

        public virtual ICollection<NomCatalog> NomCatalogs { get; set; }
        public virtual ICollection<VisibleUserCatalog> VisibleCatalogUsers { get; set; }
        public virtual ICollection<NomUserFavorite> NomUserFavorites { get; set; }
        public ICollection<CatalogDiscount> CatalogDiscounts { get; set; }
        public ICollection<NomDiscountCatalog> NomDiscounts { get; set; }

        public bool HasClosedByTime() {
            return close_time != null && close_time < DateTimeOffset.UtcNow;
        }
    }
}