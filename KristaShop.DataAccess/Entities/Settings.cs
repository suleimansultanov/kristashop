﻿using System;

namespace KristaShop.DataAccess.Entities {
    public class Settings {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public bool OnlyRootAccess { get; set; }
    }
}