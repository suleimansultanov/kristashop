﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("nom_preorder")]
    public class NomPreorder
    {
        [Key]
        public Guid id { get; set; }

        public Guid nom_id { get; set; }

        [ForeignKey("nom_id")]
        public NomenclatureModel Nomenclature { get; set; }

        public int max_amout { get; set; }
        public int counter { get; set; }
    }
}