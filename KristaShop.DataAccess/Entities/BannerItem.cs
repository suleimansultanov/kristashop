﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("banner_items")]
    public class BannerItem
    {
        [Key]
        public Guid id { get; set; }

        [Required, MaxLength(100)]
        public string title { get; set; }

        [Required, MaxLength(100)]
        public string caption { get; set; }

        [Required, MaxLength(100)]
        public string image_path { get; set; }

        [Required, MaxLength(2000)]
        public string description { get; set; }

        [Required]
        public string link { get; set; }

        public bool is_visible { get; set; }

        public int order { get; set; }

        [Required, MaxLength(20)]
        public string title_color { get; set; }
    }
}