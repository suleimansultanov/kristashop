﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;

namespace KristaShop.DataAccess.Entities {
    public class Feedback {
        public Guid Id { get; set; }
        public string Person { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public bool Viewed { get; set; }
        public DateTime RecordTimeStamp { get; set; }
        public Guid ReviewerUserId { get; set; }
        public DateTime? ViewTimeStamp { get; set; }
        public FeedbackType Type { get; set; }

        public ICollection<FeedbackFile> Files { get; set; }
    }
}