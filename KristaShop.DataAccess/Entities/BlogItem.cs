﻿using System;

namespace KristaShop.DataAccess.Entities {
    public class BlogItem {
        public Guid Id { get; set; }
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string LinkText { get; set; }
        public string Link { get; set; }
        public bool IsVisible { get; set; }
        public int Order { get; set; }
    }
}