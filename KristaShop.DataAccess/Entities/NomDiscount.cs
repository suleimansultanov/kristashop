﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataAccess.Entities {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomenclatureDiscountConfiguration"/>
    /// </summary>
    public class NomDiscount : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public Guid NomenclatureId { get; set; }
        public double DiscountPrice { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        
        public NomenclatureModel Nomenclature { get; set; }
        public ICollection<NomDiscountCatalog> DiscountCatalogs { get; set; }

        public DiscountType Type => DiscountType.Nom;

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}