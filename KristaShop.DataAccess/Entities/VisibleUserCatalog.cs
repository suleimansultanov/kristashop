﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KristaShop.DataAccess.Entities
{
    [Table("visible_user_catalogs")]
    public class VisibleUserCatalog
    {
        [Key]
        public Guid id { get; set; }

        public Guid catalog_id { get; set; }

        [ForeignKey("catalog_id")]
        public Catalog Catalog { get; set; }

        public Guid user_id { get; set; }
    }
}
