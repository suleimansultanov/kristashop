﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataAccess.Entities
{
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.FaqSectionContentConfiguration"/>
    /// </summary>

    public class FaqSectionContent : IEntityKeyGeneratable
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public Guid FaqSectionId { get; set; }
        public FaqSection FaqSection { get; set; }
        public ICollection<FaqSectionContentFile> FaqSectionContentFiles { get; set; }

        public FaqSectionContent() { }

        public FaqSectionContent(bool generateId)
        {
            if (generateId) GenerateKey();
        }

        public void GenerateKey()
        {
            Id = Guid.NewGuid();
        }
    }
}
