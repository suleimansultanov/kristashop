﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("nom_user_favorites")]
    public class NomUserFavorite
    {
        [Key]
        public Guid id { get; set; }

        public Guid user_id { get; set; }
        public Guid nom_id { get; set; }

        [ForeignKey("nom_id")]
        public NomenclatureModel Nomenclature { get; set; }

        public Guid catalog_id { get; set; }

        [ForeignKey("catalog_id")]
        public Catalog Catalog { get; set; }
    }
}