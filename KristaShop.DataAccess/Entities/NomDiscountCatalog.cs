﻿using System;

namespace KristaShop.DataAccess.Entities {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomDiscountCatalogConfiguration"/>
    /// </summary>
    public class NomDiscountCatalog {
        public Guid DiscountId { get; set; }
        public Guid CatalogId { get; set; }

        public NomDiscount Discount { get; set; }
        public Catalog Catalog { get; set; }

        public NomDiscountCatalog() { }

        public NomDiscountCatalog(Guid discountId, Guid catalogId) {
            DiscountId = discountId;
            CatalogId = catalogId;
        }
    }
}
