﻿using System;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataAccess.Entities {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.UserDiscountConfiguration"/>
    /// </summary>
    public class UserDiscount : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public double DiscountPrice { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DiscountType Type => DiscountType.User;

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}