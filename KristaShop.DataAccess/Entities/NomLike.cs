﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("nom_likes")]
    public class NomLike
    {
        [Key]
        public Guid id { get; set; }

        public Guid user_id { get; set; }
        public Guid nom_id { get; set; }

        [ForeignKey("nom_id")]
        public NomenclatureModel Nomenclature { get; set; }
    }
}