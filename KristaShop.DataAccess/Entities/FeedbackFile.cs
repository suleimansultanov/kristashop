﻿using System;

namespace KristaShop.DataAccess.Entities {
    public class FeedbackFile {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string Filename { get; set; }
        public string VirtualPath { get; set; }
        public DateTime CreateDate { get; set; }

        public Feedback Feedback { get; set; }
    }
}