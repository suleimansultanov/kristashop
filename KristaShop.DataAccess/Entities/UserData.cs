﻿using System;

namespace KristaShop.DataAccess.Entities {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.UserDataConfiguration"/>
    /// </summary>
    public class UserData {
        public Guid UserId { get; set; }
        public DateTimeOffset LastSignIn { get; set; }
    }
}
