﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataAccess.Entities
{
    [Table("cart_items")]
    public class CartItem
    {
        [Key]
        public Guid id { get; set; }

        public Guid nom_id { get; set; }

        [ForeignKey("nom_id")]
        public NomenclatureModel Nomenclature { get; set; }

        public Guid product_id { get; set; }
        public Guid catalog_id { get; set; }
        public int order_form_type { get; set; }
        public double price { get; set; }
        public double discount { get; set; }
        public int amount { get; set; }
        public double total_amount { get; set; }
        public Guid user_id { get; set; }
        public DateTime created_date { get; set; }
    }
}