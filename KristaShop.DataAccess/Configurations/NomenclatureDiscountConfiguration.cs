﻿using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataAccess.Configurations {
    public class NomenclatureDiscountConfiguration : IEntityTypeConfiguration<NomDiscount> {
        public void Configure(EntityTypeBuilder<NomDiscount> builder) {
            builder.ToTable("nom_discounts");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Nomenclature)
                .WithMany(x => x.NomDiscounts)
                .HasForeignKey(x => x.NomenclatureId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x=>x.NomenclatureId)
                .HasColumnName("nom_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DiscountPrice)
                .HasColumnName("discount_price")
                .IsRequired();

            builder.Property(x => x.IsActive)
                .HasColumnName("is_active")
                .IsRequired();

            builder.Property(x => x.StartDate)
                .HasColumnName("start_date")
                .IsRequired(false);

            builder.Property(x => x.EndDate)
                .HasColumnName("end_date")
                .IsRequired(false);

            builder.Ignore(x => x.Type);
        }
    }
}
