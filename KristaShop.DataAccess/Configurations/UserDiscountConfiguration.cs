﻿using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataAccess.Configurations {
    public class UserDiscountConfiguration : IEntityTypeConfiguration<UserDiscount> {
        public void Configure(EntityTypeBuilder<UserDiscount> builder) {
            builder.ToTable("user_discounts");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x=>x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DiscountPrice)
                .HasColumnName("discount_price")
                .IsRequired();

            builder.Property(x => x.IsActive)
                .HasColumnName("is_active")
                .IsRequired();

            builder.Property(x => x.StartDate)
                .HasColumnName("start_date")
                .IsRequired(false);

            builder.Property(x => x.EndDate)
                .HasColumnName("end_date")
                .IsRequired(false);

            builder.Ignore(x => x.Type);
        }
    }
}
