﻿using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataAccess.Configurations {
    public class NomDiscountCatalogConfiguration : IEntityTypeConfiguration<NomDiscountCatalog> {
        public void Configure(EntityTypeBuilder<NomDiscountCatalog> builder) {
            builder.ToTable("nom_discounts_catalogs");

            builder.HasKey(x => new {x.DiscountId, x.CatalogId});

            builder.HasOne(x => x.Catalog)
                .WithMany(x => x.NomDiscounts)
                .HasForeignKey(x => x.CatalogId);

            builder.HasOne(x => x.Discount)
                .WithMany(x => x.DiscountCatalogs)
                .HasForeignKey(x => x.DiscountId);

            builder.Property(x => x.DiscountId)
                .HasColumnName("nom_discount_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.CatalogId)
                .HasColumnName("catalog_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}
