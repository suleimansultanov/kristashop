﻿using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataAccess.Configurations {
    public class CatalogDiscountConfiguration : IEntityTypeConfiguration<CatalogDiscount> {
        public void Configure(EntityTypeBuilder<CatalogDiscount> builder) {
            builder.ToTable("catalog_discounts");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Catalog)
                .WithMany(x => x.CatalogDiscounts)
                .HasForeignKey(x => x.CatalogId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x=>x.CatalogId)
                .HasColumnName("catalog_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DiscountPrice)
                .HasColumnName("discount_price")
                .IsRequired();

            builder.Property(x => x.IsActive)
                .HasColumnName("is_active")
                .IsRequired();

            builder.Property(x => x.StartDate)
                .HasColumnName("start_date")
                .IsRequired(false);

            builder.Property(x => x.EndDate)
                .HasColumnName("end_date")
                .IsRequired(false);

            builder.Ignore(x => x.Type);
        }
    }
}