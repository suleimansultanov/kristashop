﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataReadOnly.DTOs
{
    public class OrderItemsPrice
    {
        public Guid nomenclature_id { get; set; }
        public Guid product_id { get; set; }
        public double parts_count { get; set; }
        public double items_count { get; set; }
        public double items_count_src { get; set; }
        public bool is_preorder { get; set; }
        public double price { get; set; }
        public double discount { get; set; }
        public string units { get; set; }
        public string articul { get; set; }
        public string item_name { get; set; }
        public string size_line_name { get; set; }
        public string size_name { get; set; }
        public string color_name { get; set; }
        public string color_code { get; set; }
        public string color_image { get; set; }
        public double pre_pay { get; set; }
        public int nom_type { get; set; }

        public string description
        {
            get
            {

                //string des = $"{articul} {item_name}";
                //if (!string.IsNullOrEmpty(size_line_name))
                //{
                //    des += $" Размеры: {size_line_name}";
                //}
                //if (!string.IsNullOrEmpty(size_name))
                //{
                //    des += $" Размер: {size_name}";
                //}
                //if (!string.IsNullOrEmpty(color_name))
                //{
                //    des += $" Цвет: {color_name}";
                //}
                return articul;
            }
        }

        [NotMapped]
        public string ColorImagePath => string.IsNullOrEmpty(color_image) ? string.Empty : $"/colors/{color_image}";

    }
}