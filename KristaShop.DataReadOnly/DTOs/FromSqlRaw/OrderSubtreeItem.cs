﻿using KristaShop.Common.Enums;
using System;

namespace KristaShop.DataReadOnly.DTOs
{
    public class OrderSubtreeItem
    {
        public Guid id { get; set; }
        public Guid parent_id { get; set; }
        public string doc_type { get; set; }
        public int pos { get; set; }
        public int level { get; set; }
        public UniversalDocState status { get; set; }
        public Guid? product_id { get; set; }
        public double? items_count { get; set; }
        public double? processed_count { get; set; }
    }
}