﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.DataReadOnly.DTOs
{
    public class OrderStoreIncomeItem
    {
        public Guid nomenclature_id { get; set; }
        public Guid product_id { get; set; }
        public double items_count { get; set; }
        public double parts_count { get; set; }
        public string articul { get; set; }
        public string item_name { get; set; }
        public string size_line_name { get; set; }
        public string size_name { get; set; }
        public string color_name { get; set; }
        public string color_code { get; set; }
        public string color_image { get; set; }

        [NotMapped]
        public string ColorImagePath => string.IsNullOrEmpty(color_image) ? string.Empty : $"/colors/{color_image}";
    }
}