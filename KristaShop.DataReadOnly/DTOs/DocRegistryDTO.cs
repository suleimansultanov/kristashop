﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Documents;

namespace KristaShop.DataReadOnly.DTOs {
    public class DocRegistryDTO {
        public Guid DocId { get; set; }
        public Guid SubtreeId { get; set; }
        public DateTime CreateDate { get; set; }
        public long DocNum { get; set; }
        public string DocTypeName { get; set; }
        public string RuDocTypeName { get; set; }
        public string ExtraData { get; set; }
        public UniversalDocState Status { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
        public bool IsDetail { get; set; }
        public double Sum { get; set; }
        public double SumRu { get; set; }
        public double SumPayIn { get; set; }
        public double SumRuPayIn { get; set; }
        public double SumDebt { get; set; }
        public double SumRuDebt { get; set; }
        public double PreOrderSum { get; set; }
        public double PreOrderSumRu { get; set; }
        public double PrepaySum { get; set; }
        public double PrepaySumRu { get; set; }
        public double InStockSum { get; set; }
        public double InStockSumRu { get; set; }
        public double Percent { get; set; }
        public double Rate { get; set; }
        public bool IsFileExist { get; set; }
        public DocumentType DocumentType { get; set; }
        public bool ToCounterpartyBalance { get; set; }
        public CurrencyType CurrencyType { get; set; }

        public Guid ParentId { get; set; }
        public int Level { get; set; }
        public int Pos { get; set; }
        public List<DocRegistryDTO> ChildNodes { get; set; }
        public List<DocRegistryFileDTO> Files { get; set; }

        public string FullName => $"{RuDocTypeName} №{DocNum}";

        public DocRegistryDTO() {
            CurrencyType = CurrencyType.None;
            ChildNodes = new List<DocRegistryDTO>();
        }
    }
}