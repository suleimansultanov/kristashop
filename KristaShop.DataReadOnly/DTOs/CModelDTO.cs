﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.DTOs
{
    public class CModelDTO
    {
        public Guid Id { get; set; }
        public Guid CatalogId { get; set; }
        public int Order { get; set; }
        public string PhotoPath { get; set; }
        public string Articul { get; set; }
        public string ItemName { get; set; }
        public List<string> Colors { get; set; }
        public List<string> Sizes { get; set; }
        public List<string> Catalogs { get; set; }
        public List<string> Categories { get; set; }
        public double ItemPrice { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsVisible { get; set; }
        public bool IsSet { get; set; }
        public double DiscountPrice { get; set; }
    }

    public class CModelGridDTO
    {
        public Guid ProductId { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public bool NotVisible { get; set; }
    }

    public class ProductDTO
    {
        public Guid ProductId { get; set; }
        public bool IsSet { get; set; }
        public ColorDTO Color { get; set; }
        public SizeDTO Size { get; set; }
        public SizeLineDTO SizeLine { get; set; }
    }

    public class ProductDetailsDTO
    {
        public Guid ProductId { get; set; }
        public string ItemName { get; set; }
        public double? Price { get; set; }
        public double? PriceRu { get; set; }
        public double PartsCount { get; set; }
    }
}