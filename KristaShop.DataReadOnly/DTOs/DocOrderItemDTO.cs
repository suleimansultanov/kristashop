﻿using System;

namespace KristaShop.DataReadOnly.DTOs
{
    public class DocOrderItemDTO
    {
        public Guid ProductId { get; set; }
        public Guid NomId { get; set; }
        public string Image { get; set; }
        public string Articul { get; set; }
        public string ColorCode { get; set; }
        public string ColorImg { get; set; }
        public string Size { get; set; }
        public double Amount { get; set; }
        public double TotalAmount { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public double TotalPrice { get; set; }
    }
}