﻿namespace KristaShop.DataReadOnly.DTOs
{
    public class AccessControlDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}