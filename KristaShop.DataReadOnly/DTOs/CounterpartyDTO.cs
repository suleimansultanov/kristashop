﻿using System;

namespace KristaShop.DataReadOnly.DTOs {
    public class CounterpartyDTO {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Person { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MallAddress { get; set; }
        public double Balance { get; set; }
        public string City { get; set; }
    }
}
