﻿using System;

namespace KristaShop.DataReadOnly.DTOs
{
    public class UserClientDTO
    {
        public Guid CPId { get; set; }
        public Guid UserId { get; set; }
        public string Login { get; set; }
        public string ClientFullName { get; set; }
        public string Title { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public string PhoneNumber { get; set; }
        public string ShopName { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public double DiscountPrice { get; set; }
        public bool CartStatus { get; set; }
        public string CatalogNames { get; set; }
        public DateTimeOffset LastSignIn { get; set; }

        public string TitlePerson => $"{Title} ({ClientFullName})";
    }

    public class UserGroupDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}