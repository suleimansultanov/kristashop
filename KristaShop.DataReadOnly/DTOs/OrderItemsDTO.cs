﻿using KristaShop.Common.Enums;
using System;

namespace KristaShop.DataReadOnly.DTOs
{
    public class OrderItemsDTO : ICloneable
    {
        public Guid DocumentId { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public string ColorImage { get; set; }
        public string SizeName { get; set; }
        public string DocumentType { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public int Branch { get; set; }
        public UniversalDocState Status { get; set; }
        public Guid NomId { get; set; }
        public Guid ProductId { get; set; }
        public bool IsPreorder { get; set; }
        public double PartsCount { get; set; }
        public double Quantity { get; set; }
        public double QuantitySrc { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public double Processed { get; set; }
        public double PrePay { get; set; }
        public string Units { get; set; }
        public int NomType { get; set; }

        public string GetStatus()
        {
            var status = string.Empty;
            switch (Status)
            {
                case UniversalDocState.ShipmentAwaiting:
                    status = "Ожидает отправки";
                    break;

                case UniversalDocState.Shipped:
                    status = "Отправлено";
                    break;

                case UniversalDocState.OrderProcessing:
                    status = "Обрабатывается";
                    break;

                case UniversalDocState.DocumentProcessing:
                case UniversalDocState.Completed:
                    if (DocumentType.Equals("EntityDocumentOrder") || DocumentType.Equals("EntityDocumentShipment"))
                    {
                        status = "Доставлено";
                    }
                    else if (DocumentType.Equals("EntityDocumentStorehouseReservation"))
                    {
                        status = "В резерве";
                    }
                    else if (DocumentType.Equals("EntityDocumentRequestToProduction"))
                    {
                        status = "В производстве";
                    }
                    break;
                case UniversalDocState.Canceled:
                    status = "Отменен";
                    break;
                default:
                    status = "Ожидает обработки";
                    break;
            }
            return status;
        }

        public void CopyProductInfo(OrderItemsDTO source)
        {
            Description = source.Description;
            IsPreorder = source.IsPreorder;
            Price = source.Price;
            Discount = source.Discount;
            SizeName = source.SizeName;
            ColorName = source.ColorName;
            ColorCode = source.ColorCode;
            ColorImage = source.ColorImage;
            Units = source.Units;
            NomId = source.NomId;
            PartsCount = source.PartsCount;
            PrePay = source.PrePay;
            NomType = source.NomType;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        protected bool Equals(OrderItemsDTO other)
        {
            if (other == null)
                return false;
            return ProductId == other.ProductId && IsPreorder == other.IsPreorder && Math.Abs(Price - other.Price) < 1E-6 &&
                   Math.Abs(Discount - other.Discount) < 1E-6 && DocumentType.Equals(other.DocumentType) && Status == other.Status;
        }
    }
}