﻿using System;

namespace KristaShop.DataReadOnly.DTOs
{
    public class DocRegistryFileDTO
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string VirtualPath { get; set; }
    }
}