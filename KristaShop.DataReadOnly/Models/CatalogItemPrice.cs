﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.CatalogItemPriceConfiguration"/>
    /// </summary>
    public class CatalogItemPrice {
        public Guid Id { get; set; }
        public Guid CatalogItemId { get; set; }
        public double Price { get; set; }
        public Guid PriceTypeId { get; set; }
        public Guid InputCurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }

        public PriceType PriceType { get; set; }
        public Currency InputCurrency { get; set; }
        public CatalogItem CatalogItem { get; set; }

        public bool ExchangeToCurrency(List<ExchangeRatesSnapshot> snapshots, CurrencyType toCurrency, out double value) {
            return _exchangeToCurrency(snapshots, toCurrency, out value, out _);
        }

        public bool ExchangeToCurrencyString(List<ExchangeRatesSnapshot> snapshots, CurrencyType toCurrency, out string value) {
            var result = _exchangeToCurrency(snapshots, toCurrency, out var price, out var currency);
            value = result ? $"{price.ToTwoDecimalPlaces()} {currency.Sign.ToLower()}" : "";
            return result;
        }

        public bool _exchangeToCurrency(List<ExchangeRatesSnapshot> snapshots, CurrencyType toCurrency, out double value, out Currency currency) {
            value = 0;
            currency = null;
            var snapshot = snapshots.Find(x => x.SnapshotId == ExchangeRateSnapshotId
                                               && x.ToCurrency.Type == toCurrency);
            if (snapshot == null) return false;

            value = snapshot.Exchange(Price);
            currency = snapshot.ToCurrency;
            return true;
        }
    }
}