﻿using System;
using System.Collections;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.ExchangeRatesSnapshotConfiguration"/>
    /// </summary>
    public class ExchangeRatesSnapshot {
        public Guid Id { get; set; }
        public Guid SnapshotId { get; set; }
        public Guid FromCurrencyId { get; set; }
        public Guid ToCurrencyId { get; set; }
        public double Rate { get; set; }
        public DateTime AddDate { get; set; }

        public Currency FromCurrency { get; set; }
        public Currency ToCurrency { get; set; }

        public double Exchange(double value) {
            return value * Rate;
        }
    }
}