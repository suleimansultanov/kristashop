﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.CityConfiguration"/>
    /// </summary>
    public class City {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Counterparty> Counterparties { get; set; }

        public City() { }

        public City(Guid id, string name) {
            Id = id;
            Name = name;
        }
    }
}