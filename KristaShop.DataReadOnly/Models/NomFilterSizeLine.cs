﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomFilterSizeLineConfiguration"/>
    /// </summary>
    public class NomFilterSizeLine {
        public Guid SizeLineId { get; set; }
        public Guid OptionId { get; set; }

        public virtual SizeLine SizeLine { get; set; }
        public virtual Option Option { get; set; }
    }
}