﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.SizeConfiguration"/>
    /// </summary>
    public class Size {
        public Guid Id { get; set; }
        public string SizeName { get; set; }
        public int SizeValue { get; set; }

        public virtual ICollection<NomFilterSize> NomFilterSizes { get; set; }
    }
}