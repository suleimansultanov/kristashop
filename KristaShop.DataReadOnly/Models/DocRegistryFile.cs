﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.DocRegistryFilesConfiguration"/>
    /// </summary>
    public class DocRegistryFile {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public string Filename { get; set; }
        public string VirtualPath { get; set; }
        public string Description { get; set; }
        public DateTime AddDate { get; set; }
        public bool IsForClient { get; set; }

        public DocRegistry Document { get; set; }
    }
}