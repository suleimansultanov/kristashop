﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.CurrencyConfiguration"/>
    /// </summary>
    public class Currency {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Sign { get; set; }
        public string InternationalCode { get; set; }
        public CurrencyType Type { get; set; }
        public bool IsDefault { get; set; }

        public ICollection<CatalogItemPrice> CatalogItemPrices { get; set; }
        public ICollection<ExchangeRate> ExchangeRatesFromThis { get; set; }
        public ICollection<ExchangeRate> ExchangeRatesToThis { get; set; }
        public ICollection<ExchangeRatesSnapshot> ExchangeRatesSnapshotsFromThis { get; set; }
        public ICollection<ExchangeRatesSnapshot> ExchangeRatesSnapshotsToThis { get; set; }
    }
}