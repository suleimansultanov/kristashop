﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.CounterpartyConfiguration"/>
    /// </summary>
    public class Counterparty : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int Type { get; set; } = 0;
        public string Person { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MallAddress { get; set; }
        public string CompanyAddress { get; set; }
        public double Balance { get; set; }
        public Guid? CityId { get; set; }
        public string NewCity { get; set; }
        public Guid? ManagerId { get; set; }

        public User User { get; set; }
        public City City { get; set; }
        public User Manager { get; set; }
        public ICollection<DocRegistry> Documents { get; set; }

        public string CityName => City?.Name ?? NewCity;

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}