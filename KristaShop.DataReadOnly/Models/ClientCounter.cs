﻿using System;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.ClientCounterConfiguration"/>
    /// </summary>
    public class ClientCounter : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public long Counter { get; set; }
        public DateTime? UpdateTimestamp { get; set; }

        public ClientCounter() { }

        public ClientCounter(long counter) {
            Counter = counter;
            SetUpdateTimestamp();
        }

        public void SetUpdateTimestamp() {
            UpdateTimestamp = DateTime.Now;
        }

        public void IncreaseCounter() {
            Counter++;
            SetUpdateTimestamp();
        }

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}
