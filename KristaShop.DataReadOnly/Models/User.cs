﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.UserConfiguration"/>
    /// </summary>
    public class User : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int Acl { get; set; }
        public UserStatus Status { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime? BanExpireDate { get; set; }
        public string BanReason { get; set; }
        public bool IsRoot { get; set; }
        public Guid? CounterpartyId { get; set; }
        public string Email { get; set; }

        public Counterparty Counterparty { get; set; }
        public Counterparty ManagerCounterparty { get; set; }
        public Employee Employee { get; set; }
        public ManagerRate ManagerRate { get; set; }
        public ICollection<UserGroupMembership> UserGroupMemberships { get; set; }

        public User() {
            BanReason = "";
        }

        public User(Guid id, string login, string password, Guid counterpartyId) {
            Id = id;
            Login = login;
            Password = password;
            CounterpartyId = counterpartyId;
            Status = UserStatus.Await;
            BanReason = string.Empty;
            IsRoot = false;
            RegistrationDate = DateTime.Now;
            Email = string.Empty;
        }

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}