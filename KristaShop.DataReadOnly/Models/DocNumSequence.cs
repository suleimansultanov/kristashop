﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.DocNumSequenceConfiguration>
    /// </summary>
    public class DocNumSequence {
        public long Id { get; set; }
        public DateTime AddDate { get; set; }
    }
}