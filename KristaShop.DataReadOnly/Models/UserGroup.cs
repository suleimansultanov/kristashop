﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.UserGroupConfiguration"/>
    /// </summary>
    public class UserGroup {
        public Guid Id { get; set; }
        public string GroupName { get; set; }
        public Guid AclId { get; set; }
        public UserType Type { get; set; }

        public AccessControl Acl { get; set; }
        public ICollection<UserGroupMembership> UserGroupMemberships { get; set; }
    }
}