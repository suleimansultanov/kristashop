﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.StorehouseRestsConfiguration"/>
    /// </summary>
    public class StorehouseRests {
        
        public Guid Id { get; set; }
        public int Level { get; set; }
        public Guid ProductId { get; set; }
        public Guid SubtreeId { get; set; }
        public Guid ReservationDocId { get; set; }
        public double Count { get; set; }
        public double PartsCount { get; set; }
        public Guid StorehouseId { get; set; }
        public DateTime AddDate { get; set; }

        public CatalogItem CatalogItem { get; set; }
        public Storehouse Storehouse { get; set; }
    }
}