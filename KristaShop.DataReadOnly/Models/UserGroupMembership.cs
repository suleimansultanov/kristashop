﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.UserGroupMembershipConfiguration"/>
    /// </summary>
    public class UserGroupMembership {
        public Guid UserId { get; set; }
        public Guid GroupId { get; set; }

        public User User { get; set; }
        public UserGroup UserGroup { get; set; }

        public UserGroupMembership() { }

        public UserGroupMembership(Guid userId, Guid groupId) {
            UserId = userId;
            GroupId = groupId;
        }
    }
}