﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.DocStorehouseSubItemsConfiguration"/>
    /// </summary>
    public class DocStorehouseSubItems {
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public Guid ProductId { get; set; }
        public double ItemsCount { get; set; }

        public DocRegistry DocRegistry { get; set; }
    }
}