﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Documents;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.DocRegistryConfiguration"/>
    /// </summary>
    public class DocRegistry {
        public Guid Id { get; set; }
        public int Position { get; set; }
        public int Level { get; set; }
        public Guid ParentId { get; set; }
        public Guid SubtreeId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? AppliedDate { get; set; }
        public int Acl { get; set; }
        public Guid DepartmentId { get; set; }
        public Guid CreaterId { get; set; }
        public Guid ApplierId { get; set; }
        public Guid ExecuterId { get; set; }
        public Guid CounterpartyId { get; set; }
        public string DocType { get; set; }
        public long DocNum { get; set; }
        public long DocNum1C { get; set; }
        public bool IsApplied { get; set; }
        public bool IsCompleted { get; set; }
        public UniversalDocState Status { get; set; }
        public string Description { get; set; }
        public string ExtraData { get; set; }

        public Employee Creater { get; set; }
        public Employee Applier { get; set; }
        public Employee Executer { get; set; }
        public Counterparty Counterparty { get; set; }

        public ICollection<DocOrderItem> DocOrderItems { get; set; }
        public ICollection<DocRegistry> ChildNodes { get; set; }
        public ICollection<DocRegistryFile> DocRegistryFiles { get; set; }
        public ICollection<DocStorehouseSubItems> DocStorehouseSubItems { get; set; }

        public DocumentType DocumentType => DocumentExtension.GetDocTypeByName(DocType);

        public string GetStatusName() {
            if (DocumentType.Order == DocumentType && ParentId != Guid.Empty) {
                return "Объединен";
            }

            if (Status == UniversalDocState.Completed && DocumentType.Shipment.IsEquals(DocType)) {
                return "Доставлен";
            }

            if (Status == UniversalDocState.Completed) {
                if (DocumentType.PrepayInvoice.IsEquals(DocType) ||
                    DocumentType.PaymentInvoice.IsEquals(DocType) ||
                    DocumentType.FreeInvoice.IsEquals(DocType)) {
                    return "Оплачен";
                }
            } else {
                if (DocumentType.PrepayInvoice.IsEquals(DocType) ||
                    DocumentType.PaymentInvoice.IsEquals(DocType) ||
                    DocumentType.CashboxIncomeOrder.IsEquals(DocType) ||
                    DocumentType.CashboxOutcomeOrder.IsEquals(DocType) ||
                    DocumentType.FreeInvoice.IsEquals(DocType)) {
                    return "Ожидает оплаты";
                }
            }

            return Status.ToReadableString();
        }
    }
}