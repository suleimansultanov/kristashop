﻿using System;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.WebApiRequestConfiguration"/>
    /// </summary>
    public class WebApiRequest : IEntityKeyGeneratable {
        public Guid Id { get; set; }
        public string RequestHash { get; set; }
        public DateTime? Timestamp { get; set; }

        public WebApiRequest() { }

        public WebApiRequest(string hash) {
            RequestHash = hash;
            Timestamp = DateTime.Now;
        }

        public void GenerateKey() {
            Id = Guid.NewGuid();
        }
    }
}
