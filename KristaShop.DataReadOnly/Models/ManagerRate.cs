﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.ManagerRateConfiguration"/>
    /// </summary>
    public class ManagerRate {
        public Guid UserId { get; set; }
        public double Rate { get; set; }

        public User User { get; set; }
    }
}
