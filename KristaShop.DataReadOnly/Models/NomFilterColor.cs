﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomFilterColorConfiguration"/>
    /// </summary>
    public class NomFilterColor {
        public Guid ColorId { get; set; }
        public Guid OptionId { get; set; }

        public virtual Color Color { get; set; }
        public virtual Option Option { get; set; }
    }
}