﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.DocOrderItemConfiguration"/>
    /// </summary>
    public class DocOrderItem {
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public Guid ProductId { get; set; }
        public bool IsPreorder { get; set; }
        public double ItemsCount { get; set; }
        public double ItemsCountSrc { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public double ProcessedCount { get; set; }
        public double PayIn { get; set; }
        public double CostsTotal { get; set; }
        public double PrePay { get; set; }
        public double ProdusedCount { get; set; }

        public DocRegistry Document { get; set; }
    }
}