﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.PriceTypeConfiguration"/>
    /// </summary>
    public class PriceType {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public PriceTypes Type { get; set; }

        public ICollection<CatalogItemPrice> CatalogItemPrices { get; set; }
    }
}