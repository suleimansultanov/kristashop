﻿using System;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomFilterSizeConfiguration"/>
    /// </summary>
    public class NomFilterSize {
        public Guid SizeId { get; set; }
        public Guid OptionId { get; set; }

        public virtual Size Size { get; set; }
        public virtual Option Option { get; set; }
    }
}