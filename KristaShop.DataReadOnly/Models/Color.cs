﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.ColorConfiguration"/>
    /// </summary>
    public class Color {
        public Guid Id { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public string ColorImage { get; set; }

        public virtual ICollection<NomFilterColor> NomFilterColors { get; set; }

        public string ImagePath => string.IsNullOrEmpty(ColorImage) ? string.Empty : $"/colors/{ColorImage}";
    }
}