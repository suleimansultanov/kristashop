﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.AccessControlConfiguration"/>
    /// </summary>
    public class AccessControl {
        public Guid Id { get; set; }
        public string AclName { get; set; }
        public int Acl { get; set; }

        public ICollection<UserGroup> UserGroups { get; set; }
    }
}