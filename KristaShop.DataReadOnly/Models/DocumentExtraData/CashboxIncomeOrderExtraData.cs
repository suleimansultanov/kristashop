﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class CashboxIncomeOrderExtraData {
        public double Summ { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public int IsCash { get; set; }
        public Guid OpTypeId { get; set; }
        public bool VisibleForCounterparty { get; set; }

        public CashboxIncomeOrderExtraData() {
            Summ = 0.0d;
            CurrencyId = Guid.Empty;
            ExchangeRateSnapshotId = Guid.Empty;
            IsCash = 1;
            OpTypeId = Guid.Empty;
            VisibleForCounterparty = false;
        }
    }
}