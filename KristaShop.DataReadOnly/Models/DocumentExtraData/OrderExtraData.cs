﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class OrderExtraData {
        public Guid CurrencyRateSnapshotId { get; set; }

        public OrderExtraData() {
            CurrencyRateSnapshotId = Guid.Empty;
        }
    }
}