﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class PrepayInvoiceExtraData {
        public Guid CurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public double ValueAddedTax { get; set; }
        public double Percent { get; set; }
        public string PrepayDescription { get; set; }
        public bool IsCash { get; set; }
    }
}