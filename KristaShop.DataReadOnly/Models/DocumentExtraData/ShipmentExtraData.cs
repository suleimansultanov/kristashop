﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class DocumentShipmentExtraData {
        public Guid ShippingCounterpartyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public string ExtraCityName { get; set; }
        public string ExtraCrgoCompanyName { get; set; }
        public string IDCargo { get; set; }
        public bool NeedPayment { get; set; }

        public DocumentShipmentExtraData() {
            ShippingCounterpartyId = Guid.Empty;
            ExchangeRateSnapshotId = Guid.Empty;
            ExtraCityName = string.Empty;
            ExtraCrgoCompanyName = string.Empty;
            IDCargo = string.Empty;
            NeedPayment = false;
        }
    }
}