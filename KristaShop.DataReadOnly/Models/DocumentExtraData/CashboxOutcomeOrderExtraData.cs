﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class CashboxOutcomeOrderExtraData {
        public double Summ { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public int IsCash { get; set; }
        public Guid OpTypeId { get; set; }
        public bool VisibleForCounterparty { get; set; }
        public bool ToCounterpartyBalance { get; set; }
    }
}