﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class PaymentInvoiceExtraData {
        public Guid CurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public double ValueAddedTax { get; set; }
        public bool IsCash { get; set; }

        public PaymentInvoiceExtraData() {
            CurrencyId = Guid.Empty;
            ExchangeRateSnapshotId = Guid.Empty;
            IsCash = false;
        }
    }
}