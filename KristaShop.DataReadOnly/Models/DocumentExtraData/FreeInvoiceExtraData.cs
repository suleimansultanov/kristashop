﻿using System;

namespace KristaShop.DataReadOnly.Models.DocumentExtraData {
    public class FreeInvoiceExtraData {
        public double Summ { get; set; }
        public double PayIn { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ExchangeRateSnapshotId { get; set; }
        public int IsCash { get; set; }
        public Guid OpTypeId { get; set; }
        public bool VisibleForCounterparty { get; set; }
    }
}