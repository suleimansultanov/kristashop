﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.NomenclatureConfiguration"/>
    /// </summary>
    public class Nomenclature {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int NomType { get; set; }
        public string Articul { get; set; }
        public bool IsSet { get; set; }
        public bool VisibleOnSite { get; set; }

        public virtual ICollection<CatalogItem> CatalogItems { get; set; }
    }
}