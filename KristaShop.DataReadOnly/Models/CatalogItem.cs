﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.CatalogItemConfiguration"/>
    /// </summary>
    public class CatalogItem {
        public Guid Id { get; set; }
        public int Level { get; set; }
        public Guid ProductId { get; set; }
        public Guid ParentId { get; set; }
        public Guid SubtreeId { get; set; }
        public string ItemName { get; set; }
        public double Volume { get; set; }
        public double Weight { get; set; }
        public double PartsCount { get; set; }
        public Guid OptionsId { get; set; }
        public Guid NomenclatureId { get; set; }

        public Option Option { get; set; }
        public Nomenclature Nomenclature { get; set; }

        public ICollection<CatalogItemPrice> CatalogItemPrices { get; set; }
        public ICollection<StorehouseRests> StorehouseRests { get; set; }
    }
}