﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.EmployeeConfiguration"/>
    /// </summary>
    public class Employee {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime EmploymentDate { get; set; }
        public DateTime? FiredDate { get; set; }
        public int Status { get; set; }
        public Guid? UserId { get; set; }

        public User User { get; set; }
        public ICollection<DocRegistry> DocumentsCreated { get; set; }
        public ICollection<DocRegistry> DocumentsApplied { get; set; }
        public ICollection<DocRegistry> DocumentsExecuted { get; set; }
    }
}
