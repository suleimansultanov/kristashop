﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.SizeLineConfiguration"/>
    /// </summary>
    public class SizeLine {
        public Guid Id { get; set; }
        public string SizeLineName { get; set; }
        public int SizeLinePos { get; set; }
        public string SizesIdList { get; set; }

        public virtual ICollection<NomFilterSizeLine> NomFilterSizeLines { get; set; }
    }
}