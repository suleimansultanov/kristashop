﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.StorehouseConfiguration"/>
    /// </summary>
    public class Storehouse {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsForOnlineStore { get; set; }

        public ICollection<StorehouseRests> Rests { get; set; }
    }
}