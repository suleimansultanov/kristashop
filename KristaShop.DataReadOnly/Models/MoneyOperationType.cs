﻿using System;
using KristaShop.Common.Enums;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.MoneyOperationTypeConfiguration"/>
    /// </summary>
    public class MoneyOperationType {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsIncome { get; set; }
        public bool IsDefault { get; set; }
        public MoneyOpType Type { get; set; }
    }
}