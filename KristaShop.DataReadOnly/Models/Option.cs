﻿using System;
using System.Collections.Generic;

namespace KristaShop.DataReadOnly.Models {
    /// <summary>
    /// Configuration file for this entity <see cref="Configurations.OptionsConfiguration"/>
    /// </summary>
    public class Option {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public NomFilterColor NomFilterColor { get; set; }
        public virtual NomFilterSize NomFilterSize { get; set; }
        public virtual NomFilterSizeLine NomFilterSizeLine { get; set; }

        public ICollection<CatalogItem> CatalogItems { get; set; }
    }
}