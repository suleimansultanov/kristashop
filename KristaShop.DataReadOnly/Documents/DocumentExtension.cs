﻿using System;
using System.Collections.Generic;
using System.Linq;
using KristaShop.DataReadOnly.Documents.ExtraDataDeserializers;

namespace KristaShop.DataReadOnly.Documents {
    public enum DocumentType {
        Other = -1,
        Order,
        PrepayInvoice,
        PaymentInvoice,
        CashboxIncomeOrder,
        CashboxOutcomeOrder,
        FreeInvoice,
        Shipment
    }

    public enum DocumentNameType {
        Basic,
        Balance
    }

    public class DocumentDescription {
        public string Type { get; set; }
        public string Name { get; set; }
        public Dictionary<DocumentNameType, string> Names;
        public bool HasDetailsList { get; set; }
        public IExtraData ExtraData { get; set; }

        public DocumentDescription(string type, string basicName,
            bool hasDetailsList = false, IExtraData extraData = null) {
            Type = type;
            HasDetailsList = hasDetailsList;
            ExtraData = extraData;

            Names = new Dictionary<DocumentNameType, string> {
                { DocumentNameType.Basic, basicName }
            };
        }
    }

    public static class DocumentExtension {

        public static readonly Dictionary<DocumentType, DocumentDescription> DocumentsDictionary;
        
        static DocumentExtension() {
            DocumentsDictionary = new Dictionary<DocumentType, DocumentDescription>();
            DocumentsDictionary.Add(DocumentType.Order, new DocumentDescription("EntityDocumentOrder", "Заказ", true, new OrderDeserializer()));
            DocumentsDictionary.Add(DocumentType.PrepayInvoice, new DocumentDescription("EntityDocumentPrepayInvoice", "Авансовый счет на оплату", true, new PrepayInvoiceDeserializer()));
            DocumentsDictionary.Add(DocumentType.PaymentInvoice, new DocumentDescription("EntityDocumentPaymentInvoice", "Счет на оплату", true, new PaymentInvoiceDeserializer()));
            DocumentsDictionary.Add(DocumentType.FreeInvoice, new DocumentDescription("EntityDocumentFreeInvoice", "Счет на оплату", false, new FreeInvoiceDeserializer()));

            var cashboxIncomeOrder = new DocumentDescription("EntityDocumentCashboxIncomeOrder", "Оплата по счёту", false, new CashboxIncomeOrderDeserializer());
            cashboxIncomeOrder.Names.Add(DocumentNameType.Balance, "Пополнение баланса");
            DocumentsDictionary.Add(DocumentType.CashboxIncomeOrder, cashboxIncomeOrder);

            var cashboxOutcomeOrder = new DocumentDescription("EntityDocumentCashboxOutcomeOrder", "Возврат", false, new CashboxOutcomeOrderDeserializer());
            cashboxOutcomeOrder.Names.Add(DocumentNameType.Balance, "Возврат на баланс");
            DocumentsDictionary.Add(DocumentType.CashboxOutcomeOrder, cashboxOutcomeOrder);
            
            DocumentsDictionary.Add(DocumentType.Shipment, new DocumentDescription("EntityDocumentShipment", "Отправка", true));
            DocumentsDictionary.Add(DocumentType.Other, new DocumentDescription("Other", ""));
        }

        public static string GetDocType(this DocumentType documentType) {
            if (DocumentsDictionary.ContainsKey(documentType)) {
                return DocumentsDictionary[documentType].Type;
            }

            return string.Empty;
        }

        public static string GetDocName(this DocumentType documentType, DocumentNameType type = DocumentNameType.Basic) {
            if (DocumentsDictionary.ContainsKey(documentType)) {
                if (DocumentsDictionary[documentType].Names.ContainsKey(type)) {
                    return DocumentsDictionary[documentType].Names[type];
                }

                return DocumentsDictionary[documentType].Names[DocumentNameType.Basic];
            }

            return string.Empty;
        }

        public static DocumentDescription GetDocument(this DocumentType documentType) {
            if (DocumentsDictionary.ContainsKey(documentType)) {
                return DocumentsDictionary[documentType];
            }

            return null;
        }

        public static List<string> GetDocumentTypes() {
            return DocumentsDictionary.Select(x => x.Value.Type).ToList();
        }

        public static DocumentType GetDocTypeByName(string docType) {
            var name = docType.Replace("EntityDocument", string.Empty);
            if (Enum.TryParse<DocumentType>(name, true, out var result)) {
                return result;
            }

            return DocumentType.Other;
        }

        public static bool IsEquals(this DocumentType type, string documentType) {
            return GetDocType(type).Equals(documentType);
        }
    }
}
