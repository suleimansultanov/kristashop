﻿using System;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using KristaShop.DataReadOnly.Models.DocumentExtraData;
using Newtonsoft.Json;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public class PaymentInvoiceDeserializer : IExtraData {
        public async Task<IExtraDataDataProvider> DeserializeAsync(string serializableData, Guid documentId, IDocRegistryService service, Currency currency, ExchangeRate rate = null, double sum = 0) {
            var extraData = JsonConvert.DeserializeObject<PaymentInvoiceExtraData>(serializableData);
            var result = new ExtraDataDataProvider();
            result.Rate = rate?.Rate ?? await service.GetRateSnapshotAsync(currency.Id, extraData.ExchangeRateSnapshotId);
            result.Sum = sum < 1E-6 ? await service.GetTotalSumByDocIdAsync(documentId) : sum;
            result.SumRu = result.Sum * result.Rate;
            return result;
        }
    }
}