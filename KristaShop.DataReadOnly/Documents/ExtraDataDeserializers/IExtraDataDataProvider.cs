﻿using System;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public interface IExtraDataDataProvider {
        bool VisibleForUser { get; set; }
        double Sum { get; set; }
        double SumRu { get; set; }
        double Rate { get; set; }
        double Percent { get; set; }
        bool ToCounterpartyBalance { get; set; }
        Guid CurrencyId { get; set; }
    }
}