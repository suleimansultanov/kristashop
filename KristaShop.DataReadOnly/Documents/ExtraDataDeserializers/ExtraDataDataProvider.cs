﻿using System;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public class ExtraDataDataProvider : IExtraDataDataProvider {
        public bool VisibleForUser { get; set; }
        public double Sum { get; set; }
        public double SumRu { get; set; }
        public double Rate { get; set; }
        public double Percent { get; set; }
        public bool ToCounterpartyBalance { get; set; }
        public Guid CurrencyId { get; set; }

        public ExtraDataDataProvider() {
            VisibleForUser = true;
        }
    }
}