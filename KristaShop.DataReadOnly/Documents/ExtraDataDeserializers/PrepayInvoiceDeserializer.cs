﻿using System;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using KristaShop.DataReadOnly.Models.DocumentExtraData;
using Newtonsoft.Json;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public class PrepayInvoiceDeserializer : IExtraData {
        public async Task<IExtraDataDataProvider> DeserializeAsync(string serializableData, Guid documentId, IDocRegistryService service, Currency currency, ExchangeRate rate = null, double totalSum = 0) {
            var extraData = JsonConvert.DeserializeObject<PrepayInvoiceExtraData>(serializableData);
            var result = new ExtraDataDataProvider();
            result.Rate = rate?.Rate ?? await service.GetRateSnapshotAsync(currency.Id, extraData.ExchangeRateSnapshotId);
            result.Sum = totalSum < 1E-6 ? await service.GetTotalSumByDocIdAsync(documentId, extraData.Percent) : totalSum / 100 * extraData.Percent;
            result.SumRu = result.Sum * result.Rate;
            result.Percent = extraData.Percent;
            result.CurrencyId = extraData.CurrencyId;
            //result.Rate *= result.Percent;

            return result;
        }
    }
}