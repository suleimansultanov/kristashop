﻿using System;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using KristaShop.DataReadOnly.Models.DocumentExtraData;
using Newtonsoft.Json;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public class FreeInvoiceDeserializer : IExtraData {
        public async Task<IExtraDataDataProvider> DeserializeAsync(string serializableData, Guid documentId, IDocRegistryService service, Currency currency, ExchangeRate rate = null, double sum = 0) {
            var extraData = JsonConvert.DeserializeObject<FreeInvoiceExtraData>(serializableData);
            var result = new ExtraDataDataProvider();
            result.VisibleForUser = extraData.VisibleForCounterparty;
            result.Rate = await service.GetRateSnapshotAsync(extraData.CurrencyId, extraData.ExchangeRateSnapshotId);
            if (result.Rate > 1E-6)
                result.Sum = extraData.Summ / result.Rate;
            var rateRu = rate?.Rate ?? await service.GetRateSnapshotAsync(currency.Id, extraData.ExchangeRateSnapshotId);
            result.SumRu = result.Sum * rateRu;
            return result;
        }
    }
}