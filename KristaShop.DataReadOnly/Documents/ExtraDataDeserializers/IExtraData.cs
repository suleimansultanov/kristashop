﻿using System;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.DataReadOnly.Documents.ExtraDataDeserializers {
    public interface IExtraData {
        Task<IExtraDataDataProvider> DeserializeAsync(string serializableData, Guid documentId, IDocRegistryService service, Currency currency, ExchangeRate rate = null, double sum = 0);
    }
}