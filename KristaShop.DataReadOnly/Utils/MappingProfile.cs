﻿using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.DataReadOnly.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Counterparty, UserClientDTO>()
                .ForMember(dest => dest.CPId, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.ClientFullName, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.PhoneNumber, dest => dest.MapFrom(item => item.Phone))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Email))
                .ForMember(dest => dest.ShopName, dest => dest.MapFrom(item => item.MallAddress))
                .ForMember(dest => dest.CityName, dest => dest.MapFrom(item => item.CityName));
            CreateMap<User, UserClientDTO>()
                .ForMember(dest => dest.CPId, dest => dest.MapFrom(item => item.Counterparty.Id))
                .ForMember(dest => dest.ClientFullName, dest => dest.MapFrom(item => item.Counterparty.Person))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Counterparty.Title))
                .ForMember(dest => dest.PhoneNumber, dest => dest.MapFrom(item => item.Counterparty.Phone))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Counterparty.Email))
                .ForMember(dest => dest.ShopName, dest => dest.MapFrom(item => item.Counterparty.MallAddress))
                .ForMember(dest => dest.CityName, dest => dest.MapFrom(item => item.Counterparty.CityName))
                .ForMember(dest => dest.CityId, dest => dest.MapFrom(item => item.Counterparty.CityId))
                .ForMember(dest => dest.Login, dest => dest.MapFrom(item => item.Login))
                .ForMember(dest => dest.Status, dest => dest.MapFrom(item => item.Status))
                .ForMember(dest => dest.StatusName, dest => dest.MapFrom(item => EnumExtensions<UserStatus>.GetDisplayValue(item.Status)))
                .ForMember(dest => dest.UserId, dest => dest.MapFrom(item => item.Id));

            CreateMap<User, UserSession>()
                .ForMember(dest => dest.CounterParty, dest => dest.MapFrom(item => item.Counterparty))
                .ForMember(dest => dest.IsRoot, dest => dest.MapFrom(item => item.IsRoot))
                .ForMember(dest => dest.Login, dest => dest.MapFrom(item => item.Login))
                .ForMember(dest => dest.UserId, dest => dest.MapFrom(item => item.Id));
            CreateMap<Counterparty, ClientDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.ClientFullName, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.PhoneNumber, dest => dest.MapFrom(item => item.Phone))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Email))
                .ForMember(dest => dest.ShopName, dest => dest.MapFrom(item => item.MallAddress))
                .ForMember(dest => dest.CityName, dest => dest.MapFrom(item => item.CityName));
            CreateMap<User, ClientDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Counterparty.Id))
                .ForMember(dest => dest.ClientFullName, dest => dest.MapFrom(item => item.Counterparty.Person))
                .ForMember(dest => dest.PhoneNumber, dest => dest.MapFrom(item => item.Counterparty.Phone))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Counterparty.Email))
                .ForMember(dest => dest.ShopName, dest => dest.MapFrom(item => item.Counterparty.MallAddress))
                .ForMember(dest => dest.CityName, dest => dest.MapFrom(item => item.Counterparty.CityName))
                .ForMember(dest => dest.Login, dest => dest.MapFrom(item => item.Login))
                .ForMember(dest => dest.Status, dest => dest.MapFrom(item => item.Status))
                .ForMember(dest => dest.StatusName, dest => dest.MapFrom(item => EnumExtensions<UserStatus>.GetDisplayValue((UserStatus)item.Status)))
                .ForMember(dest => dest.UserId, dest => dest.MapFrom(item => item.Id));

            CreateMap<Counterparty, CounterpartyDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(item => item.Id))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(item => item.Title))
                .ForMember(dest => dest.Person, opt => opt.MapFrom(item => item.Person))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(item => item.Phone))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(item => item.Email))
                .ForMember(dest => dest.MallAddress, opt => opt.MapFrom(item => item.MallAddress))
                .ForMember(dest => dest.Balance, opt => opt.MapFrom(item => item.Balance))
                .ForMember(dest => dest.City, opt => opt.MapFrom(item => item.CityName));

            CreateMap<Color, ColorDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Name, dest => dest.MapFrom(item => item.ColorName))
                .ForMember(dest => dest.Code, dest => dest.MapFrom(item => item.ColorCode))
                .ForMember(dest => dest.Image, dest => dest.MapFrom(item => item.ColorImage));

            CreateMap<Size, SizeDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Name, dest => dest.MapFrom(item => item.SizeName))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.SizeValue));

            CreateMap<SizeLine, SizeLineDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Name, dest => dest.MapFrom(item => item.SizeLineName))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.SizeLinePos));

            CreateMap<CatalogItem, ProductDTO>()
                .ForMember(dest => dest.ProductId, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.IsSet, dest => dest.MapFrom(item => item.Nomenclature.IsSet))
                .ForMember(dest => dest.Color, dest => dest.MapFrom(item => item.Option.NomFilterColor.Color))
                .ForMember(dest => dest.Size, dest => dest.MapFrom(item => item.Option.NomFilterSize.Size))
                .ForMember(dest => dest.SizeLine, dest => dest.MapFrom(item => item.Option.NomFilterSizeLine.SizeLine));

            CreateMap<OrderSubtreeItem, OrderItemsDTO>()
                .ForMember(dest => dest.ProductId, dest => dest.MapFrom(item => item.product_id))
                .ForMember(dest => dest.Quantity, dest => dest.MapFrom(item => item.items_count))
                .ForMember(dest => dest.DocumentType, dest => dest.MapFrom(item => item.doc_type))
                .ForMember(dest => dest.DocumentId, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Level, dest => dest.MapFrom(item => item.level))
                .ForMember(dest => dest.Status, dest => dest.MapFrom(item => item.status))
                .ForMember(dest => dest.Processed, dest => dest.MapFrom(item => item.processed_count));

            CreateMap<OrderItemsPrice, OrderItemsDTO>()
                .ForMember(dest => dest.ColorName, dest => dest.MapFrom(item => item.color_name))
                .ForMember(dest => dest.ColorCode, dest => dest.MapFrom(item => item.color_code))
                .ForMember(dest => dest.ColorImage, dest => dest.MapFrom(item => item.ColorImagePath))
                .ForMember(dest => dest.SizeName, dest => dest.MapFrom(item => string.IsNullOrEmpty(item.size_line_name) ? item.size_name : item.size_line_name))
                .ForMember(dest => dest.ProductId, dest => dest.MapFrom(item => item.product_id))
                .ForMember(dest => dest.NomId, dest => dest.MapFrom(item => item.nomenclature_id))
                .ForMember(dest => dest.PartsCount, dest => dest.MapFrom(item => item.parts_count))
                .ForMember(dest => dest.Quantity, dest => dest.MapFrom(item => item.items_count))
                .ForMember(dest => dest.QuantitySrc, dest => dest.MapFrom(item => item.items_count_src))
                .ForMember(dest => dest.IsPreorder, dest => dest.MapFrom(item => item.is_preorder))
                .ForMember(dest => dest.Price, dest => dest.MapFrom(item => item.price))
                .ForMember(dest => dest.Discount, dest => dest.MapFrom(item => item.discount))
                .ForMember(dest => dest.Units, dest => dest.MapFrom(item => item.units))
                .ForMember(dest => dest.PrePay, dest => dest.MapFrom(item => item.pre_pay))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.NomType, dest => dest.MapFrom(item => item.nom_type));

            CreateMap<OrderStoreIncomeItem, OrderItemsDTO>()
                .ForMember(dest => dest.PartsCount, dest => dest.MapFrom(item => item.parts_count))
                .ForMember(dest => dest.ColorName, dest => dest.MapFrom(item => item.color_name))
                .ForMember(dest => dest.ColorCode, dest => dest.MapFrom(item => item.color_code))
                .ForMember(dest => dest.ColorImage, dest => dest.MapFrom(item => item.ColorImagePath))
                .ForMember(dest => dest.SizeName, dest => dest.MapFrom(item => string.IsNullOrEmpty(item.size_line_name) ? item.size_name : item.size_line_name))
                .ForMember(dest => dest.ProductId, dest => dest.MapFrom(item => item.product_id))
                .ForMember(dest => dest.NomId, dest => dest.MapFrom(item => item.nomenclature_id))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.articul))
                .ForMember(dest => dest.Quantity, dest => dest.MapFrom(item => item.items_count));

            CreateMap<DocRegistryFile, DocRegistryFileDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.FileName, dest => dest.MapFrom(item => item.Filename))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.VirtualPath, dest => dest.MapFrom(item => item.VirtualPath));
        }
    }
}