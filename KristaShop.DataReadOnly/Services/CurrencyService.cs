﻿using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Services
{
    public class CurrencyService : ICurrencyService
    {
        private readonly IReadOnlyRepo<Currency> _currencyRepo;
        private readonly IReadOnlyRepo<ExchangeRate> _exchangeRateRepo;

        public CurrencyService
            (IReadOnlyRepo<Currency> currencyRepo, IReadOnlyRepo<ExchangeRate> exchangeRateRepo)
        {
            _currencyRepo = currencyRepo;
            _exchangeRateRepo = exchangeRateRepo;
        }

        public async Task<double> GetLastExchangeRate()
        {
            var defaultCurrency = await _currencyRepo.FindByFilterAsync(x => x.Type == CurrencyType.USD && x.IsDefault);
            var rubCurrency = await _currencyRepo.FindByFilterAsync(x => x.Type == CurrencyType.RUB);
            var exchangeRate = await _exchangeRateRepo.QueryFindBy(x => x.ToCurrencyId == rubCurrency.Id
                && x.FromCurrencyId == defaultCurrency.Id)
                .OrderByDescending(x => x.AddDate)
                .FirstOrDefaultAsync();
            return exchangeRate.Rate;
        }
    }
}