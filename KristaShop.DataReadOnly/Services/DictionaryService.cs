﻿using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KristaShop.DataReadOnly.Services
{
    public class DictionaryService : IDictionaryService
    {
        private readonly IReadOnlyRepo<City> _cityRepo;
        private readonly IReadOnlyRepo<AccessControl> _aclRepo;
        private readonly IReadOnlyRepo<UserGroup> _ugRepo;
        private readonly IReadOnlyRepo<Color> _colorRepo;
        private readonly IReadOnlyRepo<Size> _sizeRepo;
        private readonly IReadOnlyRepo<SizeLine> _slRepo;
        private readonly IReadOnlyRepo<Nomenclature> _nomRepo;

        public DictionaryService
            (IReadOnlyRepo<City> cityRepo, IReadOnlyRepo<AccessControl> aclRepo, IReadOnlyRepo<UserGroup> ugRepo,
            IReadOnlyRepo<Color> colorRepo, IReadOnlyRepo<Size> sizeRepo, IReadOnlyRepo<SizeLine> slRepo, IReadOnlyRepo<Nomenclature> nomRepo)
        {
            _cityRepo = cityRepo;
            _aclRepo = aclRepo;
            _ugRepo = ugRepo;
            _colorRepo = colorRepo;
            _sizeRepo = sizeRepo;
            _slRepo = slRepo;
            _nomRepo = nomRepo;
        }

        public Dictionary<Guid, string> GetPackagingList()
        {
            return _nomRepo.QueryReadOnly()
                .Where(x => !x.VisibleOnSite && x.NomType == (int)NomenclatureType.Packaging)
                .ToDictionary(x => x.Id, x => $"{x.Name} {x.CatalogItems}");
        }

        public List<CityDTO> GetCities()
        {
            return _cityRepo.QueryReadOnly()
                .Select(x => new CityDTO
                {
                    Id = x.Id,
                    Name = x.Name
                }).OrderBy(x => x.Name).ToList();
        }

        public List<UserGroupDTO> GetGroups()
        {
            return _ugRepo.QueryReadOnly()
                .Select(x => new UserGroupDTO
                {
                    Id = x.Id,
                    Name = x.GroupName
                }).ToList();
        }

        public List<AccessControlDTO> GetAcls()
        {
            return _aclRepo.QueryReadOnly()
                .Select(x => new AccessControlDTO
                {
                    Id = x.Acl,
                    Name = x.AclName
                }).ToList();
        }

        public List<SizeDTO> GetSizes()
        {
            return _sizeRepo.QueryReadOnly()
                .Select(x => new SizeDTO
                {
                    Id = x.Id,
                    Name = x.SizeName,
                    Order = x.SizeValue
                }).OrderBy(x => x.Order).ToList();
        }

        public List<SizeLineDTO> GetSizeLines()
        {
            return _slRepo.QueryReadOnly()
                .Select(x => new SizeLineDTO
                {
                    Id = x.Id,
                    Name = x.SizeLineName,
                    Order = x.SizeLinePos
                }).OrderBy(x => x.Order).ToList();
        }

        public List<ColorDTO> GetColors()
        {
            return _colorRepo.QueryReadOnly()
                .Select(x => new ColorDTO
                {
                    Id = x.Id,
                    Name = x.ColorName,
                    Code = x.ColorCode,
                    Image = x.ColorImage
                }).OrderBy(x => x.Name).ToList();
        }
    }
}