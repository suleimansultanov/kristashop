﻿using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Services
{
    public class UserService : IUserService
    {
        private readonly IReadOnlyRepo<User> _userRepo;
        private readonly IReadOnlyRepo<UserGroupMembership> _ugmRepo;
        private readonly IReadOnlyRepo<Counterparty> _counterpartyRepo;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;

        public UserService
            (IReadOnlyRepo<User> userRepo, IReadOnlyRepo<UserGroupMembership> ugmRepo, IReadOnlyRepo<Counterparty> counterpartyRepo,
            IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _userRepo = userRepo;
            _ugmRepo = ugmRepo;
            _counterpartyRepo = counterpartyRepo;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public List<UserClientDTO> GetAllUsers(UserSession user) {
            var users = _ugmRepo.QueryReadOnly()
                .Include(x => x.User).ThenInclude(x => x.Counterparty).ThenInclude(x => x.City)
                .Include(x => x.UserGroup)
                .Where(x => x.UserGroup.Type == UserType.Customer)
                .OrderBy(x => x.User.Counterparty.Title)
                .Select(x => x.User);

            bool? isAdmin = user.UserGroups?.Any(x => x.UserType != (int)UserType.Manager && x.UserType == (int)UserType.Administrator);
            if (isAdmin == true || (isAdmin == null && user.IsRoot))
            {
                return _mapper.Map<List<UserClientDTO>>(users.ToList());
            }

            bool isManager = user.UserGroups.Any(x => x.UserType == (int)UserType.Manager);
            if (isManager)
            {
                users = users.Where(x => x.Counterparty.ManagerId == user.UserId);
                return _mapper.Map<List<UserClientDTO>>(users.ToList());
            }
            return null;
        }

        public async Task<UserClientDTO> GetUserAsync(Guid id) {
            var user = await _userRepo.QueryReadOnly()
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == id);

            return user == null ? null : _mapper.Map<UserClientDTO>(user);
        }

        public async Task<CounterpartyDTO> GetCounterpartyDetailsAsync(Guid id) {
            var counterparty = await _counterpartyRepo.QueryFindBy(x => x.Id == id)
                .Include(x => x.City)
                .SingleOrDefaultAsync();

            if (counterparty != null) {
                return _mapper.Map<CounterpartyDTO>(counterparty);
            }

            return null;
        }

        public async Task<bool> IsActiveUserAsync(Guid userId)
        {
            var user = await _userRepo.FindByIdAsync(userId);
            return user.Status == UserStatus.Active;
        }

        public async Task<OperationResult> SignIn(string login, string pass, bool isPersistent = false, bool isBackend = true)
        {
            var user = await _userRepo.QueryFindBy(x => x.Login == login && x.Password == HashHelper.TransformPassword(pass))
                .Include(x => x.Counterparty).ThenInclude(x => x.City)
                .SingleOrDefaultAsync();

            var (userSession, result) = await GetUserResult(user);
            if (result.IsSuccess)
            {
                if (isBackend)
                {
                    if (!user.IsRoot && userSession.AccessLevel < (int)AccessControlLevel.Operator)
                        return OperationResult.Failure(new List<string> { "Неверный логин или пароль!" });
                    await SetClaimsAsync(userSession, isPersistent);
                }
                else
                {
                    if (userSession.AccessLevel == (int)AccessControlLevel.Manager)
                        await SetFrontClaimsAsync(userSession, isPersistent);
                    else if (user.IsRoot || userSession.AccessLevel >= (int)AccessControlLevel.Operator)
                        return OperationResult.Failure(new List<string> { "Неверный логин или пароль!" });
                    await SetFrontClaimsAsync(userSession, isPersistent);
                }
            }
            return result;
        }

        public async Task<OperationResult> SignInByLink(Guid userId)
        {
            var user = await _userRepo.QueryFindBy(x => x.Id == userId)
                .Include(x => x.Counterparty).ThenInclude(x => x.City)
                .SingleOrDefaultAsync();

            var (userSession, result) = await GetUserResult(user);
            if (result.IsSuccess)
                await SetFrontClaimsAsync(userSession, true);
            return result;
        }

        public async Task SignOut(bool isBackend = true)
        {
            if (isBackend)
                await _httpContextAccessor.HttpContext.SignOutAsync("BackendScheme");
            else
                await _httpContextAccessor.HttpContext.SignOutAsync("FrontendScheme");
        }

        private async Task<(UserSession, OperationResult)> GetUserResult(User user)
        {
            if (user == null)
                return (null, OperationResult.Failure(new List<string> { "Неверный логин или пароль!" }));

            var userSession = _mapper.Map<UserSession>(user);
            if (!userSession.IsRoot)
            {
                userSession.UserGroups = await GetUserGroups(user.Id);
                userSession.AccessLevel = userSession.UserGroups.Count > 0 ? userSession.UserGroups.Min(x => x.AccessLevel) : -1;
            }

            return user.Status switch
            {
                UserStatus.Active when userSession != null => (userSession, OperationResult.Success(new List<string> { "Вы успешно авторизовались." })),
                UserStatus.Active when userSession.AccessLevel == -1 => (null, OperationResult.Failure(new List<string> { "Данный пользователь не имеет доступа. Обратитесь к администратору." })),
                UserStatus.Await => (null, OperationResult.Failure(new List<string> { "Данный пользователь на рассмотрении." })),
                UserStatus.Banned when user.BanExpireDate == null => (null, OperationResult.Failure(new List<string> { $"Данный пользователь заблокирован навсегда." })),
                UserStatus.Banned when user.BanExpireDate != null => (null, OperationResult.Failure(new List<string> { $"Данный пользователь заблокирован до {user.BanExpireDate}." })),
                UserStatus.Banned when user.BanReason != null => (null, OperationResult.Failure(new List<string> { $"Данный пользователь заблокирован по причине {user.BanReason}." })),
                UserStatus.Deleted => (null, OperationResult.Failure(new List<string> { "Неверный логин или пароль!" })),
                _ => (null, OperationResult.Failure(new List<string> { "Неверный логин или пароль!" }))
            };
        }

        private async Task<List<GroupDTO>> GetUserGroups(Guid userId)
        {
            return await _ugmRepo.QueryFindBy(x => x.UserId == userId)
                .Select(x => new GroupDTO
                {
                    GroupId = x.UserGroup.Id,
                    UserType = (int) x.UserGroup.Type,
                    AccessLevel = x.UserGroup.Acl.Acl
                }).ToListAsync();
        }

        private async Task SetClaimsAsync(UserSession user, bool isPersistent)
        {
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(GlobalConstant.SessionKeys.BackUser, JsonConvert.SerializeObject(user)));
            var principal = new ClaimsPrincipal(identity);
            await _httpContextAccessor.HttpContext.SignInAsync("BackendScheme", principal,
              new AuthenticationProperties
              {
                  ExpiresUtc = isPersistent ? DateTime.UtcNow.AddDays(14) : (DateTime?)null,
                  IsPersistent = isPersistent,
              });
        }

        private async Task SetFrontClaimsAsync(UserSession user, bool isPersistent)
        {
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(GlobalConstant.SessionKeys.FrontUser, JsonConvert.SerializeObject(user)));
            var principal = new ClaimsPrincipal(identity);
            await _httpContextAccessor.HttpContext.SignInAsync("FrontendScheme", principal,
              new AuthenticationProperties
              {
                  ExpiresUtc = isPersistent ? DateTime.UtcNow.AddDays(14) : (DateTime?)null,
                  IsPersistent = isPersistent,
              });
        }
    }
}