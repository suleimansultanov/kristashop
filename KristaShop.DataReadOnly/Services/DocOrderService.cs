﻿using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Documents;

namespace KristaShop.DataReadOnly.Services
{
    public class DocOrderService : IDocOrderService
    {
        private readonly IMapper _mapper;
        private readonly KristaReplicaDbContext _dbContext;

        private List<OrderItemsDTO> _orderItemsStatuses;
        private List<Guid> _idList = new List<Guid>();

        public DocOrderService
            (KristaReplicaDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<OrderItemsDTO>> GetOrderItemsFromStoreIncomeAsync(Guid orderId)
        {
            var p1 = new MySqlParameter("?doc_id", orderId);
            const string request =
              @"SELECT store.product_id, store.items_count, product.parts_count,
              product.item_name, product.nomenclature_id, nomenclature.articul, sizes.size_name, size_lines.size_line_name,
              colors.color_name, colors.color_code, colors.color_image
              FROM doc_store_income_items AS store
              JOIN catalog_item AS product ON store.product_id = product.product_id
              JOIN nom_list AS nomenclature ON product.nomenclature_id = nomenclature.id
              LEFT JOIN nom_filter_color AS filter_color ON product.options_id = filter_color.option_id
              LEFT JOIN dict_colors AS colors ON filter_color.color_id = colors.id
              LEFT JOIN nom_filter_size AS filter_size ON product.options_id = filter_size.option_id AND nomenclature.is_set = 0
              LEFT JOIN dict_sizes AS sizes ON filter_size.size_id = sizes.id
              LEFT JOIN nom_filter_size_lines AS filter_size_lines ON product.options_id = filter_size_lines.option_id AND nomenclature.is_set = 1
              LEFT JOIN dict_size_lines AS size_lines ON filter_size_lines.size_line_id = size_lines.id
              WHERE store.document_id = ?doc_id";
            var items = await _dbContext.Set<OrderStoreIncomeItem>().FromSqlRaw(request, p1).ToListAsync();
            return _mapper.Map<List<OrderItemsDTO>>(items);
        }

        public async Task<List<OrderItemsDTO>> GetOrderItemsWithStatuses(Guid orderId, bool orderByStatus = true) {
            var order = await _dbContext.DocRegistries.FindAsync(orderId);
            var items = await GetOrderItemsStatusesAsync(order, await GetOrderSubtreeItemsAsync(orderId));
            var result = UpdateOrderItemsPrices(await GetOrderItemsPricesAsync(orderId), items);
            if (orderByStatus) {
               return result.OrderBy(x => x.NomType).ThenByDescending(x => x.Level).ThenBy(x => x.Status).ToList();
            }

            return result;
        }

        private async Task<List<OrderItemsDTO>> GetOrderSubtreeItemsAsync(Guid orderId)
        {
            var p1 = new MySqlParameter("?doc_id", orderId);
            var p2 = new MySqlParameter("?order_doc", DocumentType.Order.GetDocType());
            var p3 = new MySqlParameter("?guid_empty", Guid.Empty);
            var request = @"SELECT docs.id, docs.pos, docs.level, docs.status, docs.parent_id, docs.doc_type,
                          ifnull(order_items.product_id, storehouse_items.product_id) as product_id,
                          sum(ifnull(order_items.items_count, storehouse_items.items_count)) as items_count,
                          sum(ifnull(order_items.processed_count, storehouse_items.items_count)) as processed_count
                          FROM docs_registry AS docs
                          JOIN docs_registry AS current_doc ON docs.subtree_id = current_doc.subtree_id
                          LEFT JOIN doc_order_items AS order_items ON docs.id = order_items.document_id
                          LEFT JOIN doc_store_income_items AS storehouse_items ON docs.id = storehouse_items.document_id
                          WHERE current_doc.id = ?doc_id AND docs.is_aplied = 1 AND (docs.status > 0 || (docs.status = -2 and docs.doc_type = ?order_doc and docs.parent_id = ?guid_empty))
                          GROUP BY docs.id, docs.pos, docs.level, docs.status, order_items.product_id, storehouse_items.product_id
                          ORDER BY docs.level DESC";
            var items = await _dbContext.Set<OrderSubtreeItem>().FromSqlRaw(request, p1, p2, p3).ToListAsync();
            return _mapper.Map<List<OrderItemsDTO>>(items);
        }

        public async Task<List<OrderItemsDTO>> GetOrderItemsPricesAsync(Guid orderId)
        {
            var p1 = new MySqlParameter("?doc_id", orderId);
            const string request =
              @"SELECT order_items.product_id, order_items.pre_pay, order_items.is_preorder, order_items.price, order_items.discount, order_items.items_count, order_items.items_count_src,
              order_items.items_count * (order_items.price + order_items.discount) - order_items.pay_in AS total_debt,
              order_items.items_count - order_items.processed_count AS total_unprocessed, unit.name AS units,
              product.item_name, product.nomenclature_id, nomenclature.articul, product.parts_count, nomenclature.nom_type,
              colors.color_name, colors.color_code, colors.color_image, sizes.size_name, size_lines.size_line_name
              FROM doc_order_items AS order_items
              JOIN catalog_item AS product ON order_items.product_id = product.product_id AND product.level = 1
              JOIN nom_list AS nomenclature ON product.nomenclature_id = nomenclature.id
              JOIN units AS unit ON nomenclature.units_id = unit.id
              LEFT JOIN nom_filter_color AS filter_color ON product.options_id = filter_color.option_id
              LEFT JOIN dict_colors AS colors ON filter_color.color_id = colors.id
              LEFT JOIN nom_filter_size AS filter_size ON product.options_id = filter_size.option_id AND nomenclature.is_set = 0
              LEFT JOIN dict_sizes AS sizes ON filter_size.size_id = sizes.id
              LEFT JOIN nom_filter_size_lines AS filter_size_lines ON product.options_id = filter_size_lines.option_id AND nomenclature.is_set = 1
              LEFT JOIN dict_size_lines AS size_lines ON filter_size_lines.size_line_id = size_lines.id
              WHERE document_id = ?doc_id
              ORDER BY total_debt, total_unprocessed";
            var items = await _dbContext.Set<OrderItemsPrice>().FromSqlRaw(request, p1).ToListAsync();
            return _mapper.Map<List<OrderItemsDTO>>(items);
        }

        private List<OrderItemsDTO> UpdateOrderItemsPrices(List<OrderItemsDTO> orderItems, List<OrderItemsDTO> items)
        {
            foreach (var orderItem in orderItems)
            {
                var orderProductItems = items.Where(x => x.ProductId == orderItem.ProductId && string.IsNullOrEmpty(x.Description)).ToList();
                var item = orderProductItems.Find(x => Math.Abs(x.Quantity - orderItem.Quantity) <= 1E-6);
                if (item != null)
                {
                    item.CopyProductInfo(orderItem);
                }
                else
                {
                    var quantity = orderItem.Quantity;
                    foreach (var orderProductItem in orderProductItems.OrderByDescending(x => x.Level).ThenBy(x => x.Quantity))
                    {
                        if (quantity > 1E-6 && quantity - orderProductItem.Quantity < -1E-6 && orderProductItem.Clone() is OrderItemsDTO newItem)
                        {
                            newItem.Quantity -= quantity;
                            orderProductItem.Quantity = quantity;
                            items.Add(newItem);
                        }

                        if (quantity - orderProductItem.Quantity >= 0)
                        {
                            orderProductItem.CopyProductInfo(orderItem);
                            quantity -= orderProductItem.Quantity;
                        }
                    }
                }

                if (Math.Abs(orderItem.QuantitySrc - orderItem.Quantity) > 1E-6)
                {
                    var canceledItem = orderItem.Clone() as OrderItemsDTO;
                    canceledItem.Quantity = canceledItem.QuantitySrc - canceledItem.Quantity;
                    canceledItem.Status = UniversalDocState.Canceled;
                    items.Add(canceledItem);
                }
            }
            return items;
        }

        private async Task<List<OrderItemsDTO>> GetOrderItemsStatusesAsync(DocRegistry order, List<OrderItemsDTO> items)
        {
            order.ChildNodes = await BuildTreeAsync(order);
            _orderItemsStatuses = new List<OrderItemsDTO>();
            var availableDocs = new List<string> {
                "EntityDocumentOrder", "EntityDocumentStorehouseReservation",
                "EntityDocumentShipment", "EntityDocumentRequestToProduction"
            };
            PostorderOrderSubtree(order.ChildNodes.ToList(), items, availableDocs, 0);
            return _orderItemsStatuses;
        }

        private async Task<List<DocRegistry>> BuildTreeAsync(DocRegistry rootNode = null, bool fullSubTree = true)
        {
            var rawList = await _dbContext.DocRegistries.Where(x => x.SubtreeId == rootNode.SubtreeId).ToListAsync();
            int minLevel = -1;
            for (int i = 0; i < rawList.Count; i++)
            {
                if (minLevel < 0 || rawList[i].Level < minLevel)
                {
                    minLevel = rawList[i].Level;
                }
            }

            return BuildTreeFromList(rawList, rootNode == null || fullSubTree ? minLevel : rootNode.Level, fullSubTree ? null : rootNode);
        }

        private List<DocRegistry> BuildTreeFromList(List<DocRegistry> rawList, int curLevel = 0, DocRegistry parentNode = null, bool firstEntry = true)
        {
            List<DocRegistry> result = new List<DocRegistry>();

            if (firstEntry)
            {
                _idList.Clear();
            }

            if (firstEntry && parentNode != null)
            {
                result.Add(parentNode.CloneObject());
                _idList.Add(parentNode.Id);
                result[^1].ChildNodes = BuildTreeFromList(rawList, curLevel + 1, parentNode, false);
            }
            else
            {
                for (int i = 0; i < rawList.Count; i++)
                {
                    if (CheckListNode(rawList[i], curLevel, parentNode))
                    {
                        result.Add(rawList[i].CloneObject());
                        _idList.Add(rawList[i].Id);
                        result[^1].ChildNodes = BuildTreeFromList(rawList, curLevel + 1, rawList[i], false);
                    }
                }
            }

            return result;
        }

        private bool CheckListNode(DocRegistry node, int curLevel, DocRegistry parentNode)
        {
            if (node.Level == curLevel)
            {
                if (parentNode == null)
                {
                    return node.ParentId == Guid.Empty;
                }
                return node.ParentId == parentNode.Id;
            }
            return false;
        }

        private void PostorderOrderSubtree(List<DocRegistry> documents, List<OrderItemsDTO> items, List<string> availableDocs, int branch = -1)
        {
            if (documents == null)
            {
                return;
            }

            foreach (var document in documents)
            {
                if (document.IsApplied && document.Status != UniversalDocState.Draft)
                {
                    branch = documents.Count > 1 ? branch + 1 : branch;
                    PostorderOrderSubtree(document.ChildNodes.ToList(), items, availableDocs, branch);
                    if (availableDocs.Contains(document.DocType))
                    {
                        HandleOrderItems(document, branch, items.Where(x => x.DocumentId == document.Id));
                    }
                }
            }
        }

        private void HandleOrderItems(DocRegistry doc, int branch, IEnumerable<OrderItemsDTO> docItems)
        {
            foreach (var item in docItems)
            {
                var currentBranch = branch;
                var addedItems = _orderItemsStatuses.Where(x => x.ProductId == item.ProductId && x.Branch >= currentBranch)
                    .OrderByDescending(x => x.Level).ThenBy(x => x.Branch).ToList();
                var addedQuantity = GetOrderItemsAddedQuantity(addedItems, item);

                if (addedQuantity >= 0 && item.Quantity - addedQuantity > 0 && item.Clone() is OrderItemsDTO newItem)
                {
                    newItem.Quantity -= addedQuantity;
                    newItem.DocumentId = doc.Id;
                    newItem.Branch = branch;
                    _orderItemsStatuses.Add(newItem);
                }
            }
        }

        private double GetOrderItemsAddedQuantity(List<OrderItemsDTO> addedItems, OrderItemsDTO item)
        {
            var processed = item.Processed;
            double addedQuantity = 0;
            foreach (var addedItem in addedItems)
            {
                if (Math.Abs(addedItem.Quantity - item.Quantity) < 1E-6)
                {
                    return -1;
                }

                if (processed - addedItem.Quantity >= 0)
                {
                    processed -= addedItem.Quantity;
                    addedQuantity += addedItem.Quantity;
                }
                else
                {
                    _orderItemsStatuses.Remove(addedItem);
                }
            }

            return addedQuantity;
        }
    }
}