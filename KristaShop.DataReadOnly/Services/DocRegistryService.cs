﻿using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.Documents;

namespace KristaShop.DataReadOnly.Services
{
    public class DocRegistryService : IDocRegistryService
    {
        private readonly IReadOnlyRepo<DocRegistry> _docRegistryRepo;
        private readonly IReadOnlyRepo<DocRegistryFile> _docRegistryFileRepo;
        private readonly IReadOnlyRepo<Currency> _currencyRepo;
        private readonly IReadOnlyRepo<DocOrderItem> _docOrderRepo;
        private readonly IReadOnlyRepo<ExchangeRatesSnapshot> _exrSnapshotRepo;
        private readonly IReadOnlyRepo<ExchangeRate> _exrRepo;
        private readonly IReadOnlyRepo<MoneyOperationType> _moneyOperationTypeRepo;
        private readonly IMapper _mapper;

        public DocRegistryService
            (IReadOnlyRepo<DocRegistry> docRegistryRepo, IReadOnlyRepo<Currency> currencyRepo,
            IReadOnlyRepo<DocOrderItem> docOrderRepo, IReadOnlyRepo<ExchangeRatesSnapshot> exrSnapshotRepo,
            IReadOnlyRepo<DocRegistryFile> docRegistryFileRepo, IMapper mapper, IReadOnlyRepo<ExchangeRate> exrRepo,
            IReadOnlyRepo<MoneyOperationType> moneyOperationTypeRepo)
        {
            _docRegistryRepo = docRegistryRepo;
            _currencyRepo = currencyRepo;
            _docOrderRepo = docOrderRepo;
            _exrSnapshotRepo = exrSnapshotRepo;

            _docRegistryFileRepo = docRegistryFileRepo;
            _mapper = mapper;
            _exrRepo = exrRepo;
            _moneyOperationTypeRepo = moneyOperationTypeRepo;
        }
        
        public async Task<DocRegistryFileDTO> GetCounterpartyFileById(Guid id, Guid counterpartyId) {
            return await _docRegistryFileRepo.QueryReadOnly()
                .Where(x => x.Id == id && x.Document.CounterpartyId == counterpartyId && x.IsForClient)
                .ProjectTo<DocRegistryFileDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<List<DocRegistryFileDTO>> GetCounterpartyFilesByDocId(Guid docId, Guid counterpartyId) {
            return await _docRegistryFileRepo.QueryFindBy(x => x.IsForClient && x.ParentId == docId && x.Document.CounterpartyId == counterpartyId)
                .ProjectTo<DocRegistryFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<DocRegistryDTO> GetDocumentByIdAsync(Guid id, Guid counterpartyId, bool includeFiles = false) {
            var request = _docRegistryRepo
                .QueryReadOnly()
                .Include(x => x.DocOrderItems)
                .Where(x => x.Id == id && x.CounterpartyId == counterpartyId);

            if (includeFiles) {
                request = request.Include(x => x.DocRegistryFiles);
            }

            var docRegistry = await request.SingleOrDefaultAsync();
            if (docRegistry == null) {
                return null;
            }

            var docRegistryDTO = new DocRegistryDTO {
                DocId = docRegistry.Id,
                DocNum = docRegistry.DocNum1C == 0 ? docRegistry.DocNum : docRegistry.DocNum1C,
                ExtraData = docRegistry.ExtraData,
                DocTypeName = docRegistry.DocType,
                RuDocTypeName = docRegistry.DocumentType.GetDocName(),
                Status = docRegistry.Status,
                Sum = docRegistry.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PrePay),
                SumPayIn = docRegistry.DocOrderItems.Sum(item => item.PayIn + item.PrePay),
                SumDebt = docRegistry.DocOrderItems.Sum(item =>
                    item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                PreOrderSum = docRegistry.DocOrderItems.Sum(item =>
                    item.IsPreorder ? item.ItemsCount * (item.Price + item.Discount) : 0),
                StatusName = docRegistry.GetStatusName(),
                DocumentType = docRegistry.DocumentType,
                IsDetail = docRegistry.DocumentType.GetDocument().HasDetailsList,
                Files = docRegistry.DocRegistryFiles.Where(x => x.IsForClient).Select(x => new DocRegistryFileDTO {
                    Id = x.Id,
                    Description = x.Description,
                    FileName = x.Filename,
                    VirtualPath = x.VirtualPath
                }).ToList()
            };
            docRegistryDTO.PrepaySum = Math.Ceiling(docRegistryDTO.PreOrderSum * 0.3);

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            ExchangeRate exchangeRate = null;
            if (docRegistryDTO.DocumentType == DocumentType.Order && docRegistryDTO.Status != UniversalDocState.Completed) {
                exchangeRate = await _exrRepo.QueryFindBy(x => x.ToCurrencyId == rubCurrency.Id).OrderByDescending(x => x.AddDate).FirstOrDefaultAsync();
            }

            if (!await _parseExtraDataAsync(docRegistryDTO, rubCurrency, exchangeRate)) {
                return null;
            }

            return docRegistryDTO;
        }

        public async Task<DocRegistryDTO> GetOrderDocumentBySubtreeIdAsync(Guid subtreeId, Guid counterpartyId, bool includeFiles = false) {
            var request = _docRegistryRepo
                .QueryFindBy(x => x.SubtreeId == subtreeId && x.CounterpartyId == counterpartyId &&
                                  x.DocType == DocumentType.Order.GetDocType())
                .Include(x => x.DocOrderItems);

            if (includeFiles) {
                request.Include(x => x.DocRegistryFiles);
            }
                

            var docRegistry = await request.FirstOrDefaultAsync();
            if (docRegistry == null) {
                return null;
            }

            var docRegistryDTO = new DocRegistryDTO
            {
                DocId = docRegistry.Id,
                DocNum = docRegistry.DocNum1C == 0 ? docRegistry.DocNum : docRegistry.DocNum1C,
                ExtraData = docRegistry.ExtraData,
                DocTypeName = docRegistry.DocType,
                RuDocTypeName = docRegistry.DocumentType.GetDocName(),
                Status = docRegistry.Status,
                Sum = docRegistry.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount)),
                SumPayIn = docRegistry.DocOrderItems.Sum(item => item.PayIn + item.PrePay),
                SumDebt = docRegistry.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                PreOrderSum = docRegistry.DocOrderItems.Sum(item => item.IsPreorder ? item.ItemsCount * (item.Price + item.Discount) : 0),
                StatusName = docRegistry.GetStatusName(),
                DocumentType = docRegistry.DocumentType,
                IsDetail = docRegistry.DocumentType.GetDocument().HasDetailsList,
                Files = docRegistry.DocRegistryFiles?.Select(x => new DocRegistryFileDTO {
                    Id = x.Id,
                    Description = x.Description,
                    FileName = x.Filename,
                    VirtualPath = x.VirtualPath
                }).ToList()
            };

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            ExchangeRate exchangeRate = null;
            if (docRegistryDTO.DocumentType == DocumentType.Order && docRegistryDTO.Status != UniversalDocState.Completed) {
                exchangeRate = await _exrRepo.QueryFindBy(x => x.ToCurrencyId == rubCurrency.Id).OrderByDescending(x => x.AddDate).FirstOrDefaultAsync();
            }

            if (!await _parseExtraDataAsync(docRegistryDTO, rubCurrency, exchangeRate)) {
                return null;
            }

            return docRegistryDTO;
        }

        public async Task<List<DocRegistryDTO>> GetCounterpartyCurrentOrdersAsync(Guid counterpartyId) {
            var docs = await _docRegistryRepo
               .QueryFindBy(x => x.IsApplied
               && x.Status > 0
               && x.Status != UniversalDocState.Completed
               && x.CounterpartyId == counterpartyId
               && x.ParentId == Guid.Empty
               && x.DocType == DocumentType.Order.GetDocType())
               .Select(x => new DocRegistryDTO
               {
                   DocId = x.Id,
                   SubtreeId = x.SubtreeId,
                   CreateDate = x.CreateDate,
                   DocNum = x.DocNum,
                   Status = x.Status,
                   StatusName = x.GetStatusName(),
                   Sum = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount)),
                   SumPayIn = x.DocOrderItems.Sum(item => item.PayIn + item.PrePay),
                   SumDebt = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                   ExtraData = x.ExtraData,
                   IsFileExist = x.DocRegistryFiles.Any(q => q.IsForClient),
                   IsDetail = x.DocumentType.GetDocument().HasDetailsList,
                   DocTypeName = x.DocType,
                   DocumentType = x.DocumentType,
               })
               .OrderByDescending(x => x.DocNum)
               .ToListAsync();

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            var exchangeRate = await _exrRepo.QueryFindBy(x => x.ToCurrencyId == rubCurrency.Id).OrderByDescending(x => x.AddDate).FirstOrDefaultAsync();

            foreach (var item in docs.ToList()) {
                if (!await _parseExtraDataAsync(item, rubCurrency, exchangeRate)) {
                    docs.Remove(item);
                }
            }

            return docs;
        }

        public async Task<List<DocRegistryDTO>> GetCounterpartyCompletedOrdersAsync(Guid counterpartyId) {
            var docs = await _docRegistryRepo
               .QueryFindBy(x => x.IsApplied
               && x.Level == 1
               && (x.Status < 0 || x.Status == UniversalDocState.Completed)
               && x.CounterpartyId == counterpartyId
               && x.ParentId == Guid.Empty
               && x.DocType == DocumentType.Order.GetDocType())
               .Select(x => new DocRegistryDTO
               {
                   DocId = x.Id,
                   SubtreeId = x.SubtreeId,
                   CreateDate = x.CreateDate,
                   DocNum = x.DocNum,
                   Status = x.Status,
                   StatusName = x.GetStatusName(),
                   Sum = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount)),
                   SumPayIn = x.DocOrderItems.Sum(item => item.PayIn + item.PrePay),
                   SumDebt = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                   ExtraData = x.ExtraData,
                   IsFileExist = x.DocRegistryFiles.Any(q => q.IsForClient),
                   IsDetail = x.DocumentType.GetDocument().HasDetailsList,
                   DocTypeName = x.DocType,
                   DocumentType = x.DocumentType
               })
               .OrderByDescending(x => x.DocNum)
               .ToListAsync();

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            foreach (var item in docs.ToList()) {
                if (!await _parseExtraDataAsync(item, rubCurrency)) {
                    docs.Remove(item);
                }
            }

            return docs;
        }

        public async Task<List<DocRegistryDTO>> GetUserActiveDocumentsListAsync(Guid userId, bool detailedOnly = false) {
            var docs = await _docRegistryRepo
                .QueryFindBy(x => x.IsApplied && !x.IsCompleted
                                  && x.Counterparty.User.Id == userId
                                  && DocumentExtension.GetDocumentTypes().Contains(x.DocType))
                .OrderByDescending(x => x.Level)
                .Select(x => new DocRegistryDTO
                {
                    DocId = x.Id,
                    SubtreeId = x.SubtreeId,
                    CreateDate = x.CreateDate,
                    DocNum = x.DocNum1C == 0 ? x.DocNum : x.DocNum1C,
                    Status = x.Status,
                    StatusName = x.GetStatusName(),
                    Sum = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PrePay),
                    SumPayIn = x.DocOrderItems.Sum(item => item.PayIn),
                    SumDebt = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                    ExtraData = x.ExtraData,
                    IsFileExist = x.DocRegistryFiles.Any(q => q.IsForClient),
                    Description = x.Description,
                    IsDetail = x.DocumentType.GetDocument().HasDetailsList,
                    DocTypeName = x.DocType,
                    DocumentType = x.DocumentType,
                    ParentId =  x.ParentId,
                    Level = x.Level,
                    Pos = x.Position
                })
                .ToListAsync();

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            var root = _buildTreeFromList(docs);
            if (root == null) {
                return new List<DocRegistryDTO>();
            }

            await _parseTreeDocumentsExtraDataAsync(root, rubCurrency);
            docs = _getTreeAsList(root);

            if (detailedOnly) {
                docs = docs.Where(x => x.DocumentType.GetDocument().HasDetailsList).ToList();
            }

            return docs;
        }

        public async Task<List<DocRegistryDTO>> GetCounterpartyOrderSubtreeDocumentsAsync(Guid counterpartyId, Guid subtreeId, bool roundSumInPrepayDoc)
        {
            var docs = await _docRegistryRepo
                .QueryFindBy(x => x.IsApplied
                && x.CounterpartyId == counterpartyId
                && x.SubtreeId == subtreeId
                && DocumentExtension.GetDocumentTypes().Contains(x.DocType))
                .OrderByDescending(x => x.Level)
                .Select(x => new DocRegistryDTO
                {
                    DocId = x.Id,
                    SubtreeId = x.SubtreeId,
                    CreateDate = x.CreateDate,
                    DocNum = x.DocNum1C == 0 ? x.DocNum : x.DocNum1C,
                    Status = x.Status,
                    StatusName = x.GetStatusName(),
                    Sum = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PrePay),
                    SumPayIn = x.DocOrderItems.Sum(item => item.PayIn),
                    SumDebt = x.DocOrderItems.Sum(item => item.ItemsCount * (item.Price + item.Discount) - item.PayIn - item.PrePay),
                    ExtraData = x.ExtraData,
                    IsFileExist = x.DocRegistryFiles.Any(q => q.IsForClient),
                    Description = x.Description,
                    IsDetail = x.DocumentType.GetDocument().HasDetailsList,
                    DocTypeName = x.DocType,
                    DocumentType = x.DocumentType,
                    ParentId =  x.ParentId,
                    Level = x.Level,
                    Pos = x.Position
                })
                .ToListAsync();

            var rubCurrency = await _currencyRepo.QueryFindBy(x => x.Type == CurrencyType.RUB).FirstOrDefaultAsync();
            var root = _buildTreeFromList(docs);
            if (root == null) {
                return new List<DocRegistryDTO>();
            }

            await _parseTreeDocumentsExtraDataAsync(root, rubCurrency, null, roundSumInPrepayDoc);
            docs = _getTreeAsList(root);
            
            return docs;
        }

        public async Task<double> GetTotalSumByDocIdAsync(Guid docId, double? percent = null) {
            var sums = await _docOrderRepo.QueryFindBy(x => x.DocumentId == docId)
                .GroupBy(x => true)
                .Select(x => new {
                    TotalSum = x.Sum(s => (s.Price + s.Discount) * s.ItemsCount)
                }).FirstAsync();

            double result = sums.TotalSum;
            if (percent.HasValue)
                result = result / 100.0d * percent.Value;
            return result;
        }

        public async Task<double> GetRateSnapshotAsync(Guid currencyId, Guid snapshotId) {
            var exchange = await _exrSnapshotRepo.FindByFilterAsync(x => x.ToCurrencyId == currencyId && x.SnapshotId == snapshotId);
            return exchange.Rate;
        }

        public async Task<MoneyOperationType> GetMoneyOperationTypeAsync(Guid moneyOperationTypeId) {
            return await _moneyOperationTypeRepo.FindByIdAsync(moneyOperationTypeId);
        }

        private async Task<bool> _parseExtraDataAsync(DocRegistryDTO docRegistry, Currency rubCurrency, ExchangeRate exchangeRate = null, bool roundSumInPrepayDoc = false) {
            var extraData = docRegistry.DocumentType.GetDocument().ExtraData;
            if (extraData == null) {
                docRegistry.RuDocTypeName = DocumentExtension.GetDocTypeByName(docRegistry.DocTypeName).GetDocName();
                return true;
            }

            var result = await extraData.DeserializeAsync(docRegistry.ExtraData, docRegistry.DocId, this, rubCurrency, exchangeRate, docRegistry.Sum);
            if (result == null) {
                docRegistry.RuDocTypeName = DocumentExtension.GetDocTypeByName(docRegistry.DocTypeName).GetDocName();
                return true;

            }

            if (!result.VisibleForUser) {
                return false;
            }

            docRegistry.RuDocTypeName = DocumentExtension.GetDocTypeByName(docRegistry.DocTypeName)
                .GetDocName(result.ToCounterpartyBalance ? DocumentNameType.Balance : DocumentNameType.Basic);
            docRegistry.Rate = result.Rate;
            docRegistry.Sum = result.Sum;
            docRegistry.CurrencyType = rubCurrency.Type == CurrencyType.RUB && rubCurrency.Id == result.CurrencyId ? CurrencyType.RUB : CurrencyType.None;

            if (roundSumInPrepayDoc && docRegistry.DocumentType == DocumentType.PrepayInvoice &&
                docRegistry.CurrencyType != CurrencyType.RUB && docRegistry.Sum % 1 > GlobalConstant.Epsilon) {
                docRegistry.Sum = Math.Ceiling(docRegistry.Sum);
            }

            docRegistry.SumRu = Math.Round(result.SumRu, 2);
            docRegistry.Percent = result.Percent / 100;
            docRegistry.ToCounterpartyBalance = result.ToCounterpartyBalance;

            docRegistry.SumRuPayIn = Math.Round(docRegistry.SumPayIn * result.Rate, 2);
            docRegistry.SumRuDebt = Math.Round(docRegistry.SumDebt * result.Rate, 2);
            docRegistry.PreOrderSumRu = Math.Round(docRegistry.PreOrderSum * result.Rate, 2);
            docRegistry.InStockSum = Math.Round(docRegistry.Sum - docRegistry.PreOrderSum, 2);
            docRegistry.InStockSumRu = Math.Round(docRegistry.InStockSum * result.Rate, 2);
            docRegistry.PrepaySumRu =  Math.Round(docRegistry.PrepaySum * result.Rate, 2);
            if (docRegistry.SumDebt <= 0) {
                docRegistry.SumDebt = 0;
                docRegistry.SumRuDebt = 0;
                docRegistry.SumPayIn = docRegistry.Sum;
                docRegistry.SumRuPayIn = docRegistry.SumRu;
            }

            return true;
        }

        #region tree

        protected DocRegistryDTO _buildTreeFromList(List<DocRegistryDTO> docs) {
            DocRegistryDTO root = null;
            var nodesWithoutParent = new List<DocRegistryDTO>();

            var dic = docs.ToDictionary(k => k.DocId, v => v);
            foreach (var doc in docs) {
                if (dic.ContainsKey(doc.DocId)) {
                    dic.Remove(doc.DocId);
                    doc.ChildNodes = doc.ChildNodes.OrderBy(x => x.CreateDate).ToList();

                    if (dic.ContainsKey(doc.ParentId)) {
                        dic[doc.ParentId].ChildNodes.Add(doc);
                    } else if(doc.ParentId == Guid.Empty) {
                        root = doc;
                    } else {
                        nodesWithoutParent.Add(doc);
                    }
                }
            }

            if (root != null && nodesWithoutParent.Any()) {
                if (root.ChildNodes == null) {
                    root.ChildNodes = new List<DocRegistryDTO>();
                }

                root.ChildNodes.AddRange(nodesWithoutParent);
                root.ChildNodes = root.ChildNodes.OrderBy(x => x.CreateDate).ToList();
            }

            return root;
        }

        protected async Task _parseTreeDocumentsExtraDataAsync(DocRegistryDTO root, Currency currency, ExchangeRate rate = null, bool roundSumInPrepayDoc = false) {
            if (root == null || !root.ChildNodes.Any()) {
                return;
            }

            foreach (var document in root.ChildNodes.ToList()) {
                if (document.Status <= 0 || !await _parseExtraDataAsync(document, currency, rate, roundSumInPrepayDoc)) {
                    document.ChildNodes.Clear();
                    root.ChildNodes.Remove(document);
                } else {
                    await _parseTreeDocumentsExtraDataAsync(document, currency, rate);
                }
            }
        }

        protected List<DocRegistryDTO> _getTreeAsList(DocRegistryDTO node, int minLevel = 0) {
            List<DocRegistryDTO> result = new List<DocRegistryDTO>();
            for (int i = 0; i < node.ChildNodes.Count; i++) {
                if (node.ChildNodes[i].Level >= minLevel) {
                    result.Add(node.ChildNodes[i]);
                }
                if (node.ChildNodes[i].ChildNodes.Count > 0) {
                    var subList = _getTreeAsList(node.ChildNodes[i], minLevel);
                    for (int j = 0; j < subList.Count; j++) {
                        if (subList[j].Level >= minLevel) {
                            result.Add(subList[j]);
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}