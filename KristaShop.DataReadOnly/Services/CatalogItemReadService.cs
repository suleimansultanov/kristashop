﻿using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Enums;

namespace KristaShop.DataReadOnly.Services
{
    public class CatalogItemReadService : ICatalogItemReadService
    {
        private readonly IReadOnlyRepo<CatalogItem> _catalogItemRepo;
        private readonly IReadOnlyRepo<Nomenclature> _nomRepo;

        private readonly IReadOnlyRepo<StorehouseRests> _storehouseRestsRepo;

        private readonly IReadOnlyRepo<CatalogItemPrice> _cipRepo;
        private readonly IReadOnlyRepo<Currency> _currencyRepo;
        private readonly IReadOnlyRepo<PriceType> _priceRepo;

        public CatalogItemReadService
            (IReadOnlyRepo<CatalogItem> catalogItemRepo, IReadOnlyRepo<Nomenclature> nomRepo,
            IReadOnlyRepo<StorehouseRests> storehouseRestsRepo, IReadOnlyRepo<Currency> currencyRepo,
            IReadOnlyRepo<PriceType> priceRepo, IReadOnlyRepo<CatalogItemPrice> cipRepo)
        {
            _catalogItemRepo = catalogItemRepo;
            _nomRepo = nomRepo;
            _storehouseRestsRepo = storehouseRestsRepo;

            _currencyRepo = currencyRepo;
            _priceRepo = priceRepo;
            _cipRepo = cipRepo;
        }

        public async Task<double> GetPartsCountByNomId(Guid nomId)
        {
            return (double)await _catalogItemRepo.QueryFindBy(x => x.NomenclatureId == nomId)
                .Select(x => x.PartsCount)
                .FirstOrDefaultAsync();
        }

        public async Task<ProductDetailsDTO> GetPackagingProduct(Guid nomId)
        {
            var priceType = await GetPriceTypeId();
            var product = await _catalogItemRepo.QueryFindBy(x => x.NomenclatureId == nomId)
                .Select(x => new ProductDetailsDTO
                {
                    ProductId = x.ProductId,
                    Price = x.CatalogItemPrices.Count > 0
                        ? x.CatalogItemPrices.FirstOrDefault(x => x.PriceTypeId == priceType).Price
                        : (double?)null,
                    ItemName = x.ItemName
                })
                .FirstOrDefaultAsync();
            if (product == null)
                return null;
            var result = IsExistInStorehouse(product.ProductId, 1, 0);
            if (!result.IsSuccess)
                return null;
            return product;
        }

        public List<Guid> GetProductIdsByNomId(Guid nomId, List<Guid> notVisProductsId)
        {
            return _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                && x.Nomenclature.VisibleOnSite
                && x.NomenclatureId == nomId
                && x.Level == 1) //&& !notVisProductsId.Contains(x.product_id))
                .Select(x => x.ProductId)
                .ToList();
        }

        public async Task<List<SizeDTO>> GetNomenclatureSizesAsync(Guid nomenclatureId, bool isSet, List<Guid> productsId) {
            var items = await GetCatalogItemsByParamsAsync(nomenclatureId, productsId);
            if (items.Count == 0)
                return null;

            var sizes = GetSizeDTOs(ref items, isSet);
            return sizes;
        }

        private async Task<List<CatalogItem>> GetCatalogItemsByParamsAsync(Guid nomId, List<Guid> productsId)
        {
            return await _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                            && x.Nomenclature.VisibleOnSite
                            && x.NomenclatureId == nomId
                            && x.Level == 1 && productsId.Contains(x.ProductId))
                .Include(t => t.Nomenclature)
                .Include(x => x.Option)
                .ThenInclude(x => x.NomFilterColor)
                .ThenInclude(x => x.Color)
                .Include(x => x.Option)
                .ThenInclude(x => x.NomFilterSizeLine)
                .ThenInclude(x => x.SizeLine)
                .Include(x => x.Option)
                .ThenInclude(x => x.NomFilterSize)
                .ThenInclude(x => x.Size)
                .ToListAsync();
        }

        private List<SizeDTO> GetSizeDTOs(ref List<CatalogItem> items, bool isSet)
        {
            List<SizeDTO> sizes;
            Guid firstSizeId;
            if (isSet)
            {
                sizes = items
                    .Select(x => new SizeDTO
                    {
                        Id = x.Option.NomFilterSizeLine.SizeLine.Id,
                        Name = x.Option.NomFilterSizeLine.SizeLine.SizeLineName,
                        Order = x.Option.NomFilterSizeLine.SizeLine.SizeLinePos,
                    })
                    .GroupBy(x => x.Id)
                    .Select(x => x.First())
                    .OrderBy(x => x.Order)
                    .ToList();
                firstSizeId = sizes.FirstOrDefault()?.Id ?? default;
                items = items.Where(x => x.Option.NomFilterSizeLine.SizeLineId == firstSizeId).ToList();
            }
            else
            {
                sizes = items
                    .Select(x => new SizeDTO
                    {
                        Id = x.Option.NomFilterSize.Size.Id,
                        Name = x.Option.NomFilterSize.Size.SizeName,
                        Order = x.Option.NomFilterSize.Size.SizeValue
                    })
                    .GroupBy(x => x.Id)
                    .Select(x => x.First())
                    .OrderBy(x => x.Order)
                    .ToList();
                firstSizeId = sizes.FirstOrDefault()?.Id ?? default;
                items = items.Where(x => x.Option.NomFilterSize.SizeId == firstSizeId).ToList();
            }

            return sizes;
        }

        public async Task<List<ProductDTO>> GetProductsForCart(List<Guid> productsId)
        {
            var ci = await _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                    && x.Nomenclature.VisibleOnSite
                    && x.Level == 1
                    && productsId.Contains(x.ProductId))
                .Include(t => t.Nomenclature)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterColor)
                        .ThenInclude(x => x.Color)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterSizeLine)
                        .ThenInclude(x => x.SizeLine)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterSize)
                        .ThenInclude(x => x.Size)
                .ToListAsync();
            return ci.GroupBy(x => x.ProductId).Select(x => x.First()).Select(x => new ProductDTO
            {
                ProductId = x.ProductId,
                IsSet = x.Nomenclature.IsSet,
                Size = new SizeDTO
                {
                    Id = x.Nomenclature.IsSet ? x.Option.NomFilterSizeLine.SizeLine.Id : x.Option.NomFilterSize.Size.Id,
                    Name = x.Nomenclature.IsSet ? x.Option.NomFilterSizeLine.SizeLine.SizeLineName : x.Option.NomFilterSize.Size.SizeName,
                    Order = x.Nomenclature.IsSet ? x.Option.NomFilterSizeLine.SizeLine.SizeLinePos : x.Option.NomFilterSize.Size.SizeValue
                },
                Color = new ColorDTO
                {
                    Id = x.Option.NomFilterColor.Color.Id,
                    Name = x.Option.NomFilterColor.Color.ColorName,
                    Code = x.Option.NomFilterColor.Color.ColorCode,
                    Image = x.Option.NomFilterColor.Color.ImagePath
                }
            }).ToList();
        }

        public async Task<List<ColorDTO>> GetNomenclatureColorsBySizeAsync(Guid nomenclatureId, Guid sizeId,
            bool checkInStock, List<Guid> excludeProductIds) {
            var catalogItems = _catalogItemRepo.QueryReadOnly();
            if (checkInStock) {
                var productIds = _catalogItemRepo.QueryReadOnly()
                    .Where(x => x.NomenclatureId == nomenclatureId)
                    .Select(x => x.ProductId).ToList();
                productIds = GetProductIdsInStorehouse(productIds);
                catalogItems = catalogItems.Where(x => productIds.Contains(x.ProductId));
            }

            var colors = await catalogItems
                .Where(x => x.Nomenclature.NomType == 1
                            && x.Nomenclature.VisibleOnSite
                            && x.NomenclatureId == nomenclatureId
                            && x.Level == 1
                            && (x.Option.NomFilterSize.SizeId == sizeId ||
                                x.Option.NomFilterSizeLine.SizeLineId == sizeId)
                            && !excludeProductIds.Contains(x.ProductId))
                .OrderBy(x => x.Option.NomFilterColor.Color.ColorName)
                .Select(x => new ColorDTO {
                    Id = x.Option.NomFilterColor.Color.Id,
                    Name = x.Option.NomFilterColor.Color.ColorName,
                    Code = x.Option.NomFilterColor.Color.ColorCode,
                    Image = x.Option.NomFilterColor.Color.ImagePath
                })
                .Distinct()
                .ToListAsync();

            return colors.OrderBy(x => x.Name).ToList();
        }

        public List<CModelDTO> GetCatalogModels()
        {
            var noms = _nomRepo.QueryReadOnly()
                .Where(x => x.NomType == 1 && x.VisibleOnSite)
                .ToList();

            var items = _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1 && x.Nomenclature.VisibleOnSite)
                .Where(x => x.Level == 1)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterColor)
                        .ThenInclude(x => x.Color)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterSize)
                        .ThenInclude(x => x.Size)
                .Include(x => x.Option)
                    .ThenInclude(x => x.NomFilterSizeLine)
                        .ThenInclude(x => x.SizeLine);

            return noms
                .GroupJoin(
                    items,
                    nomencl => nomencl.Id,
                    item => item.NomenclatureId,
                    (nomencl, item) => new CModelDTO
                    {
                        Id = nomencl.Id,
                        ItemName = nomencl.Name,
                        Articul = nomencl.Articul,
                        IsVisible = nomencl.VisibleOnSite,
                        Colors = item
                            .Select(color => color.Option.NomFilterColor.Color.ColorName)
                            .Distinct()
                            .ToList(),
                        Sizes = nomencl.IsSet ? item
                            .Select(size => size.Option.NomFilterSizeLine.SizeLine.SizeLineName)
                            .Distinct()
                            .ToList()
                            : item
                            .Select(size => size.Option.NomFilterSize.Size.SizeName)
                            .Distinct()
                            .ToList()
                    }).ToList();
        }

        public List<Guid> GetNomenclatureByFilter(List<Guid> colors, List<Guid> sizes, bool isInstock)
        {
            var noms = _catalogItemRepo.QueryReadOnly()
                .Include(t => t.Nomenclature)
                .Where(x => x.Nomenclature.NomType == 1 && x.Nomenclature.VisibleOnSite)
                .Where(x => x.Level == 1);

            if (colors.Count > 0)
                noms = noms.Where(x => colors.Contains(x.Option.NomFilterColor.ColorId));

            if (sizes.Count > 0)
                noms = noms.Where(x => sizes.Contains(x.Option.NomFilterSize.SizeId));

            if (isInstock)
            {
                var catItems = noms.ToList();
                var shRest = _storehouseRestsRepo.QueryFindBy(x => x.ReservationDocId == Guid.Empty
                        && x.Level == 1
                        && x.Storehouse.IsForOnlineStore
                        && x.Count >= 1)
                    .ToList();
                var nomIds = shRest
                    .Join(catItems,
                        sr => sr.ProductId,
                        ci => ci.ProductId,
                        (srrest, catit) => catit.NomenclatureId)
                    .GroupBy(x => x)
                    .Select(x => x.Key)
                    .ToList();
                return noms.Where(x => nomIds.Contains(x.NomenclatureId)).ToList()
                    .GroupBy(x => x.NomenclatureId, x => x)
                    .Select(s => s.Key)
                    .ToList();
            }
            return noms.ToList()
                .GroupBy(x => x.NomenclatureId, x => x)
                .Select(s => s.Key)
                .ToList();
        }

        public async Task<CModelDTO> GetCatalogModel(Guid id)
        {
            var cmodel = await _nomRepo.FindByIdAsync(id);
            if (cmodel == null)
                return null;

            return new CModelDTO
            {
                Id = cmodel.Id,
                Articul = cmodel.Articul,
                ItemName = cmodel.Name,
                IsSet = cmodel.IsSet
            };
        }

        public List<CModelGridDTO> GetCatalogModelGrid(Guid nomId, List<Guid> productIds)
        {
            return _catalogItemRepo.QueryFindBy(x =>
                x.Nomenclature.NomType == 1 && x.Nomenclature.VisibleOnSite &&
                x.Level == 1 && x.NomenclatureId == nomId)
                .Select(x => new CModelGridDTO
                {
                    ProductId = x.ProductId,
                    Color = x.Option.NomFilterColor.Color.ColorName,
                    Size = x.Nomenclature.IsSet ? x.Option.NomFilterSizeLine.SizeLine.SizeLineName
                        : x.Option.NomFilterSize.Size.SizeName,
                    NotVisible = productIds.Contains(x.ProductId)
                }).ToList();
        }

        public List<Dictionary<string, object>> GetColorsSizesGrid(Guid nomId, List<Guid> productIds)
        {
            var nomList = GetCatalogModelGrid(nomId, productIds);

            var sizes = nomList.Select(x => x.Size).Distinct().ToList();
            var groupColors = nomList.GroupBy(x => x.Color)
                .Select(x => new
                {
                    Сolor = x.Key,
                    Options = x.Select(d2 => new { d2.Size, d2.NotVisible, d2.ProductId })
                }).ToList();

            var list = new List<Dictionary<string, object>>();
            foreach (var g in groupColors)
            {
                var dict = new Dictionary<string, object>
                {
                    ["Цвет"] = g.Сolor
                };
                foreach (var size in sizes)
                {
                    dict.Add(size, null);
                }
                foreach (var opt in g.Options)
                {
                    dict[opt.Size] = new OptAttributes(opt.ProductId, opt.NotVisible);
                }
                list.Add(dict);
            }
            return list;
        }

        public List<Guid> GetProductIdsInStorehouse(List<Guid> productIds)
        {
            return _storehouseRestsRepo.QueryFindBy(x =>
                x.ReservationDocId == Guid.Empty &&
                x.Level == 1 && x.Storehouse.IsForOnlineStore &&
                productIds.Contains(x.ProductId))
                .GroupBy(x => x.ProductId)
                .Select(s => s.Key).ToList();
        }

        public OperationResult IsExistInStorehouse(Guid productId, int amount, int reservedAmount)
        {
            double total = _storehouseRestsRepo.QueryFindBy(x =>
                x.ReservationDocId == Guid.Empty &&
                x.Level == 1 && x.Storehouse.IsForOnlineStore &&
                x.ProductId == productId)
                .Sum(x => x.Count);
            total -= reservedAmount;
            if (total >= amount)
                return OperationResult.Success();
            else
                return OperationResult.Failure(
                    new List<string> {
                        "На заданном складе модель отсутствует в необходимом количестве.",
                        $"В данный момент на складе имеется в количестве {total}."
                    });
        }

        public List<Dictionary<string, object>> GetSHAmountGrid(Guid nomId)
        {
            var shAmounts = _storehouseRestsRepo.QueryFindBy(x =>
                x.ReservationDocId == Guid.Empty &&
                x.Level == 1 && x.Storehouse.IsForOnlineStore)
                .GroupBy(x => x.ProductId)
                .Select(s => new
                {
                    ProductId = s.Key,
                    Amount = s.Sum(c => c.Count)
                }).ToList();

            var nomList = GetCatalogModelGrid(nomId, new List<Guid>());

            var bindGrid = nomList
                .Join(shAmounts,
                    nl => nl.ProductId,
                    sha => sha.ProductId,
                    (nomList, shAmounts) => new
                    {
                        nomList.Color,
                        nomList.Size,
                        shAmounts.Amount
                    }).ToList();
            var sizes = bindGrid.Select(x => x.Size).Distinct().ToList();
            var groupColors = bindGrid.GroupBy(x => x.Color)
                .Select(x => new
                {
                    Сolor = x.Key,
                    Options = x.Select(d2 => new { d2.Size, d2.Amount })
                }).ToList();

            var list = new List<Dictionary<string, object>>();
            foreach (var g in groupColors)
            {
                var dict = new Dictionary<string, object>
                {
                    ["Цвет"] = g.Сolor
                };
                foreach (var size in sizes)
                {
                    dict.Add(size, null);
                }
                foreach (var opt in g.Options)
                {
                    dict[opt.Size] = opt.Amount;
                }
                list.Add(dict);
            }
            return list;
        }

        public List<ColorDTO> GetColorsByNomId(Guid nomId)
        {
            var colors = _catalogItemRepo.QueryFindBy(x =>
                x.Nomenclature.NomType == 1 && x.Nomenclature.VisibleOnSite &&
                x.Level == 1 && x.NomenclatureId == nomId)
                .Select(x => x.Option.NomFilterColor.Color).ToList();

            return colors.GroupBy(x => new { color_name = x.ColorName, id = x.Id }).Select(x => new ColorDTO
            {
                Id = x.Key.id,
                Name = x.Key.color_name
            }).ToList();
        }

        public async Task<double?> GetProductPrice(Guid productId)
        {
            var catalogItemId = await GetCatalogItemIdByProductAsync(productId);
            if (!catalogItemId.HasValue)
                return 0;

            var currencyId = await GetDefaultCurrencyId();
            var priceTypeId = await GetPriceTypeId();
            var cip = await _cipRepo
                .FindByFilterAsync(x => x.CatalogItemId == catalogItemId
                && x.PriceTypeId == priceTypeId
                && x.InputCurrencyId == currencyId);
            return cip?.Price;
        }

        private async Task<Guid?> GetCatalogItemIdByProductAsync(Guid productId)
        {
            var catalogItem = await _catalogItemRepo.QueryReadOnly()
                .Include(t => t.Nomenclature)
                .Where(x => x.Nomenclature.NomType == 1 && x.Nomenclature.VisibleOnSite)
                .Where(x => x.Level == 1 && x.ProductId == productId).SingleOrDefaultAsync();
            return catalogItem?.Id;
        }

        private async Task<Guid> GetDefaultCurrencyId()
        {
            var currency = await _currencyRepo.FindByFilterAsync(x => x.Type == CurrencyType.USD && x.IsDefault);
            return currency.Id;
        }

        private async Task<Guid> GetPriceTypeId()
        {
            var price = await _priceRepo.FindByFilterAsync(x => x.Type == PriceTypes.RetailPrice);
            return price.Id;
        }

        public async Task<ProductDetailsDTO> GetProductIdByParams(Guid nomId, Guid sizeId, Guid colorId, bool isSet)
        {
            var catalogItems = _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Level == 1 && x.NomenclatureId == nomId)
                .Where(x => x.Nomenclature.NomType == 1
                    && x.Nomenclature.VisibleOnSite
                    && x.Nomenclature.IsSet == isSet)
                .Where(x => x.Option.NomFilterColor.ColorId == colorId);

            if (isSet)
                catalogItems = catalogItems.Where(x => x.Option.NomFilterSizeLine.SizeLineId == sizeId);
            else
                catalogItems = catalogItems.Where(x => x.Option.NomFilterSize.SizeId == sizeId);

            var catalogItem = await catalogItems.SingleOrDefaultAsync();
            if (catalogItem == null)
                return null;
            else
                return new ProductDetailsDTO
                {
                    ProductId = catalogItem.ProductId,
                    PartsCount = catalogItem.PartsCount
                };
        }

        public void GetProductDetailsById(Guid productId)
        {
            var productDetail = _catalogItemRepo.QueryReadOnly()
                .Where(x => x.ProductId == productId)
                .Select(x => new
                {
                    ProductColor = x.Option.NomFilterColor.Color,
                    ProductSize = x.Option.NomFilterSize.Size
                });
        }

        public async Task<List<ColorDTO>> GetColorsByListNomId(List<Guid> nomIds)
        {
            var colors = await _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                    && x.Nomenclature.VisibleOnSite
                    && x.Level == 1
                    && nomIds.Contains(x.NomenclatureId))
                .Select(x => x.Option.NomFilterColor.Color)
                .ToListAsync();
            return colors
                .GroupBy(x => x.Id)
                .Select(x => new ColorDTO
                {
                    Id = x.FirstOrDefault().Id,
                    Name = x.FirstOrDefault().ColorName
                })
                .OrderBy(x => x.Name)
                .ToList();
        }

        public async Task<List<SizeDTO>> GetSizesByListNomId(List<Guid> nomIds)
        {
            var sizes = await _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                    && x.Nomenclature.VisibleOnSite
                    && !x.Nomenclature.IsSet
                    && x.Level == 1
                    && nomIds.Contains(x.NomenclatureId))
                .Select(x => x.Option.NomFilterSize.Size)
                .ToListAsync();
            return sizes
                .GroupBy(x => x.Id)
                .Select(x => new SizeDTO
                {
                    Id = x.FirstOrDefault().Id,
                    Name = x.FirstOrDefault().SizeName
                })
                .OrderBy(x => x.Name)
                .ToList();
        }

        public async Task<List<SizeLineDTO>> GetSizeLinesByListNomId(List<Guid> nomIds)
        {
            var sizeLines = await _catalogItemRepo.QueryReadOnly()
                .Where(x => x.Nomenclature.NomType == 1
                    && x.Nomenclature.VisibleOnSite
                    && x.Nomenclature.IsSet
                    && x.Level == 1
                    && nomIds.Contains(x.NomenclatureId))
                .Select(x => x.Option.NomFilterSizeLine.SizeLine)
                .ToListAsync();
            return sizeLines
                .GroupBy(x => x.Id)
                .Select(x => new SizeLineDTO
                {
                    Id = x.FirstOrDefault().Id,
                    Name = x.FirstOrDefault().SizeLineName
                })
                .OrderBy(x => x.Name)
                .ToList();
        }
    }
}