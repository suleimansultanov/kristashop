﻿using System;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class DocRegistryConfiguration : IEntityTypeConfiguration<DocRegistry> {
        public void Configure(EntityTypeBuilder<DocRegistry> builder) {
            builder.ToTable("docs_registry");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Creater)
                .WithMany(x => x.DocumentsCreated)
                .HasForeignKey(x => x.CreaterId);

            builder.HasOne(x => x.Applier)
                .WithMany(x => x.DocumentsApplied)
                .HasForeignKey(x => x.ApplierId);

            builder.HasOne(x => x.Executer)
                .WithMany(x => x.DocumentsExecuted)
                .HasForeignKey(x => x.ExecuterId);

            builder.HasOne(x => x.Counterparty)
                .WithMany(x => x.Documents)
                .HasForeignKey(x => x.CounterpartyId);

            builder.Property<Guid>(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Position)
                .HasColumnName("pos")
                .IsRequired();

            builder.Property(x => x.Level)
                .HasColumnName("level")
                .IsRequired();

            builder.Property(x => x.ParentId)
                .HasColumnName("parent_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.SubtreeId)
                .HasColumnName("subtree_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.CreateDate)
                .HasColumnName("create_date")
                .IsRequired();

            builder.Property(x => x.AppliedDate)
                .HasColumnName("aplied_date");

            builder.Property(x => x.Acl)
                .HasColumnName("acl")
                .IsRequired();

            builder.Property(x => x.DepartmentId)
                .HasColumnName("department_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.CreaterId)
                .HasColumnName("creater_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ApplierId)
                .HasColumnName("aplier_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ExecuterId)
                .HasColumnName("executer_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.CounterpartyId)
                .HasColumnName("counterparty_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DocType)
                .HasColumnName("doc_type")
                .IsRequired();

            builder.Property(x => x.DocNum)
                .HasColumnName("doc_num")
                .IsRequired();

            builder.Property(x => x.DocNum1C)
                .HasColumnName("doc_num_1c")
                .IsRequired();

            builder.Property(x => x.IsApplied)
                .HasColumnName("is_aplied")
                .IsRequired();

            builder.Property(x => x.IsCompleted)
                .HasColumnName("is_completed")
                .IsRequired();

            builder.Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("INT(11)")
                .IsRequired();

            builder.Property(x => x.Description)
                .HasColumnName("description");

            builder.Property(x => x.ExtraData)
                .HasColumnName("extra_data");

            builder.Ignore(x => x.DocumentType);

            builder.Ignore(x => x.ChildNodes);
        }
    }
}
