﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class DocRegistryFilesConfiguration : IEntityTypeConfiguration<DocRegistryFile> {
        public void Configure(EntityTypeBuilder<DocRegistryFile> builder) {
            builder.ToTable("docs_registry_files");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Document)
                .WithMany(x => x.DocRegistryFiles)
                .HasForeignKey(x => x.ParentId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ParentId)
                .HasColumnName("parent_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Filename)
                .HasColumnName("filename")
                .IsRequired();

            builder.Property(x => x.VirtualPath)
                .HasColumnName("virtual_path")
                .IsRequired();

            builder.Property(x => x.Description)
                .HasColumnName("description");

            builder.Property(x => x.AddDate)
                .HasColumnName("add_date")
                .IsRequired();

            builder.Property(x => x.IsForClient)
                .HasColumnName("is_for_client")
                .IsRequired();
        }
    }
}
