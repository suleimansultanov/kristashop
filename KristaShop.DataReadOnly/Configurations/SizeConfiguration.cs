﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    internal class SizeConfiguration : IEntityTypeConfiguration<Size> {
        public void Configure(EntityTypeBuilder<Size> builder) {
            builder.ToTable("dict_sizes");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.SizeName)
                .HasColumnName("size_name")
                .IsRequired();

            builder.Property(x => x.SizeValue)
                .HasColumnName("size_value")
                .IsRequired();
        }
    }
}