﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class ExchangeRateConfiguration : IEntityTypeConfiguration<ExchangeRate> {
        public void Configure(EntityTypeBuilder<ExchangeRate> builder) {
            builder.ToTable("exchange_rates");

            builder.HasKey(x => new {id = x.Id, add_date = x.AddDate});

            builder.HasOne(x => x.FromCurrency)
                .WithMany(x => x.ExchangeRatesFromThis)
                .HasForeignKey(x => x.FromCurrencyId);

            builder.HasOne(x => x.ToCurrency)
                .WithMany(x => x.ExchangeRatesToThis)
                .HasForeignKey(x => x.ToCurrencyId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.FromCurrencyId)
                .HasColumnName("from_currency_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ToCurrencyId)
                .HasColumnName("to_currency_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Rate)
                .HasColumnName("rate")
                .IsRequired();

            builder.Property(x => x.AddDate)
                .HasColumnName("add_date")
                .IsRequired();
        }
    }
}