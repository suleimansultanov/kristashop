﻿using System;
using System.Collections.Generic;
using System.Text;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    internal class NomFilterSizeConfiguration : IEntityTypeConfiguration<NomFilterSize> {
        public void Configure(EntityTypeBuilder<NomFilterSize> builder) {
            builder.ToTable("nom_filter_size");

            builder.HasKey(x => new { size_id = x.SizeId, option_id = x.OptionId });

            builder.HasOne(x => x.Size)
                .WithMany(x => x.NomFilterSizes)
                .HasForeignKey(x => x.SizeId);

            builder.HasOne(x => x.Option)
                .WithOne(x => x.NomFilterSize)
                .HasForeignKey<NomFilterSize>(x => x.OptionId);

            builder.Property(x => x.SizeId)
                .HasColumnName("size_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.OptionId)
                .HasColumnName("option_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}
