﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class MoneyOperationTypeConfiguration : IEntityTypeConfiguration<MoneyOperationType> {
        public void Configure(EntityTypeBuilder<MoneyOperationType> builder) {
            builder.ToTable("dict_money_operation_types");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.Property(x => x.IsIncome)
                .HasColumnName("is_income")
                .IsRequired();

            builder.Property(x => x.IsDefault)
                .HasColumnName("is_default")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("INT(11)")
                .IsRequired();
        }
    }
}
