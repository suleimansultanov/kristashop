﻿using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class CurrencyConfiguration : IEntityTypeConfiguration<Currency> {
        public void Configure(EntityTypeBuilder<Currency> builder) {
            builder.ToTable("currencies");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.Property(x => x.Sign)
                .HasColumnName("sign")
                .IsRequired(false);

            builder.Property(x => x.InternationalCode)
                .HasColumnName("international_code")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("INT(11)")
                .HasDefaultValue(CurrencyType.None)
                .IsRequired();

            builder.Property(x => x.IsDefault)
                .HasColumnName("is_default")
                .IsRequired();
        }
    }
}