﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class StorehouseRestsConfiguration : IEntityTypeConfiguration<StorehouseRests> {
        public void Configure(EntityTypeBuilder<StorehouseRests> builder) {
            builder.ToTable("storehouse_rests");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Storehouse)
                .WithMany(x => x.Rests)
                .HasForeignKey(x => x.StorehouseId);

            builder.HasOne(x => x.CatalogItem)
                .WithMany(x => x.StorehouseRests)
                .HasForeignKey(x => x.ProductId)
                .HasPrincipalKey(x => x.ProductId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Level)
                .HasColumnName("level")
                .IsRequired();

            builder.Property(x => x.ProductId)
                .HasColumnName("product_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.SubtreeId)
                .HasColumnName("subtree_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ReservationDocId)
                .HasColumnName("reservation_doc_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Count)
                .HasColumnName("count")
                .IsRequired();

            builder.Property(x => x.PartsCount)
                .HasColumnName("parts_count")
                .IsRequired();

            builder.Property(x => x.StorehouseId)
                .HasColumnName("storehouse_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.AddDate)
                .HasColumnName("add_date")
                .IsRequired();
        }
    }
}