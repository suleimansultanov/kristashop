﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class CounterpartyConfiguration : IEntityTypeConfiguration<Counterparty> {
        public void Configure(EntityTypeBuilder<Counterparty> builder) {
            builder.ToTable("dict_counterparties");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.City)
                .WithMany(x => x.Counterparties)
                .HasForeignKey(x => x.CityId);

            builder.HasOne(x => x.Manager)
                .WithOne(x=>x.ManagerCounterparty)
                .HasForeignKey<Counterparty>(x => x.ManagerId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Title)
                .HasColumnName("title")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .IsRequired();

            builder.Property(x => x.Person)
                .HasColumnName("person");

            builder.Property(x => x.Phone)
                .HasColumnName("person_phone");

            builder.Property(x => x.Email)
                .HasColumnName("person_email");

            builder.Property(x => x.MallAddress)
                .HasColumnName("mall_address");

            builder.Property(x => x.CityId)
                .HasColumnName("city_id")
                .HasColumnType("binary(16)");

            builder.Property(x => x.NewCity)
                .HasColumnName("new_city");

            builder.Property(x => x.CompanyAddress)
                .HasColumnName("company_address");

            builder.Property(x => x.ManagerId)
                .HasColumnName("manager_id")
                .HasColumnType("binary(16)");

            builder.Property(x => x.Balance)
                .HasColumnName("money_balance")
                .IsRequired();

            builder.Ignore(x => x.CityName);
        }
    }
}
