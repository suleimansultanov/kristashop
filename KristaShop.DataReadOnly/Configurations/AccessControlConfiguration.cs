﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class AccessControlConfiguration : IEntityTypeConfiguration<AccessControl> {
        public void Configure(EntityTypeBuilder<AccessControl> builder) {
            builder.ToTable("group_acl");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.AclName)
                .HasColumnName("acl_name")
                .IsRequired();

            builder.Property(x => x.Acl)
                .HasColumnName("acl")
                .IsRequired();
        }
    }
}