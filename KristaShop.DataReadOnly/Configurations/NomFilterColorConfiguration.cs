﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class NomFilterColorConfiguration : IEntityTypeConfiguration<NomFilterColor> {
        public void Configure(EntityTypeBuilder<NomFilterColor> builder) {
            builder.ToTable("nom_filter_color");

            builder.HasKey(x => new { x.ColorId, x.OptionId});

            builder.HasOne(x => x.Color)
                .WithMany(x => x.NomFilterColors)
                .HasForeignKey(x => x.ColorId);

            builder.HasOne(x => x.Option)
                .WithOne(x => x.NomFilterColor)
                .HasForeignKey<NomFilterColor>(x => x.OptionId);

            builder.Property(x => x.ColorId)
                .HasColumnName("color_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.OptionId)
                .HasColumnName("option_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}
