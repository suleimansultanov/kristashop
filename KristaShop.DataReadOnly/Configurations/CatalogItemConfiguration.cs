﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class CatalogItemConfiguration : IEntityTypeConfiguration<CatalogItem> {
        public void Configure(EntityTypeBuilder<CatalogItem> builder) {
            builder.ToTable("catalog_item");

            builder.HasKey(x => x.Id);

            builder.HasAlternateKey(x => new {x.ProductId});

            builder.HasOne(x => x.Nomenclature)
                .WithMany(x => x.CatalogItems)
                .HasForeignKey(x => x.NomenclatureId);

            builder.HasOne(x => x.Option)
                .WithMany(x=>x.CatalogItems)
                .HasForeignKey(x => x.OptionsId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ProductId)
                .HasColumnName("product_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Level)
                .HasColumnName("level")
                .IsRequired();

            builder.Property(x => x.ParentId)
                .HasColumnName("parent_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.SubtreeId)
                .HasColumnName("subtree_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ItemName)
                .HasColumnName("item_name")
                .IsRequired();

            builder.Property(x => x.Volume)
                .HasColumnName("volume")
                .IsRequired();

            builder.Property(x => x.Weight)
                .HasColumnName("weight")
                .IsRequired();

            builder.Property(x => x.PartsCount)
                .HasColumnName("parts_count")
                .IsRequired();

            builder.Property(x => x.OptionsId)
                .HasColumnName("options_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.NomenclatureId)
                .HasColumnName("nomenclature_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}
