﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    internal class NomenclatureConfiguration : IEntityTypeConfiguration<Nomenclature> {
        public void Configure(EntityTypeBuilder<Nomenclature> builder) {
            builder.ToTable("nom_list");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.Property(x => x.NomType)
                .HasColumnName("nom_type")
                .IsRequired();

            builder.Property(x => x.Articul)
                .HasColumnName("articul")
                .IsRequired();

            builder.Property(x => x.IsSet)
                .HasColumnName("is_set")
                .IsRequired();

            builder.Property(x => x.VisibleOnSite)
                .HasColumnName("visible_on_site")
                .IsRequired();
        }
    }
}