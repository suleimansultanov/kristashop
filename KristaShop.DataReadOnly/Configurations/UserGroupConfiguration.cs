﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class UserGroupConfiguration : IEntityTypeConfiguration<UserGroup> {
        public void Configure(EntityTypeBuilder<UserGroup> builder) {
            builder.ToTable("user_groups");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Acl)
                .WithMany(x => x.UserGroups)
                .HasForeignKey(x => x.AclId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.GroupName)
                .HasColumnName("ugroup_name")
                .IsRequired();

            builder.Property(x => x.AclId)
                .HasColumnName("acl_id")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("type")
                .HasColumnType("INT(11)")
                .IsRequired();
        }
    }
}