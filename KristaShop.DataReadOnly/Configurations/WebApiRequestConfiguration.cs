﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class WebApiRequestConfiguration : IEntityTypeConfiguration<WebApiRequest> {
        public void Configure(EntityTypeBuilder<WebApiRequest> builder) {
            builder.ToTable("web_api_requests");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.RequestHash)
                .HasColumnName("request_hash")
                .IsRequired();

            builder.Property(x => x.Timestamp)
                .HasColumnName("record_time_stamp")
                .IsRequired(false);
        }
    }
}
