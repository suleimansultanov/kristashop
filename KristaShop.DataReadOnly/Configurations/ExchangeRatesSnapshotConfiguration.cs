﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class ExchangeRatesSnapshotConfiguration : IEntityTypeConfiguration<ExchangeRatesSnapshot> {
        public void Configure(EntityTypeBuilder<ExchangeRatesSnapshot> builder) {
            builder.ToTable("exchange_rates_snapshots");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.FromCurrency)
                .WithMany(x => x.ExchangeRatesSnapshotsFromThis)
                .HasForeignKey(x => x.FromCurrencyId);

            builder.HasOne(x => x.ToCurrency)
                .WithMany(x => x.ExchangeRatesSnapshotsToThis)
                .HasForeignKey(x => x.ToCurrencyId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x=>x.SnapshotId)
                .HasColumnName("snapshot_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.FromCurrencyId)
                .HasColumnName("from_currency_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ToCurrencyId)
                .HasColumnName("to_currency_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Rate)
                .HasColumnName("rate")
                .IsRequired();

            builder.Property(x => x.AddDate)
                .HasColumnName("add_date")
                .IsRequired();
        }
    }
}