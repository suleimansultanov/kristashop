﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class DocOrderItemConfiguration : IEntityTypeConfiguration<DocOrderItem> {
        public void Configure(EntityTypeBuilder<DocOrderItem> builder) {
            builder.ToTable("doc_order_items");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Document)
                .WithMany(x => x.DocOrderItems)
                .HasForeignKey(x => x.DocumentId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DocumentId)
                .HasColumnName("document_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ProductId)
                .HasColumnName("product_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.IsPreorder)
                .HasColumnName("is_preorder")
                .IsRequired();

            builder.Property(x => x.ItemsCount)
                .HasColumnName("items_count")
                .IsRequired();

            builder.Property(x => x.ItemsCountSrc)
                .HasColumnName("items_count_src")
                .IsRequired();

            builder.Property(x => x.Price)
                .HasColumnName("price")
                .IsRequired();

            builder.Property(x => x.Discount)
                .HasColumnName("discount")
                .IsRequired();

            builder.Property(x => x.ProcessedCount)
                .HasColumnName("processed_count")
                .IsRequired();

            builder.Property(x => x.PayIn)
                .HasColumnName("pay_in")
                .IsRequired();

            builder.Property(x => x.CostsTotal)
                .HasColumnName("costs_total")
                .IsRequired();

            builder.Property(x => x.PrePay)
                .HasColumnName("pre_pay")
                .IsRequired();

            builder.Property(x => x.ProdusedCount)
                .HasColumnName("prodused_count")
                .IsRequired();
        }
    }
}
