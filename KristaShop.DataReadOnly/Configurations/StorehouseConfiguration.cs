﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class StorehouseConfiguration : IEntityTypeConfiguration<Storehouse> {
        public void Configure(EntityTypeBuilder<Storehouse> builder) {
            builder.ToTable("storehouses");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.Property(x => x.IsForOnlineStore)
                .HasColumnName("is_for_online_store")
                .IsRequired();
        }
    }
}
