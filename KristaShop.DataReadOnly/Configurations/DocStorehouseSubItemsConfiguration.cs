﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class DocStorehouseSubItemsConfiguration : IEntityTypeConfiguration<DocStorehouseSubItems> {
        public void Configure(EntityTypeBuilder<DocStorehouseSubItems> builder) {
            builder.ToTable("doc_store_income_items");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.DocRegistry)
                .WithMany(x => x.DocStorehouseSubItems)
                .HasForeignKey(x => x.DocumentId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.DocumentId)
                .HasColumnName("document_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ProductId)
                .HasColumnName("product_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ItemsCount)
                .HasColumnName("items_count")
                .IsRequired();
        }
    }
}