﻿using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class PriceTypeConfiguration : IEntityTypeConfiguration<PriceType> {
        public void Configure(EntityTypeBuilder<PriceType> builder) {
            builder.ToTable("price_types");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName("name")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("price_type")
                .HasColumnType("INT(11)")
                .HasDefaultValue(PriceTypes.None)
                .IsRequired();
        }
    }
}