﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class UserGroupMembershipConfiguration : IEntityTypeConfiguration<UserGroupMembership> {
        public void Configure(EntityTypeBuilder<UserGroupMembership> builder) {
            builder.ToTable("user_group_membership");

            builder.HasKey(x => new {user_id = x.UserId, group_id = x.GroupId});

            builder.HasOne(x => x.User)
                .WithMany(x => x.UserGroupMemberships)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.UserGroup)
                .WithMany(x => x.UserGroupMemberships)
                .HasForeignKey(x => x.GroupId);

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.GroupId)
                .HasColumnName("group_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}