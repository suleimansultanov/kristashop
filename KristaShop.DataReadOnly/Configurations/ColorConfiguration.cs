﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class ColorConfiguration : IEntityTypeConfiguration<Color> {
        public void Configure(EntityTypeBuilder<Color> builder) {
            builder.ToTable("dict_colors");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ColorName)
                .HasColumnName("color_name")
                .IsRequired();

            builder.Property(x => x.ColorCode)
                .HasColumnName("color_code")
                .IsRequired(false);

            builder.Property(x => x.ColorImage)
                .HasColumnName("color_image")
                .IsRequired(false);

            builder.Ignore(x => x.ImagePath);
        }
    }
}