﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class CatalogItemPriceConfiguration : IEntityTypeConfiguration<CatalogItemPrice> {
        public void Configure(EntityTypeBuilder<CatalogItemPrice> builder) {
            builder.ToTable("catalog_item_price");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.CatalogItem)
                .WithMany(x => x.CatalogItemPrices)
                .HasForeignKey(x => x.CatalogItemId);

            builder.HasOne(x => x.PriceType)
                .WithMany(x => x.CatalogItemPrices)
                .HasForeignKey(x => x.PriceTypeId);

            builder.HasOne(x => x.InputCurrency)
                .WithMany(x => x.CatalogItemPrices)
                .HasForeignKey(x => x.InputCurrencyId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.CatalogItemId)
                .HasColumnName("catalog_item_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Price)
                .HasColumnName("price")
                .IsRequired();

            builder.Property(x => x.PriceTypeId)
                .HasColumnName("price_type_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.InputCurrencyId)
                .HasColumnName("input_currency_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.ExchangeRateSnapshotId)
                .HasColumnName("exchange_rate_snapshot_id")
                .HasColumnType("binary(16)")
                .IsRequired();
        }
    }
}