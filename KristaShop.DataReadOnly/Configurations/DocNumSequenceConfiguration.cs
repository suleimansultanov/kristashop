﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class DocNumSequenceConfiguration : IEntityTypeConfiguration<DocNumSequence> {
        public void Configure(EntityTypeBuilder<DocNumSequence> builder) {
            builder.ToTable("doc_num_sequence");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.AddDate)
                .HasColumnName("add_date")
                .IsRequired();
        }
    }
}
