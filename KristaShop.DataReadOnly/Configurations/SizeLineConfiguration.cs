﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class SizeLineConfiguration : IEntityTypeConfiguration<SizeLine> {
        public void Configure(EntityTypeBuilder<SizeLine> builder) {
            builder.ToTable("dict_size_lines");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.SizeLineName)
                .HasColumnName("size_line_name")
                .IsRequired();

            builder.Property(x => x.SizeLinePos)
                .HasColumnName("size_line_pos")
                .IsRequired();

            builder.Property(x => x.SizesIdList)
                .HasColumnName("sizes_id_list")
                .IsRequired();
        }
    }
}