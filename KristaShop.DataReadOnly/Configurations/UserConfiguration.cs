﻿using System;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class UserConfiguration : IEntityTypeConfiguration<User> {
        public void Configure(EntityTypeBuilder<User> builder) {
            builder.ToTable("users");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Counterparty)
                .WithOne(x => x.User)
                .HasForeignKey<User>(x => x.CounterpartyId);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Login)
                .HasColumnName("login")
                .HasMaxLength(64)
                .IsRequired();

            builder.Property(x => x.Password)
                .HasColumnName("password")
                .IsRequired();

            builder.Property(x => x.Acl)
                .HasColumnName("acl")
                .IsRequired();

            builder.Property(x => x.Status)
                .HasColumnName("status")
                .HasColumnType("INT(11)")
                .IsRequired();

            builder.Property(x => x.RegistrationDate)
                .HasColumnName("registration_date")
                .IsRequired(false);

            builder.Property(x => x.BanExpireDate)
                .HasColumnName("ban_expire_date")
                .IsRequired(false);

            builder.Property(x => x.BanReason)
                .HasColumnName("ban_reason")
                .IsRequired();

            builder.Property(x => x.IsRoot)
                .HasColumnName("is_root")
                .HasDefaultValue(false)
                .IsRequired();

            builder.Property(x => x.CounterpartyId)
                .HasColumnName("counterparty_id")
                .HasColumnType("binary(16)")
                .HasDefaultValue(Guid.Empty)
                .IsRequired(false);

            builder.Property(x => x.Email)
                .HasColumnName("email")
                .IsRequired(false);
        }
    }
}