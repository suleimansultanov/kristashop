﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class ManagerRateConfiguration : IEntityTypeConfiguration<ManagerRate> {
        public void Configure(EntityTypeBuilder<ManagerRate> builder) {
            builder.ToTable("user_manager_rate");

            builder.HasKey(x => x.UserId);

            builder.HasOne(x => x.User)
                .WithOne(x => x.ManagerRate)
                .HasForeignKey<ManagerRate>(x => x.UserId);

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Rate)
                .HasColumnName("rate")
                .IsRequired();
        }
    }
}
