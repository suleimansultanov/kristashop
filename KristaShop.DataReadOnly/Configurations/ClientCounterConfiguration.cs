﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class ClientCounterConfiguration : IEntityTypeConfiguration<ClientCounter> {
        public void Configure(EntityTypeBuilder<ClientCounter> builder) {
            builder.ToTable("client_counter");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Counter)
                .HasColumnName("counter")
                .IsRequired();

            builder.Property(x => x.UpdateTimestamp)
                .HasColumnName("update_time_stamp")
                .IsRequired(false);
        }
    }
}
