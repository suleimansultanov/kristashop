﻿using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    internal class OptionsConfiguration : IEntityTypeConfiguration<Option> {
        public void Configure(EntityTypeBuilder<Option> builder) {
            builder.ToTable("nom_options");

            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.NomFilterColor)
                .WithOne(x => x.Option);

            builder.HasOne(x => x.NomFilterSize)
                .WithOne(x => x.Option);

            builder.HasOne(x => x.NomFilterSizeLine)
                .WithOne(x => x.Option);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.Type)
                .HasColumnName("option_type")
                .IsRequired();

            builder.Property(x => x.Value)
                .HasColumnName("value")
                .IsRequired();
        }
    }
}