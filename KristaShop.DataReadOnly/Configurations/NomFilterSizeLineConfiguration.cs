﻿using System;
using System.Collections.Generic;
using System.Text;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KristaShop.DataReadOnly.Configurations {
    public class NomFilterSizeLineConfiguration : IEntityTypeConfiguration<NomFilterSizeLine> {
        public void Configure(EntityTypeBuilder<NomFilterSizeLine> builder) {
            builder.ToTable("nom_filter_size_lines");

            builder.HasKey(x => new { size_line_id = x.SizeLineId, option_id = x.OptionId });

            builder.HasOne(x => x.SizeLine)
                .WithMany(x => x.NomFilterSizeLines)
                .HasForeignKey(x => x.SizeLineId);

            builder.HasOne(x => x.Option)
                .WithOne(x => x.NomFilterSizeLine)
                .HasForeignKey<NomFilterSizeLine>(x => x.OptionId);

            builder.Property(x => x.SizeLineId)
                .HasColumnName("size_line_id")
                .HasColumnType("binary(16)")
                .IsRequired();

            builder.Property(x => x.OptionId)
                .HasColumnName("option_id")
                .HasColumnType("binary(16)")
                .IsRequired();

        }
    }
}
