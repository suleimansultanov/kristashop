﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataReadOnly.Documents;
using KristaShop.DataReadOnly.Interfaces.Repositories;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.DataReadOnly.Repositories {
    public class DocRegistryRepository : ReadRepository<DocRegistry, Guid>, IDocRegistryRepository<DocRegistry, Guid> {
        public DocRegistryRepository(DbContext context) : base(context) { }

        public override IOrderedQueryable<DocRegistry> AllOrdered => All.OrderBy(x => x.DocNum);

        public Task<int> NewOrdersCountAsync(Guid userId, int userAcl) {
            return All.Where(x => x.DocType.Equals(DocumentType.Order.GetDocType())
                                  && x.Status == UniversalDocState.AwaitOrderProcessing
                                  && (x.Creater.UserId == userId || userAcl > x.Acl))
                .CountAsync();
        }
    }
}
