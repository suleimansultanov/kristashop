﻿using KristaShop.DataReadOnly.Configurations;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.DataReadOnly.Domain {
    public class KristaReplicaDbContext : DbContext {
        public KristaReplicaDbContext(DbContextOptions<KristaReplicaDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new AccessControlConfiguration());
            builder.ApplyConfiguration(new CatalogItemConfiguration());
            builder.ApplyConfiguration(new CatalogItemPriceConfiguration());
            builder.ApplyConfiguration(new CityConfiguration());
            builder.ApplyConfiguration(new ClientCounterConfiguration());
            builder.ApplyConfiguration(new ColorConfiguration());
            builder.ApplyConfiguration(new CounterpartyConfiguration());
            builder.ApplyConfiguration(new CurrencyConfiguration());
            builder.ApplyConfiguration(new DocNumSequenceConfiguration());
            builder.ApplyConfiguration(new DocOrderItemConfiguration());
            builder.ApplyConfiguration(new DocRegistryConfiguration());
            builder.ApplyConfiguration(new DocRegistryFilesConfiguration());
            builder.ApplyConfiguration(new DocStorehouseSubItemsConfiguration());
            builder.ApplyConfiguration(new ExchangeRateConfiguration());
            builder.ApplyConfiguration(new ExchangeRatesSnapshotConfiguration());
            builder.ApplyConfiguration(new MoneyOperationTypeConfiguration());
            builder.ApplyConfiguration(new ManagerRateConfiguration());
            builder.ApplyConfiguration(new NomenclatureConfiguration());
            builder.ApplyConfiguration(new NomFilterColorConfiguration());
            builder.ApplyConfiguration(new NomFilterSizeConfiguration());
            builder.ApplyConfiguration(new NomFilterSizeLineConfiguration());
            builder.ApplyConfiguration(new OptionsConfiguration());
            builder.ApplyConfiguration(new PriceTypeConfiguration());
            builder.ApplyConfiguration(new SizeConfiguration());
            builder.ApplyConfiguration(new SizeLineConfiguration());
            builder.ApplyConfiguration(new StorehouseConfiguration());
            builder.ApplyConfiguration(new StorehouseRestsConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new UserGroupConfiguration());
            builder.ApplyConfiguration(new UserGroupMembershipConfiguration());
            builder.ApplyConfiguration(new EmployeeConfiguration());
            builder.ApplyConfiguration(new WebApiRequestConfiguration());

            builder.Entity<OrderItemsPrice>().HasNoKey().ToView(null);
            builder.Entity<OrderSubtreeItem>().HasNoKey().ToView(null);
            builder.Entity<OrderStoreIncomeItem>().HasNoKey().ToView(null);
        }

        public DbSet<AccessControl> AccessControls { get; set; }
        public DbSet<CatalogItem> CatalogItems { get; set; }
        public DbSet<CatalogItemPrice> CatalogItemPrices { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ClientCounter> ClientCounter { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<DocNumSequence> DocNumSequences { get; set; }
        public DbSet<DocOrderItem> DocOrderItems { get; set; }
        public DbSet<DocRegistry> DocRegistries { get; set; }
        public DbSet<DocRegistryFile> DocRegistryFiles { get; set; }
        public DbSet<DocStorehouseSubItems> DocStorehouseSubItems { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<ExchangeRatesSnapshot> ExchangeRatesSnapshots { get; set; }
        public DbSet<MoneyOperationType> MoneyOperationTypes { get; set; }
        public DbSet<ManagerRate> ManagerRates { get; set; }
        public DbSet<Nomenclature> Nomenclatures { get; set; }
        public DbSet<NomFilterColor> NomFilterColors { get; set; }
        public DbSet<NomFilterSize> NomFilterSizes { get; set; }
        public DbSet<NomFilterSizeLine> NomFilterSizeLines { get; set; }
        public DbSet<Option> Option { get; set; }
        public DbSet<PriceType> PriceTypes { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<SizeLine> SizeLines { get; set; }
        public DbSet<Storehouse> StoreHouses { get; set; }
        public DbSet<StorehouseRests> StoreHouseRests { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserGroupMembership> UserGroupMemberships { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<WebApiRequest> WebApiRequests { get; set; }

        public DbSet<OrderItemsPrice> OrderItemsPrices { get; set; }
        public DbSet<OrderSubtreeItem> OrderSubtreeItems { get; set; }
        public DbSet<OrderStoreIncomeItem> OrderStoreIncomeItems { get; set; }
    }
}