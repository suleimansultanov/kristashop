﻿using System;
using System.Threading.Tasks;
using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.DataReadOnly.Interfaces.Repositories {
    public interface IDocRegistryRepository<TEntity, in TKey> : IReadRepository<TEntity, TKey> where TEntity : class {
        Task<int> NewOrdersCountAsync(Guid userId, int acl);
    }
}
