﻿using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.DataReadOnly.Interfaces
{
    public interface IDocRegistryService {
        Task<DocRegistryFileDTO> GetCounterpartyFileById(Guid id, Guid counterpartyId);

        Task<List<DocRegistryFileDTO>> GetCounterpartyFilesByDocId(Guid docId, Guid counterpartyId);

        Task<DocRegistryDTO> GetDocumentByIdAsync(Guid id, Guid counterpartyId, bool includeFiles = false);

        Task<DocRegistryDTO> GetOrderDocumentBySubtreeIdAsync(Guid id, Guid counterpartyId, bool includeFiles = false);

        Task<List<DocRegistryDTO>> GetCounterpartyCurrentOrdersAsync(Guid counterpartyId);

        Task<List<DocRegistryDTO>> GetCounterpartyCompletedOrdersAsync(Guid counterpartyId);

        Task<List<DocRegistryDTO>> GetUserActiveDocumentsListAsync(Guid userId, bool detailedOnly = false);

        Task<List<DocRegistryDTO>> GetCounterpartyOrderSubtreeDocumentsAsync(Guid counterpartyId, Guid subtreeId, bool roundSumInPrepayDoc);

        Task<double> GetRateSnapshotAsync(Guid currencyId, Guid snapshotId);

        Task<MoneyOperationType> GetMoneyOperationTypeAsync(Guid moneyOperationTypeId);

        Task<double> GetTotalSumByDocIdAsync(Guid docId, double? percent = null);
    }
}