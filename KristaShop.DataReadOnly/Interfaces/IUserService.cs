﻿using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Interfaces
{
    public interface IUserService
    {
        Task<bool> IsActiveUserAsync(Guid userId);

        Task<OperationResult> SignIn(string login, string pass, bool isPersistent = false, bool isBackend = true);

        Task<OperationResult> SignInByLink(Guid userId);

        Task SignOut(bool isBackend = true);

        List<UserClientDTO> GetAllUsers(UserSession user);

        Task<UserClientDTO> GetUserAsync(Guid id);

        Task<CounterpartyDTO> GetCounterpartyDetailsAsync(Guid id);
    }
}