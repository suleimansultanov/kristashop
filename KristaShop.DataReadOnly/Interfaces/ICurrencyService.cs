﻿using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Interfaces
{
    public interface ICurrencyService
    {
        Task<double> GetLastExchangeRate();
    }
}