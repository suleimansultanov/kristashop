﻿using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Interfaces
{
    public interface ICatalogItemReadService {
        Task<double> GetPartsCountByNomId(Guid nomId);

        Task<ProductDetailsDTO> GetPackagingProduct(Guid nomId);

        Task<List<ProductDTO>> GetProductsForCart(List<Guid> productsId);

        List<CModelGridDTO> GetCatalogModelGrid(Guid nomId, List<Guid> productIds);

        List<Guid> GetNomenclatureByFilter(List<Guid> colors, List<Guid> sizes, bool isInstock);

        List<Dictionary<string, object>> GetColorsSizesGrid(Guid nomId, List<Guid> productIds);

        List<Dictionary<string, object>> GetSHAmountGrid(Guid nomId);

        List<CModelDTO> GetCatalogModels();

        List<Guid> GetProductIdsByNomId(Guid numId, List<Guid> notVisProductsId);

        Task<List<SizeDTO>> GetNomenclatureSizesAsync(Guid nomenclatureId, bool isSet, List<Guid> productsId);

        List<Guid> GetProductIdsInStorehouse(List<Guid> productIds);

        OperationResult IsExistInStorehouse(Guid productId, int amount, int reservedAmount);

        Task<CModelDTO> GetCatalogModel(Guid id);

        List<ColorDTO> GetColorsByNomId(Guid nomId);

        Task<List<ColorDTO>> GetNomenclatureColorsBySizeAsync(Guid nomId, Guid sizeId, bool isInstock, List<Guid> excludeProductIds);

        Task<double?> GetProductPrice(Guid productId);

        Task<ProductDetailsDTO> GetProductIdByParams(Guid nomId, Guid sizeId, Guid colorId, bool isSet);

        Task<List<ColorDTO>> GetColorsByListNomId(List<Guid> nomIds);

        Task<List<SizeDTO>> GetSizesByListNomId(List<Guid> nomIds);

        Task<List<SizeLineDTO>> GetSizeLinesByListNomId(List<Guid> nomIds);
    }
}