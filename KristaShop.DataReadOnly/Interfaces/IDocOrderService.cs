﻿using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.DataReadOnly.Interfaces {
    public interface IDocOrderService {
        Task<List<OrderItemsDTO>> GetOrderItemsFromStoreIncomeAsync(Guid orderId);

        Task<List<OrderItemsDTO>> GetOrderItemsWithStatuses(Guid orderId, bool orderByStatus = true);

        Task<List<OrderItemsDTO>> GetOrderItemsPricesAsync(Guid orderId);
    }
}