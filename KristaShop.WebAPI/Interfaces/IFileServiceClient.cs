﻿using System.Net.Http;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Services
{
    public interface IFileServiceClient
    {
        Task<string> GetResponseFile(string virtualPath);
    }
}