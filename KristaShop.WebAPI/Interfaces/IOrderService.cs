﻿using System.Threading.Tasks;
using KristaShop.Common.Models;
using KristaShop.WebAPI.ViewModels;

namespace KristaShop.WebAPI.Interfaces
{
    public interface IOrderService
    {
        Task<OperationResult> RegistryOrderDocAsync(OrderVM order);
    }
}