﻿using KristaShop.Common.Models;
using KristaShop.WebAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Interfaces
{
    public interface IIdentityService
    {
        Task<OperationResult> UpdateClient(UserUpdateViewModel model);

        Task<OperationResult> ChangeClientStatus(UserUpdateViewModel model);

        Task<OperationResult> UserRegistrate(RegHashViewModel model);

        List<int> GetUserGroups(Guid? userId);
    }
}