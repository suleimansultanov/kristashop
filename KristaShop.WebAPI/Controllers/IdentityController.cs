﻿using System;
using KristaShop.Common.Models;
using KristaShop.WebAPI.Interfaces;
using KristaShop.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Exceptions;
using KristaShop.Common.Models.ApiResponses;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.WebAPI.Utils.Extensions;
using Newtonsoft.Json;
using Serilog;

namespace KristaShop.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _service;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public IdentityController(IIdentityService service, IUserService userService, IMapper mapper, ILogger logger) {
            _service = service;
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public string Get() => "API is running";

        [HttpPost("Register")]
        public async Task<IActionResult> Register(RegistrationViewModel registrationModel, string hash) {
            try {
                if (ModelState.IsValid) {
                    var actualHash = JsonConvert.SerializeObject(registrationModel).ComputeSha256Hash();
                    if (actualHash != hash) {
                        return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Hash code is invalid"));
                    }

                    if (!await _userService.IsUserRegisteredAsync(hash)) {
                        var newCounterparty = _mapper.Map<CounterpartyDTO>(registrationModel);
                        newCounterparty.UserId = Guid.NewGuid();
                        if (!await _userService.RegisterAsync(newCounterparty)) {
                            return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Failed to register new counterparty"));
                        }

                        try {
                            await _userService.AddRegistrationHashAsync(hash);
                        } catch (Exception ex) {
                            _logger.Error(ex, "Failed to add registration hash for new counterparty. {message}", ex.Message);
                        }

                        return Ok(new RegisterResponse(HttpStatusCode.OK, newCounterparty.UserId, string.Empty, "Вы успешно зарегестрировались на сайте. Ожидайте подтверждения менеджера."));
                    }
                } else {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Failed to register new counterparty, model state is invalid", ModelState.Values.ErrorsToString()));
                }
            } catch (ExceptionBase ex) {
                _logger.Warning(ex, "Failed to register new counterparty. {readableMessage}. {message}", ex.ReadableMessage, ex.Message);
                return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, $"{ex.Message}\r\n {ex}", ex.ReadableMessage));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to register new counterparty. {message}", ex.Message);
                return StatusCode(500, new BaseResponse(HttpStatusCode.InternalServerError, $"{ex.Message}\r\n {ex}"));
            }

            return Ok(new BaseResponse(HttpStatusCode.OK, string.Empty, "Вы успешно зарегестрировались на сайте. Ожидайте подтверждения менеджера."));
        }

        [HttpPost("ActivateUser")]
        public async Task<IActionResult> ActivateUser(Guid userId, string hash) {
            try {
                var actualHash = JsonConvert.SerializeObject(userId.ToString()).ComputeSha256Hash();
                if (actualHash != hash) {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Hash code is invalid"));
                }

                await _userService.ActivateUserAsync(userId);
                return Ok(new BaseResponse(HttpStatusCode.OK, string.Empty, "Пользователь успешно активирован."));
            } catch (ExceptionBase ex) {
                _logger.Warning(ex, "Failed to activate user. {readableMessage}. {message}", ex.ReadableMessage, ex.Message);
                return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, $"{ex.Message}\r\n {ex}", ex.ReadableMessage));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to activate user {userId}. {message}", userId, ex.Message);
                return StatusCode(500, new BaseResponse(HttpStatusCode.InternalServerError, $"{ex.Message}\r\n {ex}"));
            }
        }

        [HttpPost("BanUser")]
        public async Task<IActionResult> BanUser(BanViewModel banModel) {
            try {
                var userToBan = _mapper.Map<BanUserDTO>(banModel);
                await _userService.BanUserAsync(userToBan);
                return Ok(new BaseResponse(HttpStatusCode.OK, string.Empty, "Пользователь успешно заблокирован."));
            } catch (ExceptionBase ex) {
                _logger.Warning(ex, "Failed to ban user. {readableMessage}. {message}", ex.ReadableMessage, ex.Message);
                return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, $"{ex.Message}\r\n {ex}", ex.ReadableMessage));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to ban user. {@model} {message}", banModel, ex.Message);
                return StatusCode(500, new BaseResponse(HttpStatusCode.InternalServerError, $"{ex.Message}\r\n {ex}"));
            }
        }

        [HttpPost("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UpdateUserViewModel userModel, string hash) {
            try {
                var actualHash = JsonConvert.SerializeObject(userModel).ComputeSha256Hash();
                if (actualHash != hash) {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Hash code is invalid"));
                }
                var user = _mapper.Map<UpdateUserDTO>(userModel);
                if (!await _userService.UpdateUserAsync(user)) {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Failed to update user"));
                }
                return Ok(new BaseResponse(HttpStatusCode.OK, string.Empty, "Данные пользователя успешно обновлены."));
            } catch (ExceptionBase ex) {
                _logger.Warning(ex, "Failed to update user. {readableMessage}. {message}", ex.ReadableMessage, ex.Message);
                return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, $"{ex.Message}\r\n {ex}", ex.ReadableMessage));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to update user. {@model} {message}", userModel, ex.Message);
                return StatusCode(500, new BaseResponse(HttpStatusCode.InternalServerError, $"{ex.Message}\r\n {ex}"));
            }
        }

        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword(NewPasswordViewModel passwordModel, string hash) {
            try {
                var actualHash = JsonConvert.SerializeObject(passwordModel).ComputeSha256Hash();
                if (actualHash != hash) {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Hash code is invalid"));
                }

                if (!await _userService.ChangeUserPasswordAsync(passwordModel.UserId, passwordModel.Password)) {
                    return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, "Failed to save changes for user's password"));
                }

                return Ok(new BaseResponse(HttpStatusCode.OK, string.Empty, "Пароль успешно обновлен."));
            } catch (ExceptionBase ex) {
                _logger.Warning(ex, "Failed to change user's password. {readableMessage}. {message}", ex.ReadableMessage, ex.Message);
                return BadRequest(new BaseResponse(HttpStatusCode.BadRequest, $"{ex.Message}\r\n {ex}", ex.ReadableMessage));
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to change user's password. {@model} {message}", passwordModel, ex.Message);
                return StatusCode(500, new BaseResponse(HttpStatusCode.InternalServerError, $"{ex.Message}\r\n {ex}"));
            }
        }

        #region deprecated api methods
        // This methods will be removed soon
        
        [HttpPost]
        public async Task<OperationResult> Post(RegHashViewModel model)
        {
            var modelJson = model.Reg.ToJson();
            string hashcode = modelJson.ComputeSha256Hash();
            if (model.Hash.HashCode != hashcode)
                return OperationResult.Failure(new List<string> { "Неудача!" });
            return await _service.UserRegistrate(model);
        }

        [HttpPost("update")]
        public async Task<OperationResult> Update(UserUpdateViewModel model)
        {
            if (model.IsFullUpdate)
                return await _service.UpdateClient(model);
            else
                return await _service.ChangeClientStatus(model);
        }

        #endregion
    }
}