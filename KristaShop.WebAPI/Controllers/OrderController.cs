﻿using KristaShop.Common.Models;
using KristaShop.WebAPI.Interfaces;
using KristaShop.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _service;

        public OrderController
            (IOrderService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<OperationResult> Post(OrderVM model)
        {
            var modelJson = model.OrderItem.ToJson();
            string hashcode = modelJson.ComputeSha256Hash();
            if (model.Hash.HashCode != hashcode)
                return OperationResult.Failure(new List<string> { "Неудача!" });
            return await _service.RegistryOrderDocAsync(model);
        }
    }
}