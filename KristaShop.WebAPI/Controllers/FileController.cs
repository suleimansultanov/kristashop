﻿using KristaShop.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IFileServiceClient _service;

        public FileController
            (IFileServiceClient service)
        {
            _service = service;
        }

        [HttpGet("{virtualPath}")]
        public async Task<string> Get(string virtualPath)
        {
            return await _service.GetResponseFile(virtualPath);
        }
    }
}