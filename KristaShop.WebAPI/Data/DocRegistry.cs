﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("docs_registry")]
    public class DocRegistry
    {
        [Key]
        public Guid id { get; set; }
        public int pos { get; set; }
        public int level { get; set; }
        public Guid parent_id { get; set; }
        public Guid subtree_id { get; set; }
        public DateTime create_date { get; set; }
        public DateTime aplied_date { get; set; }
        public int acl { get; set; }
        public Guid department_id { get; set; }
        public Guid creater_id { get; set; }
        public Guid aplier_id { get; set; }
        public Guid executer_id { get; set; }
        public Guid counterparty_id { get; set; }
        public string doc_type { get; set; }
        public long doc_num { get; set; }
        public bool is_aplied { get; set; }
        public bool is_completed { get; set; }
        public int status { get; set; }
        public string extra_data { get; set; }
        public string description { get; set; }
    }
}
