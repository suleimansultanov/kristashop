﻿using KristaShop.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("storehouse_movements")]
    public class StoreHouseMovement
    {
        [Key]
        public Guid id { get; set; }
        public DateTime add_date { get; set; }
        public MovementType op_type { get; set; }
        public MovementDirection op_dir { get; set; }
        public Guid storehouse_from_id { get; set; }
        public Guid storehouse_to_id { get; set; }
        public Guid product_id { get; set; }
        public double count { get; set; }
        public Guid document_id { get; set; }
    }
}
