﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("exchange_rates_snapshots")]
    public class ExchangeRatesSnapshot
    {
        [Key]
        public Guid id { get; set; }

        public Guid snapshot_id { get; set; }
        public Guid from_currency_id { get; set; }
        public Guid to_currency_id { get; set; }
        public double rate { get; set; }
        public DateTime add_date { get; set; }
    }
}