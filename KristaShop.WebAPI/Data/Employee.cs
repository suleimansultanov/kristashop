﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Data
{
    [Table("staff_employees")]
    public class Employee
    {
        [Key]
        public Guid id { get; set; }
        public Guid user_id { get; set; }
    }
}
