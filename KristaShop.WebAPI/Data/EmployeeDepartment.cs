﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("staff_employee_departments")]
    public class EmployeeDepartment
    {
        public Guid employee_id { get; set; }
        public Guid department_id { get; set; }
    }
}
