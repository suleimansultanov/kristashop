﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("currencies")]
    public class Currency
    {
        [Key]
        public Guid id { get; set; }

        public string name { get; set; }
        public string sign { get; set; }
        public string international_code { get; set; }
        public int type { get; set; }
        public bool is_default { get; set; }
    }
}