﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("storehouse_rests")]
    public class StoreHouseRest : ICloneable
    {
        [Key]
        public Guid id { get; set; }
        public DateTime add_date { get; set; }
        public int pos { get; set; }
        public int level { get; set; }
        public Guid storehouse_id { get; set; }
        [ForeignKey("storehouse_id")]
        public StoreHouse StoreHouse { get; set; }
        public Guid product_id { get; set; }
        public double count { get; set; }
        public double price { get; set; }
        public Guid reservation_doc_id { get; set; }
        public Guid exchange_rate_snapshot_id { get; set; }
        public double parts_count { get; set; }

        public Guid parent_id { get; set; }
        [ForeignKey("parent_id")]
        public StoreHouseRest Parent { get; set; }

        public virtual ICollection<StoreHouseRest> Children { get; set; }
        public Guid subtree_id { get; set; }

        public object Clone()
        {
            return new StoreHouseRest();
        }
    }
}
