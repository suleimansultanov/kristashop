﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Data
{
    public class CurrentDateTime
    {
        public DateTime ServerTime { get; set; }
    }
}
