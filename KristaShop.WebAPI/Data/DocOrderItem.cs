﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("doc_order_items")]
    public class DocOrderItem
    {
        [Key]
        public Guid id { get; set; }
        public Guid document_id { get; set; }
        public Guid product_id { get; set; }
        public bool is_preorder { get; set; }
        public double items_count { get; set; }
        public double items_count_src { get; set; }
        public double price { get; set; }
        public double discount { get; set; }
        public double processed_count { get; set; }
        public double pay_in { get; set; }
        public double costs_total { get; set; }
        public double pre_pay { get; set; }
        public double prodused_count { get; set; }
    }
}
