﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Data
{
    public class DocumentOrderExtraData
    {
        public DocumentOrderExtraData()
        {
            CurrencyRateSnapshotId = Guid.Empty;
        }

        public Guid CurrencyRateSnapshotId { get; set; }
    }
}
