﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KristaShop.WebAPI.Data
{
    [Table("doc_num_sequence")]
    public class DocNumSequence
    {
        [Key]
        public long id { get; set; }
        public DateTime add_date { get; set; }
    }
}
