﻿using Microsoft.EntityFrameworkCore;

namespace KristaShop.WebAPI.Data
{
    public class KristaAsupDbContext : DbContext
    {
        public KristaAsupDbContext
               (DbContextOptions<KristaAsupDbContext> options)
               : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserRate>()
                .HasKey(wt => new { wt.user_id });

            builder.Entity<UserGroupMembership>()
                .HasKey(wt => new { wt.user_id, wt.group_id });

            builder.Entity<EmployeeDepartment>()
                .HasKey(em => new { em.employee_id, em.department_id });

            builder.Entity<CurrentDateTime>().HasNoKey().ToView(null);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserGroupMembership> UserGroupMemberships { get; set; }
        public DbSet<AccessControl> AccessControls { get; set; }
        public DbSet<UserRate> UserRates { get; set; }
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<WebApiRequest> WebApiRequests { get; set; }
        public DbSet<ClientCounter> ClientCounters { get; set; }

        public DbSet<DocRegistry> DocRegistries { get; set; }
        public DbSet<DocOrderItem> DocOrderItems { get; set; }
        public DbSet<DocNumSequence> DocNumSequences { get; set; }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeDepartment> EmployeeDepartments { get; set; }

        public DbSet<StoreHouse> StoreHouses { get; set; }
        public DbSet<StoreHouseRest> StoreHouseRests { get; set; }
        public DbSet<StoreHouseMovement> StoreHouseMovements { get; set; }

        public DbSet<Currency> Currencies { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<ExchangeRatesSnapshot> ExchangeRatesSnapshots { get; set; }

        public DbSet<CurrentDateTime> ServerTime { get; set; }
    }
}
