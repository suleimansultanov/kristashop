﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace KristaShop.WebAPI.Utils.Extensions {
    public static class ModelStateExtensions {
        public static string ErrorsToString(this ModelStateDictionary.ValueEnumerable values) {
            var errors = values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage);
            return string.Join(". ", errors.ToList());
        }
    }
}
