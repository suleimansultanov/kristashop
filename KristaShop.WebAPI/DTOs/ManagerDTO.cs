﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.DTOs
{
    public class ManagerDTO
    {
        public Guid? Id { get; set; }
        public int Acl { get; set; }
        public Guid? DepartmentId { get; set; }
        public string Email { get; set; }
    }
}
