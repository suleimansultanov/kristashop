using System;
using AutoMapper;
using KristaShop.Business.Utils;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.Common.Services;
using KristaShop.WebAPI.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO.Compression;
using System.Linq;
using System.Net;
using KristaShop.Common.Models.ApiResponses;
using KristaShop.ServicesAsup.Infrastructure;
using KristaShop.WebAPI.Utils.Extensions;
using KristaShop.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace KristaShop.WebAPI
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment) {
            _environment = environment;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (!_environment.IsProduction()) {
                services.AddSwaggerGen();
            }

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });

            services.AddAsupDbContextOld(Configuration);
            services.AddApiBusinessOld();
            services.AddTransient<IEmailService, EmailService>();
            services.Configure<FileOptionSetting>(Configuration.GetSection("FileOptionSetting"));
            services.Configure<EmailsSetting>(Configuration.GetSection("EmailsSetting"));

            services.AddAsupDbContext(Configuration);
            services.AddAsupServices();

            services.AddControllers()
                .ConfigureApiBehaviorOptions(options => {
                    options.InvalidModelStateResponseFactory = context => new BadRequestObjectResult(new BaseResponse(HttpStatusCode.BadRequest, $"Failed to execute operation {context.HttpContext.Request.Path}. Model state is invalid", context.ModelState.Values.ErrorsToString()));
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsProduction()) {
                app.UseSwagger();
                app.UseSwaggerUI(options => {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Krista Shop API v1");
                });
            }

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
