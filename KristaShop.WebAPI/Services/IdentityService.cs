﻿using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Helpers;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebAPI.Data;
using KristaShop.WebAPI.Interfaces;
using KristaShop.WebAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly KristaAsupDbContext _context;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;

        public IdentityService
            (KristaAsupDbContext context, IMapper mapper, IEmailService emailService)
        {
            _context = context;
            _mapper = mapper;
            _emailService = emailService;
        }

        public async Task<OperationResult> UpdateClient(UserUpdateViewModel model)
        {
            try
            {
                var user = await _context.Users.Include(x=>x.Counterparty).AsNoTracking().FirstOrDefaultAsync(x => x.id == model.UserId);
                user.login = model.ClientUserName;
                user.password = model.ClientPassword;
                user.email = model.Email;
                user.Counterparty.person_email = model.Email;
                user.Counterparty.city_id = model.CityId;
                user.Counterparty.mall_address = model.ShopName;
                user.Counterparty.person_phone = model.PhoneNumber;
                _context.Update(user);
                await _context.SaveChangesAsync();
                return OperationResult.Success();
            }
            catch (Exception ex)
            {
                return OperationResult.Failure(new List<string> { $"Ошибка: {ex.StackTrace}" });
            }
        }

        public async Task<OperationResult> ChangeClientStatus(UserUpdateViewModel model)
        {
            try
            {
                var user = await _context.Users.AsNoTracking().FirstOrDefaultAsync(x => x.id == model.UserId);
                user.status = model.IsActive ? (int)UserStatus.Active : (int)UserStatus.Banned;
                _context.Update(user);
                await _context.SaveChangesAsync();
                return OperationResult.Success();
            }
            catch (Exception ex)
            {
                return OperationResult.Failure(new List<string> { $"Ошибка: {ex.StackTrace}" });
            }
        }

        public async Task<OperationResult> UserRegistrate(RegHashViewModel model)
        {
            using var dbContextTransaction = _context.Database.BeginTransaction();
            try
            {
                if (IsExistHash(model.Hash.HashCode))
                    return OperationResult.Success(new List<string> { "Вы успешно зарегестрировались на сайте.", "Ожидайте подтверждения менеджера." });
                AddRequestHash(model.Hash.HashCode);

                (Guid? managerId, string managerEmail) = await BalancedRoundRobin();
                if (managerId == null)
                    return OperationResult.Failure(new List<string> { "На данный момент менеджеров нет." });

                var cparty = AddCounterparty(model.Reg, managerId.Value);
                if (IsExistUser(cparty.id))
                    return OperationResult.Failure(new List<string> { "Такой пользователь уже существует." });

                Guid userId = AddUser(cparty);

                await _context.SaveChangesAsync();
                await dbContextTransaction.CommitAsync();

                if (!string.IsNullOrEmpty(managerEmail))
                {
                    const string subject = "Зарегестрировался новый пользователь.";
                    string content = $"Зарегестрировался новый пользователь c именем {cparty.person}. Адрес магазина {cparty.mall_address}.";
                    EmailMessage emailMessage = new EmailMessage(managerEmail, subject, content);
                    await _emailService.SendEmailAsync(emailMessage);
                }
                return OperationResult.Success(new List<string> { "Вы успешно зарегестрировались на сайте.", "Ожидайте подтверждения менеджера." }, userId.ToString());
            }
            catch (Exception ex)
            {
                await dbContextTransaction.RollbackAsync();
                return OperationResult.Failure(new List<string> { $"Ошибка: {ex.StackTrace}" });
            }
        }

        private bool IsExistHash(string hashCode)
        {
            return _context.WebApiRequests.Any(x => x.request_hash == hashCode);
        }

        private void AddRequestHash(string hashCode)
        {
            WebApiRequest request = new WebApiRequest
            {
                id = Guid.NewGuid(),
                request_hash = hashCode
            };
            _context.WebApiRequests.Add(request);
        }

        private Counterparty AddCounterparty(UserRegistrationVM model, Guid managerId)
        {
            Counterparty cparty = _mapper.Map<Counterparty>(model);
            Counterparty existCParty = _context.Counterparties
                .FirstOrDefault(x => x.person == cparty.person && x.person_phone == cparty.person_phone);
            if (existCParty != null)
                return existCParty;

            cparty.manager_id = managerId;
            _context.Counterparties.Add(cparty);
            return cparty;
        }

        private bool IsExistUser(Guid cpartyId)
        {
            return _context.Users.Any(x => x.counterparty_id == cpartyId);
        }

        private Guid AddUser(Counterparty cparty)
        {
            string[] clientFullName = cparty.person.Split(" ");
            string fname = string.Concat(clientFullName[0].Select(c => TranslateHelper.Map[c]));
            User user = new User
            {
                id = Guid.NewGuid(),
                login = $"{fname}{cparty.person_phone}",
                password = HashHelper.TransformPassword(CreatePassword(8)),
                counterparty_id = cparty.id
            };
            var group = AddUserToGroup(user.id);
            user.acl = GetAclNumber(group.acl_id);
            _context.Users.Add(user);
            return user.id;
        }

        private UserGroup AddUserToGroup(Guid userId)
        {
            var group = _context.UserGroups.FirstOrDefault(x => x.type == (int)UserType.Customer);
            UserGroupMembership membership = new UserGroupMembership
            {
                user_id = userId,
                group_id = group.id
            };
            _context.UserGroupMemberships.Add(membership);
            return group;
        }

        private int GetAclNumber(Guid aclId)
        {
            var acl = _context.AccessControls.Find(aclId);
            return acl.acl;
        }

        private string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        private async Task<(Guid?, string)> BalancedRoundRobin()
        {
            long clientCounter = await UpdateClientCounterAsync();
            var orderedRates = _context.UserRates.Where(x => x.rate > GlobalConstant.Epsilon);
            if (!orderedRates.Any())
                return (null, null);
            double remainder = 1 / orderedRates.Sum(x => x.rate);

            var anonRates = orderedRates.Select(x => new
            {
                Rate = Math.Round(x.rate * remainder, 4),
                UserId = x.user_id,
                RegDate = x.User.registration_date,
                Email = x.User.email
            }).OrderBy(x => x.RegDate).ToList();

            double minRate = anonRates.Min(x => x.Rate);
            int maxValueCounter = anonRates.Count;

            int totalClientAmount = Convert.ToInt32(1 / minRate);
            double someValue = (clientCounter % (double)totalClientAmount) / totalClientAmount;
            someValue += minRate;

            double tempRate = 0;
            foreach (var item in anonRates)
            {
                tempRate += item.Rate;
                bool flag = (someValue - tempRate) <= GlobalConstant.Epsilon;
                if (flag)
                {
                    return (item.UserId, item.Email);
                }
            }
            return (null, null);
        }

        private async Task<long> UpdateClientCounterAsync()
        {
            var counter = await _context.ClientCounters.FirstOrDefaultAsync();
            if (counter == null)
            {
                counter = new ClientCounter
                {
                    id = Guid.NewGuid(),
                    update_time_stamp = DateTime.Now,
                    counter = 1
                };
                _context.Add(counter);
                return counter.counter - 1;
            }
            else
            {
                counter.counter++;
                counter.update_time_stamp = DateTime.Now;
                _context.Update(counter);
                return counter.counter - 1;
            }
        }

        public List<int> GetUserGroups(Guid? userId)
        {
            var userGroup = _context.UserGroupMemberships.Where(x => x.user_id == userId)
                .Select(x => x.UserGroup.AccessControl.acl);

            return userGroup.ToList();
        }
    }
}
