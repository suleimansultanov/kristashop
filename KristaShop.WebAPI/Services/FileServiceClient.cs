﻿using KristaShop.Common.Models;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace KristaShop.WebAPI.Services
{
    public class FileServiceClient : IFileServiceClient
    {
        private readonly HttpClient _client;
        private readonly FileOptionSetting _setting;

        public FileServiceClient
            (HttpClient client, IOptions<FileOptionSetting> options)
        {
            _client = client;
            _setting = options.Value;
        }

        public async Task<string> GetResponseFile(string virtualPath)
        {
            var uri = new Uri($"{_setting.ServiceUri}?filepath={_setting.FilePath}{virtualPath}");
            var bytes = await _client.GetByteArrayAsync(uri);
            return Convert.ToBase64String(bytes);
        }
    }
}