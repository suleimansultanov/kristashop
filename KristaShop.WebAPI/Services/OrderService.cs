﻿using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;
using KristaShop.WebAPI.Data;
using KristaShop.WebAPI.DTOs;
using KristaShop.WebAPI.Interfaces;
using KristaShop.WebAPI.ViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Exceptions;
using KristaShop.DataReadOnly.Models;
using Counterparty = KristaShop.WebAPI.Data.Counterparty;
using DocNumSequence = KristaShop.WebAPI.Data.DocNumSequence;
using DocOrderItem = KristaShop.WebAPI.Data.DocOrderItem;
using DocRegistry = KristaShop.WebAPI.Data.DocRegistry;
using ExchangeRatesSnapshot = KristaShop.WebAPI.Data.ExchangeRatesSnapshot;
using User = KristaShop.WebAPI.Data.User;

namespace KristaShop.WebAPI.Services
{
    public class OrderService : IOrderService
    {
        private readonly KristaAsupDbContext _context;
        private readonly IIdentityService _userService;
        private readonly List<StoreHouseRest> _newSHRests;
        private readonly DateTime _nowDate;
        private readonly IEmailService _emailService;

        public OrderService
            (KristaAsupDbContext context, IIdentityService userService, IEmailService emailService)
        {
            _context = context;
            _userService = userService;
            _newSHRests = new List<StoreHouseRest>();
            CurrentDateTime currentDateTime = context.ServerTime.FromSqlRaw("SELECT NOW() as ServerTime").First();
            _nowDate = currentDateTime.ServerTime;
            _emailService = emailService;
        }

        public async Task<OperationResult> RegistryOrderDocAsync(OrderVM order)
        {
            using var dbContextTransaction = _context.Database.BeginTransaction();
            try {
                if (order.OrderItem.CartItems.Count == 0)
                    return OperationResult.Failure(new List<string>
                        {"Не задан список позиций каталога для оприходования на склад."});

                var counterparty = await GetCounterPartyByUserId(order.OrderItem.UserId);
                if (counterparty == null)
                    return OperationResult.Failure(new List<string> {"Такой покупатель не найден."});

                ManagerDTO manager = await GetManagerByCPId(counterparty.id);
                if (manager == null)
                    return OperationResult.Failure(new List<string> {"У данного покупателя менеджер не найден."});

                var (extraData, snapshotResult) = await CreateSnapshot();
                if (!snapshotResult.IsSuccess)
                    return snapshotResult;
                DocRegistry docRegistry =
                    AddDocRegistry(counterparty.id, manager, extraData, order.OrderItem.Description);

                StoreHouse storeHouse = await _context.StoreHouses.FirstOrDefaultAsync(x => x.is_for_online_store);
                if (storeHouse == null)
                    return OperationResult.Failure(new List<string> {"Отсутствует склад, закрепленный за магазином."});

                AddListDocOrderItems(docRegistry.id, order.OrderItem.CartItems);
                await _context.SaveChangesAsync();

                foreach (var item in order.OrderItem.CartItems) {
                    if (item.IsPreorder) continue;

                    var result = await AddToReservationByProductIdAsync(docRegistry.id, storeHouse.id, item);
                    if (result.IsSuccess) {
                        await _context.SaveChangesAsync();
                    } else {
                        throw new EntityUpdateException("Failed to add product to reservation", "Не удалось добавить позицию в резерв", nameof(StorehouseRests));
                    }
                }

                await _context.SaveChangesAsync();
                await dbContextTransaction.CommitAsync();

                if (!string.IsNullOrEmpty(manager.Email)) {
                    const string subject = "Оформлен новый заказ.";
                    string content = $"Оформлен новый заказ с номером {docRegistry.doc_num} от {counterparty.person}.";
                    EmailMessage emailMessage = new EmailMessage(manager.Email, subject, content);
                    await _emailService.SendEmailAsync(emailMessage);
                }

                return OperationResult.Success(new List<string>
                    {$"Заказ успешно оформлен под номером {docRegistry.doc_num}."});
            } catch (Exception ex) {
                await dbContextTransaction.RollbackAsync();
                return OperationResult.Failure(new List<string> {"Ошибка: Не удалось оформить заказ"}, ex.ToString());
            }
        }

        private async Task<Counterparty> GetCounterPartyByUserId(Guid userId)
        {
            Counterparty counterparty = await _context.Users.Where(x => x.id == userId).Select(x => x.Counterparty).FirstOrDefaultAsync();
            return counterparty;
        }

        private async Task<ManagerDTO> GetManagerByCPId(Guid cpId)
        {
            var cp = await _context.Counterparties.FirstOrDefaultAsync(x => x.id == cpId);
            if (cp == null)
                return null;
            else if (cp.manager_id == Guid.Empty)
                return null;

            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.user_id == cp.manager_id);
            if (employee == null)
                return null;

            var employeeDepartment = await _context.EmployeeDepartments.FirstOrDefaultAsync(x => x.employee_id == employee.id);
            if (employeeDepartment == null)
                return null;

            var managerGroups = _userService.GetUserGroups(cp.manager_id);
            if (managerGroups.Count == 0)
                return null;

            User manager = await _context.Users.FirstOrDefaultAsync(x => x.id == employee.user_id);

            return new ManagerDTO
            {
                Id = employee.id,
                Acl = managerGroups.Min(),
                DepartmentId = employeeDepartment?.department_id,
                Email = manager.email
            };
        }

        private long AddDocNumSequence()
        {
            DocNumSequence docNumSequence = new DocNumSequence
            {
                add_date = _nowDate
            };
            _context.DocNumSequences.Add(docNumSequence);
            _context.SaveChanges();
            return docNumSequence.id;
        }

        private async Task<(DocumentOrderExtraData extraData, OperationResult result)> CreateSnapshot()
        {
            var currencies = await _context.Currencies.ToListAsync();
            var defaultCurrency = currencies.Find(x => x.is_default);
            List<ExchangeRatesSnapshot> exchangeRates = new List<ExchangeRatesSnapshot>();
            DocumentOrderExtraData extraData = new DocumentOrderExtraData
            {
                CurrencyRateSnapshotId = Guid.NewGuid()
            };
            foreach (var item in currencies)
            {
                var exchangeRate = await _context.ExchangeRates
                    .Where(x => x.to_currency_id == item.id
                        && x.from_currency_id == defaultCurrency.id)
                    .OrderByDescending(x => x.add_date)
                    .FirstOrDefaultAsync();
                if (exchangeRate == null)
                    return (null, OperationResult.Failure(new List<string> { "Не задан курс валют." }));
                ExchangeRatesSnapshot snapshot = new ExchangeRatesSnapshot
                {
                    id = Guid.NewGuid(),
                    snapshot_id = extraData.CurrencyRateSnapshotId,
                    from_currency_id = exchangeRate.from_currency_id,
                    to_currency_id = exchangeRate.to_currency_id,
                    rate = exchangeRate.rate,
                    add_date = _nowDate
                };
                exchangeRates.Add(snapshot);
            }
            _context.ExchangeRatesSnapshots.AddRange(exchangeRates);
            return (extraData, OperationResult.Success());
        }

        private DocRegistry AddDocRegistry(Guid cpId, ManagerDTO manager, DocumentOrderExtraData extraData, string description)
        {
            int maxPos = _context.DocRegistries.Where(x => x.level == 1).Max(x => x.pos);
            DocRegistry docRegistry = new DocRegistry
            {
                id = Guid.NewGuid(),
                create_date = _nowDate,
                aplied_date = _nowDate,
                status = (int)UniversalDocState.AwaitOrderProcessing,
                subtree_id = Guid.NewGuid(),
                parent_id = Guid.Empty,
                is_aplied = true,
                is_completed = false,
                level = 1,
                doc_type = "EntityDocumentOrder",
                pos = maxPos,
                doc_num = AddDocNumSequence(),
                extra_data = JsonConvert.SerializeObject(extraData),
                creater_id = manager.Id.Value,
                department_id = manager.DepartmentId.Value,
                acl = manager.Acl,
                counterparty_id = cpId,
                executer_id = manager.Id.Value,
                aplier_id = manager.Id.Value,
                description = description
            };
            _context.DocRegistries.Add(docRegistry);
            return docRegistry;
        }

        private void AddListDocOrderItems(Guid docId, List<CartItemVM> cartItems)
        {
            List<DocOrderItem> docOrderItems = new List<DocOrderItem>();
            foreach (var item in cartItems)
            {
                DocOrderItem orderItem = new DocOrderItem
                {
                    id = Guid.NewGuid(),
                    document_id = docId,
                    items_count = item.Amount,
                    items_count_src = item.Amount,
                    product_id = item.ProductId,
                    price = item.Price,
                    discount = item.Discount,
                    is_preorder = item.IsPreorder,
                };
                docOrderItems.Add(orderItem);
            }
            _context.DocOrderItems.AddRange(docOrderItems);
        }

        private async Task<OperationResult> AddToReservationByProductIdAsync(Guid docId, Guid shId, CartItemVM cartItem)
        {
            List<StoreHouseRest> shRests = await _context.StoreHouseRests
                .Where(x => x.storehouse_id == shId && x.reservation_doc_id == Guid.Empty && x.product_id == cartItem.ProductId && x.level == 1)
                .OrderBy(x => x.add_date)
                .AsNoTracking()
                .ToListAsync();

            double totalCount = cartItem.Amount;
            cartItem.Amount = await UpdateSHRests(shRests, docId, shId, cartItem.Amount);
            AddStoreHouseMovement(cartItem.ProductId, totalCount, docId, shId);

            if (cartItem.Amount > 0.001d)
                return OperationResult.Failure(new List<string> { $"На заданном складе отсутствует {cartItem.Articul}, размер {cartItem.Size}, цвет {cartItem.ColorName} в необходимом количестве." });

            return OperationResult.Success();
        }

        private async Task<int> UpdateSHRests(List<StoreHouseRest> shRests, Guid docId, Guid shId, int itemCount)
        {
            foreach (var restNode in shRests)
            {
                List<StoreHouseRest> shRestNodes = await _context.StoreHouseRests
                .Where(x => x.storehouse_id == shId && x.subtree_id == restNode.subtree_id)
                .OrderBy(x => x.pos)
                .OrderBy(x => x.level)
                .AsNoTracking()
                .ToListAsync();

                if (restNode.count <= itemCount)
                {
                    itemCount -= Convert.ToInt32(restNode.count);
                    shRestNodes.ForEach(x => x.reservation_doc_id = docId);
                }
                else
                {
                    var clonedRestNodes = shRestNodes.CloneObject();
                    if (clonedRestNodes.Count >= 1)
                    {
                        int maxPos = await _context.StoreHouseRests.Where(x => x.level == 1).MaxAsync(x => x.pos);
                        StoreHouseRest oldSHRest = clonedRestNodes.Find(x => x.parent_id == Guid.Empty);
                        StoreHouseRest parentRestNode = oldSHRest.CloneObject();
                        Guid tempParentId = parentRestNode.id;
                        Guid newSubtreeId = Guid.NewGuid();
                        parentRestNode.id = Guid.NewGuid();
                        parentRestNode.subtree_id = newSubtreeId;
                        parentRestNode.count = itemCount;
                        parentRestNode.reservation_doc_id = docId;
                        parentRestNode.pos = maxPos + 1;

                        _newSHRests.Add(parentRestNode);
                        AddNestedChild(clonedRestNodes.ToList(), tempParentId, parentRestNode, docId, newSubtreeId, itemCount);
                        _context.StoreHouseRests.AddRange(_newSHRests);
                        await _context.SaveChangesAsync();
                        _newSHRests.Clear();
                    }
                    shRestNodes.ForEach(x => x.count -= itemCount);
                    itemCount = 0;
                }
                _context.StoreHouseRests.UpdateRange(shRestNodes);
                if (itemCount <= 0)
                    return itemCount;
            }

            return itemCount;
        }

        private void AddNestedChild(List<StoreHouseRest> clonedRestNodes, Guid tempParentId, StoreHouseRest newParent, Guid docId, Guid subtreeId, int itemCount)
        {
            List<StoreHouseRest> shRestShildren = clonedRestNodes.Where(x => x.parent_id == tempParentId).ToList();
            if (shRestShildren.Count >= 1)
            {
                foreach (var item in shRestShildren)
                {
                    tempParentId = item.id;
                    StoreHouseRest storeHouseRest = item.CloneObject();
                    storeHouseRest.id = Guid.NewGuid();
                    storeHouseRest.Parent = newParent;
                    storeHouseRest.parent_id = newParent.id;
                    storeHouseRest.subtree_id = subtreeId;
                    storeHouseRest.count = itemCount;
                    storeHouseRest.reservation_doc_id = docId;
                    _newSHRests.Add(storeHouseRest);
                    AddNestedChild(clonedRestNodes, tempParentId, storeHouseRest, docId, subtreeId, itemCount);
                }
            }
        }

        private void AddStoreHouseMovement(Guid productId, double totalCount, Guid docId, Guid shId)
        {
            StoreHouseMovement shMovement = new StoreHouseMovement
            {
                id = Guid.NewGuid(),
                document_id = docId,
                count = totalCount,
                add_date = _nowDate,
                product_id = productId,
                op_type = MovementType.Reservation,
                op_dir = MovementDirection.Out,
                storehouse_from_id = shId,
                storehouse_to_id = shId
            };
            _context.StoreHouseMovements.Add(shMovement);
        }
    }
}