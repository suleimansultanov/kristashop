﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KristaShop.WebAPI.ViewModels {
    public class RegistrationViewModel {
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "ФИО")]
        [RegularExpression("^[а-яА-Я ]+$", ErrorMessage = "{0} должно быть на кириллице.")]
        public string Person { get; set; }

        [Display(Name = "Город")]
        //[RequiredThisOrOther(nameof(NewCity), ErrorMessage = "Заполните поле {0} или {1}")]
        public Guid? CityId { get; set; }

        [Display(Name = "Название города")]
        public string NewCity { get; set; }

        [Display(Name = "Название торгового центра")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string MallAddress { get; set; }

        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Phone { get; set; }

        [Display(Name = "Электронная почта")]
        [EmailAddress(ErrorMessage = "Введена некорректная почта")]
        public string Email { get; set; }

        [Display(Name = "Адрес")]
        public string CompanyAddress { get; set; }
    }
}