﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KristaShop.WebAPI.ViewModels {
    public class NewPasswordViewModel {
        [Display(Name = "Пользователь")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public Guid UserId { get; set; }

        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Password { get; set; }
    }
}
