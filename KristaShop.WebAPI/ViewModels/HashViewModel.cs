﻿using System.ComponentModel.DataAnnotations;

namespace KristaShop.WebAPI.ViewModels {
    public class HashViewModel<T> where T: class {
        public T Object { get; set; }
        [Required]
        public string Hash { get; set; }
    }

    public class HashViewModel2{
        [Required]
        public RegistrationViewModel Object { get; set; }
        [Required]
        public string Hash { get; set; }
    }
}
