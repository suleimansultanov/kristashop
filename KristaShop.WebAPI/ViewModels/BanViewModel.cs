﻿using System;

namespace KristaShop.WebAPI.ViewModels {
    public class BanViewModel {
        public Guid UserId { get; set; }
        public string Reason { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}
