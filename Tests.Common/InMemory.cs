﻿using System;
using KristaShop.DataReadOnly.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Tests.Common {
    public static class InMemory {
        public static KristaAsupDbContext InitializeAsupDbDev() {
            var options = new DbContextOptionsBuilder<KristaAsupDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString("N"))
                .ConfigureWarnings(x=>x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            return new KristaAsupDbContext(options);
        }
    }
}
