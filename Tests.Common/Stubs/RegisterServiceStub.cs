﻿using System.Threading.Tasks;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.Interfaces.Services;

namespace Tests.Common.Stubs {
    public class RegisterServiceStub : IRegisterService {
        public async Task<bool> RegisterAsync(CounterpartyDTO newCounterparty) {
            return true;
        }

        public async Task<bool> IsUserRegisteredAsync(string hash) {
            return true;
        }

        public async Task<bool> AddRegistrationHashAsync(string hash) {
            return true;
        }
    }
}
