﻿using System.Threading.Tasks;
using KristaShop.Common.Interfaces;
using KristaShop.Common.Models;

namespace Tests.Common.Stubs {
    public class EmailServiceStub : IEmailService {
        public async Task SendEmailAsync(EmailMessage message) { }
    }
}
