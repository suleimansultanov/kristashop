﻿using System;
using KristaShop.Business.Interfaces.DataProviders;

namespace KristaShop.Business.DTOs {
    /// <summary>
    /// This DTO uses <see cref="Mappings.VideoGalleryMappingProfile"/> mapping profile
    /// </summary>
    public class VideoGalleryDTO {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IFileDataProvider Preview { get; set; }
        public string PreviewPath { get; set; }
        public string VideoPath { get; set; }
        public bool IsOpen { get; set; }
        public bool IsVisible { get; set; }
        public string Slug { get; set; }
        public int Order { get; set; }
        public int VideosCount { get; set; }
    }
}
