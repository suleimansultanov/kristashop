﻿using System;

namespace KristaShop.Business.DTOs
{
    public class PhotoDTO
    {
        public Guid Id { get; set; }
        public string PhotoPath { get; set; }
        public string OldPhotoPath { get; set; }
        public Guid? ColorId { get; set; }
        public string ColorName { get; set; }
        public int Order { get; set; }
        public Guid NomId { get; set; }
    }
}