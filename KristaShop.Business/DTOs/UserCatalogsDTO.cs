﻿using System;

namespace KristaShop.Business.DTOs {
    public class UserCatalogsDTO {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsBlockedForUser { get; set; }
    }
}
