﻿namespace KristaShop.Business.DTOs {
    public class CartTotalsDTO {
        public int Amount { get; set; }
        public double TotalAmount { get; set; }
        public double Discount { get; set; }
        public double DiscountRu { get; set; }
        public double TotalPrice { get; set; }
        public double TotalPriceRu { get; set; }
    }
}
