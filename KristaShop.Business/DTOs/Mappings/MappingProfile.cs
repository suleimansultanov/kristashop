﻿using System;
using System.Linq;
using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.DataAccess.Entities;
using Newtonsoft.Json;

namespace KristaShop.Business.DTOs.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile() {
            CreateMap<AuthorizationLink, AuthorizationLinkDTO>();
            
            CreateMap<UserData, UserDataDTO>();
            CreateMap<UserDataDTO, UserData>();

            CreateMap<MenuItem, MenuItemDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.MenuType, dest => dest.MapFrom(item => item.menu_type))
                .ForMember(dest => dest.ControllerName, dest => dest.MapFrom(item => item.controller_name))
                .ForMember(dest => dest.ActionName, dest => dest.MapFrom(item => item.action_name))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.title))
                .ForMember(dest => dest.URL, dest => dest.MapFrom(item => item.url))
                .ForMember(dest => dest.Params, dest => dest.MapFrom(item => item.parameters))
                .ForMember(dest => dest.Icon, dest => dest.MapFrom(item => item.icon))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.order));
            CreateMap<MenuItemDTO, MenuItem>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.menu_type, dest => dest.MapFrom(item => item.MenuType))
                .ForMember(dest => dest.controller_name, dest => dest.MapFrom(item => item.ControllerName))
                .ForMember(dest => dest.action_name, dest => dest.MapFrom(item => item.ActionName))
                .ForMember(dest => dest.title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.url, dest => dest.MapFrom(item => item.URL))
                .ForMember(dest => dest.parameters, dest => dest.MapFrom(item => item.Params))
                .ForMember(dest => dest.icon, dest => dest.MapFrom(item => item.Icon))
                .ForMember(dest => dest.order, dest => dest.MapFrom(item => item.Order));

            CreateMap<MenuContent, MenuContentDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.URL, dest => dest.MapFrom(item => item.Url))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.Body, dest => dest.MapFrom(item => item.Body))
                .ForMember(dest => dest.Layout, dest => dest.MapFrom(item => item.Layout))
                .ForMember(dest => dest.IsOpen, dest => dest.MapFrom(item => item.IsOpen))
                .ForMember(dest => dest.MetaTitle, dest => dest.MapFrom(item => item.MetaTitle))
                .ForMember(dest => dest.MetaKeywords, dest => dest.MapFrom(item => item.MetaKeywords))
                .ForMember(dest => dest.MetaDescription, dest => dest.MapFrom(item => item.MetaDescription))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.Order));
            CreateMap<MenuContentDTO, MenuContent>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Url, dest => dest.MapFrom(item => item.URL))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.Body, dest => dest.MapFrom(item => item.Body))
                .ForMember(dest => dest.Layout, dest => dest.MapFrom(item => item.Layout))
                .ForMember(dest => dest.IsOpen, dest => dest.MapFrom(item => item.IsOpen))
                .ForMember(dest => dest.MetaTitle, dest => dest.MapFrom(item => item.MetaTitle))
                .ForMember(dest => dest.MetaKeywords, dest => dest.MapFrom(item => item.MetaKeywords))
                .ForMember(dest => dest.MetaDescription, dest => dest.MapFrom(item => item.MetaDescription))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.Order));

            CreateMap<Settings, SettingsDTO>();
            CreateMap<SettingsDTO, Settings>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => Guid.NewGuid()));

            CreateMap<UrlAccess, UrlAccessDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.URL, dest => dest.MapFrom(item => item.url))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.Acl, dest => dest.MapFrom(item => item.acl))
                .ForMember(dest => dest.AccessGroupsJson, dest => dest.MapFrom(item => JsonConvert.DeserializeObject(item.access_groups_json)))
                .ForMember(dest => dest.DeniedGroupsJson, dest => dest.MapFrom(item => JsonConvert.DeserializeObject(item.denied_groups_json)));
            CreateMap<UrlAccessDTO, UrlAccess>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.url, dest => dest.MapFrom(item => item.URL))
                .ForMember(dest => dest.description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.acl, dest => dest.MapFrom(item => item.Acl))
                .ForMember(dest => dest.access_groups_json, dest => dest.MapFrom(item => JsonConvert.SerializeObject(item.AccessGroupsJson)))
                .ForMember(dest => dest.denied_groups_json, dest => dest.MapFrom(item => JsonConvert.SerializeObject(item.DeniedGroupsJson)));

            CreateMap<Category, CategoryDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Name, dest => dest.MapFrom(item => item.name))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.is_visible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.image_path))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.order));
            CreateMap<CategoryDTO, Category>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.name, dest => dest.MapFrom(item => item.Name))
                .ForMember(dest => dest.is_visible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.image_path, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.order, dest => dest.MapFrom(item => item.Order));

            CreateMap<Catalog, CatalogDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Name, dest => dest.MapFrom(item => item.name))
                .ForMember(dest => dest.Uri, dest => dest.MapFrom(item => item.uri))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.OrderForm, dest => dest.MapFrom(item => item.order_form))
                .ForMember(dest => dest.OrderFormName, dest => dest.MapFrom(item => EnumExtensions<OrderFormType>.GetDisplayValue((OrderFormType)item.order_form)))
                .ForMember(dest => dest.MetaTitle, dest => dest.MapFrom(item => item.meta_title))
                .ForMember(dest => dest.MetaKeywords, dest => dest.MapFrom(item => item.meta_keywords))
                .ForMember(dest => dest.MetaDescription, dest => dest.MapFrom(item => item.meta_description))
                .ForMember(dest => dest.AdditionalDescription, dest => dest.MapFrom(item => item.additional_description))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.order))
                .ForMember(dest => dest.IsDisableDiscount, dest => dest.MapFrom(item => item.is_disable_discount))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.is_visible))
                .ForMember(dest => dest.IsOpen, dest => dest.MapFrom(item => item.is_open))
                .ForMember(dest => dest.IsSet, dest => dest.MapFrom(item => item.is_set))
                .ForMember(dest => dest.PreviewPath, dest => dest.MapFrom(item => item.preview_path))
                .ForMember(dest => dest.VideoPath, dest => dest.MapFrom(item => item.video_path))
                .ForMember(dest => dest.CloseTime, dest => dest.MapFrom(item => item.close_time));

            CreateMap<CatalogDTO, Catalog>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.name, dest => dest.MapFrom(item => item.Name))
                .ForMember(dest => dest.uri, dest => dest.MapFrom(item => item.Uri))
                .ForMember(dest => dest.description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.order_form, dest => dest.MapFrom(item => item.OrderForm))
                .ForMember(dest => dest.meta_title, dest => dest.MapFrom(item => item.MetaTitle))
                .ForMember(dest => dest.meta_keywords, dest => dest.MapFrom(item => item.MetaKeywords))
                .ForMember(dest => dest.meta_description, dest => dest.MapFrom(item => item.MetaDescription))
                .ForMember(dest => dest.additional_description, dest => dest.MapFrom(item => item.AdditionalDescription))
                .ForMember(dest => dest.order, dest => dest.MapFrom(item => item.Order))
                .ForMember(dest => dest.is_disable_discount, dest => dest.MapFrom(item => item.IsDisableDiscount))
                .ForMember(dest => dest.is_visible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.is_open, dest => dest.MapFrom(item => item.IsOpen))
                .ForMember(dest => dest.is_set, dest => dest.MapFrom(item => item.IsSet))
                .ForMember(dest => dest.preview_path, dest => dest.MapFrom(item => item.PreviewPath))
                .ForMember(dest => dest.video_path, dest => dest.MapFrom(item => item.VideoPath))
                .ForMember(dest => dest.close_time, dest => dest.MapFrom(item => item.CloseTime));

            CreateMap<Feedback, FeedbackDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Person, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.Phone, dest => dest.MapFrom(item => item.Phone))
                .ForMember(dest => dest.Message, dest => dest.MapFrom(item => item.Message))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Email))
                .ForMember(dest => dest.Viewed, dest => dest.MapFrom(item => item.Viewed))
                .ForMember(dest => dest.RecordTimeStamp, dest => dest.MapFrom(item => item.RecordTimeStamp))
                .ForMember(dest => dest.FormattedDate, dest => dest.MapFrom(item => item.RecordTimeStamp.ToSystemString()));
            CreateMap<FeedbackDTO, Feedback>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => Guid.NewGuid()))
                .ForMember(dest => dest.Person, dest => dest.MapFrom(item => item.Person))
                .ForMember(dest => dest.Phone, dest => dest.MapFrom(item => item.Phone))
                .ForMember(dest => dest.Message, dest => dest.MapFrom(item => item.Message))
                .ForMember(dest => dest.Email, dest => dest.MapFrom(item => item.Email))
                .ForMember(dest => dest.Viewed, dest => dest.MapFrom(item => false))
                .ForMember(dest => dest.RecordTimeStamp, dest => dest.MapFrom(item => DateTime.Now));
            CreateMap<FeedbackCreateFileDTO, FeedbackFile>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => Guid.NewGuid()))
                .ForMember(dest => dest.Filename, dest => dest.MapFrom(item => item.OriginalName));

            CreateMap<FeedbackFile, FeedbackFileDTO>();
            CreateMap<FeedbackFileDTO, FeedbackFile>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => Guid.NewGuid()));

            CreateMap<NomenclatureModel, NomModelDTO>()
                .ForMember(dest => dest.NomId, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Articul, dest => dest.MapFrom(item => item.articul))
                .ForMember(dest => dest.ItemName, dest => dest.MapFrom(item => item.name))
                .ForMember(dest => dest.DefaultPrice, dest => dest.MapFrom(item => item.default_price))
                .ForMember(dest => dest.VideoUrl, dest => dest.MapFrom(item => item.youtube_link))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.image_path))
                .ForMember(dest => dest.MetaTitle, dest => dest.MapFrom(item => item.meta_title))
                .ForMember(dest => dest.LinkName, dest => dest.MapFrom(item => item.link_name))
                .ForMember(dest => dest.MetaKeywords, dest => dest.MapFrom(item => item.meta_keywords))
                .ForMember(dest => dest.MetaDescription, dest => dest.MapFrom(item => item.meta_description))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.is_visible))
                .ForMember(dest => dest.IsSet, dest => dest.MapFrom(item => item.is_set))
                .ForMember(dest => dest.PartsCount, dest => dest.MapFrom(item => item.parts_count))
                .ForMember(dest => dest.CreatedDate, dest => dest.MapFrom(item => item.created_date))
                .ForMember(dest => dest.ImageAlternativeText, dest => dest.MapFrom(item => item.image_alternative_text));
            CreateMap<NomModelDTO, NomenclatureModel>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.NomId))
                .ForMember(dest => dest.articul, dest => dest.MapFrom(item => item.Articul))
                .ForMember(dest => dest.name, dest => dest.MapFrom(item => item.ItemName))
                .ForMember(dest => dest.default_price, dest => dest.MapFrom(item => item.DefaultPrice))
                .ForMember(dest => dest.youtube_link, dest => dest.MapFrom(item => item.VideoUrl))
                .ForMember(dest => dest.description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.image_path, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.meta_title, dest => dest.MapFrom(item => item.MetaTitle))
                .ForMember(dest => dest.link_name, dest => dest.MapFrom(item => item.LinkName))
                .ForMember(dest => dest.meta_keywords, dest => dest.MapFrom(item => item.MetaKeywords))
                .ForMember(dest => dest.meta_description, dest => dest.MapFrom(item => item.MetaDescription))
                .ForMember(dest => dest.is_visible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.is_set, dest => dest.MapFrom(item => item.IsSet))
                .ForMember(dest => dest.parts_count, dest => dest.MapFrom(item => item.PartsCount))
                .ForMember(dest => dest.created_date, dest => dest.MapFrom(item => item.CreatedDate))
                .ForMember(dest => dest.image_alternative_text, dest => dest.MapFrom(item => item.ImageAlternativeText));

            CreateMap<BlogItem, BlogItemDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.Link, dest => dest.MapFrom(item => item.Link))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.Order))
                .ForMember(dest => dest.LinkText, dest => dest.MapFrom(item => item.LinkText));
            CreateMap<BlogItemDTO, BlogItem>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.Link, dest => dest.MapFrom(item => item.Link))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.LinkText, dest => dest.MapFrom(item => item.LinkText));

            CreateMap<BannerItem, BannerItemDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.title))
                .ForMember(dest => dest.Caption, dest => dest.MapFrom(item => item.caption))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.Link, dest => dest.MapFrom(item => item.link))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.is_visible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.image_path))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.order))
                .ForMember(dest => dest.TitleColor, dest => dest.MapFrom(item => item.title_color));
            CreateMap<BannerItemDTO, BannerItem>()
                .ForMember(dest => dest.id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.caption, dest => dest.MapFrom(item => item.Caption))
                .ForMember(dest => dest.description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.link, dest => dest.MapFrom(item => item.Link))
                .ForMember(dest => dest.is_visible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.image_path, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.order, dest => dest.MapFrom(item => item.Order))
                .ForMember(dest => dest.title_color, dest => dest.MapFrom(item => item.TitleColor));

            CreateMap<GalleryItem, GalleryItemDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.Link, dest => dest.MapFrom(item => item.Link))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.Order));
            CreateMap<GalleryItemDTO, GalleryItem>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.Description))
                .ForMember(dest => dest.Link, dest => dest.MapFrom(item => item.Link))
                .ForMember(dest => dest.IsVisible, dest => dest.MapFrom(item => item.IsVisible))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.ImagePath))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.Order));

            CreateMap<NomPhoto, PhotoDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Order, dest => dest.MapFrom(item => item.order))
                .ForMember(dest => dest.ColorId, dest => dest.MapFrom(item => item.color_id))
                .ForMember(dest => dest.PhotoPath, dest => dest.MapFrom(item => item.photo_path))
                .ForMember(dest => dest.OldPhotoPath, dest => dest.MapFrom(item => item.old_photo_path));

            CreateMap<NomenclatureModel, NomModelDetailDTO>()
                .ForMember(dest => dest.IsSet, dest => dest.MapFrom(item => item.is_set))
                .ForMember(dest => dest.NomId, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Catalogs, dest => dest.MapFrom(item => item.NomCatalogs.Select(x => x.Catalog)))
                .ForMember(dest => dest.Categories, dest => dest.MapFrom(item => item.NomCategories.Select(x => x.Category)))
                .ForMember(dest => dest.Photos, dest => dest.MapFrom(item => item.NomPhotos))
                .ForMember(dest => dest.Articul, dest => dest.MapFrom(item => item.articul))
                .ForMember(dest => dest.DefaultPrice, dest => dest.MapFrom(item => item.default_price))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.ItemName, dest => dest.MapFrom(item => item.name))
                .ForMember(dest => dest.LinkName, dest => dest.MapFrom(item => item.link_name))
                .ForMember(dest => dest.PartsCount, dest => dest.MapFrom(item => item.parts_count))
                .ForMember(dest => dest.VideoUrl, dest => dest.MapFrom(item => item.youtube_link))
                .ForMember(dest => dest.ImageAlternativeText, dest => dest.MapFrom(item => item.image_alternative_text));

            CreateMap<NomenclatureModel, RecommendedNomenclatureDto>()
                .ForMember(dest => dest.NomId, dest => dest.MapFrom(item => item.id))
                .ForMember(dest => dest.Articul, dest => dest.MapFrom(item => item.articul))
                .ForMember(dest => dest.ItemName, dest => dest.MapFrom(item => item.name))
                .ForMember(dest => dest.ImageAlternativeText, dest => dest.MapFrom(item => item.image_alternative_text))
                .ForMember(dest => dest.ImagePath, dest => dest.MapFrom(item => item.image_path))
                .ForMember(dest => dest.Description, dest => dest.MapFrom(item => item.description))
                .ForMember(dest => dest.CatalogUri, dest => dest.MapFrom(item => item.NomCatalogs.FirstOrDefault().Catalog.uri));

            CreateMap<CartItemDTO, CartItem>()
                .ForMember(dest => dest.nom_id, dest => dest.MapFrom(item => item.NomId))
                .ForMember(dest => dest.product_id, dest => dest.MapFrom(item => item.ProductId))
                .ForMember(dest => dest.price, dest => dest.MapFrom(item => item.Price))
                .ForMember(dest => dest.discount, dest => dest.MapFrom(item => item.Discount))
                .ForMember(dest => dest.catalog_id, dest => dest.MapFrom(item => item.CatalogId))
                .ForMember(dest => dest.amount, dest => dest.MapFrom(item => item.Amount));

            CreateMap<Faq, FaqDTO>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.FaqSections, dest => dest.MapFrom(item => item.FaqSections));

            CreateMap<FaqSection, FaqSectionDto>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Title, dest => dest.MapFrom(item => item.Title))
                .ForMember(dest => dest.FaqId, dest => dest.MapFrom(item => item.FaqId))
                .ForMember(dest => dest.IconUrl, dest => dest.MapFrom(item => item.IconUrl))
                .ForMember(dest => dest.FaqSectionContents, dest => dest.MapFrom(item => item.FaqSectionContents));

            CreateMap<FaqSectionContent, FaqSectionContentDto>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.Content, dest => dest.MapFrom(item => item.Content))
                .ForMember(dest => dest.FaqSectionId, dest => dest.MapFrom(item => item.FaqSectionId))
                .ForMember(dest => dest.FaqSectionContentFileDtos, dest => dest.MapFrom(item => item.FaqSectionContentFiles));

            CreateMap<FaqSectionContentFile, FaqSectionContentFileDto>()
                .ForMember(dest => dest.Id, dest => dest.MapFrom(item => item.Id))
                .ForMember(dest => dest.FileUrl, dest => dest.MapFrom(item => item.FileUrl))
                .ForMember(dest => dest.Type, dest => dest.MapFrom(item => item.Type));
        }
    }
}