﻿using System.Linq;
using AutoMapper;
using KristaShop.DataAccess.Entities;

namespace KristaShop.Business.DTOs.Mappings {
    public class DiscountsMappingProfile : Profile {
        public DiscountsMappingProfile() {
            CreateMap<CatalogDiscount, DiscountDTO>()
                .ForMember(dest => dest.ParentId, dest => dest.MapFrom(item => item.CatalogId));
            CreateMap<DiscountDTO, CatalogDiscount>()
                .ForMember(dest => dest.CatalogId, dest => dest.MapFrom(item => item.ParentId));

            CreateMap<NomDiscount, DiscountDTO>()
                .ForMember(dest => dest.ParentId, dest => dest.MapFrom(item => item.NomenclatureId))
                .ForMember(dest => dest.CatalogIds, dest => dest.MapFrom(item => item.DiscountCatalogs.Select(x => x.CatalogId)));
            CreateMap<DiscountDTO, NomDiscount>()
                .ForMember(dest => dest.NomenclatureId, dest => dest.MapFrom(item => item.ParentId));

            CreateMap<UserDiscount, DiscountDTO>()
                .ForMember(dest => dest.ParentId, dest => dest.MapFrom(item => item.UserId));
            CreateMap<DiscountDTO, UserDiscount>()
                .ForMember(dest => dest.UserId, dest => dest.MapFrom(item => item.ParentId));
        }
    }
}