﻿using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace KristaShop.Business.DTOs
{
    public class NomSearchDTO
    {
        public List<CategoryDTO> Categories { get; set; }
        public List<ColorDTO> Colors { get; set; }
        public List<SizeDTO> Sizes { get; set; }
        public List<SizeLineDTO> SizeLines { get; set; }
    }
}
