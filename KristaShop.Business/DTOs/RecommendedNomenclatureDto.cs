﻿using System;

namespace KristaShop.Business.DTOs {
    public class RecommendedNomenclatureDto {
        public Guid NomId { get; set; }
        public string Articul { get; set; }
        public string ItemName { get; set; }
        public string ImageAlternativeText { get; set; }
        public string CatalogUri { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }

        public string ImageAltText => string.IsNullOrEmpty(ImageAlternativeText) ? $"{Articul} {ItemName}".Trim() : ImageAlternativeText;
    }
}
