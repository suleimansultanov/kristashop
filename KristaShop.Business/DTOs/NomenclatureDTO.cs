﻿using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using Org.BouncyCastle.Asn1;

namespace KristaShop.Business.DTOs
{
    public class NomModelDTO {
        public Guid NomId { get; set; }
        public string Articul { get; set; }
        public string ItemName { get; set; }
        public DateTime CreatedDate { get; set; }
        public double DefaultPrice { get; set; }
        public double DefaultPriceRu { get; set; }
        public double? NomenclatureDiscount { get; set; }
        public double ExchangeRate { get; set; }
        public string VideoUrl { get; set; }
        public string ImagePath { get; set; }
        public string ImageAlternativeText { get; set; }
        public string Description { get; set; }
        public List<Guid> Catalogs { get; set; }
        public List<Guid> Categories { get; set; }
        public List<Guid> Clients { get; set; }
        public List<string> PhotoPaths { get; set; } = new List<string>();
        public string MetaTitle { get; set; }
        public string LinkName { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public bool IsVisible { get; set; }
        public bool IsSet { get; set; }
        public double PartsCount { get; set; }

        public string ImageAltText => string.IsNullOrEmpty(ImageAlternativeText) ? $"{Articul} {ItemName}".Trim() : ImageAlternativeText;

        public double GetDefaultPriceWithDiscount(DiscountDTO discount) {
            if (discount.Type == DiscountType.User) {
                return DefaultPrice + discount.DiscountPrice;
            }

            if (NomenclatureDiscount != null) {
                return DefaultPrice + (double) NomenclatureDiscount;
            }

            if (discount.Type == DiscountType.Catalog) {
                return DefaultPrice + discount.DiscountPrice;
            }

            return DefaultPrice;
        }

        public double Exchange(double discount) {
            return discount * ExchangeRate;
        }
    }

    public class NomModelDetailDTO
    {
        public bool IsSet { get; set; }
        public Guid NomId { get; set; }
        public string Articul { get; set; }
        public string ItemName { get; set; }
        public string ImageAlternativeText { get; set; }
        public double DefaultPrice { get; set; }
        public double DefaultPriceRu { get; set; }
        public string VideoUrl { get; set; }
        public string Description { get; set; }
        public List<SizeDTO> Sizes { get; set; }
        public List<ColorDTO> Colors { get; set; }
        public List<CatalogDTO> Catalogs { get; set; }
        public List<CategoryDTO> Categories { get; set; }
        public List<PhotoDTO> Photos { get; set; }
        public string MetaTitle { get; set; }
        public string LinkName { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public Guid CatalogId { get; set; }
        public string CatalogUri { get; set; }
        public bool IsFavorite { get; set; }
        public double PartsCount { get; set; }
        public Guid SelectedProductId { get; set; }

        public string ImageAltText => string.IsNullOrEmpty(ImageAlternativeText) ? $"{Articul} {ItemName}".Trim() : ImageAlternativeText;
    }

    public class NomUserDTO
    {
        public Guid UserId { get; set; }
        public string ClientFullName { get; set; }
        public string CityName { get; set; }
        public string ClientLogin { get; set; }
        public string MallAddress { get; set; }
        public bool NotVisible { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }

    public class NomFilterDTO
    {
        public NomFilterDTO()
        {
            CategoryIds = new List<Guid>();
            ColorIds = new List<Guid>();
            SizeIds = new List<Guid>();
            SizeLineIds = new List<Guid>();
            OrderColumn = "";
        }

        public Guid? CatalogId { get; set; }
        public string CatalogUri { get; set; }
        public string Name { get; set; }
        public List<Guid> CategoryIds { get; set; }

        public List<Guid> ColorIds { get; set; }
        public List<Guid> SizeIds { get; set; }
        public List<Guid> SizeLineIds { get; set; }
        public double? PriceMin { get; set; }
        public double? PriceMax { get; set; }
        public bool OrderPrice { get; set; }
        public bool OrderNew { get; set; }
        public string OrderColumn { get; set; }
        public bool IsSet { get; set; }
    }

    public class NomFavoriteDTO
    {
        public Guid NomId { get; set; }
        public Guid CatalogId { get; set; }
        public string CatalogUri { get; set; }
        public string ItemName { get; set; }
        public string ImagePath { get; set; }
        public double DefaultPrice { get; set; }
        public double DefaultPriceRu { get; set; }
    }
}