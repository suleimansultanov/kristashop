﻿using System;
using System.Collections.Generic;
using System.Text;
using KristaShop.Common.Enums;

namespace KristaShop.Business.DTOs
{
    public class FaqSectionContentFileDto
    {
        public Guid Id { get; set; }
        public string FileUrl { get; set; }
        public FaqFileType Type { get; set; }
    }
}
