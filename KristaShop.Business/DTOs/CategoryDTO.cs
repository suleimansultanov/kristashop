﻿using Microsoft.AspNetCore.Http;
using System;

namespace KristaShop.Business.DTOs
{
    public class CategoryDTO
    {
        public Guid Id { get; set; }
        public IFormFile Image { get; set; }
        public string ImagePath { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public int Order { get; set; }
        public bool IsVisible { get; set; }
        public int NomCount { get; set; }
    }
}