﻿using System;

namespace KristaShop.Business.DTOs {
    public class UserDataDTO {
        public Guid UserId { get; set; }
        public DateTimeOffset LastSignIn { get; set; }
    }
}
