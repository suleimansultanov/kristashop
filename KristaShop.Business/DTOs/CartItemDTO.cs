﻿using System;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.DTOs;

namespace KristaShop.Business.DTOs {
    public class CartItemDTO {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid NomId { get; set; }
        public string Image { get; set; }
        public string Articul { get; set; }
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
        public string ColorImg { get; set; }
        public string Size { get; set; }
        public int Amount { get; set; }
        public double TotalAmount { get; set; }
        public double Price { get; set; }
        public double PriceRu { get; set; }
        public double DisplayPrice { get; set; }
        public double DisplayPriceRu { get; set; }
        public double DisplayDiscount { get; set; }
        public double DisplayDiscountRu { get; set; }
        public double Discount { get; set; }
        public double DiscountRu { get; set; }
        public double TotalPrice { get; set; }
        public double TotalPriceRu { get; set; }
        public bool IsPreorder { get; set; }
        public Guid CatalogId { get; set; }
        public Guid UserId { get; set; }
        public UniversalDocState Status { get; set; }
        public string StatusName { get; set; }
        public double PartsCount { get; set; }
        public string FullName => $"{Articul}, Цвет: {ColorName}, Размер: {Size}";

        #region constructors

        public CartItemDTO() { }

        public CartItemDTO(double price, double discount, double exchangeRate) {
            Price = price;
            PriceRu = price * exchangeRate;
            Discount = discount;
            DiscountRu = discount * exchangeRate;
            if (discount < 0) {
                DisplayPrice = price;
                DisplayPriceRu = price * exchangeRate;
                DisplayDiscount = discount;
                DisplayDiscountRu = discount * exchangeRate;
            } else {
                DisplayPrice = price + discount;
                DisplayPriceRu = (price + discount) * exchangeRate;
                DisplayDiscount = 0;
                DisplayDiscountRu = 0;
            }
        }

        public CartItemDTO(CartItemDTO cartItem, ProductDTO product, double exchangeRate) {
            Id = cartItem.Id;
            ProductId = cartItem.ProductId;
            NomId = cartItem.NomId;
            Image = cartItem.Image;
            Articul = cartItem.Articul;
            Amount = cartItem.Amount;
            TotalAmount = cartItem.TotalAmount;
            CatalogId = cartItem.CatalogId;
            UserId = cartItem.UserId;
            TotalPrice = cartItem.TotalPrice;
            TotalPriceRu = cartItem.TotalPrice * exchangeRate;
            IsPreorder = cartItem.IsPreorder;
            PartsCount = cartItem.PartsCount;
            Status = cartItem.Status;
            StatusName = cartItem.StatusName;
            Price = cartItem.Price;
            PriceRu = cartItem.Price * exchangeRate;
            Discount = cartItem.Discount;
            DiscountRu = cartItem.Price * exchangeRate;
            if (cartItem.Discount < 0) {
                DisplayPrice = cartItem.Price;
                DisplayPriceRu = cartItem.Price * exchangeRate;
                DisplayDiscount = cartItem.Discount;
                DisplayDiscountRu = cartItem.Discount * exchangeRate;
            } else {
                DisplayPrice = cartItem.Price + cartItem.Discount;
                DisplayPriceRu = (cartItem.Price + cartItem.Discount) * exchangeRate;
                DisplayDiscount = 0;
                DisplayDiscountRu = 0;
            }
            Size = product.Size.Name;
            ColorName = product.Color.Name;
            ColorCode = product.Color.Code;
            ColorImg = product.Color.Image;
        }

        public CartItemDTO(OrderItemsDTO orderItem, string image, double exchangeRate, double? percent = null) {
            NomId = orderItem.NomId;
            ProductId = orderItem.ProductId;
            Articul = orderItem.Description;
            Price = orderItem.Price;
            PriceRu = orderItem.Price * exchangeRate;
            Discount = orderItem.Discount;
            DiscountRu = orderItem.Discount * exchangeRate;
            IsPreorder = orderItem.IsPreorder;
            TotalAmount = orderItem.Quantity * orderItem.PartsCount;
            if (percent > 1E-6) {
                TotalPrice = ((orderItem.Price + orderItem.Discount) * orderItem.Quantity - orderItem.PrePay) * percent.Value;
                TotalPriceRu = ((orderItem.Price + orderItem.Discount) * orderItem.Quantity - orderItem.PrePay) * percent.Value * exchangeRate;
            } else {
                TotalPrice = (orderItem.Price + orderItem.Discount) * orderItem.Quantity - orderItem.PrePay;
                TotalPriceRu = ((orderItem.Price + orderItem.Discount) * orderItem.Quantity - orderItem.PrePay) * exchangeRate;
            }
            if (orderItem.Discount < 0) {
                DisplayPrice = orderItem.Price;
                DisplayPriceRu = orderItem.Price * exchangeRate;
                DisplayDiscount = orderItem.Discount;
                DisplayDiscountRu = orderItem.Discount * exchangeRate;
            } else {
                DisplayPrice = orderItem.Price + orderItem.Discount;
                DisplayPriceRu = (orderItem.Price + orderItem.Discount) * exchangeRate;
                DisplayDiscount = 0;
                DisplayDiscountRu = 0;
            }

            Image = image;
            Size = orderItem.SizeName;
            ColorName = orderItem.ColorName;
            ColorCode = orderItem.ColorCode;
            ColorImg = orderItem.ColorImage;
            Status = orderItem.Status;
            StatusName = orderItem.GetStatus();
        }

        #endregion
    }
}