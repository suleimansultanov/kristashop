﻿using System;
using System.Collections.Generic;
using KristaShop.Common.Enums;
using KristaShop.Common.Models;

namespace KristaShop.Business.DTOs {
    /// <summary>
    /// This DTO uses <see cref="Mappings.DiscountsMappingProfile"/>
    /// </summary>
    public class DiscountDTO {
        public Guid Id { get; set; }
        public Guid ParentId { get; set; }
        public DiscountType Type { get; set; }
        public double DiscountPrice { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<Guid> CatalogIds { get; set; }

        public DiscountDTO() { }

        public DiscountDTO(DiscountType type, double discountPrice = 0) {
            Type = type;
            DiscountPrice = discountPrice;
        }
    }
}