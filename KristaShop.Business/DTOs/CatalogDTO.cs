﻿using System;
using System.Collections.Generic;
using KristaShop.Business.Interfaces.DataProviders;

namespace KristaShop.Business.DTOs
{
    public class CatalogDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int OrderForm { get; set; }
        public int NomCount { get; set; }
        public string OrderFormName { get; set; }
        public string Uri { get; set; }
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string AdditionalDescription { get; set; }
        public int Order { get; set; }
        public bool IsDisableDiscount { get; set; }
        public bool IsVisible { get; set; }
        public bool IsOpen { get; set; }
        public bool IsSet { get; set; }
        public double DiscountPrice { get; set; }
        public IFileDataProvider Preview { get; set; }
        public string PreviewPath { get; set; }
        public string VideoPath { get; set; }
        public DateTimeOffset? CloseTime { get; set; }
    }

    public class NomCatalogDTO
    {
        public CatalogDTO Catalog { get; set; }
        public List<NomModelDTO> NomModels { get; set; }
    }
}