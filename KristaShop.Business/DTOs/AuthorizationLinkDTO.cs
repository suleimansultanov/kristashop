﻿using System;
using KristaShop.Common.Enums;

namespace KristaShop.Business.DTOs {
    public class AuthorizationLinkDTO {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public Guid UserId { get; set; }
        public DateTime RecordTimeStamp { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? LoginDate { get; set; }
        public AuthorizationLinkType Type { get; set; }
    }
}
