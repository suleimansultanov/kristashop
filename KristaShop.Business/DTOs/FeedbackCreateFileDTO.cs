﻿using System.IO;
using KristaShop.Business.Interfaces.DataProviders;

namespace KristaShop.Business.DTOs {
    public class FeedbackCreateFileDTO : IFileDataProvider {
        public string OriginalName { get; set; }
        public Stream FileStream { get; set; }
        public string FilesDirectoryPath { get; set; }
        public string Directory { get; set; }
    }
}
