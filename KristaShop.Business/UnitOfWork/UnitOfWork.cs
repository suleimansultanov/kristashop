﻿using System;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataAccess.Domain;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;
using KristaShop.DataAccess.Repositories;
using Serilog;

namespace KristaShop.Business.UnitOfWork {
    public class UnitOfWork : UnitOfWorkBase, IUnitOfWork {
        public UnitOfWork(KristaShopDbContext context, ILogger logger) : base(context, logger) { }

        #region Shop repositories implementation
        
        private IRepository<AuthorizationLink, Guid> _authorizationLinksRepository;
        private IRepository<CatalogDiscount, Guid> _catalogDiscountRepository;
        private IRepository<NomDiscount, Guid> _nomenclatureDiscountRepository;
        private IRepository<NomDiscountCatalog, Guid, Guid> _nomenclatureDiscountCatalogsRepository;
        private IRepository<Video, Guid> _videoRepository;
        private IVideoGalleryRepository<VideoGallery, Guid> _videoGalleryRepository;
        private IFaqRepository<Faq, Guid> _faqRepository;
        private IFaqSectionRepository<FaqSection, Guid> _faqSectionRepository;
        private IFaqSectionContentFileRepository<FaqSectionContentFile, Guid> _faqSectionContentFileRepository;
        private IFaqSectionContentRepository<FaqSectionContent, Guid> _faqSectionContentRepository;
        private IVideoGalleryVideosRepository<VideoGalleryVideos, Guid> _videoGalleryVideosRepository;
        private IMenuContentRepository<MenuContent, Guid> _menuContentRepository;
        private IRepository<UserData, Guid> _userDataRepository;
        private IRepository<UserDiscount, Guid> _userDiscountRepository;

        public IRepository<AuthorizationLink, Guid> AuthorizationLinks {
            get { return _authorizationLinksRepository ??= new Repository<AuthorizationLink, Guid>(Context); }
        }

        public IRepository<CatalogDiscount, Guid> CatalogDiscounts {
            get { return _catalogDiscountRepository ??= new Repository<CatalogDiscount, Guid>(Context); }
        }

        public IRepository<Video, Guid> Videos {
            get { return _videoRepository ??= new Repository<Video, Guid>(Context); }
        }

        public IVideoGalleryRepository<VideoGallery, Guid> VideoGalleries {
            get { return _videoGalleryRepository ??= new VideoGalleryRepository(Context); }
        }

        public IVideoGalleryVideosRepository<VideoGalleryVideos, Guid> VideoGalleryVideos {
            get { return _videoGalleryVideosRepository ??= new VideoGalleryVideosRepository(Context); }
        }

        public IMenuContentRepository<MenuContent, Guid> MenuContent {
            get { return _menuContentRepository ??= new MenuContentRepository(Context); }
        }

        public IRepository<NomDiscount, Guid> NomDiscounts {
            get { return _nomenclatureDiscountRepository ??= new Repository<NomDiscount, Guid>(Context); }
        }

        public IRepository<NomDiscountCatalog, Guid, Guid> NomDiscountCatalogs {
            get { return _nomenclatureDiscountCatalogsRepository ??= new Repository<NomDiscountCatalog, Guid, Guid>(Context); }
        }

        public IRepository<UserData, Guid> UserData {
            get { return _userDataRepository ??= new Repository<UserData, Guid>(Context); }
        }

        public IRepository<UserDiscount, Guid> UserDiscounts {
            get { return _userDiscountRepository ??= new Repository<UserDiscount, Guid>(Context); }
        }

        public IFaqRepository<Faq, Guid> FaqRepository {
            get { return _faqRepository ??= new FaqRepository(Context); }
        }

        public IFaqSectionRepository<FaqSection, Guid> FaqSectionRepository {
            get { return _faqSectionRepository ??= new FaqSectionRepository(Context); }
        }

        public IFaqSectionContentRepository<FaqSectionContent, Guid> FaqSectionContentRepository {
            get { return _faqSectionContentRepository ??= new FaqSectionContentRepository(Context); }
        }

        public IFaqSectionContentFileRepository<FaqSectionContentFile, Guid> FaqSectionContentFileRepository {
            get { return _faqSectionContentFileRepository ??= new FaqSectionContentFileRepository(Context); }
        }

        #endregion
    }
}