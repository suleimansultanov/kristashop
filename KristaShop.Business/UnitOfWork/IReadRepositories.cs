﻿using System;
using KristaShop.DataReadOnly.Interfaces.Repositories;
using KristaShop.DataReadOnly.Models;

namespace KristaShop.Business.UnitOfWork {
    public interface IReadRepositories {
        IDocRegistryRepository<DocRegistry, Guid> Documents { get; }
    }
}
