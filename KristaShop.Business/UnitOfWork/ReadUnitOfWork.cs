﻿using System;
using System.Threading.Tasks;
using KristaShop.Common.Implementation.DataAccess;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.Interfaces.Repositories;
using KristaShop.DataReadOnly.Models;
using KristaShop.DataReadOnly.Repositories;
using Serilog;

namespace KristaShop.Business.UnitOfWork {
    public class ReadUnitOfWork : UnitOfWorkBase, IReadUnitOfWork {
        public ReadUnitOfWork(KristaReplicaDbContext context, ILogger logger) : base(context, logger) { }
        
        private IDocRegistryRepository<DocRegistry, Guid> _documentsRepository;

        public IDocRegistryRepository<DocRegistry, Guid> Documents {
            get { return _documentsRepository ??= new DocRegistryRepository(Context); }
        }

        public override bool Save() {
            throw new MethodAccessException("Read only unit work do not support update operations");
        }

        public override Task<bool> SaveAsync() {
            throw new MethodAccessException("Read only unit work do not support update operations");
        }

        public override Task<bool> SaveInTransactionAsync() {
            throw new MethodAccessException("Read only unit work do not support update operations");
        }
    }
}
