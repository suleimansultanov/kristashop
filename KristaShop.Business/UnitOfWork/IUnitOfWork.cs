﻿using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.Business.UnitOfWork {
    public interface IUnitOfWork : IUnitOfWorkBase, IShopRepositories { }
}