﻿using System;
using KristaShop.Common.Interfaces.DataAccess;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces.Repositories;

namespace KristaShop.Business.UnitOfWork {
    public interface IShopRepositories {
        IRepository<AuthorizationLink, Guid> AuthorizationLinks { get; }
        IRepository<CatalogDiscount, Guid> CatalogDiscounts { get; }
        IRepository<Video, Guid> Videos { get; }
        IVideoGalleryRepository<VideoGallery, Guid> VideoGalleries { get; }
        IVideoGalleryVideosRepository<VideoGalleryVideos, Guid> VideoGalleryVideos { get; }
        IMenuContentRepository<MenuContent, Guid> MenuContent { get; }
        IRepository<NomDiscount, Guid> NomDiscounts { get; }
        IRepository<NomDiscountCatalog, Guid, Guid> NomDiscountCatalogs { get; }
        IFaqRepository<Faq, Guid> FaqRepository { get; }
        IFaqSectionRepository<FaqSection, Guid> FaqSectionRepository { get; }
        IFaqSectionContentRepository<FaqSectionContent, Guid> FaqSectionContentRepository { get; }
        IFaqSectionContentFileRepository<FaqSectionContentFile, Guid> FaqSectionContentFileRepository { get; }
        IRepository<UserData, Guid> UserData { get; }
        IRepository<UserDiscount, Guid> UserDiscounts { get; }
    }
}
