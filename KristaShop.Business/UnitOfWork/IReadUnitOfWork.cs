﻿using KristaShop.Common.Interfaces.DataAccess;

namespace KristaShop.Business.UnitOfWork {
    public interface IReadUnitOfWork : IUnitOfWorkBase, IReadRepositories { }
}
