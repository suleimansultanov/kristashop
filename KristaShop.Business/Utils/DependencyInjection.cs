﻿using KristaShop.Business.Interfaces;
using KristaShop.Business.Services;
using KristaShop.DataAccess.Domain;
using KristaShop.DataAccess.Interfaces;
using KristaShop.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using System;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Business.Services.Core;
using KristaShop.Business.UnitOfWork;
using KristaShop.Common.Models;
using Microsoft.Extensions.Logging;

namespace KristaShop.Business.Utils
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services) {
            services.AddSingleton<IFileService, FileService>();
            services.AddSingleton<IDynamicPagesManager, DynamicPagesManager>();
            services.AddSingleton<ISettingsManager, SettingsManager<AppSettings>>();

            // Core services
            services.AddScoped<IStorehouseService, StorehouseService>();
            services.AddScoped<IBasicUserService, BasicUserService>();
            services.AddScoped<IOrdersService, OrdersService>();

            // Shop services
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IMenuContentService, MenuContentService>();
            services.AddScoped<ILinkService, LinkService>();
            services.AddScoped<ISettingService, SettingService>();
            services.AddScoped<IUrlAclService, UrlAclService>();
            services.AddScoped<IFeedbackService, FeedbackService>();
            services.AddScoped<IDiscountService, DiscountService>();

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICatalogService, CatalogService>();
            services.AddScoped<INomenclatureService, NomenclatureService>();

            services.AddScoped<IBlogService, BlogService>();
            services.AddScoped<IGalleryService, GalleryService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<IBannerService, BannerService>();
            services.AddScoped<IFavoriteService, FavoriteService>();

            services.AddScoped<IMetaHelperService, MetaHelperService>();

            services.AddScoped<IRecommendedService, RecommendedService>();
            services.AddScoped<IVideoService, VideoService>();
            services.AddScoped<IVideoGalleryService, VideoGalleryService>();

            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IFaqService, FaqService>();

            return services;
        }

        public static IServiceCollection AddShopDbContext(this IServiceCollection services, IConfiguration configuration, ILoggerFactory logger)
        {
            services.AddScoped(typeof(IShopRepository<>), typeof(ShopRepository<>));
            services.AddScoped(typeof(ICacheRepository<>), typeof(CacheRepository<>));

            services.AddScoped(typeof(INomRepository<>), typeof(NomRepository<>));

            services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();
            services.AddScoped<IReadUnitOfWork, ReadUnitOfWork>();

            /*Данная конфигурация используется для MYSQL*/
            services.AddDbContextPool<KristaShopDbContext>(options => {
                if (logger != null) {
                    options.UseLoggerFactory(logger);
                }

                options.UseMySql(
                    configuration.GetConnectionString("KristaShopMysql"), mySqlOptions => mySqlOptions
                        .ServerVersion(new ServerVersion(new Version(8, 0, 18), ServerType.MySql)));
            });

            return services;
        }
    }
}