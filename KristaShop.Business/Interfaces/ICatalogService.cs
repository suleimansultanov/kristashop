﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface ICatalogService
    {
        Task<Dictionary<Guid, string>> GetDictionaryUserCatalogs();

        Task<Guid[]> GetNonVisUserCatalogs(Guid userId);

        Task<List<UserCatalogsDTO>> GetAllCatalogsWithNonVisibleCheckAsync(Guid userId);

        Task<OperationResult> AddAllNonVisCatalogUsers(Guid ctlgId, List<Guid> userIds);

        Task<OperationResult> RemoveAllNonVisCatalogUsers(Guid ctlgId);

        Task<OperationResult> AddNonVisUserAllCatalogs(Guid userId);

        Task<OperationResult> AddNonVisUserCatalogs(Guid userId, List<Guid> ctlgIds);

        Task<CatalogDTO> GetCatalogDetailsNoTrack(Guid id);

        Task<NomCatalogDTO> GetCatalogNomenclatures(Guid? userId);

        Task<List<CatalogDTO>> GetCatalogsByNomId(Guid nomId);

        Task<OperationResult> DeleteCatalog(Guid id, string filesDirectoryPath);

        Task<CatalogDTO> GetCatalogDetails(Guid id);

        Task<CatalogDTO> GetCatalogDetails(Guid id, bool excludeClosed, Guid userId);

        Task<CatalogDTO> GetCatalogDetailsByUri(string uri, bool excludeClosed = true, Guid userId = default);

        Task<List<CatalogDTO>> GetCatalogsByUser(Guid? userId);

        Task<List<CatalogDTO>> GetCloseCatalogs();

        Task<List<CatalogDTO>> GetCatalogs(bool includeDiscounts = false);

        Task<OperationResult> InsertCatalog(CatalogDTO dto);

        Task<OperationResult> UpdateCatalog(CatalogDTO dto);
    }
}