﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using P.Pager;
using System;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IFavoriteService
    {
        Task<IPager<NomFavoriteDTO>> GetFavoritesAsync(NomFilterDTO filter, int page, int modelInPage, Guid userId);

        Task<OperationResult> DeleteFavoriteAsync(Guid userId, Guid nomId, Guid catalogId);

        Task<OperationResult> InsertFavoriteAsync(Guid userId, Guid nomId, Guid catalogId);

        Task<OperationResult> AddOrDeleteFavoriteAsync(Guid userId, Guid nomId, Guid catalogId);
    }
}