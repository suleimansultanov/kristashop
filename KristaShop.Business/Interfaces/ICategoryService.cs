﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface ICategoryService
    {
        Task<List<CategoryDTO>> GetCategoriesByNomId(Guid nomId);

        Task<CategoryDTO> GetCategoryDetailsNoTrack(Guid id);

        Task<OperationResult> DeleteCategory(Guid id);

        Task<List<CategoryDTO>> GetCategories();

        Task<CategoryDTO> GetCategoryDetails(Guid id);

        Task<OperationResult> InsertCategory(CategoryDTO dto);

        Task<OperationResult> UpdateCategory(CategoryDTO dto);
    }
}