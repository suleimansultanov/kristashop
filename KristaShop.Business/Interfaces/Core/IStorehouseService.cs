﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Common.Enums;

namespace KristaShop.Business.Interfaces.Core {
    public interface IStorehouseService {
        Task<List<Guid>> GetNomenclatureIdsThatHasStorehouseRestsAsync(List<Guid> nomenclatureIds);
        Task<Dictionary<Guid, string>> GetPackagingListAsync(CurrencyType priceInCurrency = CurrencyType.RUB);
    }
}
