﻿using System;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces.Core {
    public interface IOrdersService {
        Task<int> GetNewOrdersCountAsync(Guid userId, int userAcl);
    }
}
