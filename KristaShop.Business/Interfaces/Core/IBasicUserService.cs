﻿using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Business.DTOs;

namespace KristaShop.Business.Interfaces.Core {
    public interface IBasicUserService {
        Task<bool> IsActiveUserAsync(Guid userId);

        Task<OperationResult> SignIn(string login, string pass, bool isPersistent = false, bool isBackend = true);

        Task<OperationResult> SignInByLink(AuthorizationLinkDTO link);

        Task SignOut(bool isBackend = true);

        Task<bool> ValidatePasswordAsync(Guid userId, string password);

        List<UserClientDTO> GetAllUsers(UserSession user);

        Task<List<UserClientDTO>> GetAllUsersAsync(UserSession user);

        Task<UserClientDTO> GetUserAsync(Guid id);

        Task<UserClientDTO> GetUserAsync(string login);

        Task<CounterpartyDTO> GetCounterpartyDetailsAsync(Guid id);

        Task<int> GetManagerNewUsersCountAsync(Guid managerId, int currentUserAcl);
    }
}