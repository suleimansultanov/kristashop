﻿using System;
using KristaShop.Common.Models;
using System.Threading.Tasks;
using KristaShop.Common.Models.ApiResponses;

namespace KristaShop.Business.Interfaces {
    public interface IAsupApiClient<T> {
        Task<OperationResult> SendDataAsync(T data, string urlPath);

        Task<RegisterResponse> RegisterAsync<TModel>(TModel data);
        Task<BaseResponse> UpdateUserAsync<TModel>(TModel data);
        Task<BaseResponse> ActivateUserAsync(Guid userId);
        Task<BaseResponse> ChangePasswordAsync<TModel>(TModel data);

        Task<byte[]> GetFileDataAsync(string virtualFilePath, string virtualFileName);
    }
}