﻿using KristaShop.Common.Models;
using System;
using System.Threading.Tasks;
using KristaShop.Business.DTOs;
using KristaShop.Common.Enums;

namespace KristaShop.Business.Interfaces {
    public interface ILinkService {
        Task<IResult<AuthorizationLinkDTO>> GetUserIdByRandCodeAsync(string code);
        Task<IResult<string>> InsertLinkAuthAsync(Guid userId, AuthorizationLinkType type = AuthorizationLinkType.MultipleAccess, bool fullPath = true);
        Task<IResult> RemoveLinksByUserIdAsync(Guid userId);
        Task<IResult> RemoveLinkByCodeAsync(string code);
        Task<bool> IsLinkExistAsync(string code);
    }
}