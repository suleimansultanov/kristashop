﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IGalleryService
    {
        Task<List<GalleryItemDTO>> GetGallerysForFront(int quantity);

        Task<List<GalleryItemDTO>> GetTopNRandomItems(int quantity);

        Task<IPager<GalleryItemDTO>> GetPaginationGallerys(int page, int modelInPage);

        Task<List<GalleryItemDTO>> GetGallerys();

        Task<GalleryItemDTO> GetGalleryDetails(Guid id);

        Task<GalleryItemDTO> GetGalleryDetailsNoTrack(Guid id);

        Task<OperationResult> InsertGallery(GalleryItemDTO dto);

        Task<OperationResult> UpdateGallery(GalleryItemDTO dto);

        Task<OperationResult> DeleteGallery(Guid id);
    }
}