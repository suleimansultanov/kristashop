﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface ICartService
    {
        Task<List<Guid>> GetCartUserStatus();

        Task<List<CartItemDTO>> GetCartItemsNavbar(Guid userId);

        Task<List<CartItemDTO>> GetCartItems(Guid userId);

        Task<List<CartItemDTO>> GetAllCartsItemsAsync();

        Task<CartTotalsDTO> GetCartTotalsAsync();

        Task<OperationResult> InsertCartItem(CartItemDTO dto);

        Task<OperationResult> RemoveCartItemById(Guid cartId);

        Task RemoveCartItemsByUserId(Guid userId);
    }
}