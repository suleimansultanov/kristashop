﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IMenuContentService
    {
        Task<List<MenuContentDTO>> GetMenusContentAsync(bool openOnly = false, bool visibleInMenuOnly = false);

        Task<List<MenuContentDTO>> GetMenusContentByControllerAsync(string controller, bool openOnly = false, bool visibleInMenuOnly = false);

        Task<List<MenuContentDTO>> GetMenusContentByUrlsAsync(List<string> urls);

        Task<MenuContentDTO> GetMenuContentByIdAsync(Guid id);

        Task<MenuContentDTO> GetMenuContentByIdNoTrackAsync(Guid id);

        Task<MenuContentDTO> GetMenuContentByUrlAsync(string url);

        Task<OperationResult> InsertMenuContentAsync(MenuContentDTO menuContent);

        Task<OperationResult> UpdateMenuContentAsync(MenuContentDTO menuContent);

        Task<OperationResult> DeleteMenuContentAsync(Guid id, string fileDirectoryPath);

        Task<OperationResult> UpdateMenuContentOrderAsync(Guid id, int newOrder);

        Task<OperationResult> RestoreMenuContentOrderAsync();
    }
}