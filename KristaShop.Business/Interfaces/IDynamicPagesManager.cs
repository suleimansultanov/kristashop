﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Business.DTOs;
using Microsoft.Extensions.DependencyInjection;

namespace KristaShop.Business.Interfaces {
    public interface IDynamicPagesManager {
        Task InitializeAsync(IServiceScope serviceScope);
        Task ReloadAsync();
        Task ReloadAsync(Guid menuId, string oldKey = "");
        bool TryGetValue(string url, out MenuContentDTO value);
        bool TryGetValuesByController(string controller, bool openOnly, out List<MenuContentDTO> values);
        bool TryGetValuesByControllerForMenu(string controller, bool openOnly, out List<MenuContentDTO> values);
    }
}
