﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface ISettingService {
        Task<List<SettingsDTO>> GetSettingsAsync();
        
        Task<List<SettingsDTO>> GetNotRootSettingsAsync();

        Task<SettingsDTO> GetByKeyAsync(string key);

        Task<SettingsDTO> GetByIdAsync(Guid id);

        Task<OperationResult> InsertAsync(SettingsDTO dto);

        Task<OperationResult> UpdateAsync(SettingsDTO dto, bool canChangeKey);

        Task<OperationResult> DeleteAsync(Guid id);
    }
}