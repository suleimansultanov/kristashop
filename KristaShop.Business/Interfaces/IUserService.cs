﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;

namespace KristaShop.Business.Interfaces {
    public interface IUserService : IBasicUserService {
        Task<List<UserClientDTO>> GetUsersWithDataAsync(UserSession user);
    }
}
