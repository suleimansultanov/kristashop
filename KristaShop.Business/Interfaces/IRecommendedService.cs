﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Business.DTOs;

namespace KristaShop.Business.Interfaces {
    public interface IRecommendedService {
        /// <summary>
        /// Get nomenclature that has storehouse rests that in specific categories
        /// </summary>
        /// <param name="categoryIds">list of categories where nomenclature will be searching</param>
        /// <param name="quantity">Quantity of nomenclature items to select</param>
        /// <param name="userId">User id to detect which models are available to show</param>
        /// <returns></returns>
        Task<List<RecommendedNomenclatureDto>> GetNRecommendedNomenclatureItems(List<Guid> categoryIds, int quantity, Guid userId = default);
    }
}
