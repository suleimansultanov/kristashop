﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IBlogService
    {
        Task<List<BlogItemDTO>> GetBlogsForFront();

        Task<IPager<BlogItemDTO>> GetPaginationBlogs(int page, int modelInPage);

        Task<List<BlogItemDTO>> GetBlogs();

        Task<BlogItemDTO> GetBlogDetails(Guid id);

        Task<BlogItemDTO> GetBlogDetailsNoTrack(Guid id);

        Task<OperationResult> InsertBlog(BlogItemDTO dto);

        Task<OperationResult> UpdateBlog(BlogItemDTO dto);

        Task<OperationResult> DeleteBlog(Guid id);
    }
}