﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IBannerService
    {
        Task<OperationResult> DeleteBanner(Guid id);

        Task<BannerItemDTO> GetBannerDetails(Guid id);

        Task<BannerItemDTO> GetBannerDetailsNoTrack(Guid id);

        Task<List<BannerItemDTO>> GetBanners();

        Task<OperationResult> InsertBanner(BannerItemDTO dto);

        Task<OperationResult> UpdateBanner(BannerItemDTO dto);
    }
}