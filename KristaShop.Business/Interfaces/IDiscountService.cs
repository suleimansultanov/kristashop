﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Common.Enums;

namespace KristaShop.Business.Interfaces {
    public interface IDiscountService {
        Task<List<CatalogDTO>> GetCatalogsWithDiscountAsync();
        Task<List<UserClientDTO>> GetUsersWithDiscountAsync(UserSession user);
        Task<List<DiscountDTO>> GetDiscountsAsync(Guid discountId, DiscountType discountType);
        Task<DiscountDTO> GetDiscountDetailsAsync(Guid discountId, DiscountType discountType);
        Task<DiscountDTO> GetDiscountAsync(Guid catalogId, Guid userId, Guid nomId);
        Task<double> GetDiscountValueAsync(Guid catalogId, Guid userId, Guid nomId);
        Task<OperationResult> AddDiscountAsync(DiscountDTO dto);
        Task<OperationResult> UpdateDiscountAsync(DiscountDTO dto);
        Task<OperationResult> RemoveDiscountAsync(Guid id, DiscountType discountType);
    }
}