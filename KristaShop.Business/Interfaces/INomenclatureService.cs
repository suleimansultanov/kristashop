﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using KristaShop.DataReadOnly.DTOs;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface INomenclatureService {
        Task<List<LookUpItem<Guid, string>>> GetNomenclatureCatalogsLookupListAsync(Guid nomenclatureId);

        Task<CartItemDTO> GetPackagingCartItem(Guid nomId);

        Task<List<CModelDTO>> GetNomModelDiscounts();

        List<CModelDTO> GetNomModels();

        Task<List<CartItemDTO>> GetCartOrderItems(List<OrderItemsDTO> orderItems, double rate, double? percent = null);

        Task<IPager<NomModelDTO>> GetNomModelsByFilter(NomFilterDTO filter, int page, int modelInPage, Guid? userId);

        Task<OperationResult> AddNomModel(NomModelDTO dto);

        List<NomUserDTO> GetNomUsers(UserSession user, Guid nomId);

        Task<NomModelDTO> GetNomModel(Guid id);

        Task<NomModelDetailDTO> GetNomModelById(Guid id, string ctlgUri, Guid? userId);

        Task<List<ColorDTO>> GetColorsBySizeAsync(Guid catalogId, Guid nomId, Guid sizeId, bool isInstock);

        List<Dictionary<string, object>> GetProdCtlgs(Guid nomId, Guid ctlgId);

        Task AddVisProdCtlg(Guid nomId, Guid ctlgId, Guid prodId);

        List<Dictionary<string, object>> GetPrices(Guid nomId);

        Task AddProdPrice(Guid nomId, Guid prodId, double price);

        List<Dictionary<string, object>> GetSHAmounts(Guid nomId);

        Task<List<PhotoDTO>> GetNomPhotos(Guid nomId);

        Task<OperationResult> AddColorPhoto(Guid photoId, Guid colorId);

        Task<OperationResult> RemovePhoto(Guid photoId);

        List<CModelDTO> GetNomModelsByCatalog(Guid id);

        Task<OperationResult> AddModelsCtlg(Guid id, List<Guid> modelIds);

        List<ColorDTO> GetColorsByNomId(Guid nomId);

        Task<CModelDTO> GetCatalogModel(Guid id);

        Task UpdateNomCatalog(Guid id, Guid catId, int toPosition);

        Task<OperationResult> UpdateNomCatalogOrders(Guid id, Guid catId, int toPosition);

        Task UpdateNomPhotoPosition(Guid id, int toPosition);

        Task<OperationResult> UpdateNomPhotoPath(Guid id, string path);

        Task<OperationResult> AddMainPhotoToNomenclature(Guid nomId, string path);

        Task<OperationResult> UpdateNomPhotoOrders(Guid id, Guid nomId, int toPosition);

        Task<double> GetProductPriceFromCatalogItemOrDefault(Guid prodId, Guid nomId);

        Task<double> GetProductPrice(Guid nomenclatureId, Guid productId);

        Task<ProductDetailsDTO> GetProductDetails(Guid nomId, Guid sizeId, Guid colorId, Guid userId, Guid catalogId, bool isSet);

        Task<(OperationResult IsExist, int FormType)> IsProductExist(CartItemDTO dto, int reservedAmount);

        Task<List<Guid>> GetNomIdsForSearch();

        Task<List<Guid>> GetNomIdsByCatalog(Guid catalogId);

        Task<List<Guid>> GetNomIdsByUserFavorite(Guid userId);

        Task<NomSearchDTO> GetListsForSearchByNomIds(List<Guid> nomIds);

        /// <summary>
        /// Get nomenclature that has storehouse rests from first open/visible for user catalog
        /// </summary>
        /// <param name="userId">sign in user id</param>
        /// <param name="count">amount of records to select</param>
        /// <returns></returns>
        Task<NomCatalogDTO> TakeNTopNomenclatureItemsFromVisibleCatalog(Guid? userId, int count);
    }
}