﻿using KristaShop.Business.DTOs;
using KristaShop.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IFeedbackService
    {
        Task<FeedbackDTO> GetFeedbackDetails(Guid id);

        Task<List<FeedbackDTO>> GetFeedbackListAsync();

        Task<OperationResult> InsertFeedback(FeedbackDTO dto);

        Task<OperationResult> InsertFeedbackWithFileAsync(FeedbackDTO feedback, FeedbackCreateFileDTO createFile);

        Task<OperationResult> UpdateFeedback(Guid id, Guid userId);

        Task<List<FeedbackFileDTO>> GetFilesByFeedbackIdAsync(Guid id);

        Task<FeedbackFileDTO> GetFileAsync(Guid id);
    }
}