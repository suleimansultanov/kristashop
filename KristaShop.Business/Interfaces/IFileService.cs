﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces.DataProviders;
using KristaShop.Common.Models;
using Microsoft.AspNetCore.Http;

namespace KristaShop.Business.Interfaces {
    public interface IFileService {
        Task<string> SaveFileAsync(IFormFile file, string filesDirectoryPath, string directory);
        Task<string> SaveFileAsync(IFileDataProvider file, bool useOriginalName = false);
        bool RemoveFile(string filesDirectoryPath, string filepath);
        Task<byte[]> GetFileAsync(string filesDirectoryPath, string filepath);
        List<FileInfo> GetDirectoryFiles(string filesDirectoryPath, string directory);

        OperationResult GetLastError();
    }
}
