﻿using System.Threading.Tasks;

namespace KristaShop.Business.Interfaces
{
    public interface IMetaHelperService
    {
        Task<string> GetMetaInfoAsync(string urlSegment);
    }
}