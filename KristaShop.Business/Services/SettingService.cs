﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services {
    public class SettingService : ISettingService {
        private readonly IShopRepository<Settings> _settingRepository;
        private readonly IMapper _mapper;
        private readonly ISettingsManager _settingsManager;

        public SettingService(IShopRepository<Settings> settingRepository, IMapper mapper,
            ISettingsManager settingsManager) {
            _settingRepository = settingRepository;
            _mapper = mapper;
            _settingsManager = settingsManager;
        }

        public async Task<List<SettingsDTO>> GetSettingsAsync() {
            return _mapper.Map<List<SettingsDTO>>((await _settingRepository.GetAllAsync()).OrderBy(x=>x.Key));
        }

        public async Task<List<SettingsDTO>> GetNotRootSettingsAsync() {
            return _mapper.Map<List<SettingsDTO>>((await _settingRepository.GetAllFindByAsync(x => !x.OnlyRootAccess)).OrderBy(x => x.Key));
        }

        public async Task<SettingsDTO> GetByIdAsync(Guid id) {
            var entity = await _settingRepository.FindByIdAsync(id);
            return _mapper.Map<SettingsDTO>(entity);
        }

        public async Task<SettingsDTO> GetByKeyAsync(string key) {
            var entity = await _settingRepository.FindByFilterAsync(x => x.Key == key);
            return _mapper.Map<SettingsDTO>(entity);
        }

        public async Task<OperationResult> InsertAsync(SettingsDTO dto) {
            var entity = _mapper.Map<Settings>(dto);
            entity.Id = Guid.NewGuid();
            if (await _settingRepository.AddAsync(entity) == null)
                return OperationResult.Failure();

            await _settingsManager.ReloadAsync(entity.Id);
            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateAsync(SettingsDTO dto, bool canChangeKey) {
            var settingsEntity = await _settingRepository.FindByIdAsync(dto.Id);
            if (settingsEntity == null) {
                return OperationResult.Failure("Настройки с таким ключем не существует");
            }

            settingsEntity.Value = dto.Value;
            settingsEntity.Description = dto.Description;
            if (canChangeKey) {
                settingsEntity.Key = dto.Key;
            }

            if (await _settingRepository.UpdateAsync(settingsEntity) == null)
                return OperationResult.Failure();

            await _settingsManager.ReloadAsync(settingsEntity.Id);
            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteAsync(Guid id) {
            var entity = await _settingRepository.FindByIdAsync(id);
            await _settingRepository.RemoveAsync(entity);

            await _settingsManager.ReloadAsync(entity.Id, entity.Key);
            return OperationResult.Success();
        }
    }
}