﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using KristaShop.DataReadOnly.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Org.BouncyCastle.Math.EC.Rfc7748;

namespace KristaShop.Business.Services
{
    public class CartService : ICartService
    {
        private readonly IShopRepository<CartItem> _cartRepo;
        private readonly IDiscountService _discountService;
        private readonly INomenclatureService _nomenclatureService;
        private readonly ICatalogItemReadService _catalogItemReadService;
        private readonly ICurrencyService _currencyService;
        private readonly IMapper _mapper;

        public CartService
            (IShopRepository<CartItem> cartRepo, IDiscountService discountService, INomenclatureService nomenclatureService,
            IMapper mapper, ICatalogItemReadService catalogItemReadService, ICurrencyService currencyService)
        {
            _cartRepo = cartRepo;
            _mapper = mapper;
            _discountService = discountService;
            _nomenclatureService = nomenclatureService;
            _catalogItemReadService = catalogItemReadService;
            _currencyService = currencyService;
        }

        public async Task<List<Guid>> GetCartUserStatus()
        {
            return await _cartRepo.Query().GroupBy(x => x.user_id).Select(x => x.Key).ToListAsync();
        }

        public async Task<List<CartItemDTO>> GetCartItemsNavbar(Guid userId)
        {
            return await _cartRepo.QueryFindBy(x => x.user_id == userId)
                .Select(x => new CartItemDTO
                {
                    Id = x.id,
                    TotalPrice = (x.price + x.discount) * x.amount
                }).ToListAsync();
        }

        public async Task<List<CartItemDTO>> GetCartItems(Guid userId)
        {
            var rate = await _currencyService.GetLastExchangeRate();
            var cartItems = await _cartRepo.QueryFindBy(x => x.user_id == userId)
                .Select(x => new CartItemDTO
                {
                    Id = x.id,
                    Amount = x.amount,
                    TotalAmount = x.total_amount,
                    Articul = x.Nomenclature.articul,
                    Image = x.Nomenclature.image_path,
                    Discount = x.discount,
                    Price = x.price,
                    IsPreorder = x.order_form_type == (int)OrderFormType.Preorder,
                    CatalogId = x.catalog_id,
                    NomId = x.nom_id,
                    UserId = x.user_id,
                    ProductId = x.product_id,
                    TotalPrice = (x.price + x.discount) * x.amount
                }).ToListAsync();
            var products = await _catalogItemReadService.GetProductsForCart(cartItems.Select(x => x.ProductId).ToList());
            return cartItems.Join(products,
                ca => ca.ProductId,
                pr => pr.ProductId,
                (cartItem, product) => new CartItemDTO(cartItem, product, rate)).ToList();
        }

        public async Task<List<CartItemDTO>> GetAllCartsItemsAsync() {
            var rate = await _currencyService.GetLastExchangeRate();

            var cartItems = (await _cartRepo.Query()
                    .Include(x=>x.Nomenclature)
                    .ToListAsync())
                .GroupBy(x => new {x.nom_id, x.product_id, x.order_form_type})
                .Select(x => new CartItemDTO {
                    Id = x.First().id,
                    Amount = x.Sum(c => c.amount),
                    TotalAmount = x.Sum(c => c.total_amount),
                    Articul = x.First().Nomenclature.articul,
                    Image = x.First().Nomenclature.image_path,
                    Discount = x.Sum(c => c.discount),
                    Price = x.Sum(c => c.price),
                    IsPreorder = x.First().order_form_type == (int) OrderFormType.Preorder,
                    NomId = x.First().nom_id,
                    ProductId = x.First().product_id,
                    TotalPrice = (x.Sum(c => c.price) + x.Sum(c => c.discount)) * x.Sum(c => c.amount)
                }).ToList();

            var products = await _catalogItemReadService.GetProductsForCart(cartItems.Select(x => x.ProductId).ToList());
            return cartItems.Join(products,
                    ca => ca.ProductId,
                    pr => pr.ProductId,
                    (cartItem, product) => new CartItemDTO {
                        Id = cartItem.Id,
                        Amount = cartItem.Amount,
                        TotalAmount = cartItem.TotalAmount,
                        Articul = cartItem.Articul,
                        Image = cartItem.Image,
                        Discount = cartItem.Discount,
                        DiscountRu = cartItem.Discount * rate,
                        Price = cartItem.Price,
                        PriceRu = cartItem.Price * rate,
                        IsPreorder = cartItem.IsPreorder,
                        CatalogId = cartItem.CatalogId,
                        NomId = cartItem.NomId,
                        UserId = cartItem.UserId,
                        ProductId = cartItem.ProductId,
                        TotalPrice = cartItem.TotalPrice,
                        TotalPriceRu = cartItem.TotalPrice * rate,
                        Size = product.Size.Name,
                        ColorName = product.Color.Name,
                        ColorCode = product.Color.Code,
                        ColorImg = product.Color.Image
                    })
                .OrderBy(x => x.Articul)
                .ThenBy(x => x.Size)
                .ThenBy(x => x.ColorName)
                .ToList();
        }

        public async Task<CartTotalsDTO> GetCartTotalsAsync() {
            var totals = await _cartRepo.Query()
                .GroupBy(x => true)
                .Select(x => new CartTotalsDTO {
                    Amount = x.Sum(c => c.amount),
                    TotalAmount = x.Sum(c => c.total_amount),
                    Discount = x.Sum(c => c.discount),
                    TotalPrice = x.Sum(c => (c.price + c.discount) * c.amount)
                }).FirstOrDefaultAsync();

            var rate = await _currencyService.GetLastExchangeRate();
            totals.DiscountRu = totals.Discount * rate;
            totals.TotalPriceRu = totals.TotalPrice * rate;
            return totals;
        }

        public async Task<OperationResult> RemoveCartItemById(Guid cartId)
        {
            var cartItem = await _cartRepo.FindByIdAsync(cartId);
            await _cartRepo.RemoveAsync(cartItem);
            return OperationResult.Success(new List<string> { "Товар успешно удален из корзины." });
        }

        public async Task RemoveCartItemsByUserId(Guid userId)
        {
            var cartItems = await _cartRepo.GetAllFindByAsync(x => x.user_id == userId);
            await _cartRepo.RemoveRangeAsync(cartItems.ToList());
        }

        public async Task<OperationResult> InsertCartItem(CartItemDTO dto) {
            if (dto.Amount <= 0) return OperationResult.Failure("Количество должно быть больше 0");

            dto.Price = await _nomenclatureService.GetProductPrice(dto.NomId, dto.ProductId);
            dto.Discount = await _discountService.GetDiscountValueAsync(dto.CatalogId, dto.UserId, dto.NomId);
            var productInCart = await _cartRepo
                .FindByFilterAsync(x => x.product_id == dto.ProductId
                && x.nom_id == dto.NomId
                && x.catalog_id == dto.CatalogId && x.user_id == dto.UserId
                && (long)Math.Round(x.discount * 1000, 0) == (long)Math.Round(dto.Discount * dto.PartsCount * 1000, 0)
                && (long)Math.Round(x.price * 1000, 0) == (long)Math.Round(dto.Price * 1000, 0));

            if (dto.ProductId == Guid.Empty || dto.Price <= 1E-6)
                return OperationResult.Failure(new List<string> { "Данного продукта не существует." });

            DateTime accessDate = DateTime.Now.AddHours(-24);
            int reservedCount = await _cartRepo.QueryFindBy(x => x.product_id == dto.ProductId && x.created_date >= accessDate && x.user_id == dto.UserId).SumAsync(x => x.amount);

            var (result, FormType) = await _nomenclatureService.IsProductExist(dto, reservedCount);
            if (result.IsSuccess) {
                if (productInCart != null) {
                    productInCart.amount += dto.Amount;
                    productInCart.total_amount += dto.Amount * dto.PartsCount;
                    if (await _cartRepo.UpdateAsync(productInCart) != null)
                        return OperationResult.Success(new List<string> { "Успешно добавлено в корзину." });
                }
                else
                {
                    var entity = _mapper.Map<CartItem>(dto);
                    entity.id = Guid.NewGuid();
                    entity.order_form_type = FormType;
                    entity.total_amount = entity.amount * dto.PartsCount;
                    entity.discount *= dto.PartsCount;
                    entity.created_date = DateTime.Now;

                    if (await _cartRepo.AddAsync(entity) != null)
                        return OperationResult.Success(new List<string> { "Успешно добавлено в корзину." });
                }
            }
            return result;
        }
    }
}