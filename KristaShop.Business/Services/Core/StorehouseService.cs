﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Common.Enums;
using KristaShop.DataReadOnly.Interfaces;
using KristaShop.DataReadOnly.Models;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Business.Services.Core {
    public class StorehouseService : IStorehouseService {
        private readonly IReadOnlyRepo<StorehouseRests> _storehouseRestsRepo;
        private readonly IReadOnlyRepo<ExchangeRatesSnapshot> _exchangeSnapshotRepo;

        public StorehouseService(IReadOnlyRepo<StorehouseRests> storehouseRestsRepo, IReadOnlyRepo<ExchangeRatesSnapshot> exchangeSnapshotRepo) {
            _storehouseRestsRepo = storehouseRestsRepo;
            _exchangeSnapshotRepo = exchangeSnapshotRepo;
        }

        public async Task<List<Guid>> GetNomenclatureIdsThatHasStorehouseRestsAsync(List<Guid> nomenclatureIds) {
            var result = await _storehouseRestsRepo.QueryFindBy(x =>
                    x.ReservationDocId == Guid.Empty
                    && x.Storehouse.IsForOnlineStore
                    && x.Level == 1 && x.CatalogItem.Level == 1
                    && nomenclatureIds.Contains(x.CatalogItem.NomenclatureId))
                .Select(x => x.CatalogItem.NomenclatureId)
                .Distinct()
                .ToListAsync();

            return result;
        }
        
        public async Task<Dictionary<Guid, string>> GetPackagingListAsync(CurrencyType priceInCurrency = CurrencyType.RUB) {
            var items = await _storehouseRestsRepo.QueryReadOnly()
                .Where(x => x.CatalogItem.Level == 1 &&
                            x.CatalogItem.Nomenclature.NomType == (int) NomenclatureType.Packaging)
                .Include(x=>x.CatalogItem.CatalogItemPrices)
                .Select(x => new {
                    Id = x.CatalogItem.Nomenclature.Id,
                    Name = x.CatalogItem.Nomenclature.Name,
                    ItemPrice = x.CatalogItem.CatalogItemPrices.FirstOrDefault(c => c.PriceType.Type == PriceTypes.RetailPrice)
                })
                .Distinct()
                .ToListAsync();

            var snapshotIds = items.Select(x => x.ItemPrice.ExchangeRateSnapshotId);
            var snapshots = await _exchangeSnapshotRepo.QueryReadOnly()
                .Include(x=>x.ToCurrency)
                .Where(x => snapshotIds.Contains(x.SnapshotId))
                .ToListAsync();
            
            var result = new Dictionary<Guid, string>();
            foreach (var item in items) {
                item.ItemPrice.ExchangeToCurrencyString(snapshots, priceInCurrency, out var price);
                result[item.Id] = $"{item.Name} ({price})";
            }

            return result;
        }
    }
}
