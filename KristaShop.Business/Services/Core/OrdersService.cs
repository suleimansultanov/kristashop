﻿using System;
using System.Threading.Tasks;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Business.UnitOfWork;

namespace KristaShop.Business.Services.Core {
    public class OrdersService : IOrdersService {
        private readonly IReadUnitOfWork _uow;

        public OrdersService(IReadUnitOfWork uow) {
            _uow = uow;
        }

        public async Task<int> GetNewOrdersCountAsync(Guid userId, int userAcl) {
            if (userId == Guid.Empty) return 0;
            return await _uow.Documents.NewOrdersCountAsync(userId, userAcl);
        }
    }
}
