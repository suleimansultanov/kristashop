﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class BannerService : IBannerService
    {
        private readonly ICacheRepository<BannerItem> _bannerRepo;
        private readonly IMapper _mapper;

        public BannerService
            (IMapper mapper, ICacheRepository<BannerItem> bannerRepo)
        {
            _mapper = mapper;
            _bannerRepo = bannerRepo;
        }

        public async Task<List<BannerItemDTO>> GetBanners()
        {
            return _mapper.Map<List<BannerItemDTO>>(await _bannerRepo.GetAllAsync());
        }

        public async Task<BannerItemDTO> GetBannerDetails(Guid id)
        {
            var entity = await _bannerRepo.FindByIdAsync(id);
            return _mapper.Map<BannerItemDTO>(entity);
        }

        public async Task<BannerItemDTO> GetBannerDetailsNoTrack(Guid id)
        {
            var entity = await _bannerRepo.QueryFindBy(x => x.id == id).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<BannerItemDTO>(entity);
        }

        public async Task<OperationResult> InsertBanner(BannerItemDTO dto)
        {
            var entity = _mapper.Map<BannerItem>(dto);
            var lastOrder = _bannerRepo.Query().OrderByDescending(s => s.order)
                         .FirstOrDefault()?.order;
            if (lastOrder == null)
                entity.order = 1;
            else
                entity.order = lastOrder.Value + 1;
            entity.id = Guid.NewGuid();
            if (await _bannerRepo.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateBanner(BannerItemDTO dto)
        {
            var entity = _mapper.Map<BannerItem>(dto);
            var existBanner = await _bannerRepo.FindByIdAsync(dto.Id);
            existBanner.image_path = entity.image_path ?? existBanner.image_path;
            existBanner.is_visible = entity.is_visible;
            existBanner.link = entity.link;
            existBanner.title = entity.title;
            existBanner.caption = entity.caption;
            existBanner.description = entity.description;
            existBanner.order = entity.order;
            existBanner.title_color = entity.title_color;
            if (await _bannerRepo.UpdateAsync(existBanner) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteBanner(Guid id)
        {
            var entity = await _bannerRepo.FindByIdAsync(id);
            await _bannerRepo.RemoveAsync(entity);
            return OperationResult.Success();
        }
    }
}