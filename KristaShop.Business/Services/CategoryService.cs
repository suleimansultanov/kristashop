﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICacheRepository<Category> _catRepo;
        private readonly IMapper _mapper;

        public CategoryService
            (ICacheRepository<Category> catRepo,IMapper mapper)
        {
            _catRepo = catRepo;
            _mapper = mapper;
        }

        public async Task<List<CategoryDTO>> GetCategoriesByNomId(Guid nomId)
        {
            var ctgrs = await _catRepo.QueryFindBy(x => x.NomCategories.Any(n => n.nom_id == nomId)).OrderBy(x => x.order).ToListAsync();
            return _mapper.Map<List<CategoryDTO>>(ctgrs);
        }

        public async Task<CategoryDTO> GetCategoryDetailsNoTrack(Guid id)
        {
            var entity = await _catRepo.QueryFindBy(x => x.id == id).OrderBy(x => x.order).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<CategoryDTO>(entity);
        }

        public async Task<List<CategoryDTO>> GetCategories()
        {
            return await _catRepo.Query()
                .Select(s => new CategoryDTO
                {
                    Id = s.id,
                    Name = s.name,
                    IsVisible = s.is_visible,
                    ImagePath = s.image_path,
                    NomCount = s.NomCategories.Count,
                    Order = s.order,
                    Description = s.description
                }).OrderBy(x => x.Order).ToListAsync();
        }

        public async Task<CategoryDTO> GetCategoryDetails(Guid id)
        {
            var entity = await _catRepo.FindByIdAsync(id);
            return _mapper.Map<CategoryDTO>(entity);
        }

        public async Task<OperationResult> InsertCategory(CategoryDTO dto)
        {
            var entity = _mapper.Map<Category>(dto);
            var lastOrder = _catRepo.Query().OrderByDescending(s => s.order)
                         .FirstOrDefault()?.order;
            if (lastOrder == null)
                entity.order = 1;
            else
                entity.order = lastOrder.Value + 1;
            entity.id = Guid.NewGuid();
            entity.is_visible = true;
            if (await _catRepo.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateCategory(CategoryDTO dto)
        {
            var entity = _mapper.Map<Category>(dto);
            var existCat = await _catRepo.FindByIdAsync(dto.Id);
            existCat.image_path = entity.image_path ?? existCat.image_path;
            existCat.is_visible = entity.is_visible;
            existCat.name = entity.name;
            existCat.description = entity.description;
            existCat.order = entity.order;
            if (await _catRepo.UpdateAsync(existCat) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteCategory(Guid id)
        {
            var entity = await _catRepo.FindByIdAsync(id);
            await _catRepo.RemoveAsync(entity);
            return OperationResult.Success();
        }
    }
}