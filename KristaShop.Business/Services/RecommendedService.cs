﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Business.Services {
    public class RecommendedService : IRecommendedService {
        private readonly IShopRepository<NomCategory> _nomenclatureCategoryRepository;
        private readonly IShopRepository<NomenclatureModel> _nomenclatureRepository;
        private readonly IShopRepository<Catalog> _catalogRepository;
        private readonly IStorehouseService _storehouseService;

        public RecommendedService(IShopRepository<NomCategory> nomenclatureCategoryRepository,
            IShopRepository<NomenclatureModel> nomenclatureRepository, IShopRepository<Catalog> catalogRepository,
            IStorehouseService storehouseService) {
            _nomenclatureCategoryRepository = nomenclatureCategoryRepository;
            _nomenclatureRepository = nomenclatureRepository;
            _catalogRepository = catalogRepository;
            _storehouseService = storehouseService;
        }

        public async Task<List<RecommendedNomenclatureDto>> GetNRecommendedNomenclatureItems(List<Guid> categoryIds,
            int quantity, Guid userId = default) {
            List<NomenclatureModel> recommended;
            if (userId == Guid.Empty) {
                recommended = await _getNRecommendedNomenclatureItemsFromOpenCatalogs(categoryIds, quantity);
            } else {
                recommended = await _getNRecommendedNomenclatureItemsForUser(categoryIds, quantity, userId);
            }

            var result = recommended.Select(x => new RecommendedNomenclatureDto {
                NomId = x.id,
                Articul = x.articul,
                ItemName = x.name,
                ImageAlternativeText = x.image_alternative_text,
                ImagePath = x.image_path,
                Description = x.description,
                CatalogUri = x.NomCatalogs.OrderBy(c => c.Catalog.is_open)
                                 .FirstOrDefault(c => c.Catalog.is_visible &&
                                 (c.Catalog.close_time != null && c.Catalog.close_time > DateTimeOffset.UtcNow ||
                                  c.Catalog.close_time == null))?.Catalog.uri ?? string.Empty
            });

            return result.ToList();
        }

        private async Task<List<NomenclatureModel>> _getNRecommendedNomenclatureItemsFromOpenCatalogs(
            List<Guid> categoryIds, int quantity) {
            var result = await _nomenclatureCategoryRepository
                .Query()
                .Include(x => x.Nomenclature.NomCatalogs)
                .ThenInclude(x => x.Catalog)
                .Where(x => categoryIds.Contains(x.category_id)
                            && x.Nomenclature.NomCatalogs.Any(c => c.Catalog.is_open)
                            && x.Nomenclature.is_visible)
                .Select(x => x.Nomenclature)
                .Distinct()
                .OrderBy(x=> CustomMysqlFunctions.Random())
                .Take(quantity)
                .ToListAsync();
                
            return result;
        }

        private async Task<List<NomenclatureModel>> _getNRecommendedNomenclatureItemsForUser(List<Guid> categoryIds,
            int quantity, Guid userId) {
            var showNomenclatureFromOpenCatalogs = !await _catalogRepository.Query().AnyAsync(x => x.VisibleCatalogUsers.All(c => c.user_id != userId) && !x.is_open && x.is_visible);

              var request = _nomenclatureCategoryRepository
                .Query()
                .Where(x => categoryIds.Contains(x.category_id)
                            && x.Nomenclature.is_visible
                            && x.Nomenclature.VisibleNomUsers.All(v => v.user_id != userId)
                            && x.Nomenclature.NomCatalogs.All(c => c.Catalog.VisibleCatalogUsers.All(z => z.user_id != userId))
                            && x.Nomenclature.NomCatalogs.Any(c =>
                                c.Catalog.is_visible && (c.Catalog.is_open == false || showNomenclatureFromOpenCatalogs) &&
                                (c.Catalog.close_time != null && c.Catalog.close_time > DateTimeOffset.UtcNow ||
                                 c.Catalog.close_time == null)));

            //if (!showNomenclatureFromOpenCatalogs) {
            //    request = request.Where(x => x.Nomenclature.NomCatalogs.Any(c => c.Catalog.is_open == false));
            //}

            var categoryNomenclatureIds = await request.Select(x => new {
                    Id = x.Nomenclature.id,
                    CheckRests = x.Nomenclature.NomCatalogs.Any(c => c.Catalog.order_form != (int) OrderFormType.Preorder && !c.Catalog.is_open)
                })
                .ToListAsync();

            // check items from stock in storehouse rests
            var nomenclatureIds = await _storehouseService.GetNomenclatureIdsThatHasStorehouseRestsAsync(
                categoryNomenclatureIds.Where(x => x.CheckRests).Select(x => x.Id).ToList());
            // add preorder items to storehouse rests check result
            nomenclatureIds.AddRange(categoryNomenclatureIds.Where(x => !x.CheckRests).Select(x => x.Id));

            var result = await _nomenclatureRepository
                .QueryFindBy(x => nomenclatureIds.Contains(x.id))
                .Include(x => x.NomCatalogs)
                .ThenInclude(x => x.Catalog)
                .OrderBy(x => CustomMysqlFunctions.Random())
                .Take(quantity)
                .ToListAsync();

            return result;
        }
    }
}