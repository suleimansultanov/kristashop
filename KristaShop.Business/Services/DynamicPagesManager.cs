﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Business.UnitOfWork;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace KristaShop.Business.Services {
    public class DynamicPagesManager : IDynamicPagesManager {
        protected IServiceScope ServiceScope { get; set; }
        protected ILogger Logger { get; set; }
        protected IMapper _mapper { get; set; }
        protected ConcurrentDictionary<string, MenuContentDTO> MenuContent;

        public DynamicPagesManager() {
            MenuContent = new ConcurrentDictionary<string, MenuContentDTO>();
        }

        public async Task InitializeAsync(IServiceScope serviceScope) {
            ServiceScope = serviceScope;
            Logger = serviceScope.ServiceProvider.GetService<ILogger>();
            _mapper = serviceScope.ServiceProvider.GetService<IMapper>();

            await ReloadAsync();
        }

        public async Task ReloadAsync() {
            try {
                var uow = ServiceScope.ServiceProvider.GetService<IUnitOfWork>();
                var contents = _mapper.Map<List<MenuContentDTO>>(await uow.MenuContent.All.AsNoTracking().ToListAsync());
                MenuContent.Clear();
                foreach (var content in contents) {
                    MenuContent.TryAdd(content.URL.ToLower(), content);
                }
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to reload dynamic pages manager. {message}", ex.Message);
            }
        }

        public async Task ReloadAsync(Guid menuId, string oldKey = "") {
            try {
                var uow = ServiceScope.ServiceProvider.GetService<IUnitOfWork>();
                var menuContent = _mapper.Map<MenuContentDTO>(await uow.MenuContent.All.AsNoTracking().FirstOrDefaultAsync(x=>x.Id == menuId));

                if (!string.IsNullOrEmpty(oldKey)) {
                    _removeMenuContent(oldKey);
                }

                if (menuContent != null) {
                    _updateMenuContent(menuContent);
                }
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to reload dynamic pages manager. {message}", ex.Message);
            }
        }

        public bool TryGetValue(string url, out MenuContentDTO value) {
            value = null;
            try {
                var key = url.ToLower();
                if (MenuContent.ContainsKey(key)) {
                    value = MenuContent[key];
                    return true;
                }
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to get item from dynamic pages manager, item key: {key}. {message}", url, ex.Message);
                return false;
            }

            return false;
        }

        public bool TryGetValuesByController(string controller, bool openOnly, out List<MenuContentDTO> values) {
            values = null;
            try {
                values = openOnly
                    ? MenuContent.Values.Where(x => x.URL.Contains($"{controller}/") && x.IsOpen).OrderBy(x => x.Order).ToList()
                    : MenuContent.Values.Where(x => x.URL.Contains($"{controller}/")).OrderBy(x => x.Order).ToList();
                return true;
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to get items from dynamic pages manager by controller: {controller}. {message}", controller, ex.Message);
                return false;

            }
        }

        public bool TryGetValuesByControllerForMenu(string controller, bool openOnly, out List<MenuContentDTO> values) {
            values = null;
            try {
                values = openOnly
                    ? MenuContent.Values.Where(x => x.URL.Contains($"{controller}/") && x.IsOpen && x.IsVisibleInMenu).OrderBy(x => x.Order).ToList()
                    : MenuContent.Values.Where(x => x.URL.Contains($"{controller}/") && x.IsVisibleInMenu).OrderBy(x => x.Order).ToList();
                return true;
            } catch (Exception ex) {
                Logger.Error(ex, "Failed to get items from dynamic pages manager by controller: {controller}. {message}", controller, ex.Message);
                return false;

            }
        }

        protected virtual void _removeMenuContent(string key) {
            if (MenuContent.ContainsKey(key)) {
                if (!MenuContent.TryRemove(key, out _)) {
                    Logger.Error("Failed to remove item from dynamic pages manager. Item key: {key}", key);
                }
            }
        }

        protected virtual void _updateMenuContent(MenuContentDTO menuContent) {
            var key = menuContent.URL.ToLower();
            if (MenuContent.ContainsKey(key)) {
                MenuContent[key] = menuContent;
            } else {
                if (!MenuContent.TryAdd(key, menuContent)) {
                    Logger.Error("Failed to remove item from dynamic pages manager. Item: {@menuContent}", menuContent);
                }
            }
        }
    }
}
