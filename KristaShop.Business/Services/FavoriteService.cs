﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Extensions;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using KristaShop.DataReadOnly.Interfaces;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class FavoriteService : IFavoriteService
    {
        private readonly ICacheRepository<NomUserFavorite> _repo;
        private readonly ICatalogItemReadService _nomReadService;
        private readonly ICurrencyService _currencyService;

        public FavoriteService (ICacheRepository<NomUserFavorite> repo,
            ICatalogItemReadService nomReadService, ICurrencyService currencyService) {
            _repo = repo;
            _nomReadService = nomReadService;
            _currencyService = currencyService;
        }

        public async Task<IPager<NomFavoriteDTO>> GetFavoritesAsync(NomFilterDTO filter, int page, int modelInPage, Guid userId)
        {
            var rate = await _currencyService.GetLastExchangeRate();
            var nomModels = _repo.QueryFindBy(x => x.user_id == userId && x.Nomenclature.is_visible);
            if (!string.IsNullOrEmpty(filter.Name))
                nomModels = nomModels.Where(x => EF.Functions.Like(x.Nomenclature.articul, $"%{filter.Name}%"));
            if (filter.CategoryIds.Count > 0)
                nomModels = nomModels.Where(x => x.Nomenclature.NomCategories.Any(s => filter.CategoryIds.Contains(s.category_id)));
            if (filter.PriceMin.HasValue)
                nomModels = nomModels.Where(x => x.Nomenclature.default_price >= filter.PriceMin.Value);
            if (filter.PriceMax.HasValue)
                nomModels = nomModels.Where(x => x.Nomenclature.default_price <= filter.PriceMax.Value);

            var nomIds = _nomReadService.GetNomenclatureByFilter(filter.ColorIds, filter.SizeIds, false);
            nomModels = nomModels.Where(x => nomIds.Contains(x.nom_id));
            if (filter.OrderColumn.Equals("OrderNewAsc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.created_date, true);
            else if (filter.OrderColumn.Equals("OrderNewDesc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.created_date, false);
            else if (filter.OrderColumn.Equals("OrderPriceAsc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.default_price, true);
            else if (filter.OrderColumn.Equals("OrderPriceDesc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.default_price, false);

            return await nomModels
                .Select(x => new NomFavoriteDTO
                {
                    NomId = x.Nomenclature.id,
                    CatalogId = x.catalog_id,
                    CatalogUri = x.Catalog.uri,
                    ItemName = x.Nomenclature.name,
                    ImagePath = x.Nomenclature.image_path,
                    DefaultPrice = x.Nomenclature.parts_count == 0 ? x.Nomenclature.default_price : x.Nomenclature.default_price / x.Nomenclature.parts_count,
                    DefaultPriceRu = x.Nomenclature.parts_count == 0 ? x.Nomenclature.default_price * rate : (x.Nomenclature.default_price * rate) / x.Nomenclature.parts_count
                }).ToPagerListAsync(page, modelInPage);
        }

        public async Task<OperationResult> InsertFavoriteAsync(Guid userId, Guid nomId, Guid catalogId)
        {
            var favorite = new NomUserFavorite
            {
                id = Guid.NewGuid(),
                user_id = userId,
                nom_id = nomId,
                catalog_id = catalogId
            };
            if (await _repo.AddAsync(favorite) == null)
                return OperationResult.Failure(new List<string> { "Данную модель не удалось добавить в избранное." });

            return OperationResult.Success(new List<string> { "Данная модель успешно добавлена в избранное." });
        }

        public async Task<OperationResult> AddOrDeleteFavoriteAsync(Guid userId, Guid nomId, Guid catalogId) {
            var favorite = await _repo.FindByFilterAsync(x => x.user_id == userId && x.nom_id == nomId && x.catalog_id == catalogId);
            if (favorite != null) {
                return await _deleteFavoriteAsync(favorite);
            }

            return await InsertFavoriteAsync(userId, nomId, catalogId);
        }

        public async Task<OperationResult> DeleteFavoriteAsync(Guid userId, Guid nomId, Guid catalogId) {
            var favorite = await _repo.FindByFilterAsync(x => x.user_id == userId && x.nom_id == nomId && x.catalog_id == catalogId);
            if (favorite == null) {
                return OperationResult.Success();
            }

            return await _deleteFavoriteAsync(favorite);
        }

        protected virtual async Task<OperationResult> _deleteFavoriteAsync(NomUserFavorite favorite) {
            await _repo.RemoveAsync(favorite);
            return OperationResult.Success(new List<string> { "Данная модель удалена из избранного." });
        }
    }
}