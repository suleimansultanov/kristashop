﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Domain;
using KristaShop.DataAccess.Entities;
using KristaShop.DataReadOnly.DTOs;
using KristaShop.DataReadOnly.Interfaces;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.DataAccess.Interfaces;
using IUserService = KristaShop.Business.Interfaces.IUserService;

namespace KristaShop.Business.Services
{
    public class NomenclatureService : INomenclatureService
    {
        private readonly KristaShopDbContext _context;

        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ICatalogItemReadService _nomReadService;
        private readonly IDiscountService _discountService;
        private readonly ICurrencyService _currencyService;
        private readonly ICacheRepository<Catalog> _catalogRepository;
        public NomenclatureService
            (KristaShopDbContext context, IMapper mapper, ICatalogItemReadService nomReadService,
            IUserService userService, IDiscountService discountService, ICurrencyService currencyService,
            ICacheRepository<Catalog> catalogRepository)
        {
            _context = context;

            _mapper = mapper;
            _userService = userService;
            _discountService = discountService;
            _nomReadService = nomReadService;
            _currencyService = currencyService;
            _catalogRepository = catalogRepository;
        }

        public async Task<List<LookUpItem<Guid, string>>> GetNomenclatureCatalogsLookupListAsync(Guid nomenclatureId) {
            return await _context.NomCatalogs.Where(x => x.nom_id == nomenclatureId)
                .Select(x => new LookUpItem<Guid, string>(x.catalog_id, x.Catalog.name))
                .ToListAsync();
        }

        public async Task<CartItemDTO> GetPackagingCartItem(Guid nomId)
        {
            var product = await _nomReadService.GetPackagingProduct(nomId);
            if (product == null)
                return null;
            return new CartItemDTO
            {
                Articul = product.ItemName,
                Amount = 1,
                Discount = 0,
                ProductId = product.ProductId,
                Price = product.Price.Value,
                IsPreorder = false
            };
        }

        public async Task<List<CModelDTO>> GetNomModelDiscounts()
        {
            var discounts = await _context.NomDiscounts.Where(x => x.IsActive && (x.EndDate >= DateTime.Now.Date || x.EndDate == null)).ToListAsync();
            var nomDiscounts = discounts
                .GroupBy(x => x.NomenclatureId)
                .ToDictionary(x => x.Key, x => x.OrderBy(c => Math.Abs(c.DiscountPrice)).First().DiscountPrice);
            var noms = GetNomModels();

            foreach (var item in noms)
            {
                if (nomDiscounts.ContainsKey(item.Id))
                    item.DiscountPrice = nomDiscounts[item.Id];
            }

            return noms.Where(x => x.IsVisible).ToList();
        }

        public List<CModelDTO> GetNomModels()
        {
            var nomModels = _context.Nomenclatures.Select(x => new
            {
                Id = x.id,
                IsVisible = x.is_visible,
                PhotoPath = x.image_path,
                Catalogs = x.NomCatalogs.Select(x => x.Catalog.name).ToList(),
                Categories = x.NomCategories.Select(x => x.Category.name).ToList()
            }).ToList();
            var nomList = _nomReadService.GetCatalogModels();
            return nomList
                .GroupJoin(nomModels,
                    nl => nl.Id,
                    nm => nm.Id,
                    (nomList, nomModels) => new CModelDTO
                    {
                        Id = nomList.Id,
                        Articul = nomList.Articul,
                        ItemName = nomList.ItemName,
                        Sizes = nomList.Sizes,
                        Colors = nomList.Colors,
                        IsVisible = nomModels.Select(x => x.IsVisible).SingleOrDefault(),
                        PhotoPath = nomModels.Select(x => x.PhotoPath).SingleOrDefault(),
                        Catalogs = nomModels.Select(x => x.Catalogs).SingleOrDefault(),
                        Categories = nomModels.Select(x => x.Categories).SingleOrDefault()
                    }).ToList();
        }

        public async Task<List<CartItemDTO>> GetCartOrderItems(List<OrderItemsDTO> orderItems, double rate, double? percent = null)
        {
            var nomIds = orderItems.Select(x => x.NomId).ToList();
            var nomImages = await _context.Nomenclatures
                .Where(x => nomIds.Contains(x.id))
                .Select(x => new CartItemDTO { Image = x.image_path, NomId = x.id })
                .ToListAsync();
            return orderItems.GroupJoin(nomImages,
                    ca => ca.NomId,
                    pr => pr.NomId,
                    (orderItem, nomImage) => new { orderItem, nomImage })
                .SelectMany(
                    orderItem => orderItem.nomImage.DefaultIfEmpty(),
                    (x, y) => new CartItemDTO(x.orderItem, y?.Image.ToString(), rate, percent))
                .OrderBy(x => x.Articul)
                .ThenBy(x => x.ColorName)
                .ThenBy(x => x.Size)
                .ToList();
        }

        public async Task<IPager<NomModelDTO>> GetNomModelsByFilter(NomFilterDTO filter, int page, int modelInPage, Guid? userId)
        {
            var nomModels = _context.NomCatalogs.Include(x => x.Nomenclature)
                .Where(x => x.catalog_id == filter.CatalogId 
                            && x.Nomenclature.is_visible 
                            && (x.Catalog.close_time != null && x.Catalog.close_time > DateTimeOffset.UtcNow || x.Catalog.close_time == null));
            if (!string.IsNullOrEmpty(filter.Name))
                nomModels = nomModels.Where(x => EF.Functions.Like(x.Nomenclature.articul, $"%{filter.Name}%"));
            if (userId.HasValue)
                nomModels = nomModels.Where(x => x.Nomenclature.VisibleNomUsers.All(a => a.user_id != userId));
            if (filter.CategoryIds.Count > 0)
                nomModels = nomModels.Where(x => x.Nomenclature.NomCategories.Any(s => filter.CategoryIds.Contains(s.category_id)));
            if (filter.PriceMin.HasValue)
                nomModels = nomModels.Where(x => x.Nomenclature.default_price >= filter.PriceMin.Value);
            if (filter.PriceMax.HasValue)
                nomModels = nomModels.Where(x => x.Nomenclature.default_price <= filter.PriceMax.Value);

            var catalog = _context.Catalogs.Find(filter.CatalogId);
            bool isInstock = catalog.order_form == (int)OrderFormType.InStock;
            var nomIds = _nomReadService.GetNomenclatureByFilter(filter.ColorIds, filter.SizeIds, isInstock);
            nomModels = nomModels.Where(x => nomIds.Contains(x.nom_id)).OrderByDynamic(x => x.order, true);
            if (filter.OrderColumn.Equals("OrderNewAsc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.created_date, true);
            else if (filter.OrderColumn.Equals("OrderNewDesc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.created_date, false);
            else if (filter.OrderColumn.Equals("OrderPriceAsc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.default_price / (x.Nomenclature.parts_count < 1 ? 1 : x.Nomenclature.parts_count), true);
            else if (filter.OrderColumn.Equals("OrderPriceDesc"))
                nomModels = nomModels.OrderByDynamic(x => x.Nomenclature.default_price / (x.Nomenclature.parts_count < 1 ? 1 : x.Nomenclature.parts_count), false);

            var rate = await _currencyService.GetLastExchangeRate();
            var result = await nomModels
                .Select(x => new NomModelDTO
                {
                    NomId = x.Nomenclature.id,
                    ItemName = x.Nomenclature.name,
                    ImageAlternativeText = x.Nomenclature.image_alternative_text,
                    ImagePath = x.Nomenclature.image_path,
                    PartsCount = x.Nomenclature.parts_count,
                    DefaultPrice = x.Nomenclature.parts_count < 1 ? x.Nomenclature.default_price : x.Nomenclature.default_price / x.Nomenclature.parts_count,
                    DefaultPriceRu = x.Nomenclature.parts_count < 1 ? x.Nomenclature.default_price * rate : (x.Nomenclature.default_price * rate) / x.Nomenclature.parts_count,
                    NomenclatureDiscount = x.Nomenclature.NomDiscounts
                        .Where(c => c.IsActive && c.StartDate < c.EndDate && c.EndDate > DateTime.Now && c.DiscountCatalogs.Any(z => z.CatalogId == x.catalog_id))
                        .OrderBy(z => Math.Abs(z.DiscountPrice)).FirstOrDefault().DiscountPrice,
                    ExchangeRate = rate
                }).ToPagerListAsync(page, modelInPage);


            return result;
        }

        public List<NomUserDTO> GetNomUsers(UserSession user, Guid nomId)
        {
            var existNomUser = _context.VisibleNomUsers.Where(x => x.nom_id == nomId).Select(x => x.user_id).ToList();
            var users = _userService.GetAllUsers(user);
            return users.Select(x => new NomUserDTO
            {
                UserId = x.UserId,
                CityName = x.CityName,
                ClientFullName = x.ClientFullName,
                ClientLogin = x.Login,
                MallAddress = x.ShopName,
                NotVisible = existNomUser.Contains(x.UserId),
                Status = x.Status,
                StatusName = x.StatusName
            }).ToList();
        }

        public async Task<NomModelDTO> GetNomModel(Guid id)
        {
            var nomModel = await _context.Nomenclatures.FindAsync(id);
            var dto = _mapper.Map<NomModelDTO>(nomModel);
            if (dto != null)
            {
                dto.Catalogs = await _context.NomCatalogs.Where(x => x.nom_id == id).Select(x => x.catalog_id).ToListAsync();
                dto.Categories = await _context.NomCategories.Where(x => x.nom_id == id).Select(x => x.category_id).ToListAsync();
            }

            return dto;
        }

        public async Task<(OperationResult IsExist, int FormType)> IsProductExist(CartItemDTO dto, int reservedAmount)
        {
            int formType = await _context.Catalogs.Where(x => x.id == dto.CatalogId).Select(x => x.order_form).SingleOrDefaultAsync();
            if (formType == (int)OrderFormType.InStock)
                return (_nomReadService.IsExistInStorehouse(dto.ProductId, dto.Amount, reservedAmount), formType);

            return (CheckPreorder(dto.NomId, dto.Amount), formType);
        }

        public async Task<NomModelDetailDTO> GetNomModelById(Guid id, string ctlgUri, Guid? userId)
        {
            var nomModel = await GetNomModelById(id, userId);
            if (nomModel == null)
                return null;

            var catalog = await _context.Catalogs.Where(x => x.uri == ctlgUri && x.VisibleCatalogUsers.All(y => y.user_id != userId)).FirstOrDefaultAsync();
            if (catalog == null)
                return null;
            var productIds = await GetProductIdsAsync(id, catalog.id);
            if (catalog.order_form == 0)
                return null;
            else if (catalog.order_form == (int)OrderFormType.InStock)
                productIds = _nomReadService.GetProductIdsInStorehouse(productIds);
            else if (!CheckPreorder(id).IsSuccess)
                return null;

            return await GetNomModelDetailsAsync(nomModel, productIds, catalog, userId);
        }

        private async Task<NomenclatureModel> GetNomModelById(Guid id, Guid? userId)
        {
            return await _context.Nomenclatures
                .Where(x => x.is_visible && x.id == id)
                .Where(x => x.VisibleNomUsers.All(a => a.user_id != userId))
                .Include(x => x.NomPhotos)
                .Include(x => x.NomDiscounts)
                .Include(x => x.NomCatalogs)
                    .ThenInclude(x => x.Catalog)
                .Include(x => x.NomCategories)
                    .ThenInclude(x => x.Category)
                .Include(x => x.NomUserFavorites)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public async Task<List<Guid>> GetProductIdsAsync(Guid nomenclatureId, Guid catalogId)
        {
            var notVisProductsId = await _context.NotVisibleProdCtlgs
                .Where(x => x.catalog_id == catalogId && x.nom_id == nomenclatureId)
                .Select(x => x.product_id)
                .ToListAsync();

            return _nomReadService.GetProductIdsByNomId(nomenclatureId, notVisProductsId);
        }

        private OperationResult CheckPreorder(Guid id, int amount = 0)
        {
            var nomPreorder = _context.NomPreorders.Find(id);
            if (nomPreorder == null)
                return OperationResult.Success();
            if (nomPreorder.max_amout > nomPreorder.counter + amount)
                return OperationResult.Success();
            else
                return OperationResult.Failure(new List<string> { "Данная модель в заданном количестве отсутствует по предзаказу." });
        }

        private async Task<NomModelDetailDTO> GetNomModelDetailsAsync(NomenclatureModel nomModel,
            List<Guid> productIds, Catalog catalog, Guid? userId)
        {
            var sizes = await _nomReadService.GetNomenclatureSizesAsync(nomModel.id, nomModel.is_set, productIds);
            if (sizes == null)
                return null;

            var modelDetail = _mapper.Map<NomModelDetailDTO>(nomModel);
            if (userId.HasValue) {
                modelDetail.IsFavorite = nomModel.NomUserFavorites.Any(x => x.catalog_id == catalog.id && x.user_id == userId.Value);
            }
            
            modelDetail.Sizes = sizes;
            modelDetail.Colors = await GetColorsBySizeAsync(catalog.id, nomModel.id, sizes.First().Id, catalog.order_form == (int)OrderFormType.InStock);

            if (userId != null) {
                var details = await GetProductDetails(nomModel.id, sizes.First().Id, modelDetail.Colors.First().Id,
                    (Guid) userId, catalog.id, nomModel.is_set);
                modelDetail.DefaultPrice = (double) details.Price;
                modelDetail.DefaultPriceRu = (double) details.PriceRu;
                modelDetail.SelectedProductId = details.ProductId;
                modelDetail.PartsCount = details.PartsCount;
            } else {
                modelDetail.DefaultPrice = 0;
                modelDetail.DefaultPriceRu = 0;
                modelDetail.SelectedProductId = Guid.Empty;
            }

            return modelDetail;
        }

        public async Task<List<ColorDTO>> GetColorsBySizeAsync(Guid catalogId, Guid nomId, Guid sizeId, bool isInstock) {
            var notVisibleProducts = _context.NotVisibleProdCtlgs
                .Where(x => x.nom_id == nomId && x.catalog_id == catalogId)
                .Select(x => x.product_id)
                .ToList();

            var result = await _nomReadService.GetNomenclatureColorsBySizeAsync(nomId, sizeId, isInstock, notVisibleProducts);
            return result;
        }

        public async Task<OperationResult> AddNomModel(NomModelDTO dto)
        {
            await using var dbContextTransaction = _context.Database.BeginTransaction();
            try
            {
                var nomModel = _mapper.Map<NomenclatureModel>(dto);
                if (nomModel.parts_count == 0)
                    nomModel.parts_count = await AddPartsCount(nomModel.is_set, nomModel.id);

                var existNomModel = await _context.Nomenclatures.FindAsync(nomModel.id);
                if (existNomModel != null)
                {
                    nomModel.image_path ??= existNomModel.image_path;

                    _context.Entry(existNomModel).State = EntityState.Detached;
                    _context.Nomenclatures.Update(nomModel);
                }
                else
                {
                    nomModel.is_visible = true;
                    _context.Nomenclatures.Add(nomModel);
                }
                await AddNomCatalogs(dto.Catalogs, nomModel.id);
                await AddNomCategories(dto.Categories, nomModel.id);
                await AddNomUsers(dto.Clients, nomModel.id);
                await AddNomPhotos(dto.PhotoPaths, nomModel.id);

                await _context.SaveChangesAsync();
                await dbContextTransaction.CommitAsync();

                return OperationResult.Success();
            }
            catch (Exception)
            {
                await dbContextTransaction.RollbackAsync();
                return OperationResult.Failure();
            }
        }

        private async Task<double> AddPartsCount(bool isSet, Guid nomId)
        {
            if (isSet)
                return await _nomReadService.GetPartsCountByNomId(nomId);
            return 1;
        }

        private async Task AddNomCatalogs(List<Guid> catalogIds, Guid nomId) {
            var existingNomCatalogs = await _context.NomCatalogs.Where(x => x.nom_id == nomId).ToListAsync();
            var toRemove = existingNomCatalogs.Where(x => !catalogIds.Contains(x.catalog_id));
            _context.NomCatalogs.RemoveRange(toRemove);

            var toAdd = new List<NomCatalog>();
            var newCatalogIds = catalogIds.Except(existingNomCatalogs.Select(x => x.catalog_id));
            foreach (var catalogId in newCatalogIds) {
                var nomCatalog = new NomCatalog { nom_id = nomId, catalog_id = catalogId };
                var lastOrder = _context.NomCatalogs.Where(x => x.catalog_id == catalogId)
                    .OrderByDescending(s => s.order)
                    .FirstOrDefault()?.order;
                nomCatalog.order = lastOrder + 1 ?? 1;
                toAdd.Add(nomCatalog);
            }
            _context.NomCatalogs.AddRange(toAdd);
        }

        private async Task AddNomCategories(List<Guid> categIds, Guid nomId)
        {
            var existNomCtgrs = await _context.NomCategories.Where(x => x.nom_id == nomId).ToListAsync();
            _context.NomCategories.RemoveRange(existNomCtgrs.ToList());

            List<NomCategory> nomCategories = new List<NomCategory>();
            foreach (var catId in categIds)
            {
                nomCategories.Add(new NomCategory { nom_id = nomId, category_id = catId });
            }
            _context.NomCategories.AddRange(nomCategories);
        }

        private async Task AddNomUsers(List<Guid> userIds, Guid nomId)
        {
            var existNomUsers = await _context.VisibleNomUsers.Where(x => x.nom_id == nomId).ToListAsync();
            _context.VisibleNomUsers.RemoveRange(existNomUsers.ToList());

            List<VisibleNomUser> nomUsers = new List<VisibleNomUser>();
            foreach (var usId in userIds)
            {
                nomUsers.Add(new VisibleNomUser { nom_id = nomId, user_id = usId });
            }
            _context.VisibleNomUsers.AddRange(nomUsers);
        }

        private async Task AddNomPhotos(List<string> photoPaths, Guid nomId)
        {
            List<NomPhoto> nomPhotos = new List<NomPhoto>();
            int lastNomPhotoOrder = await _context.NomPhotos
                .OrderByDescending(x => x.order)
                .Where(x => x.nom_id == nomId).Select(x => x.order).FirstOrDefaultAsync();
            foreach (var path in photoPaths)
            {
                lastNomPhotoOrder++;
                nomPhotos.Add(new NomPhoto { nom_id = nomId, photo_path = path, old_photo_path = path, order = lastNomPhotoOrder });
            }
            _context.NomPhotos.AddRange(nomPhotos);
        }

        public List<Dictionary<string, object>> GetProdCtlgs(Guid nomId, Guid ctlgId)
        {
            var productIds = _context.NotVisibleProdCtlgs.Where(x => x.nom_id == nomId && x.catalog_id == ctlgId)
                .Select(s => s.product_id).ToList();

            return _nomReadService.GetColorsSizesGrid(nomId, productIds);
        }

        public async Task AddVisProdCtlg(Guid nomId, Guid ctlgId, Guid prodId)
        {
            var prodCtlg = await _context.NotVisibleProdCtlgs.SingleOrDefaultAsync(x => x.nom_id == nomId && x.product_id == prodId && x.catalog_id == ctlgId);
            if (prodCtlg != null)
            {
                _context.NotVisibleProdCtlgs.Remove(prodCtlg);
            }
            else
            {
                prodCtlg = new NotVisibleProdCtlg
                {
                    nom_id = nomId,
                    product_id = prodId,
                    catalog_id = ctlgId
                };
                _context.NotVisibleProdCtlgs.Add(prodCtlg);
            }
            await _context.SaveChangesAsync();
        }

        public List<Dictionary<string, object>> GetPrices(Guid nomId)
        {
            var productPrices = _context.NomProdPrices.Where(x => x.nom_id == nomId).ToList();
            var nomList = _nomReadService.GetCatalogModelGrid(nomId, new List<Guid>());

            var bindGrid = nomList
                .GroupJoin(productPrices,
                    nl => nl.ProductId,
                    pp => pp.product_id,
                    (nomList, productPrices) => new
                    {
                        nomList.ProductId,
                        nomList.Color,
                        nomList.Size,
                        Price = productPrices.Select(s => s.price).SingleOrDefault()
                    }).ToList();

            var sizes = bindGrid.Select(x => x.Size).Distinct().ToList();
            var groupColors = bindGrid.GroupBy(x => x.Color)
                .Select(x => new
                {
                    Сolor = x.Key,
                    Options = x.Select(d2 => new { d2.Size, d2.Price, d2.ProductId })
                }).ToList();

            var list = new List<Dictionary<string, object>>();
            foreach (var g in groupColors)
            {
                var dict = new Dictionary<string, object>
                {
                    ["Цвет"] = g.Сolor
                };
                foreach (var size in sizes)
                {
                    dict.Add(size, null);
                }
                foreach (var opt in g.Options)
                {
                    dict[opt.Size] = new OptAttributes(opt.ProductId, price: opt.Price);
                }
                list.Add(dict);
            }
            return list;
        }

        public async Task AddProdPrice(Guid nomId, Guid prodId, double price)
        {
            var prodPrice = await _context.NomProdPrices.SingleOrDefaultAsync(x => x.nom_id == nomId && x.product_id == prodId);
            if (prodPrice != null)
            {
                prodPrice.price = price;
                _context.NomProdPrices.Update(prodPrice);
            }
            else
            {
                prodPrice = new NomProdPrice
                {
                    nom_id = nomId,
                    product_id = prodId,
                    price = price
                };
                _context.NomProdPrices.Add(prodPrice);
            }
            await _context.SaveChangesAsync();
        }

        public List<Dictionary<string, object>> GetSHAmounts(Guid nomId)
        {
            return _nomReadService.GetSHAmountGrid(nomId);
        }

        public async Task<List<PhotoDTO>> GetNomPhotos(Guid nomId)
        {
            var colors = _nomReadService.GetColorsByNomId(nomId);
            var photos = await _context.NomPhotos.Where(x => x.nom_id == nomId).ToListAsync();
            return photos
                .GroupJoin(colors,
                    ph => ph.color_id,
                    cl => cl.Id,
                    (photos, colors) => new PhotoDTO
                    {
                        Id = photos.id,
                        PhotoPath = photos.photo_path,
                        OldPhotoPath = photos.old_photo_path,
                        Order = photos.order,
                        NomId = photos.nom_id,
                        ColorName = colors.Select(s => s.Name).SingleOrDefault()
                    }).ToList();
        }

        public async Task<OperationResult> AddColorPhoto(Guid photoId, Guid colorId)
        {
            NomPhoto photo = await _context.NomPhotos.FindAsync(photoId);
            if (colorId == Guid.Empty)
                photo.color_id = null;
            else
                photo.color_id = colorId;
            _context.NomPhotos.Update(photo);
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public async Task<OperationResult> RemovePhoto(Guid photoId)
        {
            NomPhoto photo = await _context.NomPhotos.FindAsync(photoId);
            _context.NomPhotos.Remove(photo);
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public async Task<OperationResult> AddMainPhotoToNomenclature(Guid nomId, string path)
        {
            if (nomId == Guid.Empty && string.IsNullOrEmpty(path))
                return OperationResult.Failure();

            var entity = await _context.Nomenclatures.Where(x => x.id == nomId)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            entity.image_path = path;
            _context.Nomenclatures.Update(entity);
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public List<CModelDTO> GetNomModelsByCatalog(Guid id)
        {
            var nomModels = _context.NomCatalogs.Where(x => x.catalog_id == id)
                .Select(x => new
                {
                    Id = x.Nomenclature.id,
                    Order = x.order,
                    Price = x.Nomenclature.default_price,
                    CreatedDate = x.Nomenclature.created_date,
                    IsVisible = x.Nomenclature.is_visible,
                    PhotoPath = x.Nomenclature.image_path,
                    Catalogs = x.Nomenclature.NomCatalogs.Select(x => x.Catalog.name).ToList(),
                    Categories = x.Nomenclature.NomCategories.Select(x => x.Category.name).ToList()
                }).ToList();
            var nomList = _nomReadService.GetCatalogModels();
            return nomList
                .Join(nomModels,
                    nl => nl.Id,
                    nm => nm.Id,
                    (nomList, nomModels) => new CModelDTO
                    {
                        Id = nomList.Id,
                        Articul = nomList.Articul,
                        ItemName = nomList.ItemName,
                        Sizes = nomList.Sizes,
                        Colors = nomList.Colors,
                        Order = nomModels.Order,
                        CatalogId = id,
                        IsVisible = nomModels.IsVisible,
                        ItemPrice = nomModels.Price,
                        CreatedDate = nomModels.CreatedDate,
                        PhotoPath = nomModels.PhotoPath,
                        Catalogs = nomModels.Catalogs,
                        Categories = nomModels.Categories
                    }).ToList();
        }

        public async Task<OperationResult> AddModelsCtlg(Guid id, List<Guid> modelIds)
        {
            var existNoms = await _context.NomCatalogs.Where(x => x.catalog_id == id).ToListAsync();
            _context.NomCatalogs.RemoveRange(existNoms.ToList());

            List<NomCatalog> nomCatalogs = new List<NomCatalog>();
            foreach (var nomId in modelIds)
            {
                nomCatalogs.Add(new NomCatalog { nom_id = nomId, catalog_id = id });
            }
            _context.NomCatalogs.AddRange(nomCatalogs);
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public List<ColorDTO> GetColorsByNomId(Guid nomId)
        {
            return _nomReadService.GetColorsByNomId(nomId);
        }

        public async Task<CModelDTO> GetCatalogModel(Guid id)
        {
            return await _nomReadService.GetCatalogModel(id);
        }

        public async Task UpdateNomCatalog(Guid id, Guid catId, int toPosition)
        {
            var entity = await _context.NomCatalogs.Where(x => x.nom_id == id && x.catalog_id == catId)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            entity.order = toPosition;
            _context.NomCatalogs.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<OperationResult> UpdateNomCatalogOrders(Guid id, Guid catId, int toPosition)
        {
            if (toPosition == 0)
                return OperationResult.Failure();

            var nomCatalogs = await _context.NomCatalogs.Where(x => x.catalog_id == catId && x.order >= toPosition)
                .OrderBy(x => x.order)
                .ToListAsync();
            int tempPosition = toPosition;
            foreach (var nomctlg in nomCatalogs)
            {
                if (nomctlg.nom_id == id)
                    nomctlg.order = toPosition;
                else
                    nomctlg.order = ++tempPosition;
            }
            _context.NomCatalogs.UpdateRange(nomCatalogs.ToList());
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public async Task UpdateNomPhotoPosition(Guid id, int toPosition)
        {
            var entity = await _context.NomPhotos.Where(x => x.id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            entity.order = toPosition;
            _context.NomPhotos.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<OperationResult> UpdateNomPhotoPath(Guid id, string path)
        {
            if (id == Guid.Empty && string.IsNullOrEmpty(path))
                return OperationResult.Failure();

            var entity = await _context.NomPhotos.Where(x => x.id == id)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            entity.photo_path = path;
            _context.NomPhotos.Update(entity);
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateNomPhotoOrders(Guid id, Guid nomId, int toPosition)
        {
            if (toPosition == 0)
                return OperationResult.Failure();

            var nomPhotos = await _context.NomPhotos.Where(x => x.nom_id == nomId && x.order >= toPosition)
                .OrderBy(x => x.order)
                .ToListAsync();
            int tempPosition = toPosition;
            foreach (var nomPhoto in nomPhotos)
            {
                if (nomPhoto.id == id)
                    nomPhoto.order = toPosition;
                else
                    nomPhoto.order = ++tempPosition;
            }
            _context.NomPhotos.UpdateRange(nomPhotos.ToList());
            await _context.SaveChangesAsync();
            return OperationResult.Success();
        }

        public async Task<double> GetProductPriceFromCatalogItemOrDefault(Guid prodId, Guid nomId) {
            double? price = await _nomReadService.GetProductPrice(prodId);
            if (price.HasValue)
                return price.Value;

            price = await GetDefaultNomPrice(nomId);
            return price ?? 0;
        }

        private async Task<double?> GetDefaultNomPrice(Guid nomId)
        {
            var nomModel = await _context.Nomenclatures.FindAsync(nomId);
            return nomModel?.default_price;
        }

        public async Task<double> GetProductPrice(Guid nomenclatureId, Guid productId) {
            var productPrice = await _context.NomProdPrices
                .Where(x => x.nom_id == nomenclatureId && x.product_id == productId)
                .FirstOrDefaultAsync();

            var price = productPrice?.price ?? await GetProductPriceFromCatalogItemOrDefault(productId, nomenclatureId);
            return price;
        }

        public async Task<ProductDetailsDTO> GetProductDetails(Guid nomId, Guid sizeId, Guid colorId, Guid userId, Guid catalogId, bool isSet)
        {
            var productDetails = await _nomReadService.GetProductIdByParams(nomId, sizeId, colorId, isSet);
            if (productDetails != null && userId != Guid.Empty)
            {
                double? productPrice = await GetProductPrice(nomId, productDetails.ProductId);
                double discount = await _discountService.GetDiscountValueAsync(catalogId, userId, nomId);
                productDetails.Price = productPrice + (discount * productDetails.PartsCount);

                var rate = await _currencyService.GetLastExchangeRate();
                double? productPriceRu = productDetails.Price * rate;
                productDetails.PriceRu = Math.Round(productPriceRu.Value, 2);
            }
            return productDetails;
        }

        public async Task<List<Guid>> GetNomIdsForSearch()
        {
            return await _context.Nomenclatures.Where(x => x.is_visible).Select(x => x.id).ToListAsync();
        }

        public async Task<List<Guid>> GetNomIdsByCatalog(Guid catalogId)
        {
            return await _context.NomCatalogs.Where(x => x.catalog_id == catalogId && x.Nomenclature.is_visible).Select(x => x.nom_id).ToListAsync();
        }

        public async Task<List<Guid>> GetNomIdsByUserFavorite(Guid userId)
        {
            return await _context.NomUserFavorites.Where(x => x.user_id == userId).Select(x => x.nom_id).ToListAsync();
        }

        public async Task<NomSearchDTO> GetListsForSearchByNomIds(List<Guid> nomIds)
        {
            var colors = await _nomReadService.GetColorsByListNomId(nomIds);
            var sizes = await _nomReadService.GetSizesByListNomId(nomIds);
            var sizeLines = await _nomReadService.GetSizeLinesByListNomId(nomIds);
            var categories = await _context.Categories.Where(x => x.is_visible)
                .OrderBy(x => x.order)
                .Select(x => new CategoryDTO {
                    Id = x.id,
                    Name = x.name
                }).ToListAsync();
            return new NomSearchDTO { Categories = categories, Colors = colors, Sizes = sizes, SizeLines = sizeLines };
        }

        public async Task<NomCatalogDTO> TakeNTopNomenclatureItemsFromVisibleCatalog(Guid? userId, int count) {
            var catalogs = _catalogRepository.QueryFindBy(x => x.is_visible)
                .Where(x => x.NomCatalogs.Any(c => c.Catalog.close_time > DateTimeOffset.UtcNow || c.Catalog.close_time == null));

            if (userId.HasValue) {
                catalogs = catalogs.Where(x => x.VisibleCatalogUsers.All(q => q.user_id != userId.Value));
            } else {
                catalogs = catalogs.Where(x => x.is_open);
            }

            var nomCat = await catalogs
                .Select(x => new NomCatalogDTO {
                    Catalog = new CatalogDTO {
                        Id = x.id,
                        Name = x.name,
                        Description = x.description,
                        Order = x.order,
                        IsSet = x.is_set,
                        IsOpen = x.is_open,
                        AdditionalDescription = x.additional_description,
                        Uri = x.uri,
                        OrderForm = x.order_form
                    },
                    NomModels = x.NomCatalogs.Where(n => n.Nomenclature.is_visible)
                        .OrderBy(n => n.order)
                        .Select(n => new NomModelDTO {
                            NomId = n.nom_id,
                            ImagePath = n.Nomenclature.image_path,
                            ImageAlternativeText = n.Nomenclature.image_alternative_text,
                            ItemName = n.Nomenclature.name,
                            Description = n.Nomenclature.description
                        }).ToList()
                })
                .OrderBy(x => x.Catalog.IsOpen)
                .ThenBy(x => x.Catalog.Order)
                .FirstOrDefaultAsync();

            if (nomCat.Catalog.OrderForm == (int) OrderFormType.InStock) {
                var nomenclatureList = new List<NomModelDTO>();
                var nomenclatureCount = 0;
                foreach (var nomenclature in nomCat.NomModels) {
                    if (nomenclatureCount >= count)
                        break;
                    if ((await GetProductIdsAsync(nomenclature.NomId, nomCat.Catalog.Id)).Any()) {
                        nomenclatureList.Add(nomenclature);
                        nomenclatureCount++;
                    }
                }

                nomCat.NomModels = nomenclatureList;
            } else {
                nomCat.NomModels = nomCat.NomModels.Take(count).ToList();
            }

            return nomCat;
        }
    }
}