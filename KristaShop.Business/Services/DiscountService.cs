﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataReadOnly.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using KristaShop.Business.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Business.Services {
    public class DiscountService : IDiscountService {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ICatalogService _catalogService;

        public DiscountService (IUnitOfWork uow, IMapper mapper, IUserService userService, ICatalogService catalogService) {
            _uow = uow;
            _mapper = mapper;
            _userService = userService;
            _catalogService = catalogService;
        }

        public async Task<List<CatalogDTO>> GetCatalogsWithDiscountAsync() {
            var catalogs = await _catalogService.GetCatalogs(true);
            return catalogs.Where(x => !x.IsDisableDiscount).ToList();
        }

        public async Task<List<UserClientDTO>> GetUsersWithDiscountAsync(UserSession user) {
            var discounts = await _uow.UserDiscounts.All.Where(x => x.IsActive && x.StartDate < x.EndDate && x.EndDate > DateTime.Now).ToListAsync();
            var userDiscounts = discounts.GroupBy(x => x.UserId).ToDictionary(x => x.Key, x => x.Min(m => m.DiscountPrice));
            var users = _userService.GetAllUsers(user);

            foreach (var item in users) {
                if (userDiscounts.ContainsKey(item.UserId)) {
                    item.DiscountPrice = userDiscounts[item.UserId];
                }
            }

            return users;
        }

        public async Task<DiscountDTO> GetDiscountAsync(Guid catalogId, Guid userId, Guid nomId) {
            var catalog = await _catalogService.GetCatalogDetails(catalogId, true, userId);
            if (catalog != null && !catalog.IsDisableDiscount) {
                var userDiscount = await _getUserDiscountPriceAsync(userId);
                if (userDiscount != null) {
                    return _mapper.Map<DiscountDTO>(userDiscount);
                }

                var nomenclatureDiscount = await _getNomDiscountPriceAsync(nomId, catalogId);
                if (nomenclatureDiscount != null) {
                    return _mapper.Map<DiscountDTO>(nomenclatureDiscount);
                }

                var catalogDiscount = await _getCatalogDiscountPriceAsync(catalogId);
                if (catalogDiscount != null) {
                    return _mapper.Map<DiscountDTO>(catalogDiscount);
                }
            }

            return new DiscountDTO(DiscountType.None);
        }

        public async Task<double> GetDiscountValueAsync(Guid catalogId, Guid userId, Guid nomId) {
            var discount = await GetDiscountAsync(catalogId, userId, nomId);
            return discount.DiscountPrice;
        }

        public async Task<List<DiscountDTO>> GetDiscountsAsync(Guid discountId, DiscountType discountType) {
            return discountType switch {
                DiscountType.Catalog => await _getCatalogDiscountsAsync(discountId),
                DiscountType.Nom => await _getNomenclatureDiscountsAsync(discountId),
                DiscountType.User => await _getUserDiscountsAsync(discountId),
                _ => null
                };
        }

        public async Task<DiscountDTO> GetDiscountDetailsAsync(Guid discountId, DiscountType discountType) {
            return discountType switch {
                DiscountType.Catalog => await _getCatalogDiscountAsync(discountId),
                DiscountType.Nom => await _getNomenclatureDiscountAsync(discountId),
                DiscountType.User => await _getUserDiscountDetailsAsync(discountId),
                _ => null
                };
        }

        public async Task<OperationResult> AddDiscountAsync(DiscountDTO dto) {
            return dto.Type switch {
                DiscountType.Catalog => await _addCatalogDiscountAsync(dto),
                DiscountType.Nom => await _addNomenclatureDiscountAsync(dto),
                DiscountType.User => await _addUserDiscountAsync(dto),
                _ => null
                };
        }

        public async Task<OperationResult> UpdateDiscountAsync(DiscountDTO dto) {
            return dto.Type switch
                {
                DiscountType.Catalog => await _updateCatalogDiscountAsync(dto),
                DiscountType.Nom => await _updateNomenclatureDiscountAsync(dto),
                DiscountType.User => await _updateUserDiscountAsync(dto),
                _ => null
                };
        }

        public async Task<OperationResult> RemoveDiscountAsync(Guid id, DiscountType discountType) {
            return discountType switch
                {
                DiscountType.Catalog => await _removeCatalogDiscountAsync(id),
                DiscountType.Nom => await _removeNomenclatureDiscountAsync(id),
                DiscountType.User => await _removeUserDiscountAsync(id),
                _ => null
                };
        }

        #region discount typed operations

        #region get active discount entity

        private async Task<CatalogDiscount> _getCatalogDiscountPriceAsync(Guid catalogId) {
            return await _uow.CatalogDiscounts.All.Where(x =>
                    x.CatalogId == catalogId && !x.Catalog.is_disable_discount && x.IsActive &&
                    x.StartDate < x.EndDate && x.EndDate > DateTime.Now)
                .OrderBy(x => Math.Abs(x.DiscountPrice))
                .FirstOrDefaultAsync();
        }

        private async Task<NomDiscount> _getNomDiscountPriceAsync(Guid nomId, Guid catalogId) {
            return await _uow.NomDiscounts.All.Where(x =>
                    x.NomenclatureId == nomId 
                    && x.IsActive && x.StartDate < x.EndDate && x.EndDate > DateTime.Now
                    && x.DiscountCatalogs.Any(c => c.CatalogId == catalogId))
                .OrderBy(x => Math.Abs(x.DiscountPrice))
                .FirstOrDefaultAsync();
        }

        private async Task<UserDiscount> _getUserDiscountPriceAsync(Guid userId) {
            return await _uow.UserDiscounts.All
                .Where(x => x.UserId == userId && x.IsActive && x.StartDate < x.EndDate && x.EndDate > DateTime.Now)
                .OrderBy(x => Math.Abs(x.DiscountPrice))
                .FirstOrDefaultAsync();
        }

        #endregion

        #region get discounts by parent id

        private async Task<List<DiscountDTO>> _getCatalogDiscountsAsync(Guid catalogId) {
            var discounts = await _uow.CatalogDiscounts.All.Where(x => x.CatalogId == catalogId)
                .ProjectTo<DiscountDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return discounts;
        }

        private async Task<List<DiscountDTO>> _getNomenclatureDiscountsAsync(Guid nomenclatureId) {
            var discounts = await _uow.NomDiscounts.All.Where(x => x.NomenclatureId == nomenclatureId)
                .ProjectTo<DiscountDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return discounts;
        }

        private async Task<List<DiscountDTO>> _getUserDiscountsAsync(Guid userId) {
            var discounts = await _uow.UserDiscounts.All.Where(x => x.UserId == userId)
                .ProjectTo<DiscountDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return discounts;
        }

        #endregion

        #region get discount by id

        private async Task<DiscountDTO> _getCatalogDiscountAsync(Guid discountId) {
            var discount = await _uow.CatalogDiscounts.GetByIdAsync(discountId);
            return _mapper.Map<DiscountDTO>(discount);
        }

        private async Task<DiscountDTO> _getNomenclatureDiscountAsync(Guid discountId) {
            var discount = await _uow.NomDiscounts.All
                .Include(x => x.DiscountCatalogs)
                .FirstOrDefaultAsync(x => x.Id == discountId);
            return _mapper.Map<DiscountDTO>(discount);
        }

        private async Task<DiscountDTO> _getUserDiscountDetailsAsync(Guid discountId) {
            var discount = await _uow.UserDiscounts.GetByIdAsync(discountId);
            return _mapper.Map<DiscountDTO>(discount);
        }

        #endregion

        #region add discount

        private async Task<OperationResult> _addCatalogDiscountAsync(DiscountDTO dto) {
            var entity = _mapper.Map<CatalogDiscount>(dto);
            await _uow.CatalogDiscounts.AddAsync(entity, true);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task<OperationResult> _addNomenclatureDiscountAsync(DiscountDTO dto) {
            var entity = _mapper.Map<NomDiscount>(dto);
            await _uow.NomDiscounts.AddAsync(entity, true);
            await _addNomenclatureDiscountCatalogsAsync(entity.Id, dto.CatalogIds);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task _addNomenclatureDiscountCatalogsAsync(Guid discountId, List<Guid> catalogIds) {
            var result = catalogIds.Select(x => new NomDiscountCatalog(discountId, x));
            await _uow.NomDiscountCatalogs.AddRangeAsync(result);
        }

        private async Task<OperationResult> _addUserDiscountAsync(DiscountDTO dto) {
            var entity = _mapper.Map<UserDiscount>(dto);
            await _uow.UserDiscounts.AddAsync(entity, true);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        #endregion

        #region update discount

        private async Task<OperationResult> _updateCatalogDiscountAsync(DiscountDTO dto) {
            var entity = await _uow.CatalogDiscounts.GetByIdAsync(dto.Id);
            if(entity == null) return OperationResult.Failure("Скидка на каталог не найдена");

            entity = _mapper.Map(dto, entity);
            _uow.CatalogDiscounts.Update(entity);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task<OperationResult> _updateNomenclatureDiscountAsync(DiscountDTO dto) {
            var entity = await _uow.NomDiscounts.GetByIdAsync(dto.Id);
            if(entity == null) return OperationResult.Failure("Скидка на номенклатуру не найдена");

            entity = _mapper.Map(dto, entity);
            _uow.NomDiscounts.Update(entity);

            await _removeNomenclatureDiscountCatalogsAsync(entity.Id);
            await _addNomenclatureDiscountCatalogsAsync(entity.Id, dto.CatalogIds);

            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task<OperationResult> _updateUserDiscountAsync(DiscountDTO dto) {
            var entity = await _uow.UserDiscounts.GetByIdAsync(dto.Id);
            if (entity == null) return OperationResult.Failure("Скидка пользователя не найдена");

            entity = _mapper.Map(dto, entity);
            _uow.UserDiscounts.Update(entity);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }
        
        #endregion

        #region delete discount

        private async Task<OperationResult> _removeCatalogDiscountAsync(Guid id) {
            await _uow.CatalogDiscounts.DeleteAsync(id);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task<OperationResult> _removeNomenclatureDiscountAsync(Guid id) {
            await _uow.NomDiscounts.DeleteAsync(id);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        private async Task _removeNomenclatureDiscountCatalogsAsync(Guid id) {
            var discountCatalogs = await _uow.NomDiscountCatalogs.All.Where(x => x.DiscountId == id).ToListAsync();
            _uow.NomDiscountCatalogs.DeleteRange(discountCatalogs);
        }

        private async Task<OperationResult> _removeUserDiscountAsync(Guid id) {
            await _uow.UserDiscounts.DeleteAsync(id);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        #endregion

        #endregion
    }
}