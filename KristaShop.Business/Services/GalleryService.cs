﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Extensions;

namespace KristaShop.Business.Services
{
    public class GalleryService : IGalleryService
    {
        private readonly ICacheRepository<GalleryItem> _galleryRepo;
        private readonly IMapper _mapper;

        public GalleryService
            (ICacheRepository<GalleryItem> galleryRepo, IMapper mapper)
        {
            _galleryRepo = galleryRepo;
            _mapper = mapper;
        }

        public async Task<List<GalleryItemDTO>> GetGallerysForFront(int quantity)
        {
            return _mapper.Map<List<GalleryItemDTO>>(await _galleryRepo.Query().OrderBy(x => x.Order).Take(quantity).ToListAsync());
        }

        public async Task<List<GalleryItemDTO>> GetTopNRandomItems(int quantity) {
            var result = await _galleryRepo.Query()
                .OrderBy(x => CustomMysqlFunctions.Random())
                .Take(quantity)
                .ToListAsync();

            return _mapper.Map<List<GalleryItemDTO>>(result);
        }

        public async Task<IPager<GalleryItemDTO>> GetPaginationGallerys(int page, int modelInPage)
        {
            return await _mapper.ProjectTo<GalleryItemDTO>(_galleryRepo.Query().OrderBy(x => x.Order)).ToPagerListAsync(page, modelInPage);
        }

        public async Task<List<GalleryItemDTO>> GetGallerys()
        {
            return _mapper.Map<List<GalleryItemDTO>>(await _galleryRepo.GetAllAsync());
        }

        public async Task<GalleryItemDTO> GetGalleryDetails(Guid id)
        {
            var entity = await _galleryRepo.FindByIdAsync(id);
            return _mapper.Map<GalleryItemDTO>(entity);
        }

        public async Task<GalleryItemDTO> GetGalleryDetailsNoTrack(Guid id)
        {
            var entity = await _galleryRepo.QueryFindBy(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<GalleryItemDTO>(entity);
        }

        public async Task<OperationResult> InsertGallery(GalleryItemDTO dto)
        {
            var entity = _mapper.Map<GalleryItem>(dto);
            var lastOrder = _galleryRepo.Query().OrderByDescending(s => s.Order)
                         .FirstOrDefault()?.Order;
            if (lastOrder == null)
                entity.Order = 1;
            else
                entity.Order = lastOrder.Value + 1;
            entity.Id = Guid.NewGuid();
            if (await _galleryRepo.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateGallery(GalleryItemDTO dto)
        {
            var entity = _mapper.Map<GalleryItem>(dto);
            var existGallery = await _galleryRepo.FindByIdAsync(dto.Id);
            existGallery.ImagePath = entity.ImagePath ?? existGallery.ImagePath;
            existGallery.IsVisible = entity.IsVisible;
            existGallery.Title = entity.Title;
            existGallery.Description = entity.Description;
            existGallery.LinkText = entity.LinkText;
            existGallery.Link = entity.Link;
            existGallery.Order = entity.Order;
            if (await _galleryRepo.UpdateAsync(existGallery) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteGallery(Guid id)
        {
            var entity = await _galleryRepo.FindByIdAsync(id);
            await _galleryRepo.RemoveAsync(entity);
            return OperationResult.Success();
        }
    }
}