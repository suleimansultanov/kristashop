﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using KristaShop.Business.UnitOfWork;

namespace KristaShop.Business.Services {
    public class MenuContentService : IMenuContentService {
        private readonly IMapper _mapper;
        private readonly IDynamicPagesManager _manager;
        private readonly IUnitOfWork _uow;
        private readonly IFileService _fileService;

        public MenuContentService(IMapper mapper, IUnitOfWork uow, IFileService fileService, IDynamicPagesManager manager) { 
            _mapper = mapper;
            _uow = uow;
            _fileService = fileService;
            _manager = manager;
        }

        public async Task<List<MenuContentDTO>> GetMenusContentAsync(bool openOnly = false, bool visibleInMenuOnly = false) {
            return await _uow.MenuContent.GetAllOrderedOpenAndVisibleInMenuOnly(openOnly, visibleInMenuOnly)
                .AsNoTracking()
                .ProjectTo<MenuContentDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<MenuContentDTO>> GetMenusContentByControllerAsync(string controller, bool openOnly = false, bool visibleInMenuOnly = false) {
            return await _uow.MenuContent.GetAllOrderedOpenAndVisibleInMenuOnly(openOnly, visibleInMenuOnly)
                .Where(x => EF.Functions.Like(x.Url, $"%{controller}/%"))
                .ProjectTo<MenuContentDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<MenuContentDTO>> GetMenusContentByUrlsAsync(List<string> urls) {
            return await _uow.MenuContent.AllOrdered
                .Where(x => urls.Contains(x.Url))
                .ProjectTo<MenuContentDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<MenuContentDTO> GetMenuContentByIdAsync(Guid id) {
            return _mapper.Map<MenuContentDTO>(await _uow.MenuContent.GetByIdAsync(id));
        }

        public async Task<MenuContentDTO> GetMenuContentByIdNoTrackAsync(Guid id) {
            return _mapper.Map<MenuContentDTO>(await _uow.MenuContent.All.Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync());
        }

        public async Task<MenuContentDTO> GetMenuContentByUrlAsync(string url) {
            return await _uow.MenuContent.All
                .Where(x => x.Url.Equals(url))
                .ProjectTo<MenuContentDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<OperationResult> InsertMenuContentAsync(MenuContentDTO menuContent) {
            var entity = _mapper.Map<MenuContent>(menuContent);
            entity.Order = await _uow.MenuContent.GetNewOrderValueAsync();

            if (menuContent.TitleIcon != null) {
                entity.TitleIconPath = await _fileService.SaveFileAsync(menuContent.TitleIcon);
                if (string.IsNullOrEmpty(entity.TitleIconPath)) {
                    return _fileService.GetLastError();
                }
            }

            await _uow.MenuContent.AddAsync(entity);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            await _manager.ReloadAsync(entity.Id);
            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateMenuContentAsync(MenuContentDTO menuContent) {
            var entity = await _uow.MenuContent.GetByIdAsync(menuContent.Id);
            var oldUrl = entity.Url;

            entity = _mapper.Map(menuContent, entity);

            if (menuContent.TitleIcon != null) {
                if (!string.IsNullOrEmpty(entity.TitleIconPath)) {
                    if (!_fileService.RemoveFile(menuContent.TitleIcon.FilesDirectoryPath, entity.TitleIconPath)) {
                        return _fileService.GetLastError();
                    }
                }

                entity.TitleIconPath = await _fileService.SaveFileAsync(menuContent.TitleIcon);
                if (string.IsNullOrEmpty(entity.TitleIconPath)) {
                    return _fileService.GetLastError();
                }
            }

            _uow.MenuContent.Update(entity);
            if (!await _uow.SaveAsync())
                return OperationResult.Failure();

            await _manager.ReloadAsync(entity.Id, oldUrl.Equals(entity.Url) ? string.Empty : oldUrl);
            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteMenuContentAsync(Guid id, string fileDirectoryPath) {
            var entity = await _uow.MenuContent.GetByIdAsync(id);

            if (!string.IsNullOrEmpty(entity.TitleIconPath)) {
                if (!_fileService.RemoveFile(fileDirectoryPath, entity.TitleIconPath)) {
                    return _fileService.GetLastError();
                }
            }

            _uow.MenuContent.Delete(entity);
            await _restoreMenuContentOrderFromPositionAsync(entity.Order);

            if (!await _uow.SaveInTransactionAsync()) {
                return OperationResult.Failure();
            }

            await _manager.ReloadAsync(entity.Id, entity.Url);
            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateMenuContentOrderAsync(Guid id, int newOrder) {
            var entity = await _uow.MenuContent.GetByIdAsync(id);
            entity.Order = newOrder;

            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            await _manager.ReloadAsync(entity.Id);
            return OperationResult.Success();
        }

        public async Task<OperationResult> RestoreMenuContentOrderAsync() {
            await _restoreMenuContentOrderFromPositionAsync(0);

            if (!await _uow.SaveInTransactionAsync()) {
                return OperationResult.Failure();
            }

            await _manager.ReloadAsync();
            return OperationResult.Success();
        }

        private async Task _restoreMenuContentOrderFromPositionAsync(int orderPosition) {
            var items = await _uow.MenuContent.All.Where(x => x.Order > orderPosition).ToListAsync();
            var order = orderPosition - 1;
            foreach (var item in items) {
                item.Order = ++order;
            }

            _uow.MenuContent.UpdateRange(items);
        }
    }
}