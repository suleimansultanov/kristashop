﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using Microsoft.EntityFrameworkCore;

namespace KristaShop.Business.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IShopRepository<Feedback> _feedbackRepository;
        private readonly IShopRepository<FeedbackFile> _feedbackFiles;
        private readonly IMapper _mapper;
        private readonly IFileService _fileService;

        public FeedbackService(IShopRepository<Feedback> feedbackRepository, IShopRepository<FeedbackFile> feedbackFiles,
            IMapper mapper, IFileService fileService)
        {
            _feedbackRepository = feedbackRepository;
            _feedbackFiles = feedbackFiles;
            _mapper = mapper;
            _fileService = fileService;
        }

        public async Task<List<FeedbackDTO>> GetFeedbackListAsync() {
            return await _feedbackRepository.Query()
                .Select(x => new FeedbackDTO {
                    Id = x.Id,
                    Phone = x.Phone,
                    Person = x.Person,
                    Message = x.Message,
                    Email = x.Email,
                    Viewed = x.Viewed,
                    RecordTimeStamp = x.RecordTimeStamp,
                    FormattedDate = x.RecordTimeStamp.ToSystemString(),
                    Type = x.Type,
                    FeedbackType = x.Type.ToReadableString(),
                    FilesCount = x.Files.Count
                })
                .OrderBy(x => x.Viewed)
                .ThenByDescending(x => x.RecordTimeStamp)
                .ToListAsync();
        }

        public async Task<FeedbackDTO> GetFeedbackDetails(Guid id)
        {
            var entity = await _feedbackRepository.FindByIdAsync(id);
            return _mapper.Map<FeedbackDTO>(entity);
        }

        public async Task<OperationResult> InsertFeedback(FeedbackDTO dto)
        {
            var entity = _mapper.Map<Feedback>(dto);
            if (await _feedbackRepository.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> InsertFeedbackWithFileAsync(FeedbackDTO feedback, FeedbackCreateFileDTO createFile) {
            string filepath = string.Empty;
            try {
                createFile.OriginalName = createFile.OriginalName.ToValidFileName();
                filepath = await _fileService.SaveFileAsync(createFile);
                if (string.IsNullOrEmpty(filepath)) {
                    return OperationResult.Failure("Не удалось сохранить файл");
                }

                var feedbackEntity = _mapper.Map<Feedback>(feedback);
                if (await _feedbackRepository.AddAsync(feedbackEntity) == null) {
                    return OperationResult.Failure("Не удалось сохранить сообщение обратной связи");
                }

                var fileEntity = _mapper.Map<FeedbackFile>(createFile);
                fileEntity.ParentId = feedbackEntity.Id;
                fileEntity.VirtualPath = filepath;
                if (await _feedbackFiles.AddAsync(fileEntity) == null) {
                    return OperationResult.Failure("Не удалось сохранить данные файла обратной связи");
                }

                return OperationResult.Success();
            } catch (Exception ex) {
                _fileService.RemoveFile(createFile.FilesDirectoryPath, filepath);
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<OperationResult> UpdateFeedback(Guid id, Guid userId) {
            var entity = await _feedbackRepository.FindByIdAsync(id);
            entity.Viewed = true;
            entity.ViewTimeStamp = DateTime.Now;
            entity.ReviewerUserId = userId;
            if (await _feedbackRepository.UpdateAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<List<FeedbackFileDTO>> GetFilesByFeedbackIdAsync(Guid id) {
            var files = await _feedbackFiles.QueryFindBy(x => x.ParentId == id).ToListAsync();
            return _mapper.Map<List<FeedbackFileDTO>>(files);
        }

        public async Task<FeedbackFileDTO> GetFileAsync(Guid id) {
            var file = await _feedbackFiles.FindByIdAsync(id);
            if (file != null) {
                return _mapper.Map<FeedbackFileDTO>(file);
            }

            return null;
        }
    }
}