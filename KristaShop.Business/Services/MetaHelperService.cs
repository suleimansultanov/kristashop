﻿using KristaShop.Business.Interfaces;
using System;
using System.Text;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class MetaHelperService : IMetaHelperService
    {
        private readonly IMenuContentService _menuContentService;
        private StringBuilder _sb = new StringBuilder();

        public MetaHelperService(IMenuContentService menuContentService)
        {
            _menuContentService = menuContentService;
        }

        public async Task<string> GetMetaInfoAsync(string urlSegment)
        {
            var content = await _menuContentService.GetMenuContentByUrlAsync(urlSegment);

            if (content == null)
                return string.Empty;

            _sb.Append("<meta name='title' content='").Append(content.Title).Append("'/>");
            _sb.Append(Environment.NewLine);
            _sb.Append("<meta name='description' content='").Append(content.MetaDescription).Append("'/>");
            _sb.Append(Environment.NewLine);
            _sb.Append("<meta name='keywords' content ='").Append(content.MetaKeywords).Append("'/>");
            string metaTag = _sb.ToString();
            _sb = null;
            return metaTag;
        }
    }
}