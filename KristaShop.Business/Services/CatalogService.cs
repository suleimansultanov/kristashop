﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Enums;
using KristaShop.Common.Extensions;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly ICacheRepository<Catalog> _catRepo;
        private readonly IShopRepository<VisibleUserCatalog> _visCtlgRepo;
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;

        public CatalogService
            (ICacheRepository<Catalog> catRepo, IMapper mapper, IShopRepository<VisibleUserCatalog> visCtlgRepo, IFileService fileService)
        {
            _catRepo = catRepo;
            _mapper = mapper;
            _visCtlgRepo = visCtlgRepo;
            _fileService = fileService;
        }

        public async Task<Guid[]> GetNonVisUserCatalogs(Guid userId)
        {
            return await _visCtlgRepo.QueryFindBy(x => x.user_id == userId).Select(x => x.catalog_id).ToArrayAsync();
        }

        public async Task<List<UserCatalogsDTO>> GetAllCatalogsWithNonVisibleCheckAsync(Guid userId) {
            return await _catRepo.Query().Select(x => new UserCatalogsDTO {
                Id = x.id,
                Name = x.name,
                IsBlockedForUser = x.VisibleCatalogUsers.Any(c => c.user_id == userId)
            }).ToListAsync();
        }

        public async Task<OperationResult> AddAllNonVisCatalogUsers(Guid ctlgId, List<Guid> userIds)
        {
            var existVisCtlgs = await _visCtlgRepo.QueryFindBy(x => x.catalog_id == ctlgId).ToListAsync();
            await _visCtlgRepo.RemoveRangeAsync(existVisCtlgs);

            List<VisibleUserCatalog> visibleCatalogUsers = new List<VisibleUserCatalog>();
            foreach (var userId in userIds)
            {
                visibleCatalogUsers.Add(new VisibleUserCatalog { user_id = userId, catalog_id = ctlgId });
            }
            await _visCtlgRepo.AddRangeAsync(visibleCatalogUsers);
            return OperationResult.Success();
        }

        public async Task<OperationResult> RemoveAllNonVisCatalogUsers(Guid ctlgId)
        {
            var existVisCtlgs = await _visCtlgRepo.QueryFindBy(x => x.catalog_id == ctlgId).ToListAsync();
            await _visCtlgRepo.RemoveRangeAsync(existVisCtlgs);
            return OperationResult.Success();
        }

        public async Task<OperationResult> AddNonVisUserAllCatalogs(Guid userId)
        {
            var existVisCtgrs = await _visCtlgRepo.QueryFindBy(x => x.user_id == userId).ToListAsync();
            await _visCtlgRepo.RemoveRangeAsync(existVisCtgrs);

            List<VisibleUserCatalog> visibleCatalogUsers = new List<VisibleUserCatalog>();
            var ctlgIds = await _catRepo.Query().Where(x => !x.is_open).Select(x => x.id).ToListAsync();
            foreach (var catId in ctlgIds)
            {
                visibleCatalogUsers.Add(new VisibleUserCatalog { user_id = userId, catalog_id = catId });
            }
            await _visCtlgRepo.AddRangeAsync(visibleCatalogUsers);
            return OperationResult.Success();
        }

        public async Task<OperationResult> AddNonVisUserCatalogs(Guid userId, List<Guid> ctlgIds)
        {
            var existVisCtgrs = await _visCtlgRepo.QueryFindBy(x => x.user_id == userId).ToListAsync();
            await _visCtlgRepo.RemoveRangeAsync(existVisCtgrs);

            List<VisibleUserCatalog> visibleCatalogUsers = new List<VisibleUserCatalog>();
            foreach (var catId in ctlgIds)
            {
                visibleCatalogUsers.Add(new VisibleUserCatalog { user_id = userId, catalog_id = catId });
            }
            await _visCtlgRepo.AddRangeAsync(visibleCatalogUsers);
            return OperationResult.Success();
        }

        public async Task<List<CatalogDTO>> GetCatalogsByNomId(Guid nomId)
        {
            var ctlgs = await _catRepo.QueryFindBy(x => x.NomCatalogs.Any(n => n.nom_id == nomId)).ToListAsync();
            return _mapper.Map<List<CatalogDTO>>(ctlgs);
        }

        public async Task<NomCatalogDTO> GetCatalogNomenclatures(Guid? userId) {
            var catalogs = _catRepo.QueryFindBy(x => x.is_visible)
                .Where(x => (x.close_time != null && x.close_time > DateTimeOffset.UtcNow) || x.close_time == null);
            if (userId.HasValue) {
                catalogs = catalogs.Where(x => !x.is_open && x.VisibleCatalogUsers.All(q => q.user_id != userId.Value));
            } else {
                catalogs = catalogs.Where(x => x.is_open);
            }

            var nomCat = await catalogs
                .Select(x => new NomCatalogDTO
                {
                    Catalog = new CatalogDTO
                    {
                        Id = x.id,
                        Name = x.name,
                        Description = x.description,
                        Order = x.order,
                        IsSet = x.is_set,
                        IsOpen = x.is_open,
                        AdditionalDescription = x.additional_description,
                        Uri = x.uri
                    },
                    NomModels = x.NomCatalogs.Where(n => n.Nomenclature.is_visible)
                        .OrderBy(n => n.order)
                        .Select(n => new NomModelDTO {
                            NomId = n.nom_id,
                            ImagePath = n.Nomenclature.image_path,
                            ImageAlternativeText = n.Nomenclature.image_alternative_text,
                            ItemName = n.Nomenclature.name,
                            Description = n.Nomenclature.description
                        }).Take(6).ToList()
                })
                .OrderBy(x => x.Catalog.Order)
                .FirstOrDefaultAsync();
            return nomCat;
        }

        public async Task<Dictionary<Guid, string>> GetDictionaryUserCatalogs()
        {
            var userCatalogs = await _visCtlgRepo.Query().Select(x => new
            {
                UserId = x.user_id,
                CatalogName = x.Catalog.name
            }).ToListAsync();
            var dict = userCatalogs
                .GroupBy(x => x.UserId)
                .ToDictionary(x => x.Key, x => string.Join(", ", x.Select(x => x.CatalogName).ToList()));
            return dict;
        }

        public async Task<List<CatalogDTO>> GetCatalogsByUser(Guid? userId)
        {
            var catalogs = _catRepo.QueryFindBy(x => x.is_visible)
                .Where(x => (x.close_time != null && x.close_time > DateTimeOffset.UtcNow) || x.close_time == null);

            if (userId.HasValue)
                catalogs = catalogs.Where(x => !x.VisibleCatalogUsers.Any(q => q.user_id == userId.Value));
            else
                catalogs = catalogs.Where(x => x.is_open);


            var result = await catalogs
                .Select(s => new CatalogDTO
                {
                    Id = s.id,
                    Name = s.name,
                    Uri = s.uri,
                    OrderFormName = EnumExtensions<OrderFormType>.GetDisplayValue((OrderFormType)s.order_form),
                    Order = s.order,
                    IsVisible = s.is_visible,
                    IsDisableDiscount = s.is_disable_discount,
                    NomCount = s.NomCatalogs.Count,
                    AdditionalDescription = s.additional_description,
                    IsOpen = s.is_open
                }).OrderBy(x => x.Order).ToListAsync();
            if (result.Any(x => !x.IsOpen)) {
                return result.Where(x => !x.IsOpen).ToList();
            }

            return result;
        }

        public async Task<List<CatalogDTO>> GetCloseCatalogs()
        {
            var catalogs = _catRepo.QueryFindBy(x => !x.is_open);
            return await catalogs
                .Select(s => new CatalogDTO
                {
                    Id = s.id,
                    Name = s.name,
                    Uri = s.uri,
                    OrderFormName = EnumExtensions<OrderFormType>.GetDisplayValue((OrderFormType)s.order_form),
                    Order = s.order,
                    IsVisible = s.is_visible,
                    IsDisableDiscount = s.is_disable_discount,
                    NomCount = s.NomCatalogs.Count
                }).ToListAsync();
        }

        public async Task<List<CatalogDTO>> GetCatalogs(bool includeDiscounts = false)
        {
            var catalogs = _catRepo.Query();
            return await catalogs
                .Select(s => new CatalogDTO
                {
                    Id = s.id,
                    Name = s.name,
                    Uri = s.uri,
                    OrderFormName = EnumExtensions<OrderFormType>.GetDisplayValue((OrderFormType)s.order_form),
                    Order = s.order,
                    IsVisible = s.is_visible,
                    IsDisableDiscount = s.is_disable_discount,
                    NomCount = s.NomCatalogs.Count,
                    DiscountPrice = includeDiscounts ? s.CatalogDiscounts.OrderBy(x => Math.Abs(x.DiscountPrice)).First(x => x.IsActive).DiscountPrice : 0
                }).ToListAsync();
        }

        public async Task<CatalogDTO> GetCatalogDetailsNoTrack(Guid id)
        {
            var entity = await _catRepo.QueryFindBy(x => x.id == id).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<CatalogDTO>(entity);
        }

        public async Task<CatalogDTO> GetCatalogDetails(Guid id)
        {
            var entity = await _catRepo.FindByIdAsync(id);
            return _mapper.Map<CatalogDTO>(entity);
        }

        public async Task<CatalogDTO> GetCatalogDetails(Guid id, bool excludeClosed, Guid userId)
        {
            var request = _catRepo.QueryFindBy(x => x.id == id);

            if (userId != Guid.Empty) {
                request = request.Where(x => x.VisibleCatalogUsers.All(q => q.user_id != userId));
            }

            var entity = await request.FirstOrDefaultAsync();

            if (entity != null && excludeClosed && !entity.is_open
                && entity.order_form == (int) OrderFormType.Preorder
                && entity.close_time != null
                && entity.close_time < DateTimeOffset.UtcNow) {
                return null;
            }

            return _mapper.Map<CatalogDTO>(entity);
        }

        public async Task<CatalogDTO> GetCatalogDetailsByUri(string uri, bool excludeClosed = true, Guid userId = default)
        {
            var request = _catRepo.QueryFindBy(x => x.uri == uri);

            if (userId != Guid.Empty) {
                request = request.Where(x => x.VisibleCatalogUsers.All(q => q.user_id != userId));
            }

            var catalog = await request.FirstOrDefaultAsync();
            if (catalog == null || catalog.HasClosedByTime() || (!catalog.is_visible && excludeClosed)) {
                return null;
            }

            return _mapper.Map<CatalogDTO>(catalog);
        }

        public async Task<OperationResult> InsertCatalog(CatalogDTO dto)
        {
            var entity = _mapper.Map<Catalog>(dto);
            var lastOrder = _catRepo.Query().OrderByDescending(s => s.order)
                         .FirstOrDefault()?.order;
            if (lastOrder == null)
                entity.order = 1;
            else
                entity.order = lastOrder.Value + 1;
            entity.id = Guid.NewGuid();
            entity.is_visible = true;

            if (dto.Preview != null) {
                entity.preview_path = await _fileService.SaveFileAsync(dto.Preview);
                if (string.IsNullOrEmpty(entity.preview_path)) {
                    return _fileService.GetLastError();
                }
            }

            if (await _catRepo.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateCatalog(CatalogDTO dto)
        {
            var entity = _mapper.Map<Catalog>(dto);

            if (dto.Preview != null) {
                if (!string.IsNullOrEmpty(entity.preview_path)) {
                    if (!_fileService.RemoveFile(dto.Preview.FilesDirectoryPath, entity.preview_path)) {
                        return _fileService.GetLastError();
                    }
                }

                entity.preview_path = await _fileService.SaveFileAsync(dto.Preview);
                if (string.IsNullOrEmpty(entity.preview_path)) {
                    return _fileService.GetLastError();
                }
            }

            if (await _catRepo.UpdateAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteCatalog(Guid id, string filesDirectoryPath)
        {
            var entity = await _catRepo.FindByIdAsync(id);
            _fileService.RemoveFile(filesDirectoryPath, entity.preview_path);
            await _catRepo.RemoveAsync(entity);
            return OperationResult.Success();
        }
    }
}