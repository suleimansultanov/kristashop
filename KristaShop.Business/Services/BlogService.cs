﻿using AutoMapper;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KristaShop.Business.Services
{
    public class BlogService : IBlogService
    {
        private readonly ICacheRepository<BlogItem> _blogRepo;
        private readonly IMapper _mapper;

        public BlogService
            (ICacheRepository<BlogItem> blogRepo, IMapper mapper)
        {
            _blogRepo = blogRepo;
            _mapper = mapper;
        }

        public async Task<List<BlogItemDTO>> GetBlogsForFront()
        {
            return _mapper.Map<List<BlogItemDTO>>(await _blogRepo.Query().OrderBy(x => x.Order).Take(4).ToListAsync());
        }

        public async Task<IPager<BlogItemDTO>> GetPaginationBlogs(int page, int modelInPage)
        {
            return await _mapper.ProjectTo<BlogItemDTO>(_blogRepo.Query().OrderBy(x => x.Order)).ToPagerListAsync(page, modelInPage);
        }

        public async Task<List<BlogItemDTO>> GetBlogs()
        {
            return _mapper.Map<List<BlogItemDTO>>(await _blogRepo.GetAllAsync());
        }

        public async Task<BlogItemDTO> GetBlogDetails(Guid id)
        {
            var blog = await _blogRepo.FindByIdAsync(id);
            return _mapper.Map<BlogItemDTO>(blog);
        }

        public async Task<BlogItemDTO> GetBlogDetailsNoTrack(Guid id)
        {
            var entity = await _blogRepo.QueryFindBy(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
            return _mapper.Map<BlogItemDTO>(entity);
        }

        public async Task<OperationResult> InsertBlog(BlogItemDTO dto)
        {
            var entity = _mapper.Map<BlogItem>(dto);
            var lastOrder = _blogRepo.Query().OrderByDescending(s => s.Order)
                         .FirstOrDefault()?.Order;
            if (lastOrder == null)
                entity.Order = 1;
            else
                entity.Order = lastOrder.Value + 1;
            entity.Id = Guid.NewGuid();
            if (await _blogRepo.AddAsync(entity) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> UpdateBlog(BlogItemDTO dto)
        {
            var blog = _mapper.Map<BlogItem>(dto);
            var existBlog = await _blogRepo.FindByIdAsync(dto.Id);
            existBlog.ImagePath = blog.ImagePath ?? existBlog.ImagePath;
            existBlog.IsVisible = blog.IsVisible;
            existBlog.LinkText = blog.LinkText;
            existBlog.Link = blog.Link;
            existBlog.Title = blog.Title;
            existBlog.Description = blog.Description;
            existBlog.Order = blog.Order;
            if (await _blogRepo.UpdateAsync(existBlog) == null)
                return OperationResult.Failure();

            return OperationResult.Success();
        }

        public async Task<OperationResult> DeleteBlog(Guid id)
        {
            var blog = await _blogRepo.FindByIdAsync(id);
            await _blogRepo.RemoveAsync(blog);
            return OperationResult.Success();
        }
    }
}