﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using KristaShop.Business.DTOs;
using KristaShop.Business.Interfaces;
using KristaShop.Business.Interfaces.Core;
using KristaShop.Business.UnitOfWork;
using KristaShop.Common.Models;
using KristaShop.DataAccess.Entities;
using KristaShop.DataReadOnly.DTOs;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace KristaShop.Business.Services {
    public class UserService : IUserService {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IBasicUserService _basicUser;
        private readonly ILogger _logger;

        public UserService(IUnitOfWork uow, IMapper mapper, IBasicUserService basicUser, ILogger logger) {
            _uow = uow;
            _mapper = mapper;
            _basicUser = basicUser;
            _logger = logger;
        }

        public async Task<List<UserDataDTO>> GetUsersDataAsync() {
            return await _uow.UserData.All
                .ProjectTo<UserDataDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<UserClientDTO>> GetUsersWithDataAsync(UserSession user) {
            var result = await _basicUser.GetAllUsersAsync(user);

            var data = (await GetUsersDataAsync()).ToDictionary(k => k.UserId, v => v);
            foreach (var item in result) {
                item.LastSignIn = data.ContainsKey(item.UserId) ? data[item.UserId].LastSignIn : DateTimeOffset.MinValue;
            }

            return result;
        }

        public async Task<OperationResult> CreateUserData(UserDataDTO userData) {
            var entity = _mapper.Map<UserData>(userData);

            await _uow.UserData.AddAsync(entity);
            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        protected async Task<OperationResult> _logSignInAsync(Guid userId) {
            var userData = await _uow.UserData.GetByIdAsync(userId);
            if (userData == null) { 
                return await _createNewUserData(userId, true);
            }

            userData.LastSignIn = DateTimeOffset.Now;
            _uow.UserData.Update(userData);

            if (!await _uow.SaveAsync()) {
                return OperationResult.Failure();
            }

            return OperationResult.Success();
        }

        protected async Task<OperationResult> _logSignInAsync(string login) {
            var user = await _basicUser.GetUserAsync(login);
            return await _logSignInAsync(user.UserId);
        }

        protected async Task<OperationResult> _createNewUserData(Guid userId, bool setLastLogin) {
            var userDataDto = new UserDataDTO {
                UserId = userId,
                LastSignIn = setLastLogin ? DateTimeOffset.Now : DateTimeOffset.MinValue
            };

            return await CreateUserData(userDataDto);
        }

        #region BasicUserService

        public async Task<bool> IsActiveUserAsync(Guid userId) {
            return await _basicUser.IsActiveUserAsync(userId);
        }

        public async Task<OperationResult> SignIn(string login, string pass, bool isPersistent = false, bool isBackend = true) {
             var result =  await _basicUser.SignIn(login, pass, isPersistent, isBackend);

             if (result.IsSuccess) {
                 try {
                    await _logSignInAsync(login);
                 } catch (Exception ex) {
                     _logger.Error(ex, "Failed to log user sign in by login {login}. {message}", login, ex.Message);
                 }
             }
             
             return result;
        }

        public async Task<OperationResult> SignInByLink(AuthorizationLinkDTO link) {
            var result =  await _basicUser.SignInByLink(link);

            if (result.IsSuccess) {
                try {
                    await _logSignInAsync(link.UserId);
                } catch (Exception ex) {
                    _logger.Error(ex, "Failed to log user sign in {userId}. {message}", link.UserId, ex.Message);
                }
            }

            return result;
        }

        public async Task SignOut(bool isBackend = true) {
            await _basicUser.SignOut(isBackend);
        }

        public Task<bool> ValidatePasswordAsync(Guid userId, string password) {
            return _basicUser.ValidatePasswordAsync(userId, password);
        }

        public List<UserClientDTO> GetAllUsers(UserSession user) {
            return _basicUser.GetAllUsers(user);
        }

        public Task<List<UserClientDTO>> GetAllUsersAsync(UserSession user) {
            return _basicUser.GetAllUsersAsync(user);
        }

        public Task<UserClientDTO> GetUserAsync(Guid id) {
            return _basicUser.GetUserAsync(id);
        }

        public Task<UserClientDTO> GetUserAsync(string login) {
            return _basicUser.GetUserAsync(login);
        }

        public Task<CounterpartyDTO> GetCounterpartyDetailsAsync(Guid id) {
            return _basicUser.GetCounterpartyDetailsAsync(id);
        }

        public Task<int> GetManagerNewUsersCountAsync(Guid managerId, int currentUserAcl) {
            return _basicUser.GetManagerNewUsersCountAsync(managerId, currentUserAcl);
        }

        #endregion
    }
}
