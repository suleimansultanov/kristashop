﻿using KristaShop.Business.Interfaces;
using KristaShop.Common.Extensions;
using KristaShop.Common.Models;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using KristaShop.Common.Models.ApiResponses;
using Newtonsoft.Json;
using Serilog;

namespace KristaShop.Business.Clients {
    public class AsupApiClient<T> : IAsupApiClient<T> where T : class {
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;
        private readonly UrlSetting _urlSetting;

        public AsupApiClient(HttpClient httpClient, IOptions<UrlSetting> options, ILogger logger) {
            httpClient.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            _httpClient = httpClient;
            _logger = logger;
            _urlSetting = options.Value;
        }

        public async Task<OperationResult> SendDataAsync(T data, string urlPath) {
            try {
                _httpClient.BaseAddress = new Uri($"{_urlSetting.KristaWebApiUrl}/{urlPath}");
                var response = await _httpClient.PostAsJsonAsync(data);
                return await response.Content.ReadAsJsonAsync<OperationResult>();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to send data. path: {path}, {@data}. {message}", _httpClient.BaseAddress.ToString(), data, ex.Message);
                return OperationResult.Failure();
            }
        }

        public async Task<RegisterResponse> RegisterAsync<TModel>(TModel data) {
            try {
                var hash = JsonConvert.SerializeObject(data).ComputeSha256Hash();
                _httpClient.BaseAddress = new Uri($"{_urlSetting.KristaWebApiUrl}/{_urlSetting.KristaWebApiRegister}?hash={hash}");
                var response = await _httpClient.PostAsJsonAsync(data);
                return await response.Content.ReadAsJsonAsync<RegisterResponse>();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to register user. path: {path}, {@data}. {message}", _httpClient.BaseAddress.ToString(), data, ex.Message);
                return new RegisterResponse(HttpStatusCode.InternalServerError, Guid.Empty, ex.Message, "Произошла ошибка во время выполнения операции");
            }
        }

        public async Task<BaseResponse> UpdateUserAsync<TModel>(TModel data) {
            try {
                var hash = JsonConvert.SerializeObject(data).ComputeSha256Hash();
                _httpClient.BaseAddress = new Uri($"{_urlSetting.KristaWebApiUrl}/{_urlSetting.KristaWebApiUpdateUser}?hash={hash}");
                var response = await _httpClient.PostAsJsonAsync(data);
                return await response.Content.ReadAsJsonAsync<BaseResponse>();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to register user. path: {path}, {@data}. {message}", _httpClient.BaseAddress.ToString(), data, ex.Message);
                return new RegisterResponse(HttpStatusCode.InternalServerError, Guid.Empty, ex.Message, "Произошла ошибка во время выполнения операции");
            }
        }

        public async Task<BaseResponse> ActivateUserAsync(Guid userId) {
            try {
                var hash = userId.ToString().ComputeSha256Hash();
                _httpClient.BaseAddress = new Uri($"{_urlSetting.KristaWebApiUrl}/{_urlSetting.KristaWebApiActivateUser}?userId={userId}&hash={hash}");
                var response = await _httpClient.PostAsync();
                return await response.Content.ReadAsJsonAsync<BaseResponse>();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to register user. path: {path}, userId: {userId}. {message}", _httpClient.BaseAddress.ToString(), userId, ex.Message);
                return new BaseResponse(HttpStatusCode.InternalServerError, ex.Message, "Произошла ошибка во время выполнения операции");
            }
        }

        public async Task<BaseResponse> ChangePasswordAsync<TModel>(TModel data) {
            try {
                var hash = JsonConvert.SerializeObject(data).ComputeSha256Hash();
                _httpClient.BaseAddress = new Uri($"{_urlSetting.KristaWebApiUrl}/{_urlSetting.KristaWebApiChangePassword}?hash={hash}");
                var response = await _httpClient.PostAsJsonAsync(data);
                return await response.Content.ReadAsJsonAsync<BaseResponse>();
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to change user's password. path: {path}, {@data}. {message}", _httpClient.BaseAddress.ToString(), data, ex.Message);
                return new BaseResponse(HttpStatusCode.InternalServerError, ex.Message, "Произошла ошибка во время выполнения операции");
            }
        }

        public async Task<byte[]> GetFileDataAsync(string virtualFilePath, string virtualFileName) {
            try {
                var uri = new Uri($"{_urlSetting.FileServiceDownloadEndpoint}{virtualFilePath}/{Uri.EscapeDataString(virtualFileName)}");
                var response = await _httpClient.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK) {
                    return await response.Content.ReadAsByteArrayAsync();
                }
                return null;
            } catch (Exception ex) {
                _logger.Error(ex, "Failed to get file path: {path} name: {name}. {message}", virtualFilePath, virtualFileName, ex.Message);
                return null;
            }
        }
    }
}