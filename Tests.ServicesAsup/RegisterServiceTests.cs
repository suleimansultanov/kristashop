using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KristaShop.Common.Enums;
using KristaShop.Common.Exceptions;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.Models;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.DTOs.Mappings;
using KristaShop.ServicesAsup.Implementation.Services;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Tests.Common;
using Tests.Common.Stubs;

namespace Tests.ServicesAsup {
    public class Tests {
        private IUnitOfWork _uow;
        private KristaAsupDbContext _context;
        private IRegisterService _registerService;

        [SetUp]
        public void Setup() {
            _context = InMemory.InitializeAsupDbDev();
            _initializeDbContextData();

            _uow = new UnitOfWork(_context, null);
            _registerService = new RegisterService(_uow, _initializeMapper(), new EmailServiceStub(), new SerilogLoggerStub());
        }

        #region initialization

        private static IMapper _initializeMapper() {
            var configuration = new MapperConfiguration(cfg => { cfg.AddProfile<CounterpartyMappingProfile>(); });
            return configuration.CreateMapper();
        }

        private void _initializeDbContextData() {
            _context.Cities.AddRange(new List<City> {
                new City(new Guid("eb5c2b4b-5010-46b4-b1b7-b9fc9385b759"), "������"),
                new City(new Guid("da2b016e-07d8-4a53-84dd-f4c5c7c55bb6"), "�����-���������")
            });

            _context.SaveChanges();
        }

        private static CounterpartyDTO _initializeFullCounterpartyDtoWithCityId() {
            var newCounterparty = new CounterpartyDTO {
                Person = "��������� ������",
                CityId = new Guid("eb5c2b4b-5010-46b4-b1b7-b9fc9385b759"),
                NewCity = string.Empty,
                CompanyAddress = "������, ��. ����������",
                Email = "pushkin@mail.ru",
                MallAddress = "������, ��. ����������, �� ������",
                Phone = "+79912341933",
                UserId = new Guid("7d33272b-ef8a-4ead-8baa-c73cfede9c4e")
            };
            return newCounterparty;
        }

        private static CounterpartyDTO _initializeFullCounterpartyDtoWithNonExistentCityId() {
            var newCounterparty = new CounterpartyDTO {
                Person = "��������� ������",
                CityId = new Guid("795abc81-4748-40f1-ab2a-63d7b20ba7b5"),
                NewCity = string.Empty,
                CompanyAddress = "������, ��. ����������",
                Email = "pushkin@mail.ru",
                MallAddress = "������, ��. ����������, �� ������",
                Phone = "+79912341933",
                UserId = new Guid("7d33272b-ef8a-4ead-8baa-c73cfede9c4e")
            };
            return newCounterparty;
        }

        private static CounterpartyDTO _initializeFullCounterpartyDtoWithCityName() {
            var newCounterparty = new CounterpartyDTO {
                Person = "��������� ������",
                CityId = Guid.Empty,
                NewCity = "���������",
                CompanyAddress = "������, ��. ����������",
                Email = "pushkin@mail.ru",
                MallAddress = "������, ��. ����������, �� ������",
                Phone = "+79912341933",
                UserId = new Guid("7d33272b-ef8a-4ead-8baa-c73cfede9c4e")
            };
            return newCounterparty;
        }
        
        private async Task _initializeOneManagerWithRate1Async() {
            var managerRate = new ManagerRate {UserId = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Rate = 1};
            var manager = new User {
                Id = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Acl = 20, Email = "manager@email.test", IsRoot = false,
                Login = "manager"
            };

            await _context.Users.AddAsync(manager);
            await _context.ManagerRates.AddAsync(managerRate);
        }

        private async Task _initialize3ManagersWithSameRates() {
            var managerRates = new List<ManagerRate> {
                new ManagerRate {UserId = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Rate = 0.333},
                new ManagerRate {UserId = new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), Rate = 0.333},
                new ManagerRate {UserId = new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231"), Rate = 0.333}
            };

            var managers = new List<User> {
                new User {Id = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Acl = 20, Email = "manager1@email.test", IsRoot = false, Login = "manager1"},
                new User {Id = new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), Acl = 20, Email = "manager2@email.test", IsRoot = false, Login = "manager2"},
                new User {Id = new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231"), Acl = 20, Email = "manager3@email.test", IsRoot = false, Login = "manager3"}
            };

            await _context.Users.AddRangeAsync(managers);
            await _context.ManagerRates.AddRangeAsync(managerRates);
        }

        private async Task _initialize3ManagersWithDifferentRates() {
            var managerRates = new List<ManagerRate> {
                new ManagerRate {UserId = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Rate = 0.5},
                new ManagerRate {UserId = new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), Rate = 0.25},
                new ManagerRate {UserId = new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231"), Rate = 0.25}
            };

            var managers = new List<User> {
                new User {Id = new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), Acl = 20, Email = "manager1@email.test", IsRoot = false, Login = "manager1"},
                new User {Id = new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), Acl = 20, Email = "manager2@email.test", IsRoot = false, Login = "manager2"},
                new User {Id = new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231"), Acl = 20, Email = "manager3@email.test", IsRoot = false, Login = "manager3"}
            };

            await _context.Users.AddRangeAsync(managers);
            await _context.ManagerRates.AddRangeAsync(managerRates);
        }

        private async Task _initializeAclsAndUserGroupsForClientAndManagerAsync() {
            var acls = new List<AccessControl> {
                new AccessControl {Id = new Guid("69e0ae8c-65bc-463a-8de4-84c2aabf00a7"), Acl = 10, AclName = "������"},
                new AccessControl {Id = new Guid("d3d3a490-d72a-483a-8163-2005b791d919"), Acl = 20, AclName = "��������"},
            };
            _context.AccessControls.AddRange(acls);

            var groups = new List<UserGroup> {
                new UserGroup {
                    Id = new Guid("d29bfacb-6b85-41f5-a9ca-33b5d38e1b05"),
                    AclId = new Guid("69e0ae8c-65bc-463a-8de4-84c2aabf00a7"), Type = UserType.Customer, GroupName = "�������"
                },
                new UserGroup {
                    Id = new Guid("a9768b03-3cfa-4e65-b9f5-53961e409ad6"),
                    AclId = new Guid("eadfa5e1-d3fc-47bb-abe9-44e39bead900"), Type = UserType.Manager, GroupName = "���������"
                }
            };
            await _context.UserGroups.AddRangeAsync(groups);
        }

        #endregion

        [TearDown]
        public void Dispose() {
            _uow.Dispose();
            _context = null;
        }

        [Test]
        public void When_RegisterNewUser_CounterpartyWithNameAndPhoneAlreadyExists_Then_ThrowsEntityAlreadyExists() {
            _context.Counterparties.Add(new Counterparty {Title = "��������� ������", Person = "��������� ������", Phone = "+79912341933"});
            _context.SaveChanges();

            var newCounterparty = _initializeFullCounterpartyDtoWithCityName();

            AsyncTestDelegate register = () =>_registerService.RegisterAsync(newCounterparty);
            Assert.That(register, Throws.TypeOf<EntityAlreadyExistsException>());
        }

        [Test]
        public void When_RegisterNewUser_CityIdNotExistsAndNewCityNotProvided_Then_ThrowsEntityNotFound() {
            var newCounterparty = _initializeFullCounterpartyDtoWithNonExistentCityId();

            AsyncTestDelegate register = () =>_registerService.RegisterAsync(newCounterparty);
            Assert.That(register, Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public async Task When_RegisterNewUser_UserWithLoginAlreadyExist_Then_EntityAlreadyExistException() {
            _context.Users.Add(new User {
                Id = new Guid("e68c2551-7b0e-4231-9edc-73dcaa8f57c5"),
                Login = "Aleksandr79912341933",
                RegistrationDate = DateTime.Now,
                Acl = 10,
                BanReason = "",
                CounterpartyId = null,
                Status = UserStatus.Await
            });
            await _context.SaveChangesAsync();
            
            var newCounterparty = _initializeFullCounterpartyDtoWithCityId();

            AsyncTestDelegate register = () =>_registerService.RegisterAsync(newCounterparty);
            Assert.That(register, Throws.TypeOf<EntityAlreadyExistsException>());
        }

        
        [Test]
        public void When_RegisterNewUser_ManagersListIsEmpty_Then_ThrowsManagerRateNotFound() {
            var newCounterparty = _initializeFullCounterpartyDtoWithCityId();

            AsyncTestDelegate register = () =>_registerService.RegisterAsync(newCounterparty);
            Assert.That(register, Throws.TypeOf<ManagerRateNotFoundException>());
        }

        [Test]
        public async Task When_RegisterNewUser_CustomerGroupNotExist_Then_ThrowsUserGroupNotFound() {

            await _initializeOneManagerWithRate1Async();
            _context.SaveChanges();

            var newCounterparty = _initializeFullCounterpartyDtoWithCityName();

            AsyncTestDelegate register = () =>_registerService.RegisterAsync(newCounterparty);
            Assert.That(register, Throws.TypeOf<UserGroupNotFoundException>());
        }

        
        [Test]
        public async Task When_RegisterNewUser_AllDataCorrectWithCityId_Then_NewUserCreated() {
            await _initializeAclsAndUserGroupsForClientAndManagerAsync();
            await _initializeOneManagerWithRate1Async();
            await _context.SaveChangesAsync();

            var newCounterparty = _initializeFullCounterpartyDtoWithCityId();
            var result = await _registerService.RegisterAsync(newCounterparty);

            var user = await _context.Users
                .Include(x => x.Counterparty)
                .Include(x=>x.UserGroupMemberships)
                .FirstOrDefaultAsync(x => x.Login == "Aleksandr79912341933");

            Assert.AreEqual(true, result);
            Assert.NotNull(user);
            Assert.AreEqual(UserStatus.Await, user.Status);
            Assert.AreNotEqual(Guid.Empty, user.Id);
            Assert.AreNotEqual("", user.Password);
            Assert.AreNotEqual(Guid.Empty, user.CounterpartyId);
            Assert.AreEqual(10, user.Acl);

            Assert.AreEqual("��������� ������", user.Counterparty.Person);
            Assert.AreEqual("��������� ������", user.Counterparty.Title);
            Assert.AreEqual("+79912341933", user.Counterparty.Phone);
            Assert.AreEqual("������, ��. ����������, �� ������", user.Counterparty.MallAddress);
            Assert.AreEqual("������, ��. ����������", user.Counterparty.CompanyAddress);
            Assert.AreEqual("pushkin@mail.ru", user.Counterparty.Email);
            Assert.AreEqual(new Guid("eb5c2b4b-5010-46b4-b1b7-b9fc9385b759"), user.Counterparty.CityId);
            Assert.AreEqual(new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), user.Counterparty.ManagerId);

            var isInCustomerGroupOnly = user.UserGroupMemberships.All(x => x.GroupId == new Guid("d29bfacb-6b85-41f5-a9ca-33b5d38e1b05"));
            Assert.AreEqual(true, isInCustomerGroupOnly);
        }

        [Test]
        public async Task When_RegisterNewUser_AllDataCorrectWithNewCity_Then_NewUserCreated() {
            await _initializeAclsAndUserGroupsForClientAndManagerAsync();
            await _initializeOneManagerWithRate1Async();
            await _context.SaveChangesAsync();

            var newCounterparty = _initializeFullCounterpartyDtoWithCityName();
            var result = await _registerService.RegisterAsync(newCounterparty);

            var user = await _context.Users
                .Include(x => x.Counterparty)
                .Include(x=>x.UserGroupMemberships)
                .FirstOrDefaultAsync(x => x.Login == "Aleksandr79912341933");

            Assert.AreEqual(true, result);
            Assert.NotNull(user);
            Assert.AreEqual(UserStatus.Await, user.Status);
            Assert.AreNotEqual(Guid.Empty, user.Id);
            Assert.AreNotEqual("", user.Password);
            Assert.AreNotEqual(Guid.Empty, user.CounterpartyId);
            Assert.AreEqual(10, user.Acl);

            Assert.AreEqual("��������� ������", user.Counterparty.Person);
            Assert.AreEqual("��������� ������", user.Counterparty.Title);
            Assert.AreEqual("+79912341933", user.Counterparty.Phone);
            Assert.AreEqual("������, ��. ����������, �� ������", user.Counterparty.MallAddress);
            Assert.AreEqual("������, ��. ����������", user.Counterparty.CompanyAddress);
            Assert.AreEqual("pushkin@mail.ru", user.Counterparty.Email);
            Assert.AreEqual(new Guid("00000000-0000-0000-0000-000000000000"), user.Counterparty.CityId);
            Assert.AreEqual("���������", user.Counterparty.NewCity);
            Assert.AreEqual(new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), user.Counterparty.ManagerId);

            var isInCustomerGroupOnly = user.UserGroupMemberships.All(x => x.GroupId == new Guid("d29bfacb-6b85-41f5-a9ca-33b5d38e1b05"));
            Assert.AreEqual(true, isInCustomerGroupOnly);
        }

        [Test]
        public async Task When_RegisterUsers_WithSeveralManagersWithSameRates_Then_UsersHaveCorrectManager() {
            await _initializeAclsAndUserGroupsForClientAndManagerAsync();
            await _initialize3ManagersWithSameRates();

            await _context.SaveChangesAsync();

            int counter = 0;
            var answers = new[] {new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231")};
            for (var i = 0; i < 101; i++) {
                var newCounterparty = _initializeFullCounterpartyDtoWithCityId();
                newCounterparty.UserId = Guid.NewGuid();

                newCounterparty.Phone = (i + 1).ToString();
                var result = await _registerService.RegisterAsync(newCounterparty);

                var user = await _context.Users
                    .Include(x => x.Counterparty)
                    .FirstOrDefaultAsync(x => x.Login == $"Aleksandr{i + 1}");

                Assert.AreEqual(user.Counterparty.ManagerId, answers[counter], $"Iteration #{i + 1}");

                if ((i + 1) % 3 == 0) {
                    counter = 0;
                } else {
                    counter++;
                }
            }
        }

        [Test]
        public async Task When_RegisterUsers_WithSeveralManagersWithDifferentRates_Then_UsersHaveCorrectManager() {
            await _initializeAclsAndUserGroupsForClientAndManagerAsync();
            await _initialize3ManagersWithDifferentRates();

            await _context.SaveChangesAsync();

            int counter = 0;
            var answers = new[] {new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), new Guid("5583175d-4ff3-4dc6-87d8-2dce972128ed"), new Guid("89b9c4cb-1755-463e-9cc8-e191ded6a20a"), new Guid("b25fd885-a7d7-48ec-9528-3b95ad873231")};
            for (var i = 0; i < 101; i++) {
                var newCounterparty = _initializeFullCounterpartyDtoWithCityId();
                newCounterparty.UserId = Guid.NewGuid();

                newCounterparty.Phone = (i + 1).ToString();
                var result = await _registerService.RegisterAsync(newCounterparty);

                var user = await _context.Users
                    .Include(x => x.Counterparty)
                    .FirstOrDefaultAsync(x => x.Login == $"Aleksandr{i + 1}");

                Assert.AreEqual(user.Counterparty.ManagerId, answers[counter], $"Iteration #{i + 1}");

                if ((i + 1) % 4 == 0) {
                    counter = 0;
                } else {
                    counter++;
                }
            }
        }
    }
}