﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KristaShop.Common.Enums;
using KristaShop.Common.Exceptions;
using KristaShop.Common.Helpers;
using KristaShop.DataReadOnly.Domain;
using KristaShop.DataReadOnly.Models;
using KristaShop.ServicesAsup.DTOs;
using KristaShop.ServicesAsup.Implementation.Services;
using KristaShop.ServicesAsup.Interfaces.Services;
using KristaShop.ServicesAsup.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Tests.Common;
using Tests.Common.Stubs;


namespace Tests.ServicesAsup {
    public class UserServiceTests {
        private IUnitOfWork _uow;
        private KristaAsupDbContext _context;
        private IUserService _userService;

        [SetUp]
        public async Task Setup() {
            _context = InMemory.InitializeAsupDbDev();
            await _initializeDbContextDataAsync();

            _uow = new UnitOfWork(_context, null);
            _userService = new UserService(_uow, new RegisterServiceStub());
        }

        [TearDown]
        public void Dispose() {
            _uow.Dispose();
            _context = null;
        }

        #region Initialization

        private async Task _initializeDbContextDataAsync() {
            var users = new List<User> {
                new User { Id = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08"), Acl = 10, IsRoot = false, Login = "user1", Status = UserStatus.Await},
                new User { Id = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84"), Acl = 10, IsRoot = false, Login = "user2", Status = UserStatus.None},
                new User { Id = new Guid("4d095ba6-cdca-4495-a51a-59ad94faff9c"), Acl = 10, IsRoot = false, Login = "user3", Status = UserStatus.Banned, BanExpireDate = DateTime.Now.AddDays(7), BanReason = "Test ban"},
                new User { Id = new Guid("b781f1c5-9879-48a1-9916-7e589fd408fa"), Acl = 10, IsRoot = false, Login = "user4", Status = UserStatus.Deleted},
                new User { Id = new Guid("b6359236-2f42-4297-ab1d-d429d7d48c8f"), Acl = 10, IsRoot = false, Login = "user5", Status = UserStatus.Await, CounterpartyId = new Guid("4d41925b-8738-407f-b61e-e2f56b009778") },
                new User { Id = new Guid("083c611c-bdba-452c-b137-2fce1acb8cd0"), Acl = 10, IsRoot = false, Login = "user6", Status = UserStatus.Banned, BanExpireDate = DateTime.Now.AddDays(7), BanReason = "Test ban", CounterpartyId = new Guid("336ecd06-9448-40a2-94e6-7442a1547c88") },
                new User { Id = new Guid("a6b251ad-4040-448f-80d0-af177a0f6776"), Acl = 10, IsRoot = false, Login = "user7", Status = UserStatus.Deleted}
            };

            var counterparties = new List<Counterparty> {
                new Counterparty { Id =  new Guid("4d41925b-8738-407f-b61e-e2f56b009778"), Title = "Александр Пушкин", Type = 0, Person = "Александр Пушкин", Phone="+79912341233", Email = "pushkin@email.test", MallAddress = "Москва, ул. Московская, ТЦ Москва", CompanyAddress = "Москва, ул. Московская", NewCity = "Москва"},
                new Counterparty { Id =  new Guid("336ecd06-9448-40a2-94e6-7442a1547c88"), Title = "Михаил Булгаков", Type = 0, Person = "Михаил Булгаков", Phone="+79987987875", Email = "bulgakov1232@email.test", MallAddress = "ТЦ Город", CompanyAddress = "Санкт-Петербург, ул. Ленинградская", NewCity = "Санкт-Петербург"}
            };

            await _context.Counterparties.AddRangeAsync(counterparties);
            await _context.Users.AddRangeAsync(users);
            await _context.SaveChangesAsync();
        }

        #endregion

        #region ActivateUser tests

        [Test]
        public void When_ActivateUser_UserNotExist_Then_ThrowsEntityNotFoundException() {
            AsyncTestDelegate activate = () => _userService.ActivateUserAsync(new Guid("fd0666ea-564b-4ec1-8856-664f5cc2ed0d"));
            Assert.That(activate, Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public async Task When_ActivateUser_UserExistWithStatusAwait_Then_UserStatusChangedToActive() { 
           var userId = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08");
           var actual = await _userService.ActivateUserAsync(userId);

           var user = await _context.Users.FindAsync(userId);

           Assert.AreEqual(true, actual);
           Assert.AreEqual("user1", user.Login);
           Assert.AreEqual(UserStatus.Active, user.Status);
           Assert.IsNull(user.BanExpireDate);
        }

        [Test]
        public async Task When_ActivateUser_UserExistWithStatusNone_Then_UserStatusChangedToActive() { 
            var userId = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84");
            var actual = await _userService.ActivateUserAsync(userId);

            var user = await _context.Users.FindAsync(userId);

            Assert.AreEqual(true, actual);
            Assert.AreEqual("user2", user.Login);
            Assert.AreEqual(UserStatus.Active, user.Status);
            Assert.IsNull(user.BanExpireDate);
        }

        [Test]
        public async Task When_ActivateUser_UserExistWithStatusBanned_Then_UserStatusChangedToActive() { 
            var userId = new Guid("4d095ba6-cdca-4495-a51a-59ad94faff9c");
            var actual = await _userService.ActivateUserAsync(userId);

            var user = await _context.Users.FindAsync(userId);

            Assert.AreEqual(true, actual);
            Assert.AreEqual("user3", user.Login);
            Assert.AreEqual(UserStatus.Active, user.Status);
            Assert.AreEqual(string.Empty, user.BanReason);
            Assert.IsNull(user.BanExpireDate);
        }

        [Test]
        public async Task When_ActivateUser_UserExistWithStatusDeleted_Then_ThrowsEntityUpdateException() {
            var userId = new Guid("b781f1c5-9879-48a1-9916-7e589fd408fa");
            AsyncTestDelegate activate = () => _userService.ActivateUserAsync(userId);
            Assert.That(activate, Throws.TypeOf<EntityUpdateException>());

            var user = await _context.Users.FindAsync(userId);
            
            Assert.AreEqual("user4", user.Login);
            Assert.AreEqual(UserStatus.Deleted, user.Status);
        }

        #endregion

        #region BanUser tests

        [Test]
        public void When_BanUser_UserNotExist_Then_ThrowsEntityNotFoundException() {
            var banModel = new BanUserDTO(new Guid("fd0666ea-564b-4ec1-8856-664f5cc2ed0d"), "Test reason", DateTime.Now.AddDays(7));
            AsyncTestDelegate activate = () => _userService.BanUserAsync(banModel);
            Assert.That(activate, Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public async Task When_BanUser_UserExistWithStatusAwait_Then_UserStatusChangedToBanned() {
            var now = DateTime.Now.AddDays(7);
            var userId = new Guid("ada014f0-5102-465d-af09-dc94eff1ba08");

            var banModel = new BanUserDTO(userId, "Test reason", now);
            var actual = await _userService.BanUserAsync(banModel);

            var user = await _context.Users.FindAsync(userId);

            Assert.AreEqual(true, actual);
            Assert.AreEqual("user1", user.Login);
            Assert.AreEqual(UserStatus.Banned, user.Status);
            Assert.AreEqual(now, user.BanExpireDate);
            Assert.AreEqual("Test reason", user.BanReason);
        }
        
        [Test]
        public async Task When_BanUser_UserExistWithStatusNone_Then_UserStatusChangedToBanned() {
            var now = DateTime.Now.AddDays(7);
            var userId = new Guid("43e0b5e1-9fd0-4ae4-ba74-e351c6b47e84");

            var banModel = new BanUserDTO(userId, "Test reason", now);
            var actual = await _userService.BanUserAsync(banModel);

            var user = await _context.Users.FindAsync(userId);

            Assert.AreEqual(true, actual);
            Assert.AreEqual("user2", user.Login);
            Assert.AreEqual(UserStatus.Banned, user.Status);
            Assert.AreEqual(now, user.BanExpireDate);
            Assert.AreEqual("Test reason", user.BanReason);
        }

        [Test]
        public async Task When_BanUser_UserExistWithStatusBanned_Then_UserStatusChangedToBanned() { 
            var now = DateTime.Now.AddDays(15);
            var userId = new Guid("4d095ba6-cdca-4495-a51a-59ad94faff9c");

            var banModel = new BanUserDTO(userId, "Test reason", now);
            var actual = await _userService.BanUserAsync(banModel);

            var user = await _context.Users.FindAsync(userId);

            Assert.AreEqual(true, actual);
            Assert.AreEqual("user3", user.Login);
            Assert.AreEqual(UserStatus.Banned, user.Status);
            Assert.AreEqual(now, user.BanExpireDate);
            Assert.AreEqual("Test reason", user.BanReason);
        }

        [Test]
        public async Task When_BanUser_UserExistWithStatusDeleted_Then_ThrowsEntityUpdateException() {
            var userId = new Guid("b781f1c5-9879-48a1-9916-7e589fd408fa");
            var banModel = new BanUserDTO(userId, "Test reason", DateTime.Now.AddDays(7));
            AsyncTestDelegate activate = () => _userService.BanUserAsync(banModel);
            Assert.That(activate, Throws.TypeOf<EntityUpdateException>());

            var user = await _context.Users.FindAsync(userId);
            
            Assert.AreEqual("user4", user.Login);
            Assert.AreEqual(UserStatus.Deleted, user.Status);
        }

        #endregion

        #region UpdateUser tests

        [Test]
        public void When_UpdateUser_UserNotExist_Then_ThrowsEntityNotFoundException() {
            var userModel = new UpdateUserDTO {Id = new Guid("fd0666ea-564b-4ec1-8856-664f5cc2ed0d")};
            AsyncTestDelegate updateUser = () => _userService.UpdateUserAsync(userModel);
            Assert.That(updateUser, Throws.TypeOf<EntityNotFoundException>());
        }

        [Test]
        public async Task When_UpdateUser_UserWithSameLoginAlreadyExist_Then_ThrowsEntityAlreadyExistException() {
            _context.Users.Add(new User {
                Id = new Guid("e68c2551-7b0e-4231-9edc-73dcaa8f57c5"),
                Login = "Aleksandr79912341933",
                RegistrationDate = DateTime.Now,
                Acl = 10,
                BanReason = "",
                CounterpartyId = null,
                Status = UserStatus.Await
            });
            await _context.SaveChangesAsync();

            var userModel = new UpdateUserDTO {Id = new Guid("fd0666ea-564b-4ec1-8856-664f5cc2ed0d"), Login = "Aleksandr79912341933"};
            AsyncTestDelegate updateUser = () => _userService.UpdateUserAsync(userModel);
            Assert.That(updateUser, Throws.TypeOf<EntityAlreadyExistsException>());
        }

        [Test]
        public async Task When_UpdateUser_UserStatusAwait_AllDataCorrect_Then_UserDataUpdatedStatusActive() {
            
            var userId = new Guid("b6359236-2f42-4297-ab1d-d429d7d48c8f");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var userModel = new UpdateUserDTO {Id = userId, Login = "user5NewLogin", Password = password, Person = "Александр Сергеевич Пушкин", Phone = "+79900000015", Email = "pushkin.a@mail.test", MallAddress = "ТЦ Москва", CompanyAddress = "Москва, ул. Центральная", Status = UserStatus.Active};

            var actual = await _userService.UpdateUserAsync(userModel);
            Assert.AreEqual(true, actual);

            var user = await _uow.Users.All
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == userId);

            Assert.AreEqual("user5NewLogin", user.Login);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(UserStatus.Active, user.Status);
            Assert.AreEqual("Александр Сергеевич Пушкин", user.Counterparty.Person);
            Assert.AreEqual("+79900000015", user.Counterparty.Phone);
            Assert.AreEqual("pushkin.a@mail.test", user.Counterparty.Email);
            Assert.AreEqual("ТЦ Москва", user.Counterparty.MallAddress);
            Assert.AreEqual("Москва, ул. Центральная", user.Counterparty.CompanyAddress);
        }

        [Test]
        public async Task When_UpdateUser_UserStatusAwait_AllDataCorrect_Then_UserDataUpdatedStatusBanned() {
            
            var userId = new Guid("b6359236-2f42-4297-ab1d-d429d7d48c8f");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var now = DateTime.Now.AddDays(7);
            var userModel = new UpdateUserDTO {Id = userId, Login = "user5NewLogin", Password = password, Person = "Александр Сергеевич Пушкин", Phone = "+79900000015", Email = "pushkin.a@mail.test", MallAddress = "ТЦ Москва", CompanyAddress = "Москва, ул. Центральная", Status = UserStatus.Banned, BanReason = "Test reason", BanExpireDate = now};

            var actual = await _userService.UpdateUserAsync(userModel);
            Assert.AreEqual(true, actual);

            var user = await _uow.Users.All
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == userId);

            Assert.AreEqual("user5NewLogin", user.Login);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(UserStatus.Banned, user.Status);
            Assert.AreEqual("Test reason", user.BanReason);
            Assert.AreEqual(now, user.BanExpireDate);

            Assert.AreEqual("Александр Сергеевич Пушкин", user.Counterparty.Person);
            Assert.AreEqual("+79900000015", user.Counterparty.Phone);
            Assert.AreEqual("pushkin.a@mail.test", user.Counterparty.Email);
            Assert.AreEqual("ТЦ Москва", user.Counterparty.MallAddress);
            Assert.AreEqual("Москва, ул. Центральная", user.Counterparty.CompanyAddress);
        }

        [Test]
        public async Task When_UpdateUser_UserStatusBanned_AllDataCorrect_Then_UserDataUpdatedStatusActive() {
            
            var userId = new Guid("b6359236-2f42-4297-ab1d-d429d7d48c8f");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var now = DateTime.Now.AddDays(7);
            var userModel = new UpdateUserDTO {Id = userId, Login = "user6NewLogin", Password = password, Person = "Булгаков М.", Phone = "+79900000016", Email = "bulgakovmihail@mail.test", MallAddress = "ТЦ Питер", CompanyAddress = "Санкт-Петербург, ул. Центральная", Status = UserStatus.Active, BanReason = "Test reason", BanExpireDate = now};

            var actual = await _userService.UpdateUserAsync(userModel);
            Assert.AreEqual(true, actual);

            var user = await _uow.Users.All
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == userId);

            Assert.AreEqual("user6NewLogin", user.Login);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(UserStatus.Active, user.Status);
            Assert.AreEqual(string.Empty, user.BanReason);
            Assert.IsNull(user.BanExpireDate);

            Assert.AreEqual("Булгаков М.", user.Counterparty.Person);
            Assert.AreEqual("+79900000016", user.Counterparty.Phone);
            Assert.AreEqual("bulgakovmihail@mail.test", user.Counterparty.Email);
            Assert.AreEqual("ТЦ Питер", user.Counterparty.MallAddress);
            Assert.AreEqual("Санкт-Петербург, ул. Центральная", user.Counterparty.CompanyAddress);
        }

        [Test]
        public async Task When_UpdateUser_UserStatusBanned_AllDataCorrect_Then_UserDataUpdatedStatusBanned() {
            
            var userId = new Guid("b6359236-2f42-4297-ab1d-d429d7d48c8f");
            var password =  HashHelper.TransformPassword(Generator.NewString(10));
            var now = DateTime.Now.AddDays(7);
            var userModel = new UpdateUserDTO {Id = userId, Login = "user6NewLogin", Password = password, Person = "Булгаков М.", Phone = "+79900000016", Email = "bulgakovmihail@mail.test", MallAddress = "ТЦ Питер", CompanyAddress = "Санкт-Петербург, ул. Центральная", Status = UserStatus.Banned, BanReason = "Test reason", BanExpireDate = now};

            var actual = await _userService.UpdateUserAsync(userModel);
            Assert.AreEqual(true, actual);

            var user = await _uow.Users.All
                .Include(x => x.Counterparty)
                .FirstOrDefaultAsync(x => x.Id == userId);

            Assert.AreEqual("user6NewLogin", user.Login);
            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(UserStatus.Banned, user.Status);
            Assert.AreEqual("Test reason", user.BanReason);
            Assert.AreEqual(now, user.BanExpireDate);

            Assert.AreEqual("Булгаков М.", user.Counterparty.Person);
            Assert.AreEqual("+79900000016", user.Counterparty.Phone);
            Assert.AreEqual("bulgakovmihail@mail.test", user.Counterparty.Email);
            Assert.AreEqual("ТЦ Питер", user.Counterparty.MallAddress);
            Assert.AreEqual("Санкт-Петербург, ул. Центральная", user.Counterparty.CompanyAddress);
        }

        [Test]
        public async Task When_UpdateUser_UserStatusDeleted_AllDataCorrect_Then_ThrowsEntityUpdateException() {
            
            var userId = new Guid("a6b251ad-4040-448f-80d0-af177a0f6776");
            var userModel = new UpdateUserDTO {Id = userId};

            AsyncTestDelegate activate = () => _userService.UpdateUserAsync(userModel);
            Assert.That(activate, Throws.TypeOf<EntityUpdateException>());

            var user = await _uow.Users.GetByIdAsync(userId);
            
            Assert.AreEqual("user7", user.Login);
            Assert.AreEqual(UserStatus.Deleted, user.Status);
        }

        #endregion
    }
}
